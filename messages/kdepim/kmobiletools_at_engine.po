# translation of kmobiletools_at_engine.po to Russian
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the kmobiletools_at_engine package.
#
# Dimitriy Ryazantcev <DJm00n@mail.ru>, 2008.
# Nick Shaforostoff <shaforostoff@kde.ru>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kmobiletools_at_engine\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-03-10 17:13+0100\n"
"PO-Revision-Date: 2008-02-23 16:49+0200\n"
"Last-Translator: Nick Shaforostoff <shaforostoff@kde.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: LoKalize 0.2\n"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 38
#: rc.cpp:3
msgid "De&vice Options"
msgstr "&Параметры устройства"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 44
#: rc.cpp:6
msgid "Device Type"
msgstr "Тип устройства"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 54
#: rc.cpp:9
msgid "&USB Cable"
msgstr "&Кабель USB"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 61
#: rc.cpp:12
msgid "I&nfrared"
msgstr "&Инфракрасный"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 72
#: rc.cpp:15
msgid "Bluetoot&h"
msgstr "Bluetoot&h"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 79
#: rc.cpp:18
msgid "&Serial Cable"
msgstr "&Последовательный кабель"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 90
#: rc.cpp:21
msgid "User Defined"
msgstr "Пользовательский"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 145
#: rc.cpp:24
msgid "&Devices List"
msgstr "&Список устройств"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 173
#: rc.cpp:27
msgid "Dev&ices Options"
msgstr "О&пции устройств"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 214
#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 18
#: rc.cpp:30 rc.cpp:73
msgid "Initialization string:"
msgstr "Строка инициализации:"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 224
#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 28
#: rc.cpp:33 rc.cpp:76
msgid "Secondary initialization string:"
msgstr "Вторая строка инициализации:"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 234
#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 38
#: rc.cpp:36 rc.cpp:79
msgid "Connection speed:"
msgstr "Скорость соединения:"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 244
#: rc.cpp:39
msgid "Encoding:"
msgstr "Кодировка:"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 313
#: rc.cpp:42
msgid "Motorola \"Mode &2\" fix"
msgstr "Исправление Motorola \"Mode &2\""

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 316
#: rc.cpp:45
msgid "Alt+2"
msgstr "Alt+2"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 328
#: rc.cpp:48
msgid "Memor&y Options"
msgstr "Оп&ции памяти"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 358
#: rc.cpp:51
msgid "Phonebook Memory Slots"
msgstr "Слоты памяти телефонной книги"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 368
#: rc.cpp:54
msgid "SMS Memory Slots"
msgstr "Слоты памяти сообщений"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 379
#: rc.cpp:57
msgid "O&ther Options"
msgstr "&Другие опции"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 404
#: rc.cpp:60
msgid ""
"KMobileTools needs to know how to dial numbers with your mobile phone.\n"
"If you can't understand these options, just try all them until you see that "
"dialing works correctly."
msgstr ""
"KMobileTools необходимо знать как набирать номера на вашем телефоне.\n"
"Если вы не понимаете этих опций, то просто перепробуйте все, пока набор не "
"заработает."

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 407
#: rc.cpp:64
msgid "Dialing Method"
msgstr "Метод набора"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 431
#: rc.cpp:67
msgid "&CKPD (KeyPress Emulation)"
msgstr "&CKPD (Эмуляция нажатия клавиш)"

#. i18n: tag string
#. i18n: file ./cfgwidgets/engineconfig.ui line 438
#: rc.cpp:70
msgid "&ATD (Standard Modem Dial)"
msgstr "&ATD (Стандартный модемный набор)"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 52
#: rc.cpp:82
msgid "AT S7=45 S0=0 V1 X4 &c1 E0"
msgstr "AT S7=45 S0=0 V1 X4 &c1 E0"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 66
#: rc.cpp:85
msgid "9600 (9 kbps)"
msgstr "9600 (9 кбит/с)"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 71
#: rc.cpp:88
msgid "38400 (38 kbps)"
msgstr "38400 (38 кбит/с)"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 76
#: rc.cpp:91
msgid "57600 (56 kbps)"
msgstr "57600 (56 кбит/с)"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 81
#: rc.cpp:94
msgid "115200 (115 kbps)"
msgstr "115200 (115 кбит/с)"

#. i18n: tag string
#. i18n: file ./wizard/serial.ui line 86
#: rc.cpp:97
msgid "230400 (203 kbps)"
msgstr "230400 (203 кбит/с)"

#. i18n: tag string
#. i18n: file ./wizard/userconnection.ui line 22
#: rc.cpp:100
msgid "Serial Devices Path"
msgstr "Путь к последовательному устройству"

#. i18n: tag string
#. i18n: file ./wizard/bluetooth.ui line 16
#: rc.cpp:103
msgid "To be implemented...."
msgstr "Не реализовано...."

#: wizard/at_devicesfoundpage.cpp:111
#, kde-format
msgctxt "AT Engine wizard - device details html code"
msgid ""
"<qt><ul><li>Manufacturer: %1</li>    <li>Model: %2</li>    <li>IMEI: %3</"
"li>    <li>Device: %4</li></ul></qt>"
msgstr ""
"<qt><ul><li>Производитель: %1</li>    <li>Модель: %2</li>    <li>IMEI: %3</"
"li>    <li>Устройство: %4</li></ul></qt>"

#: wizard/at_connectionspage.cpp:39
msgctxt "Connections list"
msgid "USB Cable"
msgstr "Кабель USB"

#: wizard/at_connectionspage.cpp:40
msgctxt "Connections list"
msgid "Serial Cable"
msgstr "Последовательный кабель"

#: wizard/at_connectionspage.cpp:41
msgctxt "Connections list"
msgid "Bluetooth"
msgstr "Bluetooth"

#: wizard/at_connectionspage.cpp:42
msgctxt "Connections list"
msgid "InfraRed"
msgstr "Инфракрасный"

#: wizard/at_connectionspage.cpp:43
msgctxt "Connections list"
msgid "User Defined"
msgstr "Пользовательский"

#: wizard/at_connectionspage.cpp:73
msgctxt "Bluetooth Connection details tab title in the wizard"
msgid "Bluetooth"
msgstr "Bluetooth"

#: wizard/at_connectionspage.cpp:82
msgctxt "User Defined Connection details tab title in the wizard"
msgid "User Defined"
msgstr "Пользовательский"

#: wizard/at_scanprogresspage.cpp:93
msgctxt "Wizard - probe finished"
msgid "All devices were analyzed."
msgstr "Все устройства проанализированы."

#: wizard/at_scanprogresspage.cpp:96
#, kde-format
msgctxt "Wizard - probed device.."
msgid "%1 done."
msgstr "%1 готово."

#: at_engine.cpp:154
#, kde-format
msgid "Network: %1"
msgstr "Сеть: %1"

#: at_engine.cpp:657
msgctxt "At Engine Configuration Dialog title"
msgid "AT Engine"
msgstr "AT Движок"

#: at_engine.cpp:659
msgctxt "At Engine Configuration Dialog Header"
msgid "AT Engine Configuration Page"
msgstr "Окно настройки AT Движка"

#: at_engine.cpp:671
msgctxt "USB Connection"
msgid "USB"
msgstr "USB"

#: at_engine.cpp:672
msgctxt "Serial Connection"
msgid "Serial Ports"
msgstr "Последовательные порты"

#: at_engine.cpp:673
msgctxt "Bluetooth Connection"
msgid "Bluetooth"
msgstr "Bluetooth"

#: at_engine.cpp:674
msgctxt "IrDA Connection"
msgid "Infrared"
msgstr "Инфракрасный"

#: at_engine.cpp:675
msgctxt "User-defined Connection"
msgid "User defined"
msgstr "Пользовательский"

#: at_engine.cpp:677
#, kde-format
msgctxt "AT Wizard summary - using <connection types>"
msgid "Using connection: %2"
msgid_plural "Using connections: %2"
msgstr[0] "Использую соединение: %2"
msgstr[1] "Использую соединения: %2"
msgstr[2] "Использую соединений: %2"

#: sms_jobs.cpp:299
msgid "Unsupported character encoding"
msgstr "Неподдерживаемая кодировка символов"
