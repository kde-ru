# translation of kabc_slox.po to Russian
# KDE3 - kdepim/kabc_slox.po Russian translation.
# Copyright (C) 2004, KDE Russian translation team.
# Andrey Cherepanov <sibskull@mail.ru>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kabc_slox\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-03-07 20:12+0100\n"
"PO-Revision-Date: 2005-09-19 13:47+0400\n"
"Last-Translator: Andrey Cherepanov <sibskull@mail.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: kabcresourceslox.cpp:219
msgid "Downloading contacts"
msgstr "Загружаются контакты"

#: kabcresourceslox.cpp:528
msgid "Uploading contacts"
msgstr "Обновляются контакты на сервере"

#: kabcresourcesloxconfig.cpp:52
msgid "URL:"
msgstr "Ссылка:"

#: kabcresourcesloxconfig.cpp:58 kcalresourcesloxconfig.cpp:61
msgid "User:"
msgstr "Пользователь:"

#: kabcresourcesloxconfig.cpp:64 kcalresourcesloxconfig.cpp:67
msgid "Password:"
msgstr "Пароль:"

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 19
#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 25
#: kabcresourcesloxconfig.cpp:71 kcalresourcesloxconfig.cpp:74 rc.cpp:12
#: rc.cpp:36
msgid "Only load data since last sync"
msgstr "Загружать данные, изменённые с последней синхронизации"

#: kabcresourcesloxconfig.cpp:75
msgid "Select Folder..."
msgstr "Выбор папки..."

#: kcalresourceslox.cpp:178
#, kde-format
msgid "Non-http protocol: '%1'"
msgstr "Не HTTP-протокол: '%1'"

#: kcalresourceslox.cpp:233
msgid "Downloading events"
msgstr "Загружаются события"

#: kcalresourceslox.cpp:278
msgid "Downloading to-dos"
msgstr "Загрузка задач"

#: kcalresourceslox.cpp:387
msgid "Uploading incidence"
msgstr "Размещение данных"

#: kcalresourceslox.cpp:1227
msgid "Added"
msgstr "Добавлено"

#: kcalresourceslox.cpp:1228
msgid "Changed"
msgstr "Изменено"

#: kcalresourceslox.cpp:1229
msgid "Deleted"
msgstr "Удалено"

#: kcalresourcesloxconfig.cpp:54
msgid "Download from:"
msgstr "Загрузить с сервера:"

#: kcalresourcesloxconfig.cpp:78
msgid "Calendar Folder..."
msgstr "Папка календаря..."

#: kcalresourcesloxconfig.cpp:82
msgid "Task Folder..."
msgstr "Папка задач..."

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 10
#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 10
#: rc.cpp:3 rc.cpp:21
msgid "Base Url"
msgstr "Базовая ссылка"

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 13
#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 13
#: rc.cpp:6 rc.cpp:24
msgid "User Name"
msgstr "Имя пользователя"

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 16
#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 16
#: rc.cpp:9 rc.cpp:27
msgid "Password"
msgstr "Пароль"

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 23
#: rc.cpp:15 sloxfolderdialog.cpp:43
msgid "Folder ID"
msgstr "Идентификатор папки"

#. i18n: tag label
#. i18n: file kresources_kabc_slox.kcfg line 27
#: rc.cpp:18
msgid "Last Sync"
msgstr "Последняя синхронизация"

#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 19
#: rc.cpp:30
msgid "Last Event Sync"
msgstr "Последняя синхронизация событий"

#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 22
#: rc.cpp:33
msgid "Last To-do Sync"
msgstr "Последняя синхронизация задач"

#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 29
#: rc.cpp:39
msgid "Calendar Folder"
msgstr "Папка календаря"

#. i18n: tag label
#. i18n: file kresources_kcal_slox.kcfg line 33
#: rc.cpp:42
msgid "Task Folder"
msgstr "Папка задач"

#: rc.cpp:43
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Андрей Черепанов"

#: rc.cpp:44
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sibskull@mail.ru"

#: sloxfolder.cpp:45
msgid "Global Addressbook"
msgstr "Глобальная адресная книга"

#: sloxfolder.cpp:47
msgid "Internal Addressbook"
msgstr "Внутренняя адресная книга"

#: sloxfolderdialog.cpp:35
msgid "Select Folder"
msgstr "Выбор папки"

#: sloxfolderdialog.cpp:38
msgid "Reload"
msgstr ""

#: sloxfolderdialog.cpp:42
msgid "Folder"
msgstr "Папка"

#: sloxfoldermanager.cpp:163
msgid "Private Folder"
msgstr "Персональная папка"

#: sloxfoldermanager.cpp:165
msgid "Public Folder"
msgstr "Публичная папка"

#: sloxfoldermanager.cpp:167
msgid "Shared Folder"
msgstr "Общая папка"

#: sloxfoldermanager.cpp:169
msgid "System Folder"
msgstr "Системная папка"
