# translation of plasma_applet_timer.po to Russian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Pesotsky Denis <St.MPA3b@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_timer\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-08-01 08:32+0200\n"
"PO-Revision-Date: 2008-07-28 20:59+0400\n"
"Last-Translator: Pesotsky Denis <St.MPA3b@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. i18n: tag string
#. i18n: file predefinedTimersConfig.ui line 13
#: rc.cpp:3
msgid "Timer Configuration"
msgstr "Настройка таймера"

#. i18n: tag string
#. i18n: file timerConfig.ui line 16
#: rc.cpp:6
msgid "Actions on timeout"
msgstr "Действия при окончании отсчёта"

#. i18n: tag string
#. i18n: file timerConfig.ui line 22
#: rc.cpp:9
msgid "Show a message"
msgstr "Показать сообщение"

#. i18n: tag string
#. i18n: file timerConfig.ui line 47
#: rc.cpp:12
msgid "Message:"
msgstr "Сообщение:"

#. i18n: tag string
#. i18n: file timerConfig.ui line 75
#: rc.cpp:15
msgid "Run a command"
msgstr "Запустить команду"

#. i18n: tag string
#. i18n: file timerConfig.ui line 100
#: rc.cpp:18
msgid "Command:"
msgstr "Команда:"

#: timer.cpp:70
msgid "Timer Timeout"
msgstr "Окончание отсчёта"

#: timer.cpp:76
msgid "Start"
msgstr "Старт"

#: timer.cpp:80
msgid "Stop"
msgstr "Стоп"

#: timer.cpp:84
msgid "Reset"
msgstr "Сброс"

#: timer.cpp:99
msgid "Predefined Timers"
msgstr "Предустановленные таймеры"

#~ msgid "Form"
#~ msgstr "Форма"
