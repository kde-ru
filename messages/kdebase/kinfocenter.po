# translation of kinfocenter.po to Russian
# translation of kcontrol.po into Russian
#
# Denis Pershin <dyp@perchine.com>, 1998, 1999.
# Hermann Zheboldov <Hermann.Zheboldov@shq.ru>, 2000.
# Leonid Kanter <leon@asplinux.ru>, 2004, 2008.
# Gregory Mokhin <mok@kde.ru>, 2005.
# Nick Shaforostoff <shaforostoff@kde.ru>, 2008.
# KDE3 - kcontrol.pot  Russian translation
# Copyright (C) 1998, KDE Team.
# Leon Kanter <leon@asplinux.ru> 2002
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-04-24 01:38+0200\n"
"PO-Revision-Date: 2008-07-09 18:23+0300\n"
"Last-Translator: Leonid Kanter <leon@asplinux.ru>\n"
"Language-Team: Russian <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: base/info_aix.cpp:54
msgid "Name"
msgstr "Имя"

#: base/info_aix.cpp:55
msgid "Status"
msgstr "Состояние"

#: base/info_aix.cpp:56
msgid "Location"
msgstr "Расположение"

#: base/info_aix.cpp:57 base/info_fbsd.cpp:305
msgid "Description"
msgstr "Описание"

#: base/info_aix.cpp:175 base/info_hpux.cpp:163 base/info_hpux.cpp:438
#: base/info_linux.cpp:113 base/info_netbsd.cpp:53 base/info_openbsd.cpp:49
msgid "Information"
msgstr "Сведения"

#: base/info_aix.cpp:176 base/info_hpux.cpp:164 base/info_hpux.cpp:439
#: base/info_linux.cpp:113 base/info_netbsd.cpp:53 base/info_openbsd.cpp:49
#: base/info_solaris.cpp:697
msgid "Value"
msgstr "Значение"

#: base/info_aix.cpp:305 base/info_hpux.cpp:242 base/info_hpux.cpp:356
#: base/info_linux.cpp:334 base/info_linux.cpp:445
msgid "MB"
msgstr "МБ"

#: base/info_aix.cpp:310 base/info_fbsd.cpp:277 base/info_fbsd.cpp:305
#: base/info_hpux.cpp:361 base/info_linux.cpp:476 base/info_netbsd.cpp:161
#: base/info_netbsd.cpp:273 base/info_openbsd.cpp:172
#: base/info_openbsd.cpp:267 base/info_solaris.cpp:174
msgid "Device"
msgstr "Устройство"

#: base/info_aix.cpp:311 base/info_fbsd.cpp:277 base/info_hpux.cpp:362
#: base/info_linux.cpp:476 base/info_netbsd.cpp:273 base/info_openbsd.cpp:267
#: base/info_solaris.cpp:174
msgid "Mount Point"
msgstr "Точка монтирования"

#: base/info_aix.cpp:312 base/info_fbsd.cpp:277 base/info_hpux.cpp:363
#: base/info_linux.cpp:476 base/info_netbsd.cpp:273 base/info_openbsd.cpp:267
#: base/info_solaris.cpp:174
msgid "FS Type"
msgstr "Тип ФС"

#: base/info_aix.cpp:313 base/info_hpux.cpp:364 base/info_linux.cpp:476
#: base/info_netbsd.cpp:273 base/info_solaris.cpp:174
msgid "Total Size"
msgstr "Общий размер"

#: base/info_aix.cpp:314 base/info_hpux.cpp:365 base/info_linux.cpp:476
#: base/info_netbsd.cpp:273 base/info_solaris.cpp:174
msgid "Free Size"
msgstr "Свободно"

#: base/info_aix.cpp:332 base/info_aix.cpp:338 base/info_hpux.cpp:383
#: base/info_hpux.cpp:389
msgid "n/a"
msgstr "н/д"

#: base/info_fbsd.cpp:81
#, kde-format
msgid "CPU %1: %2, %3 MHz"
msgstr "ЦП %1: %2, %3 МГц"

#: base/info_fbsd.cpp:83
#, kde-format
msgid "CPU %1: %2, unknown speed"
msgstr "ЦП %1: %2, частота неизвестна"

#: base/info_fbsd.cpp:145
msgid ""
"Your sound system could not be queried.  /dev/sndstat does not exist or is "
"not readable."
msgstr ""

#: base/info_fbsd.cpp:173
msgid ""
"SCSI subsystem could not be queried: /sbin/camcontrol could not be found"
msgstr ""

#: base/info_fbsd.cpp:178
msgid ""
"SCSI subsystem could not be queried: /sbin/camcontrol could not be executed"
msgstr ""

#: base/info_fbsd.cpp:226
msgid ""
"Could not find any programs with which to query your system's PCI information"
msgstr ""

#: base/info_fbsd.cpp:243
#, kde-format
msgid "PCI subsystem could not be queried: %1 could not be executed"
msgstr ""

#: base/info_fbsd.cpp:255
msgid "The PCI subsystem could not be queried, this may need root privileges."
msgstr ""

#: base/info_fbsd.cpp:270
msgid "Could not check file system info: "
msgstr ""

#: base/info_fbsd.cpp:277 base/info_linux.cpp:476 base/info_openbsd.cpp:267
#: base/info_solaris.cpp:174
msgid "Mount Options"
msgstr "Параметры монтирования"

#: base/info_hpux.cpp:129
msgid "PA-RISC Processor"
msgstr "Процессор PA-RISC"

#: base/info_hpux.cpp:131
msgid "PA-RISC Revision"
msgstr "Версия PA-RISC"

#: base/info_hpux.cpp:168
msgid "Machine"
msgstr "Машина"

#: base/info_hpux.cpp:175
msgid "Model"
msgstr "Модель"

#: base/info_hpux.cpp:182
msgid "Machine Identification Number"
msgstr "Идентификационный номер машины"

#: base/info_hpux.cpp:183 base/info_solaris.cpp:619
msgid "(none)"
msgstr "(нет)"

#: base/info_hpux.cpp:188
msgid "Number of Active Processors"
msgstr "Количество активных процессов"

#: base/info_hpux.cpp:192
msgid "CPU Clock"
msgstr "Частота ЦП"

#: base/info_hpux.cpp:193 base/info_solaris.cpp:69
msgid "MHz"
msgstr "МГц"

#: base/info_hpux.cpp:226
msgid "(unknown)"
msgstr "(неизвестно)"

#: base/info_hpux.cpp:230
msgid "CPU Architecture"
msgstr "Архитектура ЦП"

#: base/info_hpux.cpp:236
msgid "enabled"
msgstr "включено"

#: base/info_hpux.cpp:236
msgid "disabled"
msgstr "выключено"

#: base/info_hpux.cpp:238
msgid "Numerical Coprocessor (FPU)"
msgstr "Числовой сопроцессор (FPU)"

#: base/info_hpux.cpp:243
msgid "Total Physical Memory"
msgstr "Всего физической памяти"

#: base/info_hpux.cpp:245
msgid "Bytes"
msgstr "Байт"

#: base/info_hpux.cpp:246
msgid "Size of One Page"
msgstr "Размер одной страницы"

#: base/info_hpux.cpp:441
msgid "Audio Name"
msgstr ""

#: base/info_hpux.cpp:442
msgid "Vendor"
msgstr "Производитель"

#: base/info_hpux.cpp:443
msgid "Alib Version"
msgstr "Версия Alib"

#: base/info_hpux.cpp:447
msgid "Protocol Revision"
msgstr "Версия протокола"

#: base/info_hpux.cpp:451
msgid "Vendor Number"
msgstr "Номер производителя"

#: base/info_hpux.cpp:454
msgid "Release"
msgstr "Выпуск"

#: base/info_hpux.cpp:457
msgid "Byte Order"
msgstr "Порядок байт"

#: base/info_hpux.cpp:458
msgid "ALSBFirst (LSB)"
msgstr ""

#: base/info_hpux.cpp:459
msgid "AMSBFirst (MSB)"
msgstr ""

#: base/info_hpux.cpp:460
msgid "Invalid Byteorder."
msgstr "Неверный порядок байт."

#: base/info_hpux.cpp:462
msgid "Bit Order"
msgstr "Порядок бит"

#: base/info_hpux.cpp:464
msgid "ALeastSignificant (LSB)"
msgstr ""

#: base/info_hpux.cpp:466
msgid "AMostSignificant (MSB)"
msgstr ""

#: base/info_hpux.cpp:466
msgid "Invalid Bitorder."
msgstr "Неверный порядок бит."

#: base/info_hpux.cpp:468
msgid "Data Formats"
msgstr "Форматы данных"

#: base/info_hpux.cpp:475
msgid "Sampling Rates"
msgstr ""

#: base/info_hpux.cpp:481
msgid "Input Sources"
msgstr "Иточники ввода"

#: base/info_hpux.cpp:483
msgid "Mono-Microphone"
msgstr "Моно-микрофон"

#: base/info_hpux.cpp:485
msgid "Mono-Auxiliary"
msgstr ""

#: base/info_hpux.cpp:487
msgid "Left-Microphone"
msgstr ""

#: base/info_hpux.cpp:489
msgid "Right-Microphone"
msgstr ""

#: base/info_hpux.cpp:491
msgid "Left-Auxiliary"
msgstr ""

#: base/info_hpux.cpp:493
msgid "Right-Auxiliary"
msgstr ""

#: base/info_hpux.cpp:496
msgid "Input Channels"
msgstr "Каналы ввода"

#: base/info_hpux.cpp:498 base/info_hpux.cpp:522
msgid "Mono-Channel"
msgstr ""

#: base/info_hpux.cpp:500 base/info_hpux.cpp:524
msgid "Left-Channel"
msgstr ""

#: base/info_hpux.cpp:502 base/info_hpux.cpp:526
msgid "Right-Channel"
msgstr ""

#: base/info_hpux.cpp:505
msgid "Output Destinations"
msgstr ""

#: base/info_hpux.cpp:507
msgid "Mono-InternalSpeaker"
msgstr ""

#: base/info_hpux.cpp:509
msgid "Mono-Jack"
msgstr ""

#: base/info_hpux.cpp:511
msgid "Left-InternalSpeaker"
msgstr ""

#: base/info_hpux.cpp:513
msgid "Right-InternalSpeaker"
msgstr ""

#: base/info_hpux.cpp:515
msgid "Left-Jack"
msgstr ""

#: base/info_hpux.cpp:517
msgid "Right-Jack"
msgstr ""

#: base/info_hpux.cpp:520
msgid "Output Channels"
msgstr "Каналы вывода"

#: base/info_hpux.cpp:529
msgid "Gain"
msgstr "Усиление"

#: base/info_hpux.cpp:530
msgid "Input Gain Limits"
msgstr ""

#: base/info_hpux.cpp:532
msgid "Output Gain Limits"
msgstr ""

#: base/info_hpux.cpp:535
msgid "Monitor Gain Limits"
msgstr ""

#: base/info_hpux.cpp:538
msgid "Gain Restricted"
msgstr ""

#: base/info_hpux.cpp:542
msgid "Lock"
msgstr ""

#: base/info_hpux.cpp:544
msgid "Queue Length"
msgstr "Длина очереди"

#: base/info_hpux.cpp:546
msgid "Block Size"
msgstr "Размер блока"

#: base/info_hpux.cpp:548
msgid "Stream Port (decimal)"
msgstr ""

#: base/info_hpux.cpp:550
msgid "Ev Buffer Size"
msgstr ""

#: base/info_hpux.cpp:552
msgid "Ext Number"
msgstr ""

#: base/info_linux.cpp:130
msgid "DMA-Channel"
msgstr "Канал DMA"

#: base/info_linux.cpp:130 base/info_linux.cpp:181
msgid "Used By"
msgstr "Используется"

#: base/info_linux.cpp:181
msgid "I/O-Range"
msgstr ""

#: base/info_linux.cpp:206
msgid "Devices"
msgstr "Устройства"

#: base/info_linux.cpp:206
msgid "Major Number"
msgstr ""

#: base/info_linux.cpp:206
msgid "Minor Number"
msgstr ""

#: base/info_linux.cpp:220
msgid "Character Devices"
msgstr "Символьные устройства"

#: base/info_linux.cpp:227
msgid "Block Devices"
msgstr "Блочные устройства"

#: base/info_linux.cpp:262
msgid "Miscellaneous Devices"
msgstr "Различные устройства"

#: base/info_netbsd.cpp:161 base/info_openbsd.cpp:172
msgid "IRQ"
msgstr ""

#: base/info_netbsd.cpp:179 base/info_openbsd.cpp:185
msgid "No PCI devices found."
msgstr "Устройства PCI не найдены."

#: base/info_netbsd.cpp:188 base/info_openbsd.cpp:194
msgid "No I/O port devices found."
msgstr ""

#: base/info_netbsd.cpp:199 base/info_openbsd.cpp:203
msgid "No audio devices found."
msgstr "Звуковые устройства не найдены."

#: base/info_netbsd.cpp:237 base/info_openbsd.cpp:247
msgid "No SCSI devices found."
msgstr "Устройства SCSI не найдены."

#: base/info_netbsd.cpp:273
msgid "Total Nodes"
msgstr "Всего узлов"

#: base/info_netbsd.cpp:273
msgid "Free Nodes"
msgstr "Свободно узлов"

#: base/info_netbsd.cpp:273
msgid "Flags"
msgstr "Флаги"

#: base/info_openbsd.cpp:261
msgid "Unable to run /sbin/mount."
msgstr "Не удаётся выполнить /sbin/mount."

#: base/info_osx.cpp:70
#, kde-format
msgid "Kernel is configured for %1 CPUs"
msgstr "Ядро настроено для %1 ЦП"

#: base/info_osx.cpp:72
#, kde-format
msgid "CPU %1: %2"
msgstr "ЦП %1: %2"

#: base/info_osx.cpp:121
#, kde-format
msgid "Device Name: %1"
msgstr "Имя устройства: %1"

#: base/info_osx.cpp:129
#, kde-format
msgid "Manufacturer: %1"
msgstr "Производитель: %1"

#: base/info_solaris.cpp:69
msgid "Instance"
msgstr ""

#: base/info_solaris.cpp:69
msgid "CPU Type"
msgstr "Тип ЦП"

#: base/info_solaris.cpp:69
msgid "FPU Type"
msgstr "Тип FPU"

#: base/info_solaris.cpp:69
msgid "State"
msgstr "Состояние"

#: base/info_solaris.cpp:174
msgid "Mount Time"
msgstr "Время монтирования"

#: base/info_solaris.cpp:444
msgid "character special"
msgstr "специальное символьное"

#: base/info_solaris.cpp:446
msgid "block special"
msgstr "специальное блочное"

#: base/info_solaris.cpp:449
msgid "Spectype:"
msgstr ""

#: base/info_solaris.cpp:462
msgid "Nodetype:"
msgstr ""

#: base/info_solaris.cpp:469
msgid "Major/Minor:"
msgstr ""

#: base/info_solaris.cpp:540
msgid "(no value)"
msgstr "(нет значения)"

#: base/info_solaris.cpp:605
msgid "Driver Name:"
msgstr "Имя драйвера:"

#: base/info_solaris.cpp:607
msgid "(driver not attached)"
msgstr "(драйвер не подключен)"

#: base/info_solaris.cpp:614
msgid "Binding Name:"
msgstr ""

#: base/info_solaris.cpp:629
msgid "Compatible Names:"
msgstr "Совместимые имена:"

#: base/info_solaris.cpp:633
msgid "Physical Path:"
msgstr "Физический путь:"

#: base/info_solaris.cpp:643
msgid "Properties"
msgstr "Свойства"

#: base/info_solaris.cpp:657
msgid "Type:"
msgstr "Тип:"

#: base/info_solaris.cpp:661
msgid "Value:"
msgstr "Значение:"

#: base/info_solaris.cpp:672
msgid "Minor Nodes"
msgstr ""

#: base/info_solaris.cpp:697
msgid "Device Information"
msgstr "Сведения об устройстве"

#: aboutwidget.cpp:38 main.cpp:91
msgid "KDE Info Center"
msgstr "Центр сведений KDE"

#: aboutwidget.cpp:40
msgid "Get system and desktop environment information"
msgstr "Получить информацию о системе и среде"

#: aboutwidget.cpp:42
msgid ""
"Welcome to the \"KDE Info Center\", a central place to find information "
"about your computer system."
msgstr ""
"Добро пожаловать в Центр сведений КДЕ, основное место для поиска сведений о "
"вашей компьютерной системе."

#: aboutwidget.cpp:46
msgid ""
"Click on the \"Help\" tab on the left to view help for the active control "
"module. Use the \"Search\" tab if you are unsure where to look for a "
"particular configuration option."
msgstr ""
"Нажмите на вкладку \"Справка\" слева, чтобы просмотреть справку по активному "
"модулю управления. Используйте вкладку \"Поиск\", если вы не уверены, где "
"именно искать нужный параметр."

#: aboutwidget.cpp:52
msgid "KDE version:"
msgstr "Версия KDE:"

#: aboutwidget.cpp:53
msgid "User:"
msgstr "Пользователь:"

#: aboutwidget.cpp:54
msgid "Hostname:"
msgstr "Имя узла:"

#: aboutwidget.cpp:55
msgid "System:"
msgstr "Система:"

#: aboutwidget.cpp:56
msgid "Release:"
msgstr "Ядро:"

#: aboutwidget.cpp:57
msgid "Machine:"
msgstr "Процессор:"

#: dockcontainer.cpp:69
#, kde-format
msgid "<p>Click here to consult the full <a href=\"%1\">Manual</a>.</p>"
msgstr ""
"<p>Нажмите, чтобы получить доступ к полному <a href=\"%1\">Руководству</a>.</"
"p>"

#: dockcontainer.cpp:74
msgid ""
"<h1>KDE Info Center</h1>There is no quick help available for the active info "
"module.<br /><br />Click <a href = \"kinfocenter/index.html\">here</a> to "
"read the general Info Center manual."
msgstr ""
"<h1>Центр информации KDE</h1> Для данного модуля информации отсутствует "
"быстрая справка.<br><br>Нажмите на эту <a href = \"kinfocenter/index.html"
"\">ссылку</a>, чтобы открыть общее справочное руководство Центра инфомации "
"KDE."

#: dockcontainer.cpp:91
msgid "<big><b>Loading...</b></big>"
msgstr "<big><b>Загрузка...</b></big>"

#: dockcontainer.cpp:147
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before running the new module or discard "
"the changes?"
msgstr ""
"В текущем модуле имеются несохраненные изменения.\n"
"Применить изменения перед запуском нового модуля или отклонить изменения?"

#: dockcontainer.cpp:149
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before exiting the Control Center or "
"discard the changes?"
msgstr ""
"В текущем модуле имеются несохраненные изменения.\n"
"Применить изменения перед выходом из Центра управления или отклонить "
"изменения?"

#: dockcontainer.cpp:151
msgid "Unsaved Changes"
msgstr "Несохраненные изменения"

#: indexwidget.cpp:45
msgid "Filter:"
msgstr "Фильтр:"

#: main.cpp:92
msgid "The KDE Info Center"
msgstr "Центр информации KDE"

#: main.cpp:93
msgid "(c) 1998-2004, The KDE Control Center Developers"
msgstr "(c) 1998-2004, разработчики Центра управления KDE"

#: main.cpp:98
msgid "Nicolas Ternisien"
msgstr "Nicolas Ternisien"

#: main.cpp:98
msgid "Current Maintainer"
msgstr "Текущее сопровождение"

#: main.cpp:99
msgid "Helge Deller"
msgstr ""

#: main.cpp:99
#, fuzzy
#| msgid "Current Maintainer"
msgid "Previous Maintainer"
msgstr "Текущее сопровождение"

#: main.cpp:100
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:101
msgid "Matthias Elter"
msgstr "Matthias Elter"

#: main.cpp:102
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: main.cpp:103
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: moduletreeview.cpp:43 toplevel.cpp:221
msgid "General Information"
msgstr "Общие сведения"

#: proxywidget.cpp:51
msgid "The currently loaded configuration module."
msgstr "Текущий модуль настройки."

#: rc.cpp:1
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Денис Першин,Герман Жеболдов,Григорий Мохин, Леонид Кантер"

#: rc.cpp:2
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"dyp@perchine.com,Hermann.Zheboldov@shq.ru,mokhin@bog.msu.ru,leon@asplinux.ru"

#: toplevel.cpp:119 toplevel.cpp:150 toplevel.cpp:217
msgid "About Current Module"
msgstr "О текущем модуле"

#: toplevel.cpp:126 toplevel.cpp:158 toplevel.cpp:223
msgid "&Report Bug..."
msgstr "&Сообщить об ошибке..."

#: toplevel.cpp:146
#, kde-format
msgctxt "Help menu->about <modulename>"
msgid "About %1"
msgstr "О модуле %1"

#: toplevel.cpp:160
#, kde-format
msgid "Report Bug on Module %1..."
msgstr "Сообщить об ошибке в модуле %1..."
