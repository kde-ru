# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Artem Sereda <overmind88@gmail.com>, 2008.
# Evgeniy Ivanov <powerfox@kde.ru>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kollision\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-12 05:44+0200\n"
"PO-Revision-Date: 2008-08-01 20:14+0400\n"
"Last-Translator: Evgeniy Ivanov <powerfox@kde.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 0.2\n"

#: main.cpp:20
msgid "Kollision"
msgstr "Kollision"

#: main.cpp:21
msgid "KDE collision game"
msgstr "Игра в столкновения для KDE"

#: main.cpp:22
msgid "(c) 2007 Paolo Capriotti"
msgstr "(c) 2007 Paolo Capriotti"

#: main.cpp:23
msgid "Paolo Capriotti"
msgstr "Paolo Capriotti"

#: main.cpp:24
msgid "Dmitry Suzdalev"
msgstr "Дмитрий Суздалев"

#: main.cpp:25
msgid "Matteo Guarnieri"
msgstr "Matteo Guarnieri"

#: main.cpp:25
msgid "Original idea"
msgstr "Автор идеи"

#: mainarea.cpp:70
msgid ""
"Welcome to Kollision\n"
"Click to start a game"
msgstr ""
"Добро пожаловать в Kollision\n"
"Щёлкните, чтобы начать игру"

#: mainarea.cpp:181
msgid ""
"Game paused\n"
"Click or press P to resume"
msgstr ""
"Игра приостановлена\n"
"Щёлкните или нажмите P для продолжения"

#: mainarea.cpp:234
msgid "4 balls"
msgstr "4 шара"

#: mainarea.cpp:473
#, kde-format
msgid "%1 balls"
msgstr "шары: %1"

#: mainarea.cpp:481
#, kde-format
msgid ""
"GAME OVER\n"
"You survived for %1 second\n"
"Click to restart"
msgid_plural ""
"GAME OVER\n"
"You survived for %1 seconds\n"
"Click to restart"
msgstr[0] ""
"ИГРА ОКОНЧЕНА\n"
"Вам удалось выжить %1 секунду\n"
"Щёлкните для перезапуска"
msgstr[1] ""
"ИГРА ОКОНЧЕНА\n"
"Вам удалось выжить %1 секунды\n"
"Щёлкните для перезапуска"
msgstr[2] ""
"ИГРА ОКОНЧЕНА\n"
"Вам удалось выжить %1 секунд\n"
"Щёлкните для перезапуска"

#: mainwindow.cpp:76
msgid "Abort game"
msgstr "Прервать игру"

#: mainwindow.cpp:84
msgid "&Play Sounds"
msgstr "Проигрывать &звуки"

#: mainwindow.cpp:121
#, kde-format
msgid "Balls: %1"
msgstr "Шаров: %1"

#: mainwindow.cpp:127
#, kde-format
msgid "Time: %1 second"
msgid_plural "Time: %1 seconds"
msgstr[0] "Время: %1 секунда"
msgstr[1] "Время: %1 секунды"
msgstr[2] "Время: %1 секунд"

#. i18n: tag label
#. i18n: file kollision.kcfg line 9
#: rc.cpp:3 rc.cpp:32
msgid "Whether sound effects should be played."
msgstr "Должны ли использоваться звуковые эффекты."

#. i18n: tag label
#. i18n: file kollision.kcfg line 15
#: rc.cpp:6 rc.cpp:35
msgid "Difficulty of the game."
msgstr "Сложность игры."

#. i18n: tag text
#. i18n: file kollisionui.rc line 5
#: rc.cpp:9 rc.cpp:38
msgid "&Game"
msgstr "&Игра"

#. i18n: tag string
#. i18n: file preferences.ui line 16
#: rc.cpp:12 rc.cpp:41
msgid "Enable &sounds"
msgstr "Включить &звуки"

#. i18n: tag string
#. i18n: file preferences.ui line 25
#: rc.cpp:15 rc.cpp:44
msgid "&Difficulty:"
msgstr "&Уровень сложности:"

#. i18n: tag string
#. i18n: file preferences.ui line 36
#: rc.cpp:18 rc.cpp:47
msgctxt "Difficulty level"
msgid "Easy"
msgstr "Лёгкий"

#. i18n: tag string
#. i18n: file preferences.ui line 41
#: rc.cpp:21 rc.cpp:50
msgctxt "Difficulty level"
msgid "Medium"
msgstr "Средний"

#. i18n: tag string
#. i18n: file preferences.ui line 46
#: rc.cpp:24 rc.cpp:53
msgctxt "Difficulty level"
msgid "Hard"
msgstr "Сложный"

#. i18n: tag string
#. i18n: file preferences.ui line 58
#: rc.cpp:27 rc.cpp:56
#, fuzzy
msgid "&Animation smoothness:"
msgstr "Сглаженность &анимации"

#: rc.cpp:28
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Artem Sereda"

#: rc.cpp:29
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "overmind88@gmail.com"
