# translation of kpercentage_devel.po to Russian
# Copyright (C) 2004 Free Software Foundation, Inc.
# Valia V. Vaneeva <fattie@altlinux.ru>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kpercentage_devel\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:22+0000\n"
"PO-Revision-Date: 2004-04-19 17:05-0600\n"
"Last-Translator: Valia V. Vaneeva <fattie@altlinux.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: devel.docbook:2
#, no-c-format
msgid "Developer's Guide to &kpercentage;"
msgstr "Руководство разработчика &kpercentage;"

#. Tag: para
#: devel.docbook:4
#, no-c-format
msgid ""
"If you want to contribute &kpercentage;, feel free to send me a mail "
"<email>bmlmessmer@web.de</email>."
msgstr ""
"Если вы хотите помочь в разработке &kpercentage;, отправьте сообщение автору "
"программы по адресу <email>bmlmessmer@web.de</email>."
