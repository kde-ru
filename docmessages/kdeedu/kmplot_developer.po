# KDE3 - kmplot_developer.pot Russian translation
# Translation of kmplot_developer.po into Russian
# Copyright (C) 2003 KDE Russian translation team
#
# Nick Shafff <admin@program.net.ua>, 2003.
# Nickolai Shaforostoff <shafff@ukr.net>, 2004.
# Nick Shaforostoff <shafff@ukr.net>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kmplot_developer\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:22+0000\n"
"PO-Revision-Date: 2006-09-19 21:01+0300\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: developer.docbook:2
#, no-c-format
msgid "Developer's Guide to &kmplot;"
msgstr "Руководство разработчика &kmplot;"

#. Tag: para
#: developer.docbook:4
#, no-c-format
msgid ""
"If you want to contribute to &kmplot; feel free to send a mail to &Klaus-"
"Dieter.Moeller.mail;, <email>f_edemar@linux.se</email> or "
"<email>david@bluehaze.org</email>."
msgstr ""
"Если вы хотите внести свой вклад в разработку &kmplot;, отправьте сообщение "
"автору (на английском) - &Klaus-Dieter.Moeller.mail; или "
"<email>f_edemar@linux.se</email>"
