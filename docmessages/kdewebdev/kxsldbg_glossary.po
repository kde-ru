# KDE3 - docs/quanta/kxsldbg_glossary.po Russian translation.
# Copyright (C) 2004, KDE Team.
# Nick Shafff <shafff@ukr.net>, 2003
# Nick Shafff <shafff@voliacable.com>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: kxsldbg_glossary\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:20+0000\n"
"PO-Revision-Date: 2003-12-18 00:21+0200\n"
"Last-Translator: Nick Shafff <shafff@voliacable.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: author
#: glossary.docbook:5
#, no-c-format
msgid "<firstname>Keith</firstname> <surname>Isdale</surname>"
msgstr "<firstname>Keith</firstname> <surname>Isdale</surname>"

#. Tag: email
#: glossary.docbook:9
#, no-c-format
msgid "keith@kdewebdev.org"
msgstr ""

#. Tag: trans_comment
#: glossary.docbook:12
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Николай</"
"firstname><surname>Шафоростов</"
"surname><affiliation><address><email>shafff@voliacable.com</email></"
"address></affiliation><contrib>перевод на русский</contrib></othercredit>"

#. Tag: title
#: glossary.docbook:17
#, no-c-format
msgid "Keywords"
msgstr "Ключевые слова"

#. Tag: glossterm
#: glossary.docbook:19
#, no-c-format
msgid "&xsldbg;"
msgstr "&xsldbg;"

#. Tag: para
#: glossary.docbook:21
#, no-c-format
msgid "See <ulink url=\"http://xsldbg.sourceforge.net\"></ulink>."
msgstr "См. <ulink url=\"http://xsldbg.sourceforge.net\"></ulink>."

#. Tag: glossterm
#: glossary.docbook:28
#, no-c-format
msgid "XPath"
msgstr "XPath"

#. Tag: para
#: glossary.docbook:30
#, no-c-format
msgid ""
"A valid expression that defines what data is required. See <ulink url="
"\"http://www.w3.org\">W3C web site</ulink>."
msgstr ""
"Выражение, определяющее требуемые данные. Для подробностей см. <ulink url="
"\"http://www.w3.org\">сайт W3C</ulink>."

#. Tag: glossterm
#: glossary.docbook:38
#, no-c-format
msgid "QName"
msgstr "QName"

#. Tag: para
#: glossary.docbook:40
#, no-c-format
msgid ""
"A fully qualified name. For example, <emphasis>xsl:myvariable</emphasis>. "
"See <ulink url=\"http://www.w3.org\">W3C web site</ulink>"
msgstr ""
"Полностью выраженное имя. Например, <emphasis>xsl:myvariable</emphasis>. Для "
"подробностей см. <ulink url=\"http://www.w3.org\">сайт W3C</ulink>."

#~ msgid "k_isdale@tpg.com.au"
#~ msgstr "k_isdale@tpg.com.au"
