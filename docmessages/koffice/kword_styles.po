# translation of kword_styles.po to Russian
# translation of kword_styles.po to русский
# Copyright (C) 2005 Free Software Foundation, Inc.
# Юхта Виталий <vit09v@mail.zp.ua>, 2005.
# Nick Shaforostoff <shafff@ukr.net>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kword_styles\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-02-11 07:20+0000\n"
"PO-Revision-Date: 2005-03-20 16:54+0200\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team:  <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: author
#: styles.docbook:4
#, no-c-format
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr "<firstname>Mike</firstname> <surname>McBride</surname>"

#. Tag: trans_comment
#: styles.docbook:8
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"> <firstname>Виталий</"
"firstname><surname>Юхта</surname><affiliation><address><email>vit09v@mail.zp."
"ua</email></address></affiliation><contrib>Перевод на русский</contrib></"
"othercredit>"

#. Tag: title
#: styles.docbook:11
#, no-c-format
msgid "Text styles"
msgstr "Стили текста"

#. Tag: primary
#: styles.docbook:12 styles.docbook:69 styles.docbook:98 styles.docbook:245
#: styles.docbook:276 styles.docbook:333
#, no-c-format
msgid "text styles"
msgstr "стили текста"

#. Tag: secondary
#: styles.docbook:12
#, no-c-format
msgid "introduction"
msgstr "Введение"

#. Tag: title
#: styles.docbook:14
#, no-c-format
msgid "Introduction to text styles"
msgstr "Общие сведения о стилях текста"

#. Tag: para
#: styles.docbook:15
#, no-c-format
msgid ""
"When you build documents that are more complex than a business letter, such "
"as a book, newsletter, or resume, the document is often broken down into "
"sections. Each of these sections may have a similar appearance."
msgstr ""
"При создании документов сложнее, чем деловые письма (это может быть брошюра, "
"конспект, резюме), документ очень часто разбивается на разделы, и некоторые "
"из них часто имеют одинаковое оформление."

#. Tag: para
#: styles.docbook:20
#, no-c-format
msgid ""
"You can use text styles to easily keep a consistent look throughout your "
"document."
msgstr ""
"Вы можете использовать стили для поддержания во всём документе определённого "
"оформления, уменьшения действий, требуемых для изменения оформления."

#. Tag: para
#: styles.docbook:24
#, fuzzy, no-c-format
msgid ""
"If you are familiar with Styles in &Microsoft; <application>Word</"
"application>, &kword; text styles perform similar functions in &kword;. You "
"can <link linkend=\"style-select\">skip to the next section</link>."
msgstr ""
"Если вы хорошо знакомы со стилями &Microsoft; <application>Word</"
"application>, то можете <link linkend=\"style-select\">перейти к следующему "
"разделу</link>, т.к. стили в &kword; выполняют такие же функции."

#. Tag: para
#: styles.docbook:29
#, fuzzy, no-c-format
msgid ""
"Lets look at a section of a document and try to identify appropriate text "
"styles:"
msgstr ""
"Например, если мы посмотрим на фрагмент документа, можно выделить различные "
"стили в определённых частях:"

#. Tag: para
#: styles.docbook:38
#, no-c-format
msgid "In this example, you can see several different text styles at work:"
msgstr ""
"В этом примере вы можете увидеть несколько различных стилей в действии:"

#. Tag: para
#: styles.docbook:41
#, no-c-format
msgid "The title is centered, underlined and in bold text"
msgstr ""
"Заголовок, оформленный полужирным шрифтом с подчёркиванием и выравниванием "
"по центру"

#. Tag: para
#: styles.docbook:42
#, no-c-format
msgid "The section title (Income) is boldfaced."
msgstr "Заголовок раздела, оформленный полужирным шрифтом."

#. Tag: para
#: styles.docbook:43
#, no-c-format
msgid "The caption for figure 1-1 is smaller than normal text and boldface."
msgstr ""
"Подпись к диаграмме, оформленная полужирным шрифтом, размер шрифта меньше, "
"чем у основного текста."

#. Tag: para
#: styles.docbook:44
#, no-c-format
msgid "The rest of the document is in a standard font."
msgstr "Основной текст документа, оформленный обычным шрифтом."

#. Tag: para
#: styles.docbook:48
#, no-c-format
msgid ""
"We do not need to know exactly how we want the text and paragraphs of these "
"sections to look yet. All we need to do is identify these sections of text "
"as <quote>Section Titles</quote>, <quote>Normal Text</quote>, etc."
msgstr ""
"Вам не требуется абсолютно точно знать, как должен выглядеть текст или "
"абзац. Всё что вам нужно, это обозначить этот текст как <quote>Заголовок</"
"quote>, <quote>Обычный текст</quote> и т.д."

#. Tag: para
#: styles.docbook:53
#, no-c-format
msgid ""
"Once we have finished the document, you can change the look of all the text "
"labeled <quote>Section Title</quote> all at once."
msgstr ""
"Как только документ закончен, вы можете мгновенно изменить вид всех участков "
"текста, обозначенных как <quote>Заголовок</quote>."

#. Tag: para
#: styles.docbook:56
#, no-c-format
msgid ""
"This will ensure that all section titles appear consistent throughout your "
"document."
msgstr ""
"Это гарантирует, что все заголовки будут однотипны по всему вашему документу."

#. Tag: para
#: styles.docbook:59
#, no-c-format
msgid "&kword; has 8 predefined text styles."
msgstr "&kword; содержит 8 встроенных стилей."

#. Tag: para
#: styles.docbook:61
#, no-c-format
msgid ""
"Text styles should not be confused with <link linkend=\"tablestyle\">table "
"styles</link> or <link linkend=\"framestyles\">frame styles</link>."
msgstr ""
"Стили текста не стоит путать со <link linkend=\"tablestyle\">стилями таблиц</"
"link> или <link linkend=\"framestyles\">стилями врезок</link>."

#. Tag: para
#: styles.docbook:63
#, no-c-format
msgid ""
"Text styles determine how the characters and paragraphs appear. Framestyles, "
"control the borders and background color for the frame. Tablestyles "
"determine how tables appear in the finished document."
msgstr ""
"Стиль текста определяет вид символов и вид абзаца. Стили врезок определяют "
"вид рамки и фона врезок. Стили таблиц определяют формат таблицы в документе."

#. Tag: title
#: styles.docbook:68
#, no-c-format
msgid "Changing the text style of text"
msgstr "Изменение стиля текста"

#. Tag: secondary
#: styles.docbook:69
#, no-c-format
msgid "changing text style of selected text"
msgstr "изменение стиля выделенного текста"

#. Tag: para
#: styles.docbook:71
#, no-c-format
msgid ""
"To change the text style of text, first, <link linkend=\"select\">select the "
"text</link> you want the changes to apply to."
msgstr ""
"Для изменение стиля текста предварительно <link linkend=\"select\">выделите "
"текст</link>, который вы хотите изменить."

#. Tag: para
#: styles.docbook:74
#, no-c-format
msgid "You can now change the text style in one of two ways:"
msgstr "Вы можете изменить стиль текста одним из двух способов:"

#. Tag: para
#: styles.docbook:78
#, fuzzy, no-c-format
msgid ""
"Select <menuchoice> <guimenu>Format</guimenu><guisubmenu>Style</guisubmenu></"
"menuchoice> from the menubar. This will show the list of available text "
"styles, select the text style from the list."
msgstr ""
"Выберите пункт меню <menuchoice> <guimenu>Формат</"
"guimenu><guimenuitem>Стиль</guimenuitem></menuchoice>. Откроется список "
"доступных стилей, выберите стиль в списке."

#. Tag: para
#: styles.docbook:85
#, no-c-format
msgid ""
"You can select the text style using the combo box on the <link linkend="
"\"paragraph-toolbar\">Paragraph toolbar</link>. This combo box looks like "
"this: <inlinemediaobject><imageobject><imagedata fileref=\"chstylebut.png\" "
"format=\"PNG\"/></imageobject></inlinemediaobject>."
msgstr ""
"Вы можете выбрать стиль с помощью выпадающего списка "
"<inlinemediaobject><imageobject><imagedata fileref=\"chstylebut.png\" format="
"\"PNG\"/></imageobject></inlinemediaobject> на <link linkend=\"paragraph-"
"toolbar\">панели инструментов Абзац</link>."

#. Tag: para
#: styles.docbook:90
#, no-c-format
msgid "Simply select the new text style from the list provided."
msgstr "Просто выберите новый стиль в этом списке."

#. Tag: para
#: styles.docbook:92
#, no-c-format
msgid ""
"You can change the text style of an entire paragraph. Simply place the "
"cursor in a paragraph (making sure that no characters are selected) and "
"select a new style. Every character in the paragraph will be converted to "
"the new text style."
msgstr ""

#. Tag: title
#: styles.docbook:97
#, no-c-format
msgid "Creating a text style"
msgstr "Создание стиля текста"

#. Tag: secondary
#: styles.docbook:98
#, no-c-format
msgid "creating"
msgstr "создание"

#. Tag: para
#: styles.docbook:100
#, no-c-format
msgid ""
"If you plan on using text styles extensively in a large document, you will "
"probably want to create new text styles specific for your needs."
msgstr ""
"Если вы планируете повсеместно использовать стили в большом документе, вы, "
"возможно, захотите создать стили с необходимыми вам параметрами."

#. Tag: para
#: styles.docbook:104
#, fuzzy, no-c-format
msgid "There are two ways to create a new text style:"
msgstr "Есть два основных способа создания нового стиля:"

#. Tag: para
#: styles.docbook:107
#, no-c-format
msgid ""
"<link linkend=\"style-add-dialog\">Create a text style based on another text "
"style using a dialog</link>."
msgstr ""
"<link linkend=\"style-add-dialog\">Создание стиля, основанного на другом "
"стиле</link>."

#. Tag: para
#: styles.docbook:108
#, no-c-format
msgid ""
"<link linkend=\"style-add-text\">Format a block of text to the desired "
"format, then create a text style based on that text</link>."
msgstr ""
"<link linkend=\"style-add-text\">Отформатируйте участок текста, а затем "
"создайте стиль на его основе</link>."

#. Tag: title
#: styles.docbook:114
#, no-c-format
msgid "Creating a new text style based on a current text style"
msgstr "Создание нового стиля на основе существующего стиля текста"

#. Tag: para
#: styles.docbook:115
#, no-c-format
msgid ""
"To create a new text style select <menuchoice> <guimenu>Format</"
"guimenu><guimenuitem>Style Manager...</guimenuitem></menuchoice> from the "
"menubar."
msgstr ""
"Для создания нового стиля, выберите пункт меню <menuchoice> <guimenu>Формат</"
"guimenu><guimenuitem>Менеджер стилей</guimenuitem></menuchoice>."

#. Tag: para
#: styles.docbook:117 styles.docbook:252 styles.docbook:292
#, no-c-format
msgid "This will bring up a dialog box."
msgstr "Появится диалоговое окно."

#. Tag: para
#: styles.docbook:125
#, no-c-format
msgid ""
"In the list box on the left, is a list of all the currently defined text "
"styles. Select the current style that most closely resembles the new text "
"style. Click on that text style once with the &LMB;."
msgstr ""
"В списке, расположенном слева вы увидите все определённые стили. Выберите "
"стиль, имеющий наибольшее сходство с создаваемым. Щёлкните на названии стиля "
"левой кнопкой мыши."

#. Tag: para
#: styles.docbook:128
#, no-c-format
msgid "Click the <guibutton>New</guibutton> button."
msgstr "Нажмите кнопку <guibutton>Создать</guibutton>."

#. Tag: para
#: styles.docbook:130
#, fuzzy, no-c-format
msgid ""
"&kword; will create a new text style based on the selected text style. It "
"will assign it a temporary name [New Style Template (8)]."
msgstr ""
"&kword; создаст новый стиль, основанный на выбранном стиле. Стилю будет "
"присвоено имя [Шаблон нового стиля (8).]"

#. Tag: para
#: styles.docbook:132
#, no-c-format
msgid ""
"Type a descriptive name in the box labeled <guilabel>Name</guilabel>. This "
"will be the name of the text style."
msgstr ""
"Введите название стиля в текстовом поле <guilabel>Имя</guilabel>. Это "
"название будет присвоено стилю."

#. Tag: para
#: styles.docbook:135
#, no-c-format
msgid ""
"The rest of the dialog is used to make the alterations needed to create the "
"new text style."
msgstr ""
"Остальные элементы диалога используются для выполнения необходимых изменений "
"в создаваемом стиле."

#. Tag: para
#: styles.docbook:138 styles.docbook:303
#, no-c-format
msgid "The preview box will show you what your new text style will look like."
msgstr ""
"Окно предварительного просмотра отображает, как будет выглядеть ваш новый "
"стиль текста."

#. Tag: para
#: styles.docbook:141
#, fuzzy, no-c-format
msgid ""
"Use the tabs labeled <link linkend=\"style-font\">Font</link>, <link linkend="
"\"style-space\">Indent &amp; Spacing</link>, <link linkend=\"style-align"
"\">General Layout</link>, <link linkend=\"style-borders\">Decorations</"
"link>, <link linkend=\"style-numbering\">Bullets/Numbers</link>, and <link "
"linkend=\"style-tabulators\">Tabulators</link>, to format the text style."
msgstr ""
"Воспользуйтесь вкладками <link linkend=\"style-font\">Шрифт</link>, <link "
"linkend=\"style-space\">Отступы и интервалы</link>, <link linkend=\"style-"
"align\">Выравнивание</link>, <link linkend=\"style-borders\">Границы</link>, "
"<link linkend=\"style-numbering\">Списки</link> и <link linkend=\"style-"
"tabulators\">Позиции табуляции</link> для создания стиля текста."

#. Tag: para
#: styles.docbook:149
#, no-c-format
msgid "Click <guibutton>OK</guibutton> to create your new text style."
msgstr "Нажмите кнопку <guibutton>OK</guibutton> для создания вашего стиля."

#. Tag: para
#: styles.docbook:150
#, no-c-format
msgid ""
"Click <guibutton>Cancel</guibutton> to abort the creation of the text style."
msgstr ""
"Нажмите кнопку <guibutton>Отмена</guibutton>, чтобы не создавать стиль."

#. Tag: title
#: styles.docbook:154
#, no-c-format
msgid "Changing the Font Size, Type etc."
msgstr "Изменение размера шрифта, стиля шрифта и т.д."

#. Tag: para
#: styles.docbook:156
#, no-c-format
msgid ""
"The tab labeled <guilabel>Font</guilabel> is used to set the font type, font "
"style, formatting, etc."
msgstr ""
"Откройте вкладку <guilabel>Шрифт</guilabel>, которая используется для "
"установки шрифта, стиля шрифта и других параметров шрифта."

#. Tag: para
#: styles.docbook:158
#, no-c-format
msgid ""
"This tab functions identically to the <link linkend=\"change-font-dialog"
"\">change font dialog</link> used to edit general text. Changes to this tab "
"affect to all text formated with this text style."
msgstr ""
"Назначение этой вкладки идентично <link linkend=\"change-font-dialog"
"\">диалогу выбора шрифта</link>, который используется для установки основных "
"параметров шрифта. Изменение этой вкладки повлияет на все участки текста "
"отформатированные с использование этого стиля."

#. Tag: title
#: styles.docbook:165
#, no-c-format
msgid "Changing Paragraph Spacing and Indents"
msgstr "Изменение отступов и интервалов"

#. Tag: para
#: styles.docbook:167
#, no-c-format
msgid ""
"The tab labeled <guilabel>Indents &amp; Spacing</guilabel> is used to adjust "
"spacing between lines, and paragraph indentation."
msgstr ""
"Вкладка <guilabel>Отступы и интервалы</guilabel> используется для изменения "
"межстрочного интервала и абзацных отступов."

#. Tag: para
#: styles.docbook:170
#, no-c-format
msgid ""
"This tab functions identically to the same tab in the <link linkend="
"\"indents-and-spaces\">Format Paragraph</link> dialog. Changes to this tab "
"affect to all text formated with this text style."
msgstr ""
"Назначение этой вкладки идентично соответствующей вкладке диалога <link "
"linkend=\"indents-and-spaces\">Абзац</link>. Изменение этой вкладки повлияет "
"на все участки текста, отформатированные с использование этого стиля."

#. Tag: title
#: styles.docbook:176
#, no-c-format
msgid "Changing Paragraph Alignment"
msgstr "Изменение выравнивания текста"

#. Tag: para
#: styles.docbook:178
#, no-c-format
msgid ""
"The tab labeled <guilabel>General Layout</guilabel> determines how the text "
"is placed <emphasis>within the line</emphasis>. With other applications, you "
"may have referred to this as <emphasis>Alignment</emphasis>, or "
"<emphasis>Justification</emphasis>."
msgstr ""
"Вкладка <guilabel>Выравнивание</guilabel> определяет, как размещается текст "
"<emphasis> на строке</emphasis>."

#. Tag: para
#: styles.docbook:183
#, no-c-format
msgid ""
"This tab functions identically to the same tab in the <link linkend=\"para-"
"aligns\">Format Paragraph</link> dialog. Changes to this tab affect to all "
"text formated with this text style."
msgstr ""
"Назначение этой вкладки идентично соответствующей вкладке диалога <link "
"linkend=\"para-aligns\">Абзац</link>. Изменение этой вкладки повлияет на все "
"участки текста отформатированные с использованием этого стиля."

#. Tag: title
#: styles.docbook:190
#, no-c-format
msgid "Changing Paragraph Borders"
msgstr "Изменение рамки абзаца"

#. Tag: para
#: styles.docbook:192
#, fuzzy, no-c-format
msgid ""
"The tab labeled <guilabel>Decorations</guilabel> is used to define and "
"configure graphical borders around your paragraphs."
msgstr ""
"Вкладка <guilabel>Границы</guilabel> используется для установки рамки вокруг "
"абзаца и выбора её вида."

#. Tag: para
#: styles.docbook:195
#, no-c-format
msgid ""
"This tab functions identically to the same tab in the <link linkend=\"para-"
"borders\">Format Paragraph</link> dialog. Changes to this tab affect to all "
"text formated with this text style."
msgstr ""
"Назначение этой вкладки идентично соответствующей вкладке диалога <link "
"linkend=\"para-borders\">Абзац</link>. Изменение этой вкладки повлияет на "
"все участки текста, отформатированные с использованием этого стиля."

#. Tag: title
#: styles.docbook:202
#, no-c-format
msgid "Changing Paragraph Numbering/Bullets"
msgstr "Изменение нумерации/маркеров"

#. Tag: para
#: styles.docbook:204
#, no-c-format
msgid ""
"The tab labeled <guilabel>Bullets/Numbers</guilabel> is used to make all "
"text formatted with this text style into a list."
msgstr ""
"Вкладка <guilabel>Списки</guilabel> используется для указания параметров "
"нумерации этого стиля."

#. Tag: para
#: styles.docbook:206
#, no-c-format
msgid ""
"This tab functions identically to the same tab in the <link linkend=\"para-"
"bullets-and-numbers\">Format Paragraph</link> dialog. For more information "
"see the section entitled <link linkend=\"lists\">Lists</link>."
msgstr ""
"Назначение этой вкладки идентично соответствующей вкладке диалога <link "
"linkend=\"para-bullets-and-numbers\">Абзац</link>. Подробную информацию "
"можно посмотреть в разделе справки <link linkend=\"lists\">Списки</link>."

#. Tag: title
#: styles.docbook:213
#, no-c-format
msgid "Changing Tab-stops."
msgstr "Изменение табуляторов."

#. Tag: para
#: styles.docbook:215
#, no-c-format
msgid ""
"Using the tab labeled <guilabel>Tabulators</guilabel> it is possible to "
"define tab stops for the new text style."
msgstr ""
"Вкладка <guilabel>Позиции табуляции</guilabel> позволяет указать позиции "
"табулятора для вашего стиля."

#. Tag: para
#: styles.docbook:217
#, fuzzy, no-c-format
msgid ""
"This tab functions identically to the same tab in the <link linkend=\"tab-"
"stop-setting-dialog\">Format Paragraph</link> dialog. For more information "
"see the section entitled <link linkend=\"tab-stops\">Using Tab Stops</link>."
msgstr ""
"Назначение этой вкладки идентично соответствующей вкладке диалога <link "
"linkend=\"tab-stop-setting-dialog\">Абзац</link>. Подробную информацию можно "
"посмотреть в разделе справки <link linkend=\"tab-stops\">Табуляторы</link>."

#. Tag: title
#: styles.docbook:224
#, no-c-format
msgid "Creating a text style based on formatted text"
msgstr "Создание стиля текста на основе отформатированного текста"

#. Tag: para
#: styles.docbook:225
#, no-c-format
msgid ""
"If you have text that is already formatted correctly for a new text style:"
msgstr ""

#. Tag: para
#: styles.docbook:227
#, fuzzy, no-c-format
msgid ""
"Select the text and select <menuchoice> <guimenu>Format</"
"guimenu><guimenuitem>Create Style From Selection...</guimenuitem></"
"menuchoice> from the menubar."
msgstr ""
"Выберите пункт меню <menuchoice> <guimenu>Формат</"
"guimenu><guimenuitem>Менеджер стилей</guimenuitem></menuchoice>."

#. Tag: para
#: styles.docbook:232
#, no-c-format
msgid ""
"&kword; will prompt you for a name for your text style. Enter the name in "
"the text box."
msgstr ""

#. Tag: para
#: styles.docbook:233
#, fuzzy, no-c-format
msgid "Click the <guibutton>OK</guibutton> button."
msgstr "Нажмите кнопку <guibutton>Создать</guibutton>."

#. Tag: para
#: styles.docbook:236
#, fuzzy, no-c-format
msgid ""
"A new text style is created with the font, paragraph spacing, paragraph "
"alignment, borders and shadows of the currently selected text."
msgstr ""
"&kword; отобразит диалог для ввода имени вашего стиля текста. Введите имя в "
"поле ввода и нажмите кнопку <guibutton>ОК</guibutton>. Будет создан новый "
"стиль текста, содержащий парамеры шрифта, отступы абзаца, выравнивание "
"текста, рамку и тень как в выделенном тексте."

#. Tag: para
#: styles.docbook:238
#, no-c-format
msgid ""
"Future formatting changes to this selected text will not automatically "
"change the text style you just created. If you want the changes to become "
"part of the text style, you must <link linkend=\"style-edit\">edit the text "
"style</link>."
msgstr ""
"Последующие изменения параметров форматирования этого текста не приведут к "
"изменению только что созданного стиля. Если вы хотите изменить стиль, вы "
"должны выполнить <link linkend=\"style-edit\">изменение стиля текста</link>."

#. Tag: title
#: styles.docbook:244
#, no-c-format
msgid "Deleting a text style"
msgstr "Удаление стиля"

#. Tag: secondary
#: styles.docbook:245
#, no-c-format
msgid "deleting"
msgstr "удаление"

#. Tag: para
#: styles.docbook:247
#, no-c-format
msgid "Deleting an unneeded text style is easy."
msgstr "Удалить ненужный стиль легко."

#. Tag: para
#: styles.docbook:250 styles.docbook:290
#, no-c-format
msgid ""
"Select <menuchoice> <guimenu>Format</guimenu><guimenuitem>Style Manager...</"
"guimenuitem></menuchoice> from the menubar."
msgstr ""
"Выберите пункт меню <menuchoice> <guimenu>Формат</"
"guimenu><guimenuitem>Менеджер стилей</guimenuitem></menuchoice>."

#. Tag: para
#: styles.docbook:261
#, no-c-format
msgid ""
"From the list of available text styles, select the style you want to delete "
"by clicking once with the &LMB;."
msgstr ""
"В списке доступных стилей выберите удаляемый стиль щелчком левой кнопки мыши."

#. Tag: para
#: styles.docbook:265
#, no-c-format
msgid ""
"Be sure you have selected the correct text style before you click "
"<guibutton>Delete</guibutton>. &kword; will not ask for confirmation, so you "
"will not be given an opportunity to back out."
msgstr ""
"Убедитесь, что вы выделили нужный стиль, прежде чем нажать кнопку "
"<guibutton>Удалить</guibutton>. &kword; не запрашивает подтверждения, "
"поэтому вы не сможете отменить удаление стиля."

#. Tag: para
#: styles.docbook:269
#, no-c-format
msgid "Click <guibutton>Delete</guibutton>."
msgstr "Нажмите кнопку <guibutton>Удалить</guibutton>."

#. Tag: para
#: styles.docbook:270
#, no-c-format
msgid "The text style is now deleted."
msgstr "Этот стиль будет удалён."

#. Tag: title
#: styles.docbook:275
#, no-c-format
msgid "Editing a text style"
msgstr "Изменение стиля"

#. Tag: secondary
#: styles.docbook:276
#, no-c-format
msgid "editing"
msgstr "изменение"

#. Tag: para
#: styles.docbook:278
#, no-c-format
msgid ""
"The true power of text styles, is the ability to edit the formatting options "
"of that text style <emphasis>after</emphasis> the text style is defined. By "
"changing the formatting of the text style, &kword; will immediately change "
"every paragraph with that text style, and maintain a consistent look to the "
"document."
msgstr ""
"Преимуществом стилей является возможность изменения параметров "
"форматирования <emphasis>после</emphasis> создания стиля. При изменении "
"параметров форматирования стиля &kword; немедленно изменит каждый абзац "
"оформленный этим стилем, и сохранит оформление в документе."

#. Tag: para
#: styles.docbook:284
#, no-c-format
msgid ""
"&kword; uses the same interfaces to edit the formatting options of a text "
"style, that it used to create the text style in the first place."
msgstr ""
"Для изменения параметров форматирования стиля текста &kword; использует тот "
"же диалог, который использовался для создания стиля."

#. Tag: para
#: styles.docbook:288
#, no-c-format
msgid "To edit a current text style:"
msgstr "Изменение существующего стиля текста:"

#. Tag: para
#: styles.docbook:300
#, no-c-format
msgid ""
"Select the text style you want to edit from the list on the left by clicking "
"once with the &LMB;. Now you can make the changes you want to this text "
"style."
msgstr ""
"Выберите стиль текста, который вы хотите изменить в списке слева щелчком "
"левой кнопки мыши. Теперь вы можете изменить в этом стиле всё, что хотите."

#. Tag: para
#: styles.docbook:307
#, fuzzy, no-c-format
msgid "Do not change the name of your text style."
msgstr "Не изменяйте имя стиля."

#. Tag: para
#: styles.docbook:309
#, no-c-format
msgid ""
"Doing so will delete your current text style from the list (and create a new "
"one with the new name)."
msgstr ""
"Этим вы удалите ваш стиль из списка стилей (и создадите новый стиль с "
"указанным именем)."

#. Tag: para
#: styles.docbook:312
#, no-c-format
msgid ""
"Any paragraphs which were originally formatted with this text style, will "
"revert to <quote>Standard</quote>."
msgstr ""
"Каждый абзац, который был оформлен этим стилем, будет оформлен стилем "
"<quote>Обычный</quote>."

#. Tag: para
#: styles.docbook:316
#, fuzzy, no-c-format
msgid ""
"Use the tabs labeled <link linkend=\"style-font\">Font</link>, <link linkend="
"\"style-space\">Indent &amp; Spacing</link>, <link linkend=\"style-align"
"\">General Layout</link>, <link linkend=\"style-borders\">Decorations</"
"link>, <link linkend=\"style-numbering\">Bullets/Numbers</link>, and <link "
"linkend=\"style-tabulators\">Tabulators</link> to alter the look of the text "
"style."
msgstr ""
"Воспользуйтесь вкладками <link linkend=\"style-font\">Шрифт</link>, <link "
"linkend=\"style-space\">Отступы и интервалы</link>, <link linkend=\"style-"
"align\">Выравнивание</link>, <link linkend=\"style-borders\">Границы</link>, "
"<link linkend=\"style-numbering\">Списки</link> и <link linkend=\"style-"
"tabulators\">Позиции табуляции</link> для создания стиля текста."

#. Tag: para
#: styles.docbook:324
#, no-c-format
msgid "Click <guibutton>OK</guibutton> to commit your changes."
msgstr ""
"Нажмите кнопку <guibutton>ОК</guibutton> для сохранения ваших изменений."

#. Tag: para
#: styles.docbook:326
#, no-c-format
msgid ""
"Click <guibutton>Cancel</guibutton> to abort all changes to this text style."
msgstr ""
"Нажмите кнопку <guibutton>Отмена</guibutton> для отмены всех изменений в "
"этом стиле."

#. Tag: title
#: styles.docbook:332
#, no-c-format
msgid "Import a text style"
msgstr "Импорт стиля"

#. Tag: secondary
#: styles.docbook:333
#, no-c-format
msgid "importing"
msgstr "импортирование"

#. Tag: para
#: styles.docbook:335
#, no-c-format
msgid ""
"&kword; has the ability to import a text style from one &kword; document and "
"include it in the list of text text styles in another &kword; document."
msgstr ""
"&kword; может импортировать стили текста из одного документа &kword; и "
"добавить их в список стилей текста другого документа &kword;."

#. Tag: para
#: styles.docbook:337
#, no-c-format
msgid ""
"To import a text style, select <menuchoice><guimenu>Format</"
"guimenu><guimenuitem>Import Style...</guimenuitem></menuchoice> from the "
"menubar."
msgstr ""
"Для импорта стилей выберите пункт меню <menuchoice><guimenu>Формат</"
"guimenu><guimenuitem>Импорт стиля...</guimenuitem></menuchoice>."

#. Tag: para
#: styles.docbook:338
#, no-c-format
msgid "This will bring up an empty dialog box."
msgstr "Появится пустое диалоговое окно."

#. Tag: para
#: styles.docbook:339
#, no-c-format
msgid ""
"Click on the <guibutton>Load</guibutton> button. This will bring up a <link "
"linkend=\"file-dialog\">file selection dialog</link>. Choose the &kword; "
"file you want to import the text style from and click <guibutton>OK</"
"guibutton>."
msgstr ""
"Нажмите кнопку <guibutton>Загрузка</guibutton>. Появится диалоговое окно "
"<link linkend=\"file-dialog\">выбора файлов</link>. Выделите документ "
"&kword;, стили которого вы хотите импортировать и нажмите кнопку "
"<guibutton>ОК</guibutton>."

#. Tag: para
#: styles.docbook:342
#, no-c-format
msgid ""
"The dialog box will now fill with all available text text styles available "
"for import."
msgstr ""
"В диалог будут помещены названия всех доступных для импортирования стилей "
"текста."

#. Tag: para
#: styles.docbook:343
#, no-c-format
msgid ""
"If &kword; encounters a duplicate text style name in the selected file, it "
"will append a number to the end of the text style name to identify the "
"imported style."
msgstr ""
"Если &kword; встретит в импортируемом документе уже существующее имя стиля, "
"то он добавит номер к имени импортируемого стиля."

#. Tag: para
#: styles.docbook:345
#, no-c-format
msgid ""
"As an example, if you import the <emphasis>Standard</emphasis> text style "
"from another &kword; file, &kword; will change the text style name to "
"<emphasis>Standard-1</emphasis>."
msgstr ""
"Например, если вы импортируете стиль <emphasis>Обычный</emphasis> из другого "
"документа &kword;, то название этого стиля будет изменено на "
"<emphasis>Обычный-1</emphasis>."

#. Tag: para
#: styles.docbook:347
#, no-c-format
msgid ""
"Select <emphasis>all</emphasis> the text style you want to import. Then "
"click <guibutton>OK</guibutton>."
msgstr ""
"Выберите <emphasis>все</emphasis> стили, которые вы хотите импортировать и "
"нажмите кнопку <guibutton>ОК</guibutton>."

#, fuzzy
#~ msgid ""
#~ "Use the buttons labeled <link linkend=\"style-font\">Font</link>, <link "
#~ "linkend=\"style-space\">Indent and Spacing</link>, <link linkend=\"style-"
#~ "align\">Aligns</link>, <link linkend=\"style-borders\">Decorations</"
#~ "link>, <link linkend=\"style-numbering\">Bullets/Numbers</link>, and "
#~ "<link linkend=\"style-tabulators\">Tabulators</link> to alter the look of "
#~ "the text style."
#~ msgstr ""
#~ "Используйте вкладки <link linkend=\"style-font\">Шрифт</link>, <link "
#~ "linkend=\"style-space\">Отступы и интервалы</link>, <link linkend=\"style-"
#~ "align\">Выравнивание</link>, <link linkend=\"style-borders\">Границы</"
#~ "link>, <link linkend=\"style-numbering\">Списки</link>, и <link linkend="
#~ "\"style-tabulators\">Позиции табуляции</link> для внесения изменений в "
#~ "стиль текста."

#~ msgid "This will bring up a dialog box"
#~ msgstr "Появится диалоговое окно"

#~ msgid ""
#~ "If you have text that is already formatted correctly for a new text "
#~ "style, simply select the text and select <menuchoice> <guimenu>Format</"
#~ "guimenu><guimenuitem>Create Style From Selection</guimenuitem></"
#~ "menuchoice> from the menubar."
#~ msgstr ""
#~ "Если вы уже имеете текст отформатированный соответствующим образом, "
#~ "просто выделите его и выберите пункт меню <menuchoice><guimenu>Формат</"
#~ "guimenu><guimenuitem>Создать стиль из выделения</guimenuitem></"
#~ "menuchoice>."
