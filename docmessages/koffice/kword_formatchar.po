# KDE3 - docs/koffice/kword_formatchar.po Russian translation.
# Copyright (C) 2002, KDE Team.
# Kernel Panic <rzhevskiy@mail.ru>, 2002.
#
msgid ""
msgstr ""
"Project-Id-Version: docs/koffice/kword_formatchar.po\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-02-11 07:20+0000\n"
"PO-Revision-Date: 2002-08-17 21:49+0700\n"
"Last-Translator: Andrey S. Cherepanov <sibskull@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.6\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: author
#: formatchar.docbook:4
#, no-c-format
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr ""

#. Tag: trans_comment
#: formatchar.docbook:8
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""

#. Tag: title
#: formatchar.docbook:11
#, no-c-format
msgid "Formatting Characters"
msgstr "Форматирование текста"

#. Tag: primary
#: formatchar.docbook:12 formatchar.docbook:31 formatchar.docbook:33
#: formatchar.docbook:35 formatchar.docbook:37 formatchar.docbook:39
#: formatchar.docbook:41 formatchar.docbook:43 formatchar.docbook:45
#: formatchar.docbook:47 formatchar.docbook:49 formatchar.docbook:267
#, no-c-format
msgid "text"
msgstr ""

#. Tag: secondary
#: formatchar.docbook:12 formatchar.docbook:13
#, fuzzy, no-c-format
msgid "formatting"
msgstr "Форматирование текста"

#. Tag: primary
#: formatchar.docbook:13
#, fuzzy, no-c-format
msgid "font"
msgstr "Шрифт"

#. Tag: primary
#: formatchar.docbook:14
#, fuzzy, no-c-format
msgid "formatting text"
msgstr "Форматирование текста"

#. Tag: para
#: formatchar.docbook:16
#, no-c-format
msgid ""
"This section will cover the changes that can be made to individual "
"characters or blocks of characters including:"
msgstr ""
"В этом разделе описано, как можно изменить такие параметры форматирования "
"текста и отдельных символов, как:"

#. Tag: link
#: formatchar.docbook:20
#, no-c-format
msgid "Character size"
msgstr "Размер"

#. Tag: link
#: formatchar.docbook:21
#, no-c-format
msgid "<link>Font</link>"
msgstr ""

#. Tag: link
#: formatchar.docbook:22
#, fuzzy, no-c-format
msgid "Font style (italic, bold, etc)."
msgstr "Стиль шрифта (курсив, полужирный и т. д.)."

#. Tag: link
#: formatchar.docbook:23
#, no-c-format
msgid "Underlines, superscript, subscript, strikeout, etc."
msgstr "Подчёркивание, зачёркивание, верхний и нижний индекс."

#. Tag: link
#: formatchar.docbook:24
#, no-c-format
msgid "Character color and background color."
msgstr "Цвет текста и фона."

#. Tag: link
#: formatchar.docbook:25
#, no-c-format
msgid "Character case."
msgstr "Регистр текста (строчные или прописные буквы)."

#. Tag: title
#: formatchar.docbook:30
#, fuzzy, no-c-format
msgid "Changing Size, Font, Font Style, Text color and Background Color"
msgstr "Изменение размера, шрифта, стиля шрифта, цвета текста и фона"

#. Tag: secondary
#: formatchar.docbook:31
#, fuzzy, no-c-format
msgid "change size"
msgstr "Размер"

#. Tag: primary
#: formatchar.docbook:32
#, fuzzy, no-c-format
msgid "change text size"
msgstr "Размер"

#. Tag: secondary
#: formatchar.docbook:33
#, fuzzy, no-c-format
msgid "change font"
msgstr "Размер"

#. Tag: primary
#: formatchar.docbook:34
#, no-c-format
msgid "change text font"
msgstr ""

#. Tag: secondary
#: formatchar.docbook:35
#, fuzzy, no-c-format
msgid "change color"
msgstr "Цвет фона"

#. Tag: primary
#: formatchar.docbook:36
#, no-c-format
msgid "change text color"
msgstr ""

#. Tag: secondary
#: formatchar.docbook:37
#, fuzzy, no-c-format
msgid "change background color"
msgstr "Цвет фона"

#. Tag: primary
#: formatchar.docbook:38
#, fuzzy, no-c-format
msgid "change text background color"
msgstr "Цвет текста и фона."

#. Tag: secondary
#: formatchar.docbook:39
#, no-c-format
msgid "bold"
msgstr ""

#. Tag: primary
#: formatchar.docbook:40
#, no-c-format
msgid "bold text"
msgstr ""

#. Tag: secondary
#: formatchar.docbook:41
#, no-c-format
msgid "italicized"
msgstr ""

#. Tag: primary
#: formatchar.docbook:42
#, no-c-format
msgid "italicized text"
msgstr ""

#. Tag: secondary
#: formatchar.docbook:43
#, fuzzy, no-c-format
msgid "underlined"
msgstr "Подчёркнутый"

#. Tag: primary
#: formatchar.docbook:44
#, fuzzy, no-c-format
msgid "underlined text"
msgstr "Подчёркнутый"

#. Tag: secondary
#: formatchar.docbook:45
#, fuzzy, no-c-format
msgid "strikethrough"
msgstr "Зачеркнутый"

#. Tag: primary
#: formatchar.docbook:46
#, fuzzy, no-c-format
msgid "strikethrough text"
msgstr "Зачеркнутый"

#. Tag: secondary
#: formatchar.docbook:47
#, fuzzy, no-c-format
msgid "superscript"
msgstr "Верхний индекс"

#. Tag: primary
#: formatchar.docbook:48
#, fuzzy, no-c-format
msgid "superscript text"
msgstr "Верхний индекс"

#. Tag: secondary
#: formatchar.docbook:49
#, fuzzy, no-c-format
msgid "subscript"
msgstr "Нижний индекс"

#. Tag: primary
#: formatchar.docbook:50
#, fuzzy, no-c-format
msgid "subscript text"
msgstr "Нижний индекс"

#. Tag: title
#: formatchar.docbook:54
#, fuzzy, no-c-format
msgid "Using the Keyboard or toolbars"
msgstr "Использование клавиатуры и панелей инструментов"

#. Tag: para
#: formatchar.docbook:56
#, no-c-format
msgid ""
"There are several keyboard shortcuts which you can use to make common "
"changes to character formatting. After selecting the text, you can use these "
"key combinations to toggle each attribute."
msgstr ""
"Существует несколько способов изменить форматирование текста. После того, "
"как вы выделите нужный вам текст, воспользуйтесь приведёнными здесь "
"сочетаниями клавиш чтобы включить или выключить отдельный параметр."

#. Tag: para
#: formatchar.docbook:60
#, fuzzy, no-c-format
msgid ""
"The <guilabel>Format</guilabel> toolbar also has buttons which you can use "
"to toggle these same attributes."
msgstr ""
"На панели инструментов с операциями форматирования вы тоже найдёте кнопки, "
"позволяющие вам изменять параметры текста."

#. Tag: para
#: formatchar.docbook:63
#, fuzzy, no-c-format
msgid ""
"The following table details each attribute, the toolbar buttons and the "
"keyboard shortcuts."
msgstr ""
"В следующей таблице приведены отдельные параметры форматирования текста, "
"изображение соответствующей кнопки на панели инструментов и сочетание "
"клавиш, требующееся, чтобы изменить каждый параметр."

#. Tag: entry
#: formatchar.docbook:68
#, no-c-format
msgid "Attribute"
msgstr "Параметр"

#. Tag: entry
#: formatchar.docbook:68
#, fuzzy, no-c-format
msgid "Toolbar Button"
msgstr "Кнопка"

#. Tag: entry
#: formatchar.docbook:69
#, no-c-format
msgid "Key Combo"
msgstr "Клавиша"

#. Tag: entry
#: formatchar.docbook:71
#, no-c-format
msgid "Font Size"
msgstr "Размер шрифта"

#. Tag: para
#: formatchar.docbook:72
#, no-c-format
msgid ""
"Decrease font size: <keycombo><keycap>Ctrl</keycap><keycap>&lt;</keycap></"
"keycombo>"
msgstr ""
"Уменьшить размер шрифта: <keycombo><keycap>Ctrl</keycap><keycap>&lt;</"
"keycap></keycombo>"

#. Tag: para
#: formatchar.docbook:72
#, no-c-format
msgid ""
"Increase font size: <keycombo><keycap>Ctrl</keycap><keycap>&gt;</keycap></"
"keycombo>"
msgstr ""
"Увеличить размер шрифта: <keycombo><keycap>Ctrl</keycap><keycap>&gt;</"
"keycap></keycombo>"

#. Tag: entry
#: formatchar.docbook:73
#, fuzzy, no-c-format
msgid "<entry>Font</entry>"
msgstr "<entry align=\"center\">-</entry>"

#. Tag: entry
#: formatchar.docbook:73 formatchar.docbook:77 formatchar.docbook:78
#: formatchar.docbook:79 formatchar.docbook:80
#, fuzzy, no-c-format
msgid "<entry>-</entry>"
msgstr "<entry align=\"center\">-</entry>"

#. Tag: entry
#: formatchar.docbook:74
#, no-c-format
msgid "Bold Face"
msgstr "Полужирный"

#. Tag: keycombo
#: formatchar.docbook:74
#, no-c-format
msgid "&Ctrl;<keycap>B</keycap>"
msgstr "&Ctrl;<keycap>B</keycap>"

#. Tag: entry
#: formatchar.docbook:75
#, no-c-format
msgid "Italics"
msgstr "Наклонный"

#. Tag: keycombo
#: formatchar.docbook:75
#, no-c-format
msgid "<keycap>Ctrl</keycap><keycap>I</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>I</keycap>"

#. Tag: entry
#: formatchar.docbook:76
#, no-c-format
msgid "Underline"
msgstr "Подчёркнутый"

#. Tag: keycombo
#: formatchar.docbook:76
#, no-c-format
msgid "<keycap>Ctrl</keycap><keycap>U</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>U</keycap>"

#. Tag: entry
#: formatchar.docbook:77
#, no-c-format
msgid "Strikethrough"
msgstr "Зачеркнутый"

#. Tag: entry
#: formatchar.docbook:78
#, no-c-format
msgid "Superscript"
msgstr "Верхний индекс"

#. Tag: entry
#: formatchar.docbook:79
#, no-c-format
msgid "Subscript"
msgstr "Нижний индекс"

#. Tag: entry
#: formatchar.docbook:80
#, no-c-format
msgid "Font Color"
msgstr "Цвет текста"

#. Tag: para
#: formatchar.docbook:85
#, fuzzy, no-c-format
msgid ""
"You cannot change the all text attributes using the keyboard or a button on "
"a toolbar. For these attributes, you must <link linkend=\"change-font-dialog"
"\">use the dialog box</link>."
msgstr ""
"Для того, чтобы изменить текущий набор символов для текста или установить "
"двойное подчёркивание, необходимо воспользоваться <link linkend=\"change-"
"font-dialog\">окном выбора шрифта</link>."

#. Tag: title
#: formatchar.docbook:93
#, no-c-format
msgid "Using the Dialog Box"
msgstr "Диалоговое окно выбора шрифта"

#. Tag: para
#: formatchar.docbook:95
#, no-c-format
msgid ""
"If you have many changes to make, you can also use a dialog box to set these "
"same attributes."
msgstr ""
"Когда вам нужно внести много изменений в параметры шрифта, бывает удобнее "
"воспользоваться для этого указанным диалоговым окном."

#. Tag: para
#: formatchar.docbook:98
#, fuzzy, no-c-format
msgid ""
"You can open the <guilabel>Select Font</guilabel> dialog box in one of three "
"ways:"
msgstr "Открыть диалоговое окно выбора шрифта можно тремя способами:"

#. Tag: para
#: formatchar.docbook:103
#, no-c-format
msgid ""
"Select <menuchoice> <guimenu>Format</guimenu><guimenuitem>Font...</"
"guimenuitem></menuchoice> from the menubar."
msgstr ""
"Выберите пункт меню <menuchoice> <guimenu>Формат</"
"guimenu><guimenuitem>Шрифт...</guimenuitem></menuchoice>."

#. Tag: para
#: formatchar.docbook:109
#, no-c-format
msgid ""
"Click on the selected text with the &RMB;. A small menu will appear. Select "
"<guimenuitem>Font...</guimenuitem> from the menu."
msgstr ""
"Щёлкните по выделенному тексту правой кнопкой мыши и в появившемся меню "
"выберите пункт <guimenuitem>Шрифт...</guimenuitem>."

#. Tag: para
#: formatchar.docbook:115
#, no-c-format
msgid ""
"You can use the keyboard shortcut: <keycombo>&Alt;&Ctrl;<keycap>F</keycap></"
"keycombo>"
msgstr ""
"Воспользуйтесь сочетанием клавиш <keycombo>&Alt;&Ctrl;<keycap>F</keycap></"
"keycombo>"

#. Tag: para
#: formatchar.docbook:120
#, no-c-format
msgid "Either method will open a dialog box"
msgstr "Каждый из этих трёх способов позволяет открыть данное окно."

#. Tag: para
#: formatchar.docbook:129
#, fuzzy, no-c-format
msgid ""
"Using this dialog box, you can select the font, size, italics and boldface "
"options from the top three list boxes."
msgstr ""
"С помощью этого окна вы можете изменить шрифт, размер, набор символов и "
"стиль шрифта (полужирный или наклонный)."

#. Tag: para
#: formatchar.docbook:132
#, no-c-format
msgid ""
"The preview box along the middle of the dialog, will show you how your "
"current settings will appear."
msgstr ""
"Посередине окна показан текст, по которому вы сможете понять, как выглядит "
"данный шрифт с выбранными параметрами."

#. Tag: para
#: formatchar.docbook:135
#, no-c-format
msgid ""
"If the default preview string is not appropriate, you can change it by "
"clicking once inside the preview box with the &LMB;. A text cursor will "
"appear. You can edit the text to suit your needs."
msgstr ""
"Если этот текст вам не подходит, щёлкните по нему левой кнопкой мыши, и вы "
"сможете его изменить."

#. Tag: para
#: formatchar.docbook:137
#, no-c-format
msgid ""
"The sample sentence will revert to the default the next time you open this "
"dialog."
msgstr ""
"Заметьте, что когда вы в следующий раз откроете это окно, текст будет "
"прежним."

#. Tag: para
#: formatchar.docbook:141
#, fuzzy, no-c-format
msgid ""
"If you click the tab labeled <guilabel>Highlighting</guilabel>, the dialog "
"changes to:"
msgstr ""
"Если вы выберете вкладку <guilabel>Эффекты шрифта</guilabel>, окно изменит "
"свой вид:"

#. Tag: para
#: formatchar.docbook:150
#, fuzzy, no-c-format
msgid ""
"The subsection labeled <guilabel>Underlining:</guilabel> has two combo boxes "
"and a color selector box. In the left most combo box select between no "
"underline (<guilabel>None</guilabel>), <guilabel>Single</guilabel>, "
"<guilabel>Simple Bold</guilabel>, <guilabel>Double</guilabel> or "
"<guilabel>Wave</guilabel> underlining."
msgstr ""
"Чтобы изменить ширину линии при подчёркивании, воспользуйтесь выпадающим "
"списком слева под строкой \"Подчёркивание\". Вы можете отключить "
"подчёркивание, выбрать простую линию, жирную или двойную."

#. Tag: para
#: formatchar.docbook:163
#, fuzzy, no-c-format
msgid ""
"Use the center combo box to select the style of underlining (Solid line, "
"dashed line, etc.)."
msgstr ""
"Чтобы изменить стиль линии (сплошная, пунктир и т. д.), воспользуйтесь "
"выпадающим списком справа."

#. Tag: para
#: formatchar.docbook:164
#, fuzzy, no-c-format
msgid ""
"Use the right color selector box to select a color for the underline using "
"the <link linkend=\"select-colors\">color selection dialog box</link>."
msgstr ""
"Нажав кнопку <guibutton>Изменить цвет</guibutton>, вы вызовете <link linkend="
"\"select-colors\">диалоговое окно выбора цвета</link>, которое позволит вам "
"изменить цвет линии."

#. Tag: para
#: formatchar.docbook:167
#, fuzzy, no-c-format
msgid ""
"The subsection labeled <guilabel>Strikethrough:</guilabel> has two combo "
"boxes. Use the left combo box to select between no strikethrough "
"(<guilabel>None</guilabel>), <guilabel>Single</guilabel>, <guilabel>Double</"
"guilabel> or <guilabel>Simple Bold</guilabel> strikethrough. Use the right "
"combo box to select the line style."
msgstr ""
"Чтобы изменить ширину и стиль линии при зачёркивании, воспользуйтесь двумя "
"выпадающими списками под строкой \"Зачёркивание\"."

#. Tag: para
#: formatchar.docbook:179
#, no-c-format
msgid ""
"The checkbox labeled <guilabel>Word by word</guilabel> determines how "
"underlining and strikethrough are drawn."
msgstr ""

#. Tag: para
#: formatchar.docbook:188
#, no-c-format
msgid ""
"The section labeled <guilabel>Capitalization</guilabel> lets you select one "
"of four special capitalization options. This will change the capitalization "
"of the selected text. Examples of each scheme are shown below."
msgstr ""

#. Tag: para
#: formatchar.docbook:198
#, no-c-format
msgid ""
"The changes to capilalization will only affect the selected text at the "
"current moment. This formatting is not enforced after closing the dialog box."
msgstr ""

#. Tag: para
#: formatchar.docbook:200
#, no-c-format
msgid ""
"The third tab labeled <guilabel>Decoration</guilabel> allows you to change "
"the color of the font, change the background color and create artificial "
"shadows for your characters. Click on the tab:"
msgstr ""

#. Tag: para
#: formatchar.docbook:209
#, no-c-format
msgid ""
"Using the buttons labeled <guilabel>Text color:</guilabel> and "
"<guilabel>Background color:</guilabel> you can change the color of the text "
"or the background behind the text."
msgstr ""

#. Tag: para
#: formatchar.docbook:211
#, no-c-format
msgid ""
"To create the illusion of a shadow behind your text, select the color you "
"want your shadow by clicking the button labeled <guilabel>Shadow color:</"
"guilabel>. Then select the distance between the shadow and the text using "
"the spin box labeld <guilabel>Shadow &amp; distance:</guilabel>. Finally, "
"select the direction of your shadow by selecting one of the eight red "
"squares. A preview of your shadow appears at the bottom of the dialog box."
msgstr ""

#. Tag: para
#: formatchar.docbook:215
#, no-c-format
msgid ""
"The fourth tab labeled <guilabel>Layout</guilabel> allows you to create "
"superscripts and subscripts. It also lets you select automatic hyphenation "
"on a character by character basis."
msgstr ""

#. Tag: para
#: formatchar.docbook:224
#, no-c-format
msgid ""
"The section labeled <guilabel>Position</guilabel> has four radio buttons and "
"two spin boxes. Selecting <guilabel>Normal</guilabel>, produces text of "
"normal height and at the normal vertical position. Selecting "
"<guilabel>Superscript</guilabel> or <guilabel>Subscript</guilabel> will "
"convert the currently selected text. If you select <guilabel>Custom</"
"guilabel>, you can use the spin box labeled <guilabel>Offset:</guilabel> to "
"move the selected text up or down relative to normal text alignment. Look at "
"the example below:"
msgstr ""

#. Tag: para
#: formatchar.docbook:233
#, no-c-format
msgid ""
"Notice how a positive offset raises the text, and a negative offset lowers "
"the text."
msgstr ""

#. Tag: para
#: formatchar.docbook:235
#, no-c-format
msgid ""
"You can also use the spinbox labeled <guilabel>Relative size:</guilabel> to "
"make the selected font larger or smaller."
msgstr ""

#. Tag: para
#: formatchar.docbook:237
#, no-c-format
msgid ""
"The checkbox labeld <guilabel>Auto hyphenation</guilabel> is used to toggle "
"automatic hyphenation on or off for the selected text."
msgstr ""

#. Tag: para
#: formatchar.docbook:239
#, no-c-format
msgid ""
"The fifth tab labeled <guilabel>Language</guilabel> allows you to change the "
"language. Click on the tab:"
msgstr ""

#. Tag: para
#: formatchar.docbook:248
#, no-c-format
msgid ""
"You can select the language to use on the selected text with the drop down "
"box. If the selected text allows for automatic hyphenation, &kword; will use "
"general rules of spelling for the selected language to automatically "
"hyphenate the selected text."
msgstr ""

#. Tag: para
#: formatchar.docbook:251
#, fuzzy, no-c-format
msgid ""
"Clicking <guibutton>OK</guibutton> will commit your changes to the document "
"and close the dialog box."
msgstr ""
"Если вас устраивают внесённые изменения, нажмите кнопку <guibutton>OK</"
"guibutton>."

#. Tag: para
#: formatchar.docbook:254
#, fuzzy, no-c-format
msgid ""
"Clicking <guibutton>Apply</guibutton> will commit your changes to the "
"document but keep the dialog box open."
msgstr ""
"Если вас устраивают внесённые изменения, нажмите кнопку <guibutton>OK</"
"guibutton>."

#. Tag: para
#: formatchar.docbook:257
#, fuzzy, no-c-format
msgid ""
"Clicking <guibutton>Reset</guibutton> will restore the default settings to "
"the selected text."
msgstr ""
"Если вас устраивают внесённые изменения, нажмите кнопку <guibutton>OK</"
"guibutton>."

#. Tag: para
#: formatchar.docbook:259
#, no-c-format
msgid "Clicking <guibutton>Cancel</guibutton> will abort all changes made."
msgstr ""
"Нажмите кнопку <guibutton>Отмена</guibutton>, если вы хотите отменить "
"внесённые изменения."

#. Tag: title
#: formatchar.docbook:266
#, no-c-format
msgid "Changing Font Case"
msgstr "Изменение регистра"

#. Tag: secondary
#: formatchar.docbook:267
#, fuzzy, no-c-format
msgid "changing case"
msgstr "Изменение регистра"

#. Tag: primary
#: formatchar.docbook:268
#, no-c-format
msgid "change case of text"
msgstr ""

#. Tag: para
#: formatchar.docbook:270
#, no-c-format
msgid ""
"&kword; can change the case (uppercase or lowercase) of large blocks of text "
"to one of five pre-defined styles. Begin by selecting the block of text you "
"want changed. Select <menuchoice> <guimenu>Tools</"
"guimenu><guimenuitem>Change Case...</guimenuitem></menuchoice> from the "
"menubar."
msgstr ""
"&kword; позволяет изменять регистр (строчные или прописные буквы) текста. "
"Сначала выделите текст, регистр которого вы хотите изменить. Далее выберите "
"пункт меню <menuchoice> <guimenu>Инструменты</guimenu><guimenuitem>Изменить "
"регистр...</guimenuitem></menuchoice>."

#. Tag: para
#: formatchar.docbook:274
#, fuzzy, no-c-format
msgid "A small dialog box will appear with five options:"
msgstr "Появится небольшое окно с четырьмя вариантами:"

#. Tag: guilabel
#: formatchar.docbook:277
#, no-c-format
msgid "Uppercase"
msgstr "Верхний регистр"

#. Tag: para
#: formatchar.docbook:278
#, no-c-format
msgid "Will convert all characters to uppercase."
msgstr "Заменяет все буквы текста на прописные."

#. Tag: para
#: formatchar.docbook:279
#, no-c-format
msgid "Example: KWORD WILL AUTOMATICALLY CHANGE CASES."
msgstr "Пример: KWORD АВТОМАТИЧЕСКИ ИЗМЕНЯЕТ РЕГИСТР ТЕКСТА."

#. Tag: guilabel
#: formatchar.docbook:284
#, no-c-format
msgid "Lowercase"
msgstr "Нижний регистр"

#. Tag: para
#: formatchar.docbook:285
#, no-c-format
msgid "Will convert all characters to lowercase."
msgstr "Заменяет все буквы текста на строчные."

#. Tag: para
#: formatchar.docbook:286
#, no-c-format
msgid "Example: kword will automatically change cases."
msgstr "Пример: kword автоматически изменяет регистр текста."

#. Tag: guilabel
#: formatchar.docbook:291
#, fuzzy, no-c-format
msgid "Title case"
msgstr "Как в заголовках"

#. Tag: para
#: formatchar.docbook:292
#, no-c-format
msgid "Will capitalize every word."
msgstr "Заменяет первую букву каждого слова на прописную."

#. Tag: para
#: formatchar.docbook:293
#, no-c-format
msgid "Example: Kword Will Automatically Change Cases."
msgstr "Пример: Kword Автоматически Изменяет Регистр Текста."

#. Tag: guilabel
#: formatchar.docbook:298
#, fuzzy, no-c-format
msgid "Toggle case"
msgstr "Переключить регистр"

#. Tag: para
#: formatchar.docbook:299
#, fuzzy, no-c-format
msgid ""
"Converts all lowercase letters to uppercase <emphasis>and</emphasis> "
"converts all uppercase letters to lowercase."
msgstr ""
"Преобразует все строчные в прописные, <emphasis>а также</emphasis> все "
"прописные в строчные."

#. Tag: guilabel
#: formatchar.docbook:305
#, fuzzy, no-c-format
msgid "Sentence case"
msgstr "Как в предложениях"

#. Tag: para
#: formatchar.docbook:306
#, no-c-format
msgid "Capitalizes the first character after a period."
msgstr "Заменяет первую букву после точки на прописную."

#. Tag: para
#: formatchar.docbook:312
#, fuzzy, no-c-format
msgid ""
"When you have selected the desired options click <guibutton>OK</guibutton> "
"and &kword; will make the changes."
msgstr ""
"Если вас устраивают внесённые изменения, нажмите кнопку <guibutton>OK</"
"guibutton>."

#~ msgid ""
#~ "Clicking the button labeled <guibutton>Change Color...</guibutton> will "
#~ "allow you to change the color of the text using the <link linkend="
#~ "\"select-colors\">color selection dialog box</link>."
#~ msgstr ""
#~ "Нажав кнопку <guibutton>Изменить цвет...</guibutton>, вы вызовете <link "
#~ "linkend=\"select-colors\">диалоговое окно выбора цвета</link>, которое "
#~ "позволит вам изменить цвет текста."

#~ msgid ""
#~ "Clicking the button labeled <guibutton>Change Background Color...</"
#~ "guibutton> will allow you to change the color of the background using the "
#~ "<link linkend=\"select-colors\">color selection dialog box</link>."
#~ msgstr ""
#~ "Нажав кнопку <guibutton>Изменить цвет фона...</guibutton>, вы вызовете "
#~ "<link linkend=\"select-colors\">диалоговое окно выбора цвета</link>, "
#~ "которое позволит вам изменить цвет фона."

#, fuzzy
#~ msgid ""
#~ "Using the check boxes located below the preview box, you can select "
#~ "superscript or subscript."
#~ msgstr ""
#~ "С помощью переключателей в нижней части окна вы можете выбрать, будет ли "
#~ "текст выглядеть, как верхний или как нижний индекс."

#~ msgid "Background Color"
#~ msgstr "Цвет фона"

#~ msgid "Font"
#~ msgstr "Шрифт"
