# KDE3 -  kexi_database.pot Russian translation.
#
# Dimitriy Ryazantcev <DJm00n@rambler.ru>, 2006.
# Nick Shaforostoff <shafff@ukr.net>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kexi_database\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-02-11 07:20+0000\n"
"PO-Revision-Date: 2007-02-05 09:16+0200\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-KDE-DocBook-SVN-URL: trunk/koffice/doc/kexi/database.docbook\n"
"X-KDE-DocBook-SVN-Changed-Revision: 481839\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: database.docbook:6
#, no-c-format
msgid "Introduction to Databases"
msgstr "Введение в базы данных"

#. Tag: title
#: database.docbook:9
#, no-c-format
msgid "What Is a Database?"
msgstr "Что такое база данных?"

#. Tag: para
#: database.docbook:10
#, no-c-format
msgid ""
"You can define a database as a collection of data on one topic. It is "
"organised in a way allowing to easily browse the information, make changes "
"or add new items."
msgstr ""
"Базу данных можно определить как набор данных с одной тематикой, "
"организованных способом, позволяющим легко их просматривать, изменять и "
"добавлять новые."

#. Tag: para
#: database.docbook:15
#, no-c-format
msgid ""
"Look at this diagram for one of the above examples: a simple phone book."
msgstr "На схеме выше приведен пример базы данных: простая телефонная книга."

#. Tag: screeninfo
#: database.docbook:19
#, no-c-format
msgid "<screeninfo>A diagram of a phone number database</screeninfo>"
msgstr "<screeninfo>Схема базы данных телефонной книги</screeninfo>"

#. Tag: phrase
#: database.docbook:25
#, no-c-format
msgid "<phrase>A diagram of a phone number database</phrase>"
msgstr "<phrase>Схема базы данных телефонной книги</phrase>"

#. Tag: para
#: database.docbook:29
#, no-c-format
msgid ""
"The above picture shows a set of two contacts each of which is presented on "
"a separate card. It appears that such a card can constitute a single row in "
"a table:"
msgstr ""
"На рисунке выше показаны два контакта, каждый из которых размещён на "
"отдельной карточке. Для каждой карточки можно выделить по одной строке в "
"таблице:"

#. Tag: emphasis
#: database.docbook:35
#, no-c-format
msgid "<guilabel>Contacts</guilabel> table"
msgstr "Таблица <guilabel>Contacts</guilabel> "

#. Tag: guilabel
#: database.docbook:40
#, no-c-format
msgid "<guilabel>Name</guilabel>"
msgstr "<guilabel>Name (Имя)</guilabel>"

#. Tag: guilabel
#: database.docbook:41
#, no-c-format
msgid "Tel No."
msgstr "Tel No. (номер телефона)"

#. Tag: entry
#: database.docbook:44 database.docbook:290 database.docbook:361
#, no-c-format
msgid "Joan"
msgstr "Joan"

#. Tag: entry
#: database.docbook:45 database.docbook:136
#, no-c-format
msgid "699 23 43 12"
msgstr "699 23 43 12"

#. Tag: entry
#: database.docbook:48 database.docbook:297 database.docbook:369
#, no-c-format
msgid "Adam"
msgstr "Adam"

#. Tag: entry
#: database.docbook:49 database.docbook:141
#, no-c-format
msgid "711 19 77 21"
msgstr "711 19 77 21"

#. Tag: para
#: database.docbook:55
#, no-c-format
msgid ""
"<emphasis>Terms and definitions</emphasis>: A single data which constitutes "
"a part of a greater collection can be called a <firstterm>row</firstterm> or "
"more professionally a <firstterm>record</firstterm>. The collection is "
"normally called a <firstterm>table</firstterm>. Moreover, the most natural "
"name for the table is one describing the data it offers/stores which is "
"<guilabel>Contacts</guilabel>. Furthermore, each row in the table consists "
"of <firstterm>columns</firstterm> often also called <firstterm>fields</"
"firstterm>. In the table <guilabel>Contacts</guilabel> there are two columns "
"(fields): <guilabel>Name</guilabel> and <guilabel>Tel No.</guilabel>."
msgstr ""
"<emphasis>Термины и определения</emphasis>: <firstterm>Таблица</firstterm> - "
"это набор однородных данных, называемых <firstterm>строками</firstterm>, или "
"<firstterm>записями</firstterm> (более профессионально). Колонки в таблице - "
"<firstterm>поля</firstterm>. В таблице <guilabel>Contacts</guilabel> два "
"поля: <guilabel>Name</guilabel> (имя) и <guilabel>Tel No.</guilabel> (номер "
"телефона)."

#. Tag: para
#: database.docbook:68
#, no-c-format
msgid ""
"For simple uses a single table can make up a <firstterm>database</"
"firstterm>. Many people consider these two equivalent. As you will see, for "
"real databases we usually need more than one table."
msgstr ""
"В упрощённом варианте базу данных может образовывать только одна таблица. "
"Некоторые люди даже считают эти понятия эквивалентными. На практике вы "
"увидите, что для базы данных обычно требуется больше одной таблицы."

#. Tag: para
#: database.docbook:73
#, no-c-format
msgid ""
"To sum up, you have already got a simple database with one table "
"<guilabel>Contacts</guilabel>."
msgstr ""
"Итак, на данный момент вы создали простую базу данных, <guilabel>Contacts</"
"guilabel>, состоящую из одной таблицы."

#. Tag: title
#: database.docbook:81
#, no-c-format
msgid "Database and Spreadsheet"
msgstr "Базы данных и электронные таблицы"

#. Tag: para
#: database.docbook:82
#, no-c-format
msgid ""
"It is very likely that you have already used spreadsheet applications like "
"KSpread, OpenOffice.org Calc or Microsoft Excel. If so, you will probably "
"wonder: since both spreadsheets and databases have tables, why should I use "
"the latter?"
msgstr ""
"Вероятно, вы уже знакомы с программами для работы с электронными таблицами, "
"например, KSpread, OpenOffice.org Calc или Microsoft Excel.  Если да, то у "
"вас, наверное, возник вопрос: и электронные таблицы, и базы данных содержат "
"таблицы - зачем же нужны вторые?"

#. Tag: para
#: database.docbook:88
#, no-c-format
msgid ""
"While comparing spreadsheets and databases you may encounter the following "
"issues which you will later see in greater detail:"
msgstr ""
"При сравнении таблиц и баз данных необходимо учитывать следующие вопросы "
"(далее мы рассмотрим их поподробнее):"

#. Tag: link
#: database.docbook:92
#, no-c-format
msgid "<link>Referential data integrity</link>"
msgstr "<link>Целостность связей между элементами данных</link>"

#. Tag: link
#: database.docbook:94
#, no-c-format
msgid "<link>Data redundancy</link>"
msgstr "<link>Избыточность данных</link>"

#. Tag: link
#: database.docbook:96
#, no-c-format
msgid "<link>Data integrity and validity</link>"
msgstr "<link>Целостность и допустимость вводимой информации</link>"

#. Tag: link
#: database.docbook:98
#, no-c-format
msgid "<link>Limiting data view</link>"
msgstr "<link>Частичный вывод данных</link>"

#. Tag: link
#: database.docbook:99
#, no-c-format
msgid "<link>Performance and capacity</link>"
msgstr ""
"<link>Производительность и максимальный объём хранимой информации</link>"

#. Tag: link
#: database.docbook:101
#, no-c-format
msgid "Convenient data entry"
msgstr "Удобный ввод информации"

#. Tag: link
#: database.docbook:102
#, no-c-format
msgid "<link>Reports</link>"
msgstr "<link>Отчёты</link>"

#. Tag: link
#: database.docbook:103
#, no-c-format
msgid "<link>Programming</link>"
msgstr "<link>Программирование</link>"

#. Tag: link
#: database.docbook:104
#, no-c-format
msgid "<link>Multiuse</link>"
msgstr "<link>Параллельное использование несколькими людьми</link>"

#. Tag: link
#: database.docbook:105
#, no-c-format
msgid "<link>Security</link>"
msgstr "<link>Безопасность</link>"

#. Tag: title
#: database.docbook:109
#, no-c-format
msgid "How Is a Database Different From a Spreadsheet?"
msgstr "Чем базы данных отличаются от электронных таблиц?"

#. Tag: para
#: database.docbook:111
#, no-c-format
msgid ""
"Gradually exceeding the capacity of a mobile phone, expand your table "
"<guilabel>Contacts</guilabel> adding a column (field) <guilabel>Address</"
"guilabel>. Add more telephone numbers (office, home) for each person and add "
"surnames to names. To make it simpler we assume the following:"
msgstr ""
"Если позволяет память вашего мобильного телефона, вы можете дополнить "
"таблицу контактов колонками (полем) <guilabel>Address</guilabel> (адрес). "
"Можно также добавить другие телефонные номера (офис, домашний), поле "
"фамилии. Для упрощения предлагается следующее:"

#. Tag: para
#: database.docbook:119
#, no-c-format
msgid ""
"the table is limited to two people (obviously, there could be hundreds and "
"thousands of them in a real database)"
msgstr ""
"в таблице только два человека (что в настоящей базе их могут быть сотни и "
"тысячи)."

#. Tag: para
#: database.docbook:122
#, no-c-format
msgid "there are no two persons with the same name and surname"
msgstr "двух человек с одинаковыми именами и фамилиями нет"

#. Tag: emphasis
#: database.docbook:125
#, no-c-format
msgid "Contacts table"
msgstr "Таблица Contacts "

#. Tag: emphasis
#: database.docbook:130 database.docbook:207
#, no-c-format
msgid "Name and surname"
msgstr "Name and surname (Имя и фамилия)"

#. Tag: emphasis
#: database.docbook:131
#, no-c-format
msgid "<emphasis>Tel</emphasis>"
msgstr "<emphasis>Tel (Тел. номер)</emphasis>"

#. Tag: emphasis
#: database.docbook:132 database.docbook:208
#, no-c-format
msgid "Address"
msgstr "Address (Адрес)"

#. Tag: entry
#: database.docbook:135 database.docbook:145 database.docbook:211
#, no-c-format
msgid "Joan Smith"
msgstr "Joan Smith"

#. Tag: entry
#: database.docbook:137 database.docbook:212
#, no-c-format
msgid "Western Gate 1, Warsaw"
msgstr "Western Gate 1, Warsaw"

#. Tag: entry
#: database.docbook:140 database.docbook:215
#, no-c-format
msgid "Adam Willson"
msgstr "Adam Willson"

#. Tag: entry
#: database.docbook:142
#, no-c-format
msgid "London, Frogs Drive 5"
msgstr "London, Frogs Drive 5"

#. Tag: entry
#: database.docbook:146
#, no-c-format
msgid "110 98 98 00"
msgstr "110 98 98 00"

#. Tag: entry
#: database.docbook:147
#, no-c-format
msgid "Western Gate 1"
msgstr "Western Gate 1"

#. Tag: entry
#: database.docbook:150
#, no-c-format
msgid "Smith Joan"
msgstr "Smith Joan"

#. Tag: entry
#: database.docbook:151
#, no-c-format
msgid "312 43 42 22"
msgstr "312 43 42 22"

#. Tag: entry
#: database.docbook:152
#, no-c-format
msgid "Warsaw, Western Gate 1"
msgstr "Warsaw, Western Gate 1"

#. Tag: entry
#: database.docbook:155
#, no-c-format
msgid "ADAM Willson"
msgstr "ADAM Willson"

#. Tag: entry
#: database.docbook:156
#, no-c-format
msgid "231 83 02 04"
msgstr "231 83 02 04"

#. Tag: entry
#: database.docbook:157 database.docbook:216
#, no-c-format
msgid "Frogs Drive 5, London"
msgstr "Frogs Drive 5, London"

#. Tag: para
#: database.docbook:163
#, no-c-format
msgid ""
"Such a table can be made both in a spreadsheet and in a database. Using a "
"spreadsheet is very easy, of couse. What problems do we encounter at this "
"stage?"
msgstr ""
"Такую таблицу можно создать в виде электронной таблицы или базы данных. "
"Первой пользоваться очень просто. Какие же проблемы могут возникнуть? "

#. Tag: title
#: database.docbook:170
#, no-c-format
msgid "<title>Referential data integrity</title>"
msgstr "<title>Целостность связей между элементами данных</title>"

#. Tag: para
#: database.docbook:171
#, no-c-format
msgid ""
"Suppose you are using a spreadsheet and you need to change the address of at "
"least one person. You have a small problem: you often have to change the "
"address in many rows. For example, Joan takes three rows. A real problem "
"will arise if you forget to change one of the rows - the address asigned to "
"this person will be <emphasis>ambiguous</emphasis>, hence <emphasis>your "
"data loses integrity</emphasis>."
msgstr ""
"Предположим, вы используете электронные таблицы и вам нужно изменить адрес "
"как минимум одного человека. Вы столкнётесь с затруднением: придётся менять "
"адреса в нескольких колонках. Например, данные о Joan занимают три записи. "
"Настоящая проблема возникнет, если вы забудете изменить одну из строк - "
"адрес человека перестанет быть однозначным и ваша запись потеряет "
"целостность."

#. Tag: para
#: database.docbook:179
#, no-c-format
msgid ""
"Moreover there is no simple way of deleting a chosen person from the table "
"since you have to remember about deleting all rows releted to him or her."
msgstr ""
"Более того, не существует простого способа удалить выбранный контакт из "
"таблицы, поскольку нужно удалить все строки, относящиеся к данному человеку."

#. Tag: title
#: database.docbook:187
#, no-c-format
msgid "<title>Data redundancy</title>"
msgstr "<title>Избыточность данных</title>"

#. Tag: para
#: database.docbook:188
#, no-c-format
msgid ""
"This is directly connected to the previous problem. In fields <guilabel>Name "
"and surname</guilabel> and <guilabel>Address</guilabel> the same data is "
"entered many times. This is typical of a spreadsheets' ineffective way of "
"storing data because the database grows unnecessarily, thus requiring more "
"computer resources (larger size of data and slower access)."
msgstr ""
"Это вытекает из предыдущего пункта. В поля <guilabel>Name and surname</"
"guilabel> (имя/фамилия) и <guilabel>Address</guilabel> (адрес) несколько раз "
"вводится одна и та же информация. Поэтому хранить данные такого рода в "
"электронных таблицах неэффективно: неприемлемо увеличивается размер базы "
"данных и замедляется доступ к ней."

#. Tag: para
#: database.docbook:195
#, no-c-format
msgid ""
"How can you solve these problems with a database? You can split information "
"into smaller chunks by creating an additional table <emphasis>Persons</"
"emphasis> with only two columns: <guilabel>Name and surname</guilabel> and "
"<guilabel>Address</guilabel>:"
msgstr ""
"Как решаются подобные вопросы в базах данных? Во-первых, данные можно "
"разбить на части и ввести дополнительную таблицу в двумя колонками: "
"<guilabel>Name and surname</guilabel> (имя/фамилия) и <guilabel>Address</"
"guilabel> (адрес)."

#. Tag: emphasis
#: database.docbook:202
#, no-c-format
msgid "<guilabel>Persons</guilabel> table"
msgstr "Таблица <guilabel>persons</guilabel>"

#. Tag: para
#: database.docbook:222
#, no-c-format
msgid ""
"Each row in the table <guilabel>Persons</guilabel> corresponds to a "
"<emphasis>single person</emphasis>. Table <guilabel>Contacts</guilabel> is "
"from now on a relation to the table <guilabel>Persons</guilabel> ."
msgstr ""
"Каждая строка соответствует <emphasis>отдельному человеку</emphasis>. "
"Таблица <guilabel>Contacts</guilabel> связывается с <guilabel>Persons</"
"guilabel>."

#. Tag: title
#: database.docbook:232
#, no-c-format
msgid "<title>Data integrity and validity</title>"
msgstr "<title>Целостность и допустимость вводимой информации</title>"

#. Tag: para
#: database.docbook:233
#, no-c-format
msgid ""
"Note the way data is entered in the fields <guilabel>Name and surname</"
"guilabel> and <guilabel>Address</guilabel>. People entering data can be "
"fallible, sometimes even negligent. In our sample data we have both "
"different sequence of entering name and surname (Joan Smith and Smith Joan; "
"Adam and ADAM) and many more ways of entering the same address. Surely you "
"can think of many other ways."
msgstr ""
"Обратите внимание на способ заполнения полей <guilabel>Name and surname</"
"guilabel> (имя/фамилия) и <guilabel>Address</guilabel> (адрес). При вводе "
"люди могут допускать ошибки или небрежности. В нашем примере имя и фамилию "
"можно ввести в разном порядке, то же касается и адреса. Могут быть и другие "
"неоднозначности."

#. Tag: para
#: database.docbook:241
#, no-c-format
msgid ""
"The above problem shows that &eg; when searching the telephone number of a "
"person whose address is \"Western Gate 1, Warsaw\" you will not get a full "
"result. You will get only one row instead of three. Moreover You will also "
"not find all the telephone numbers searching for the value \"Joan Smith\" in "
"the field <guilabel>Name and surname</guilabel>, because \"Smith Joan\" will "
"not fit to \"Joan Smith\"."
msgstr ""
"Из-за данной особенности при поиске телефонных номеров по адресу \"Western "
"Gate 1, Warsaw\" вы в результате не получите полного результата. "

#. Tag: para
#: database.docbook:249
#, no-c-format
msgid ""
"How can you solve these problems using a database? You can do this by "
"changing the design of the table <guilabel>Persons</guilabel> by:"
msgstr ""
"Как такая проблема решается в базах данных? Вы можете изменить структуру "
"таблицы <guilabel>Persons</guilabel>:"

#. Tag: para
#: database.docbook:253
#, no-c-format
msgid ""
"<emphasis>Dividing data</emphasis> in the field <guilabel>Name and surname</"
"guilabel> into two separate fields: <guilabel>Name</guilabel> and "
"<guilabel>Surname</guilabel>."
msgstr ""
"<emphasis>Разделить</emphasis> поле <guilabel>Name and surname</guilabel> "
"(имя и фамилия) на два отдельных: поле <guilabel>Name</guilabel> (имя) и "
"поле <guilabel>Surname</guilabel> (фамилия)."

#. Tag: para
#: database.docbook:258
#, no-c-format
msgid ""
"<emphasis>Dividing data</emphasis> in the field <guilabel>Address</guilabel> "
"into three separate fields: <guilabel>Street</guilabel>, <guilabel>House "
"number</guilabel> and <guilabel>City</guilabel>."
msgstr "Разбить поле Адрес на три: Улица, Номер дома и Город."

#. Tag: para
#: database.docbook:263
#, no-c-format
msgid ""
"<emphasis>Guaranteeing data correctness:</emphasis> by ensuring that no "
"fields are empty, &eg; you must always enter house number."
msgstr ""
"Гарантировать корректность введенной информации можно, если не оставлять "
"поля незаполненными, например, вы всегда обязаны ввести номер дома."

#. Tag: para
#: database.docbook:269
#, no-c-format
msgid "A modified table looks something like this:"
msgstr "Модифицированная таблица будет выглядеть так:"

#. Tag: emphasis
#: database.docbook:273 database.docbook:348
#, no-c-format
msgid "Persons table"
msgstr "Таблица Persons"

#. Tag: emphasis
#: database.docbook:283 database.docbook:353
#, no-c-format
msgid "<emphasis>Name</emphasis>"
msgstr "<emphasis>Name (имя)</emphasis>"

#. Tag: emphasis
#: database.docbook:284 database.docbook:354
#, no-c-format
msgid "Surname"
msgstr "Surname (фамилия)"

#. Tag: emphasis
#: database.docbook:285 database.docbook:355
#, no-c-format
msgid "Street"
msgstr "Street (улица)"

#. Tag: emphasis
#: database.docbook:286 database.docbook:356
#, no-c-format
msgid "House number"
msgstr "House number (номер дома)"

#. Tag: emphasis
#: database.docbook:287 database.docbook:357
#, no-c-format
msgid "City"
msgstr "City (город)"

#. Tag: entry
#: database.docbook:291 database.docbook:362
#, no-c-format
msgid "Smith"
msgstr "Smith"

#. Tag: entry
#: database.docbook:292 database.docbook:363
#, no-c-format
msgid "Western Gate"
msgstr "Western Gate"

#. Tag: entry
#: database.docbook:293 database.docbook:364
#, no-c-format
msgid "<entry>1</entry>"
msgstr "<entry>1</entry>"

#. Tag: entry
#: database.docbook:294 database.docbook:365
#, no-c-format
msgid "Warsaw"
msgstr "Warsaw"

#. Tag: entry
#: database.docbook:298 database.docbook:370
#, no-c-format
msgid "Willson"
msgstr "Willson"

#. Tag: entry
#: database.docbook:299 database.docbook:371
#, no-c-format
msgid "Frogs Drive"
msgstr "Frogs Drive"

#. Tag: entry
#: database.docbook:300 database.docbook:372
#, no-c-format
msgid "<entry>5</entry>"
msgstr "<entry>5</entry>"

#. Tag: entry
#: database.docbook:301 database.docbook:373
#, no-c-format
msgid "London"
msgstr "London"

#. Tag: emphasis
#: database.docbook:304
#, no-c-format
msgid "Conditions"
msgstr "Условия"

#. Tag: entry
#: database.docbook:307 database.docbook:308 database.docbook:309
#: database.docbook:310 database.docbook:311
#, no-c-format
msgid "required field"
msgstr "необходимое поле"

#. Tag: para
#: database.docbook:317
#, no-c-format
msgid ""
"Thanks to introducing the condition <guilabel>required field</guilabel> we "
"can be sure that the entered data is complete. In case of other tables you "
"may of course allow omitting certain fields while entering data."
msgstr ""

#. Tag: title
#: database.docbook:326
#, no-c-format
msgid "<title>Limiting data view</title>"
msgstr "<title>Частичный вывод данных</title>"

#. Tag: para
#: database.docbook:327
#, no-c-format
msgid ""
"A spreadsheet displays all rows and columns of the table which is bothersome "
"in case of very large data sheets. You may of course filter and sort rows in "
"spreadsheets, however you must be extra careful while doing so. Spreadsheet "
"users are in risk of forgetting that their data view has been filtered what "
"can lead to mistakes. For example, while calculating sums you may think you "
"have 100 rows of data while in fact there are 20 rows more hidden."
msgstr ""
"Электронная таблица, в которой показаны сразу все строки и столбцы, выглядит "
"громоздко, особенно если она очень большая. Строки можно фильтровать и "
"сортировать, но делать это нужно очень осторожно. Пользователь заранее "
"должен помнить обо всех изменениях, иначе возникают ошибки. Например, вы "
"можете просуммировать 100 строк, забыв, что ещё 20 были спрятаны."

#. Tag: para
#: database.docbook:335
#, no-c-format
msgid ""
"If you want to work on a small subset of data, &eg; to send it for others to "
"edit, you can copy and paste it to another spreadsheet and after editing "
"paste the changed data back to the main spreadsheet. Such \"manual\" editing "
"may cause data loss or incorect calculations."
msgstr ""
"Если вы хотите работать только с частью данных, например, отправить кому-то "
"другому для редактирования, вы можете скопировать их в отдельный лист, а "
"после изменения поместить назад в исходную таблицу. Такое \"редактирование\" "
"может привести к потере данных или ошибкам в вычислениях."

#. Tag: para
#: database.docbook:338
#, no-c-format
msgid ""
"To limit the <emphasis>data view</emphasis> database applications offer "
"<emphasis>queries</emphasis>, <emphasis>forms</emphasis> and "
"<emphasis>reports</emphasis>."
msgstr ""

#. Tag: para
#: database.docbook:343
#, no-c-format
msgid ""
"A very practical way of limitting is the following extended version of the "
"previously described table <guilabel>Persons</guilabel>:"
msgstr ""

#. Tag: emphasis
#: database.docbook:358
#, no-c-format
msgid "Income"
msgstr "Income (доход)"

#. Tag: entry
#: database.docbook:366
#, no-c-format
msgid "2300"
msgstr "2300"

#. Tag: entry
#: database.docbook:374
#, no-c-format
msgid "1900"
msgstr "1900"

#. Tag: para
#: database.docbook:380
#, no-c-format
msgid ""
"Let's assume that the newly introduced column <guilabel>Income</guilabel> "
"contains confidential data. How can you share &eg; contact details of the "
"persons with your coworkers but without <emphasis>revealing their income</"
"emphasis>? It is possible if <emphasis>you share only a query and not the "
"whole table</emphasis>. The query could select all columns except for the "
"column <guilabel>Income</guilabel>. In database world such a query is often "
"known as a <guilabel>view</guilabel>."
msgstr ""

#. Tag: title
#: database.docbook:393
#, no-c-format
msgid "<title>Performance and capacity</title>"
msgstr ""

#. Tag: para
#: database.docbook:394
#, no-c-format
msgid ""
"Your computer is probably quite fast, however you will easily see that it "
"doesn't help with slow, large spreadsheets. Their low efficiency is first of "
"all due to lack of indexes accelertaing the process of data search "
"(databases do offer them). Moreover if you use things like system clipboard, "
"even copying data may become troublesome with time."
msgstr ""
"У вас может быть очень быстрый компьютер, но работа с достаточно большими "
"электронными таблицами всё равно будет медленной. Низкая скорость "
"обусловлена недостатком индексов, которые обеспечивают связь между "
"элементами и ускоряют процесс поиска данных (в базах данных они "
"предусматриваются автоматически). Если используется системный буфер обмена, "
"то даже копирование информации будет сопряжено с трудностями."

#. Tag: para
#: database.docbook:401
#, no-c-format
msgid ""
"Spreadsheets containing large data sets may take ages to open. A spreadsheet "
"loads lots of data to the computer's memory while opening. Most of the data "
"loaded are probably useless/unneccessary for you at the moment. Databases "
"unlike spreadsheets load data from computer storage only when needed."
msgstr ""

#. Tag: para
#: database.docbook:407
#, no-c-format
msgid ""
"In most cases you will not have to worry how the database stores its data. "
"This means that unlike spreadsheets, databases do not care about:"
msgstr ""

#. Tag: para
#: database.docbook:412
#, no-c-format
msgid ""
"The sequence of rows since you can order the rows according to your needs. "
"Moreover, you can view the same data in many views with different orders."
msgstr ""

#. Tag: para
#: database.docbook:416
#, no-c-format
msgid "The same goes for columns (fields) of the table."
msgstr ""

#. Tag: para
#: database.docbook:421
#, no-c-format
msgid ""
"Together with <link linkend=\"data-limiting\">Limiting data view</link> "
"described in the previous paragraph these qualities constitute the advantage "
"of databases."
msgstr ""

#. Tag: title
#: database.docbook:430
#, no-c-format
msgid "Data entry"
msgstr ""

#. Tag: para
#: database.docbook:431
#, no-c-format
msgid ""
"The latest editions of applications for creating spreadsheets enable you to "
"design data-entry forms. Such forms are most useful if your data cannot be "
"conveniently displayed in tabular view, &eg; if the text occupies too many "
"rows or if all the columns do not fit on the screen."
msgstr ""

#. Tag: para
#: database.docbook:437
#, no-c-format
msgid ""
"In this case the very way the spreadsheet works is problematic. Fields for "
"data entry are placed loosely within the spreadsheet and very often are not "
"secure against the user's (intentional or accidental) intervention."
msgstr ""

#. Tag: title
#: database.docbook:446
#, no-c-format
msgid "<title>Reports</title>"
msgstr "<title>Отчёты</title>"

#. Tag: para
#: database.docbook:447
#, no-c-format
msgid ""
"Databases enable grouping, limiting and summing up data in a form of a "
"<emphasis>report</emphasis>. Spreadsheets are usually printed in a form of "
"small tables without fully automatic control over page divisions and the "
"layout of fields."
msgstr ""

#. Tag: title
#: database.docbook:456
#, no-c-format
msgid "<title>Programming</title>"
msgstr "<title>Программирование</title>"

#. Tag: para
#: database.docbook:457
#, no-c-format
msgid ""
"Applications for creating databases often contain full programming "
"languages. Newer spreadsheets have this capability too, however calculations "
"come down to modifying the spreadsheet's fields and simple data copying, "
"regardless of the relevance and integrity rules mentioned in previous "
"paragraphs."
msgstr ""

#. Tag: para
#: database.docbook:463
#, no-c-format
msgid ""
"Data processing within a spreadsheet is usually done via a graphical user's "
"interface which may slow down the data processing speed. Databases are "
"capable of working in background, outside of graphical interfaces."
msgstr ""

#. Tag: title
#: database.docbook:471
#, no-c-format
msgid "<title>Multiuse</title>"
msgstr "<title>Параллельное использование несколькими людьми</title>"

#. Tag: para
#: database.docbook:472
#, no-c-format
msgid ""
"It is hard to imagine a multiuse of one spreadsheet. Even if it is "
"technically possible in the case of the latest applications, it requires a "
"lot of discipline, attention and knowledge from the users, and these cannot "
"be guaranteed."
msgstr ""

#. Tag: para
#: database.docbook:478
#, no-c-format
msgid ""
"A classical way to sharing data saved in a spreadsheet with other person is "
"to send a file as a whole (usually using e-mail) or providing a spreadsheet "
"file in a computer network. This way of work is ineffective for larger "
"groups of people - data that could be needed in a particular time may be "
"currently locked by another person."
msgstr ""

#. Tag: para
#: database.docbook:485
#, no-c-format
msgid ""
"On the other hand, databases have been designed mainly with multiuser access "
"in mind. Even for the simplest version locking at a particular table row's "
"level is possible, which enables easy sharing of table data."
msgstr ""

#. Tag: title
#: database.docbook:493
#, no-c-format
msgid "<title>Security</title>"
msgstr ""

#. Tag: para
#: database.docbook:494
#, no-c-format
msgid ""
"Securing a spreadsheet or its particular sections with a password is only "
"symbolic activity. After providing a spreadsheet file in a computer network, "
"every person being able to copy the file can try to break the password. It "
"is sometimes not so hard as the password is stored in the same file as the "
"spreadsheet."
msgstr ""

#. Tag: para
#: database.docbook:501
#, no-c-format
msgid ""
"Features for edit locking or copy locking of a spreadsheet (or its part) is "
"equally easy to break."
msgstr ""

#. Tag: para
#: database.docbook:505
#, no-c-format
msgid ""
"Databases (except these saved in a file instead of a server) do not need to "
"be available in a single file. You're accessing them using a computer "
"network, usually by providing a user name and a password. You are gaining "
"access only to these areas (tables, forms or even selected rows and columns) "
"which were assigned to you by setting appropriate access rights."
msgstr ""

#. Tag: para
#: database.docbook:512
#, no-c-format
msgid ""
"Access rights can affect ability of data editing or only data reading. If "
"any data is not avaliable to you, it will not be even sent to your computer, "
"so there is no possibility of making a copy of the data in such easy way as "
"in case of spreadsheet files."
msgstr ""

#. Tag: title
#: database.docbook:524
#, no-c-format
msgid "Database Design"
msgstr "Проектирование баз данных"

#. Tag: para
#: database.docbook:525
#, no-c-format
msgid ""
"Database design needs careful planning. Note that <guilabel>Contacts</"
"guilabel> table redesign proposed in section 1.2 can generate problems when "
"the table is filled with data. For example, renaming a field is a simple "
"task, but splitting the <guilabel>Address</guilabel> field into separate "
"fields requires careful and tedious work."
msgstr ""

#. Tag: para
#: database.docbook:532
#, no-c-format
msgid ""
"To avoid such situations, <emphasis>rethink your database project</emphasis> "
"before you create it in your computer, and before you and others will start "
"to use it. Thus, by investing some time initially, you will most probably "
"save your time on everyday use."
msgstr ""

#. Tag: title
#: database.docbook:541
#, no-c-format
msgid "Who Needs a Database?"
msgstr "Нужны ли мне базы данных?"

#. Tag: title
#: database.docbook:543
#, no-c-format
msgid "Stick to spreadsheets if:"
msgstr ""

#. Tag: para
#: database.docbook:544
#, no-c-format
msgid ""
"Your needs are limited and your data will never grow to large volumes (can "
"you actually forecast that now?)"
msgstr ""

#. Tag: para
#: database.docbook:548
#, no-c-format
msgid ""
"You are unable to acquire the methodology of database construction. You may "
"however consider either outsourcing this task to someone else or using "
"simpler tools."
msgstr ""

#. Tag: para
#: database.docbook:553
#, no-c-format
msgid ""
"You use complicated spreadsheets and you lack time or money to switch to "
"databases. Think or ask someone whether this does not lead down a blind "
"alley. Don't count on magical tools that would change your spreadsheet "
"(regardless how well made) into a database."
msgstr ""

#. Tag: title
#: database.docbook:562
#, no-c-format
msgid "Consider using databases if:"
msgstr ""

#. Tag: para
#: database.docbook:563
#, no-c-format
msgid "Your data collection expands every week."
msgstr ""

#. Tag: para
#: database.docbook:564
#, no-c-format
msgid ""
"You often create new spreadsheets, copy within these and you feel that this "
"work is getting more and more tedious. In this case the effort of switching "
"to databases easily pays off."
msgstr ""

#. Tag: para
#: database.docbook:569
#, no-c-format
msgid ""
"You create reports and statements for which the table view of a spreadsheet "
"is not suitable. You can then consider switch to using a database with form "
"views."
msgstr ""

#. Tag: title
#: database.docbook:578
#, no-c-format
msgid "Database Creation Software"
msgstr "Программы для создания баз данных"

#. Tag: para
#: database.docbook:579
#, no-c-format
msgid ""
"So far you have learnt the general characteristics of databases without "
"going into much detail about specific applications for designing them."
msgstr ""

#. Tag: para
#: database.docbook:583
#, no-c-format
msgid ""
"The first databases were built together with large mainframe computers in "
"the 60s, &eg; IBM System/360. Those were not the days of PCs, therefore "
"these databases required a highly specialized personnel. Although the old "
"computers' hardware was unreliable, they were immeasurably slower and had "
"less storage capacity, one feature of databases still remains most "
"attractive: the data access by many users through a network."
msgstr ""

#. Tag: para
#: database.docbook:591
#, no-c-format
msgid ""
"In the 70s scientists formed the theory of relational databases (terms like: "
"<firstterm>table</firstterm>, <firstterm>record</firstterm>, "
"<firstterm>column (field)</firstterm> and <firstterm>relationality</"
"firstterm> and many others). On the basis of this theory IBM DB2 and Oracle "
"databases were created, which have been developed and used till today. In "
"the late 70s the first PCs were constructed. Their users could (gradually) "
"utilize many types of applications, including those for database "
"construction."
msgstr ""

#. Tag: para
#: database.docbook:601
#, no-c-format
msgid ""
"When it comes to large databases in companies, the situation hasn't changed: "
"they still require powerful computers or computer complexes called "
"<firstterm>clusters</firstterm>. This goes, however, beyond the topic of "
"this manual."
msgstr ""

#. Tag: para
#: database.docbook:607
#, no-c-format
msgid ""
"In the area of \"accessible\" databases with graphic user interface for PCs "
"you can choose from the following:"
msgstr ""

#. Tag: para
#: database.docbook:613
#, no-c-format
msgid ""
"<ulink url=\"http://www.dbase.com/\">DBase</ulink> - a tool for databases "
"operation for DOS popular in the 80s. Files in DBase format are still used "
"in some specific cases due to their simplicity."
msgstr ""

#. Tag: para
#: database.docbook:618
#, no-c-format
msgid ""
"<ulink url=\"http://msdn.microsoft.com/vfoxpro/productinfo/overview/"
"\">FoxPro</ulink> - an application similar to DBase (early 90s). After being "
"taken over by Microsoft, graphic user interfaces were introduced and "
"therefore it is used for creating databases on PCs. This product is still "
"offered, though seems a bit obsolete."
msgstr ""

#. Tag: para
#: database.docbook:625
#, no-c-format
msgid ""
"<ulink url=\"http://office.microsoft.com/access/\">Microsoft Access</ulink> "
"- an application for databases (data and graphic interface design) with many "
"simplifications, therefore suitable for beginners, designed in the late 80s, "
"based on 16-Bit Architecture. This product is offered and widely used till "
"today, especially by small companies, where efficiency and multiuser "
"requirements are not very demanding."
msgstr ""

#. Tag: para
#: database.docbook:632
#, no-c-format
msgid ""
"<ulink url=\"http://www.filemaker.com/\">FileMaker</ulink> - popular "
"application similar to MS Access in simplicity, operating on Windows and "
"Macintosh platforms, offered since 1985."
msgstr ""

#. Tag: para
#: database.docbook:637
#, no-c-format
msgid ""
"<ulink url=\"http://www.kexi.pl/\">&kexi;</ulink> - a multiplatform "
"application (Unix/Linux, Windows, Mac OS X) designed in 2003, developed "
"according to OpenSource principles, part of the global <ulink url=\"http://"
"www.kde.org/\">K Desktop Environment</ulink> project, &ie; graphic "
"environment for Unix/Linux systems. A significant contributor to &kexi;'s "
"development is the OpenOffice Poland company."
msgstr ""
