# translation of kmousetool.po into Russian
# Translation of kmousetool.po into Russian
# Copyright (C) 2003 KDE Russian translation team.
#
# Gregory Mokhin <mok@kde.ru>, 2003, 2005.
# Yuliya Poyarkova <ypoyarko@brisbane.redhat.com>, 2006.
# Nick Shaforostoff <shafff@ukr.net>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kmousetool\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:25+0000\n"
"PO-Revision-Date: 2006-12-14 12:20+0300\n"
"Last-Translator: Andrey Cherepanov <sibskull@mail.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: index.docbook:13
#, no-c-format
msgid "The &kmousetool; Handbook"
msgstr "Руководство &kmousetool;"

#. Tag: author
#: index.docbook:16
#, no-c-format
msgid "<firstname>Jeff</firstname> <surname>Roush</surname>"
msgstr "<firstname>Jeff</firstname> <surname>Roush</surname>"

#. Tag: email
#: index.docbook:21
#, no-c-format
msgid "jeff@kmousetool.com"
msgstr "jeff@kmousetool.com"

#. Tag: trans_comment
#: index.docbook:24
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Николай</firstname> "
"<surname>Шафоростов</surname><affiliation><address><email>shaforostoff@gmail."
"com</email></address></affiliation><contrib>Перевод на русский</contrib></"
"othercredit>"

#. Tag: holder
#: index.docbook:30
#, no-c-format
msgid "Jeff Roush"
msgstr "Jeff Roush"

#. Tag: para
#: index.docbook:39
#, no-c-format
msgid ""
"&kmousetool; clicks the mouse whenever the mouse cursor pauses briefly. It "
"was designed to help those with repetitive strain injuries, for whom "
"pressing buttons hurts."
msgstr ""
"&kmousetool; имитирует нажатие кнопки мыши при остановке курсора на объекте. "
"Программа служит для того, чтобы помочь людям, которым трудно нажимать на "
"кнопки мыши."

#. Tag: keyword
#: index.docbook:47
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#: index.docbook:48
#, no-c-format
msgid "kdeutils"
msgstr "kdeutils"

#. Tag: keyword
#: index.docbook:49
#, no-c-format
msgid "KMouseTool"
msgstr "KMouseTool"

#. Tag: keyword
#: index.docbook:50
#, no-c-format
msgid "ergonomic"
msgstr "эргономичность"

#. Tag: keyword
#: index.docbook:51
#, no-c-format
msgid "tendonitis"
msgstr "тендинит"

#. Tag: keyword
#: index.docbook:52
#, no-c-format
msgid "carpal tunnel syndrome"
msgstr "кистевой туннельный синдром"

#. Tag: title
#: index.docbook:58
#, no-c-format
msgid "Introduction"
msgstr "Введение"

#. Tag: para
#: index.docbook:60
#, no-c-format
msgid ""
"&kmousetool; clicks the mouse whenever the mouse cursor pauses briefly. It "
"was designed to help those with repetitive strain injuries, for whom "
"pressing buttons hurts. It can also drag the mouse, although this takes a "
"bit more practice."
msgstr ""
"&kmousetool; имитирует нажатие кнопки мыши при остановке курсора на объекте. "
"Программа служит тому, чтобы помочь людям, которым трудно нажимать на кнопки "
"мыши. Также возможно выполнение перемещения мыши, что, в свою очередь, "
"требует практики."

#. Tag: para
#: index.docbook:66
#, no-c-format
msgid ""
"&kmousetool; can eliminate the pain caused by clicking the mouse, and helps "
"many people to use the computer without pain. Unfortunately, some pain can "
"also be caused by simply moving the mouse around the table. If you are "
"experiencing computer-related pain and haven't yet seen a doctor, please do "
"so."
msgstr ""
"&kmousetool; может устранить проблемы, связанные с болью при нажатии на "
"кнопки мыши. К несчастью, боль может приносить даже и перемещение мыши. Если "
"вы испытываете боли, связанные с работой за компьютером, пожалуйста, "
"обратитесь к врачу."

#. Tag: para
#: index.docbook:72
#, no-c-format
msgid ""
"Just to make it absolutely clear: while &kmousetool; can help reduce the "
"pain caused by <emphasis>clicking</emphasis> the mouse, it cannot help any "
"pain caused by <emphasis>moving</emphasis> the mouse. And, like all "
"ergonomic tools, your mileage may vary."
msgstr ""
"Итак, &kmousetool; может уменьшить боль только при <emphasis>нажатии</"
"emphasis> кнопок мыши, но не при <emphasis>перемещении</emphasis>. Как со "
"всеми эргономическими утилитами, степень эффекта может быть разной."

#. Tag: title
#: index.docbook:83
#, no-c-format
msgid "Using &kmousetool;"
msgstr "Использование &kmousetool;"

#. Tag: para
#: index.docbook:84
#, no-c-format
msgid ""
"Using &kmousetool; is simple: &kmousetool; watches as you move the mouse, "
"and when you pause briefly, it clicks."
msgstr ""
"Использовать &kmousetool; очень просто: &kmousetool; следит за движениями "
"курсора, и при его остановке имитирует нажатие."

#. Tag: para
#: index.docbook:89
#, no-c-format
msgid "&kmousetool;'s Smart Drag mode allows you to drag the mouse."
msgstr "В режиме перетаскивания &kmousetool; позволяет вам передвигать курсор."

#. Tag: para
#: index.docbook:93
#, no-c-format
msgid ""
"When Smart Drag is enabled, &kmousetool; pauses after it clicks down; if you "
"move the mouse, it waits until you stop moving before it clicks up. This "
"way, you can both click and drag the mouse. Smart Drag takes a bit more "
"practice, but becomes natural once you get used to it."
msgstr ""
"При включённом режиме перетаскивания &kmousetool; \"зажимает\" кнопку мыши, "
"и вы можете переместить объект. (т.н. Drag'n'Drop); а по окончанию "
"перемещения \"отпускает\" кнопку мыши. Вам придётся немного потренироваться, "
"чтобы освоить эти действия."

#. Tag: title
#: index.docbook:104
#, no-c-format
msgid "Command Reference"
msgstr "Справочник по командам"

#. Tag: title
#: index.docbook:107
#, no-c-format
msgid "The Main &kmousetool; window"
msgstr "Главное окно &kmousetool;"

#. Tag: para
#: index.docbook:109
#, no-c-format
msgid "&kmousetool;'s options are accessed mostly via the mouse."
msgstr "Опции &kmousetool; указываются преимущественно мышью."

#. Tag: guibutton
#: index.docbook:115
#, no-c-format
msgid "Start"
msgstr "Запуск"

#. Tag: para
#: index.docbook:118
#, no-c-format
msgid "Starts (or stops) &kmousetool;."
msgstr "Запускает или останавливает &kmousetool;."

#. Tag: guilabel
#: index.docbook:122
#, no-c-format
msgid "Minimum movement:"
msgstr "Минимальный пробег:"

#. Tag: para
#: index.docbook:124
#, no-c-format
msgid ""
"The minimum movement in pixels before &kmousetool; will attempt to click the "
"mouse."
msgstr "Количество пикселов, которые должна пройти мышь до щелчка."

#. Tag: guilabel
#: index.docbook:130
#, no-c-format
msgid "Dwell time (1/10 sec):"
msgstr "Задержка (в 1/10 с):"

#. Tag: para
#: index.docbook:133
#, no-c-format
msgid ""
"The time the mouse has to pause before &kmousetool; clicks. Try increasing "
"this time if it is hard getting used to &kmousetool;"
msgstr ""
"Пауза перед нажатием кнопки. Увеличьте это значение, если &kmousetool; "
"трудно использовать."

#. Tag: guilabel
#: index.docbook:139
#, no-c-format
msgid "Smart drag"
msgstr "Умное перетаскивание"

#. Tag: para
#: index.docbook:142
#, no-c-format
msgid ""
"Enables or disables Smart Drag. Disabled use is easier, so this is the "
"default."
msgstr "Включает или выключает режим \"умного перетаскивания\"."

#. Tag: para
#: index.docbook:143
#, no-c-format
msgid ""
"If you enable <guilabel>Smart drag</guilabel> the <guilabel>Drag time (1/10 "
"sec):</guilabel> field becomes available. This is the time &kmousetool; "
"waits, after it clicks down, before it clicks back up if you don't move the "
"mouse."
msgstr ""
"При включенном режиме перетаскивания становится доступной опция "
"<guilabel>Время перетаскивания (в 1/10 с)</guilabel>. Этот параметр отвечает "
"за время, проходящее между регистрацией нажатия клавиши мыши и её "
"программным отпусканием в случае, если мышь не пермещалась."

#. Tag: guilabel
#: index.docbook:151
#, no-c-format
msgid "Enable strokes"
msgstr "Включить росчерки"

#. Tag: para
#: index.docbook:154
#, no-c-format
msgid ""
"&kmousetool; now supports strokes. When you enable strokes, a slow move to "
"the right and back, followed by a pause, will generate a <mousebutton>right</"
"mousebutton> click. A slow move left and back will generate a double click. "
"(Strokes are specified in <filename>$<envar>KDEHOME</envar>/share/config/"
"kmousetool_strokes.txt</filename>. This file is generated by &kmousetool; "
"the first time it is run, but can be modified afterwards.)"
msgstr ""
"&kmousetool; поддерживает \"росчерки\" (англ.: mouse gestures). Медленное "
"перемещение вправо и возврат назад, за которым следует пауза, имитирует "
"нажатие правой кнопки. Медленное перемещение влево и возврат назад - двойной "
"щелчок. Эти параметры задаются и могут быть отредактированы в файле "
"<filename>$<envar>KDEHOME</envar>/share/config/kmousetool_strokes.txt</"
"filename>, который &kmousetool; создастпри первом запуске."

#. Tag: guilabel
#: index.docbook:165
#, no-c-format
msgid "Audible click"
msgstr "Озвучивание нажатия"

#. Tag: para
#: index.docbook:168
#, no-c-format
msgid ""
"Plays a sound when &kmousetool; clicks down. This helps, especially with "
"Smart Drag."
msgstr ""
"Воспроизводит звук при нажатии кнопки. Это особенно полезно в режиме "
"перетаскивания."

#. Tag: para
#: index.docbook:172
#, no-c-format
msgid ""
"If the sound seems delayed, you can have &kde; speed it up. To do this open "
"the &kcontrolcenter;, click on <guimenuitem>Sound &amp; Multimedia</"
"guimenuitem>, then on <guimenuitem>Sound System</guimenuitem>, and then "
"select the <guilabel>Sound I/O</guilabel> tab. At the bottom of the tab, "
"there is an <guilabel>Audio buffer size (response time)</guilabel> "
"adjustment; slide this towards the lower number to speed up the audio "
"response time."
msgstr ""
"Если звук воспроизводится с опозданием, откройте &kcontrolcenter;, на "
"вкладке <guimenuitem>Звук</guimenuitem>, <guimenuitem>Звуковая система</"
"guimenuitem> - <guilabel>Ввод/Вывод звука</guilabel> выставите меньшее "
"значение <guilabel>Размера аудиобуфера (время отклика)</guilabel>."

#. Tag: guilabel
#: index.docbook:185
#, no-c-format
msgid "Start with KDE"
msgstr "Автозапуск с KDE"

#. Tag: para
#: index.docbook:188
#, no-c-format
msgid ""
"When this is enabled, &kmousetool; will start each time &kde; starts. At the "
"moment, this only works under &kde;. Under GNOME or other Window Managers, "
"see the documentation for the Window Manager itself to see how to start a "
"program automatically when you start the windowing system."
msgstr ""
"Запускать &kmousetool; при старте &kde;. Не работает в GNOME или других "
"менеджерах окон, для которых вы должны воспользоваться другими средствами "
"для автозапуска программы."

#. Tag: guibutton
#: index.docbook:197
#, no-c-format
msgid "Defaults"
msgstr "По умолчанию"

#. Tag: para
#: index.docbook:199
#, no-c-format
msgid "Reset all settings to their defaults."
msgstr "Восстановить первоначальные значения всех параметров."

#. Tag: guibutton
#: index.docbook:203
#, no-c-format
msgid "Reset"
msgstr "Сброс"

#. Tag: para
#: index.docbook:205
#, no-c-format
msgid ""
"Reset all settings to their state when you opened the dialog, or, if you "
"have already saved a setting with the <guibutton>Apply</guibutton> button, "
"reset all settings to the state when you last pressed <guibutton>Apply</"
"guibutton>"
msgstr ""
"Сбросить все настройки в то состояние, в котором они были в начале диалога, "
"или на момент последнего нажатия кнопки \"Применить\"."

#. Tag: guibutton
#: index.docbook:212
#, no-c-format
msgid "Apply"
msgstr "Применить"

#. Tag: para
#: index.docbook:215
#, no-c-format
msgid "After changing any settings, you must click this button."
msgstr "Нажмите для сохранения изменений настроек."

#. Tag: guibutton
#: index.docbook:219
#, no-c-format
msgid "Help"
msgstr "Справка"

#. Tag: para
#: index.docbook:221
#, no-c-format
msgid "Opens the User manual (this document)."
msgstr "Открывает данное руководство пользователя."

#. Tag: guibutton
#: index.docbook:225
#, no-c-format
msgid "Close"
msgstr "Закрыть"

#. Tag: para
#: index.docbook:227
#, no-c-format
msgid "Close the dialog without saving any settings."
msgstr "Закрыть диалог без сохранения изменений."

#. Tag: guibutton
#: index.docbook:231
#, no-c-format
msgid "Quit"
msgstr "Выход"

#. Tag: para
#: index.docbook:233
#, no-c-format
msgid "Quit &kmousetool;"
msgstr "Выход из &kmousetool;"

#. Tag: title
#: index.docbook:242
#, no-c-format
msgid "Tips"
msgstr "Советы"

#. Tag: para
#: index.docbook:244
#, no-c-format
msgid "These tips can help shorten &kmousetool;'s learning curve a bit."
msgstr "Советы помогут изучить &kmousetool; быстрее."

#. Tag: para
#: index.docbook:248
#, no-c-format
msgid ""
"You can modify the time delays &kmousetool; waits, for both clicking and for "
"dragging."
msgstr "Вы можете изменить величины пауз перед нажатием и перемещением."

#. Tag: para
#: index.docbook:250
#, no-c-format
msgid ""
"It's best to practice clicking with &kmousetool; using its defaults first. "
"You may especially want to leave Smart Drag disabled at first. Then, once "
"you are comfortable clicking, move on to practicing Smart Drag."
msgstr ""
"Лучше сначала попрактиковаться с &kmousetool; используя значения по "
"умолчанию. После приобретения навыков вы можете включить режим "
"перетаскивания."

#. Tag: para
#: index.docbook:254
#, no-c-format
msgid "When using Smart Drag, it may help to enable the Audible Click"
msgstr "В режиме перетаскивания может помочь звуковой эффект."

#. Tag: para
#: index.docbook:256
#, no-c-format
msgid ""
"When using the Audible Click, you may need to speed up &kde;'s audio "
"response."
msgstr ""
"При использовании звукового эффекта вам может понадобится изменить настройки "
"звука &kde;."

#. Tag: title
#: index.docbook:264
#, no-c-format
msgid "Questions and Answers"
msgstr "Вопросы и ответы"

#. Tag: chapter
#: index.docbook:264
#, no-c-format
msgid "&reporting.bugs; &updating.documentation;"
msgstr "&reporting.bugs; &updating.documentation;"

#. Tag: para
#: index.docbook:272
#, no-c-format
msgid "Help! &kmousetool; keeps dropping spurious clicks all over the screen!"
msgstr "Помогите! &kmousetool; постоянно создаёт ненужные щелчки!"

#. Tag: para
#: index.docbook:275
#, no-c-format
msgid ""
"When you are using &kmousetool;, you do have to learn new mousing habits."
msgstr "Вы должны овладеть новыми навыками работы с мышью."

#. Tag: para
#: index.docbook:278
#, no-c-format
msgid ""
"First, keep in mind where it's safe to click &mdash; clicking on gray parts "
"of windows or non-link areas of web browsers won't hurt anything, so you can "
"safely rest the mouse over those areas until you need it."
msgstr ""
"Прежде всего, помните, что щелчок на серой части окна или пустом месте веб-"
"страницы ни к чему плохому не приводит, так что упражняйтесь в этих частях "
"окон."

#. Tag: para
#: index.docbook:284
#, no-c-format
msgid ""
"Second, you need to know where you're going to click before you move the "
"mouse. Most of the time, when we know we're going to use the mouse, most of "
"us grab the mouse and start moving it around randomly until we figure out "
"where we want to click. With &kmousetool;, you need to know where you're "
"going to click before you move the mouse in the first place. It is also a "
"good idea to keep your hands off the mouse when you're not using it."
msgstr ""
"Далее, вы должны определиться, где нужно нажать перед перемещением мыши. "
"Большинство людей беспорядочно двигают мышь, пока не попадут туда, куда надо "
"- эта программа требует, чтобы вы четко осознавали, где вы хотите щёлкнуть "
"мышью. Также рекомендуется убирать руки с мыши, если вы её не используете."

#. Tag: para
#: index.docbook:297
#, no-c-format
msgid "My mouse sticks when I move it. Can I still use &kmousetool;?"
msgstr ""
"Моя мышь тормозит, когда я её перемещаю. Могу ли я использовать &kmousetool;?"

#. Tag: para
#: index.docbook:300
#, no-c-format
msgid ""
"Yes. If your mouse sticks, it means you need to clean your mouse. This is "
"usually easy to do; the trick is that you have to clean not just the mouse's "
"ball, but the rollers that the ball rolls against inside the mouse."
msgstr ""
"Да. если вы почистите её (не только сам шарик, но и ролики внутри мыши)."

#. Tag: para
#: index.docbook:303
#, no-c-format
msgid ""
"First, you have to remove the ball. The method for doing this varies from "
"mouse to mouse, but it's usually pretty clear how to do this if you look at "
"the bottom of the mouse."
msgstr ""
"Сначала разберите мышь и выньте шарик. Мыши бывают разные, но обычно сразу "
"ясно, как это сделать."

#. Tag: para
#: index.docbook:306
#, no-c-format
msgid ""
"Once you've removed the ball, you should be able to see the rollers on the "
"sides of the hole that contained the ball. The dirt and grime on them can be "
"easily scraped off with a flat blade or with a fingernail."
msgstr ""
"Внутри будут видны ролики, и грязь, налипшую на них, легко можно отскоблить "
"лезвием или ногтем."

#. Tag: para
#: index.docbook:314
#, no-c-format
msgid "&kmousetool; clicks down and never clicks up. What's happening?"
msgstr "&kmousetool; зажимает кнопку мыши и не отпускает её. Что случилось?"

#. Tag: para
#: index.docbook:317
#, no-c-format
msgid ""
"In some rare situations, the system suspends &kmousetool; when a drag "
"starts; if you are using Smart Drag, this means thar &kmousetool; can never "
"finish the drag by clicking up."
msgstr ""
"Иногда &kmousetool; зависает. Это может быть на некоторых системах. "
"Отключите режим перетаскивания."

#. Tag: para
#: index.docbook:320
#, no-c-format
msgid ""
"This happens when you use &kmousetool; under a non &kde; window manager and "
"then use it to move a window."
msgstr ""
"Это может случиться, например, при использовании &kmousetool; с окнами "
"приложений, не входящих в &kde;."

#. Tag: para
#: index.docbook:324
#, no-c-format
msgid ""
"The solution is to simply click manually; this resets &kmousetool;, and you "
"can then continue to work normally again."
msgstr ""
"Выход из этой ситуации - один раз щёлкнуть вручную. &kmousetool; "
"переключится и будет работать снова."

#. Tag: para
#: index.docbook:331
#, no-c-format
msgid "&kmousetool; moves the cursor when I am trying to type."
msgstr "&kmousetool; перемещает курсор, когда я пытаюсь печатать."

#. Tag: para
#: index.docbook:335
#, no-c-format
msgid ""
"Problem: When you're typing text, &kmousetool; keeps dropping clicks and "
"moving the cursor away from where you want to type."
msgstr ""
"При вводе текста &kmousetool; продолжает щёлкать и отводит курсор от текста."

#. Tag: para
#: index.docbook:337
#, no-c-format
msgid ""
"Solution: Use the mouse to place the cursor where you want it, but when "
"you're ready to type, park the mouse on a neutral area on the screen. Then, "
"if you bump the mouse, or if it moves a few pixels, any random clicks that "
"it sends won't matter. Safe places to park the mouse include the gray areas "
"around toolbars and menus, and on the title bars of windows."
msgstr ""
"Отведите курсор от текстового поля перед вводом в безопасные места - в серые "
"зоны панели инструментов или на заголовки окон."

#. Tag: para
#: index.docbook:347
#, no-c-format
msgid ""
"I'm trying to use Smart Drag, but I cannot get the hang of using it to edit "
"text."
msgstr ""
"Я пытаюсь использовать режим перетаскивания, но не могу понять, как с его "
"помощью редактировать текст."

#. Tag: para
#: index.docbook:350
#, no-c-format
msgid ""
"Smart Drag is an advanced feature of &kmousetool;, and takes some getting "
"used to. It is very possible to use it to select text, to cut and paste, and "
"use it in almost any situation where you would normally want to drag the "
"mouse. But you probably won't be able to do this at first. Here are some "
"suggestions:"
msgstr ""
"Для овладения навыками работы с режимом перетаскивания требуется время. Вот "
"несколько советов:"

#. Tag: para
#: index.docbook:357
#, no-c-format
msgid ""
"Don't use Smart Drag while you are first learning to use &kmousetool;. "
"Instead, to select text, click at one end of the text, and hold down the "
"shift key while you click at the other end of the text."
msgstr ""
"Не используйте режим перетаскивания сразу. Вы можете выделять текст с "
"помощью клавиатуры, зажав клавишу Shift и перемещая в сторону, или щёлкнув "
"один раз в каком-то месте и потом второй раз с нажатой клавишей Shift в "
"конце выделяемого блока."

#. Tag: para
#: index.docbook:361
#, no-c-format
msgid ""
"Use the Audible Click. This will play a <quote>click</quote> sound when "
"&kmousetool; clicks down, and this will tell you when the Smart Drag delay "
"is beginning. With practice, you can use Smart Drag without the Audible "
"Click, but it does take a fair amount of practice."
msgstr ""
"Используйте звуковой эффект. Это будет воспроизводить звук при нажатии "
"кнопки мыши с помощью &kmousetool;."

#. Tag: para
#: index.docbook:365
#, no-c-format
msgid "Know where you're going to click the mouse before you begin moving it."
msgstr "Вы должны знать, где хотите нажать перед началом перетаскивания."

#. Tag: title
#: index.docbook:377
#, no-c-format
msgid "Credits and License"
msgstr "Авторские права и лицензирование"

#. Tag: para
#: index.docbook:379
#, no-c-format
msgid "&kmousetool;"
msgstr "&kmousetool;"

#. Tag: para
#: index.docbook:382
#, no-c-format
msgid ""
"Program copyright 2002 Jeff Roush <email>jeff@mousetool.com</email> and 2003 "
"Gunnar Schmi Dt <email>gunnar@schmi-dt.de</email>"
msgstr ""
"Программа - &copy; 2002 Джеф Рош (Jeff Roush) <email>jeff@mousetool.com</"
"email> и 2003 Гуннар Шми Дт (Gunnar Schmi Dt)  <email>gunnar@schmi-dt.de</"
"email>"

#. Tag: para
#: index.docbook:386
#, no-c-format
msgid "Contributors:"
msgstr "Участники:"

#. Tag: para
#: index.docbook:389
#, no-c-format
msgid "Jeff Roush <email>jeff@mousetool.com</email>"
msgstr "Джеф Рош (Jeff Roush) <email>jeff@mousetool.com</email>"

#. Tag: para
#: index.docbook:390
#, no-c-format
msgid "Gunnar Schmi Dt<email>gunnar@schmi-dt.de</email>"
msgstr "Гуннар Шми Дт (Gunnar Schmi Dt) <email>gunnar@schmi-dt.de</email>"

#. Tag: para
#: index.docbook:391
#, no-c-format
msgid "Olaf Schmidt <email>ojschmidt@kde.org</email>"
msgstr "Олаф Шмидт (Olaf Schmidt) <email>ojschmidt@kde.org</email>"

#. Tag: para
#: index.docbook:395
#, no-c-format
msgid ""
"Documentation copyright 2002 Jeff Roush<email>jeff@mousetool.com</email>"
msgstr ""
"Документация - &copy; 2002 Джеф Рош (Jeff Roush) <email>jeff@mousetool.com</"
"email>"

#. Tag: trans_comment
#: index.docbook:399
#, no-c-format
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Перевод на русский - Николай Шафоростов <email>shaforostoff@gmail.com</"
"email></para>"

#. Tag: chapter
#: index.docbook:399
#, no-c-format
msgid "&underFDL; &underGPL;"
msgstr "&underFDL; &underGPL;"

#. Tag: title
#: index.docbook:406
#, no-c-format
msgid "Installation"
msgstr "Установка"

#. Tag: title
#: index.docbook:409
#, no-c-format
msgid "How to obtain &kmousetool;"
msgstr "Как получить &kmousetool;"

#. Tag: sect1
#: index.docbook:409
#, no-c-format
msgid "&install.intro.documentation;"
msgstr "&install.intro.documentation;"

#. Tag: title
#: index.docbook:416
#, no-c-format
msgid "Requirements"
msgstr "Требования"

#. Tag: para
#: index.docbook:418
#, no-c-format
msgid ""
"In order to successfully use &kappname;, you need the XTest extension "
"compiled as part of your &X-Window;. This should already be installed on "
"your system; very few systems won't have it."
msgstr ""
"Для использования &kappname;, вам понадобится расширение XTest, входящее в "
"состав &X-Window;. Оно установлено практически на всех системах."

#. Tag: para
#: index.docbook:423
#, no-c-format
msgid ""
"In order to use the Audible Click feature, you will have to have a sound "
"card and speakers, and you will have to have configured the sound card "
"correctly."
msgstr ""
"Для использования звукового эффекта вы должны иметь настроенную звуковую "
"карту."

#. Tag: title
#: index.docbook:431
#, no-c-format
msgid "Compilation and Installation"
msgstr "Сборка и установка"

#. Tag: sect1
#: index.docbook:431
#, no-c-format
msgid "&install.compile.documentation;"
msgstr "&install.compile.documentation;"
