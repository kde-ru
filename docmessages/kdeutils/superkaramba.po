# translation of superkaramba.po to 
# translation of superkaramba.po to Russian
# Dimitriy Ryazantcev <DJm00n@rambler.ru>, 2005.
# Nick Shaforostoff <shafff@ukr.net>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: superkaramba\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:23+0000\n"
"PO-Revision-Date: 2005-08-21 12:00+0300\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: index.docbook:13
#, no-c-format
msgid "The &superkaramba; Handbook"
msgstr "Руководство &superkaramba;"

#. Tag: author
#: index.docbook:15
#, no-c-format
msgid ""
"<personname><firstname>Hans</firstname> <surname>Karlsson</surname></"
"personname> <email>karlsson.h@home.se</email>"
msgstr ""
"<personname><firstname>Hans</firstname> <surname>Karlsson</surname></"
"personname> <email>karlsson.h@home.se</email>"

#. Tag: trans_comment
#: index.docbook:20
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Дмитрий</"
"firstname><surname>Рязанцев</surname> "
"<affiliation><address><email>DJm00n@rambler.ru</email></address></"
"affiliation><contrib>Перевод на русский</contrib></othercredit>"

#. Tag: holder
#: index.docbook:25
#, no-c-format
msgid "Hans Karlsson"
msgstr "Hans Karlsson"

#. Tag: para
#: index.docbook:33
#, no-c-format
msgid ""
"&superkaramba; is a tool that allows you to easily create interactive "
"widgets on your &kde; desktop."
msgstr ""
"&superkaramba; - это программа, которая позволяет легко создавать "
"интерактивные виджеты для вашего рабочего стола &kde;."

#. Tag: keyword
#: index.docbook:37
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#: index.docbook:38
#, no-c-format
msgid "desktop"
msgstr "рабочий стол"

#. Tag: keyword
#: index.docbook:39
#, no-c-format
msgid "karamba"
msgstr "karamba"

#. Tag: title
#: index.docbook:62
#, no-c-format
msgid "Questions, Answers, and Tips"
msgstr "Вопросы, ответы и советы"

#. Tag: title
#: index.docbook:65
#, no-c-format
msgid "Frequently asked questions"
msgstr "Часто задаваемые вопросы"

#. Tag: title
#: index.docbook:67
#, no-c-format
msgid "Installation"
msgstr "Установка"

#. Tag: para
#: index.docbook:70
#, no-c-format
msgid "Where can I find &superkaramba; <acronym>RPM</acronym>'s?"
msgstr "Где я могу найти <acronym>RPM</acronym>-пакеты &superkaramba;?"

#. Tag: para
#: index.docbook:75
#, fuzzy, no-c-format
msgid ""
"The &superkaramba; web site (<ulink url=\"http://netdragon.sourceforge.net"
"\"></ulink>) has a list of user-submitted RPMs for several distributions. "
"Other user-submitted RPMs have been posted to kde-look.org and to the "
"&superkaramba; Help forum."
msgstr ""
"Сайт SuperKaramba (http://netdragon.sourceforge.net) содержит список "
"отправленных пользователями RPM пакетов для разных дистрибутивов. Другие "
"отправленные пользователями RPM пакеты можно найти на kde-look.org и форуме "
"поддержки SuperKaramba."

#. Tag: para
#: index.docbook:83 index.docbook:92
#, no-c-format
msgid "When I try to compile, I get this error:"
msgstr "Когда я пытаюсь скомпилировать программу, получаю ошибку:"

#. Tag: screen
#: index.docbook:84
#, no-c-format
msgid "<computeroutput>Python.h: No such file or directory</computeroutput>"
msgstr "<computeroutput>Python.h: No such file or directory</computeroutput>"

#. Tag: para
#: index.docbook:86
#, no-c-format
msgid ""
"You will need to install the <literal>python-devel</literal> package. Once "
"that package is installed you should run <command>./configure</command> and "
"<command>make</command> again."
msgstr ""
"Вам нужно установить пакет <literal>python-devel</literal>. Когда пакет "
"будет установлен, можете запускать <command>./configure</command> и "
"<command>make</command> снова."

#. Tag: screen
#: index.docbook:93
#, no-c-format
msgid "<computeroutput>cannot find -lselinux</computeroutput>"
msgstr "<computeroutput>cannot find -lselinux</computeroutput>"

#. Tag: para
#: index.docbook:96
#, no-c-format
msgid ""
"You will need to install the <literal>libselinux-devel</literal> package. "
"Once that package is installed you should run <command>./configure</command> "
"and <command>make</command> again."
msgstr ""
"Вам нужно установить пакет <literal>libselinux-devel</literal>. Когда пакет "
"будет установлен, можете запускать <command>./configure</command> и "
"<command>make</command> снова."

#. Tag: para
#: index.docbook:103
#, no-c-format
msgid ""
"After running <command>./configure</command>, <command>make</command> and "
"<command>make install</command>, the &superkaramba; executable is not where "
"I expected it to be. When I try to run <command>superkaramba</command> from "
"the command line it says <computeroutput>command not found</computeroutput>."
msgstr ""
"После запуска <command>./configure</command>, <command>make</command> и "
"<command>make install</command>, исполняемый файл &superkaramba;  не там, "
"где я ожидал. Когда я пытаюсь запустить <command>superkaramba</command> с "
"командной строки, то получаю на выводе <computeroutput>command not found</"
"computeroutput>."

#. Tag: para
#: index.docbook:110
#, fuzzy, no-c-format
msgid ""
"In some distributions <command>./configure</command> has a default prefix "
"path that differs to what you might expect. For example, in &Mandrake; the "
"default prefix is <filename class=\"directory\">/usr/local/kde</filename> "
"and the executable is installed in <filename class=\"directory\">/usr/local/"
"kde/bin</filename>."
msgstr ""
"В некоторых дистрибутивах ./configure имеет префикс по умолчанию, который "
"отличный от того, что вы можете ожидать. Например, в Mandrake префикс по "
"умолчанию <filename class=\"directory\">/usr/local/kde</filename> а "
"приложения устанавливаются в <filename class=\"directory\">/usr/local/kde/"
"bin</filename>."

#. Tag: para
#: index.docbook:115
#, no-c-format
msgid "There are a few things you can do to fix this."
msgstr "Есть несколько способов исправить это."

#. Tag: para
#: index.docbook:116
#, fuzzy, no-c-format
msgid ""
"You could run <command>./configure <parameter>--prefix=/usr</parameter></"
"command> to specify the prefix that you prefer and then try <command>make</"
"command> and <command>make install</command> again. Or you could create a "
"symbolic link in your preferred folder that links to the executable. Or you "
"could add the install folder to your $<envar>PATH</envar>."
msgstr ""
"Вы можете запустить <command>./configure <parameter>--prefix=/usr</"
"parameter></command> для указания префикса, который вы предпочитаете, а "
"потом попробовать <command>make</command> и <command>make install</command> "
"снова. Или вы можете поместить символическую ссылку в предпочитаемый вами "
"каталог, которая будет запускать программу. Также вы можете добавить каталог "
"установки в ваш $<envar>PATH</envar>."

#. Tag: title
#: index.docbook:127
#, no-c-format
msgid "General"
msgstr "Общие"

#. Tag: para
#: index.docbook:130
#, no-c-format
msgid ""
"How do I get rid of the &kde; panel? Do I have to keep &kicker; running?"
msgstr ""
"Как мне избавиться от панели &kde;? Должен ли &kicker; быть всё время "
"запущен?"

#. Tag: para
#: index.docbook:133
#, fuzzy, no-c-format
msgid ""
"Many themes depend on &kicker; and will not work well without it. If you "
"don't want &kicker; to show on your desktop, you can set it to auto-hide. "
"Another solution is to change the auto-hide setting in a <filename>~/.kde/"
"share/autostart/panel.desktop</filename> file. For more information see the "
"wiki page about <ulink url=\"http://wiki.kdenews.org/tiki-print.php?"
"page=Secret+Config+Settings\">secret configuration settings</ulink>."
msgstr ""
"Большинство тем не зависят от &kicker; и работают без него. Если вы не "
"желаете. чтобы &kicker; отображался на вашем рабочем столе, то можно "
"настроить его на автоматическое скрытие. Другой способ - это изменить "
"значение авто скрытия в <filename>~/.kde</filename> файле. За дополнительной "
"информацией посетите страницу wiki о <ulink url=\"http://wiki.kdenews.org/"
"tiki-print.php?page=Secret+Config+Settings\">секретных параметрах</ulink>."

#. Tag: para
#: index.docbook:144
#, no-c-format
msgid ""
"Can I run a system tray theme while the &kicker; system tray is running?"
msgstr ""
"Могу ли я запустить тему для системного лотка, когда системный лоток "
"&kicker; запущен?"

#. Tag: para
#: index.docbook:147
#, fuzzy, no-c-format
msgid ""
"It's better not to. If you run a theme with a SysTray on it while a regular "
"kicker systray is running, the theme will <quote>steal</quote> all the icons "
"from the first systray. This is because in &kde; and Gnome, the systray "
"icons are really little windows and there is only one copy of each running. "
"If you want to run a system tray theme, you must remove the system tray from "
"&kicker;."
msgstr ""
"Лучше так не делать. Если вы запустите тему с системным лотком при "
"запущенном обычном системным лотком kicker, то тема <quote>захватит</quote> "
"все значки из первого лотка. Так произойдёт потому, что в &kde; и Гноме "
"значки системного лотка - это действительно маленькие окна и есть только "
"одна копия каждого из них. Если вы желаете запустить тему системного лотка, "
"вы можете удалить системный лоток из &kicker;."

#. Tag: para
#: index.docbook:157
#, no-c-format
msgid "How do I set up my themes to run automatically on startup?"
msgstr "Как мне сделать, чтобы мои темы загружались автоматически при старте?"

#. Tag: para
#: index.docbook:160
#, fuzzy, no-c-format
msgid ""
"Turn on &kde; session support in the &kcontrolcenter;. When session support "
"is enabled, any theme that is left running when you logout will "
"automatically start on startup. For some themes you will also need to &RMB; "
"click and reload the theme at least once after running it so that the config "
"file will get written to your home folder."
msgstr ""
"Включите поддержку сеансов в &kcontrolcenter;. Когда поддержка сессий "
"включена, любая тема, которая будет запущена при завершении сеанса будет "
"автоматически загружена при запуске. Для некоторых тем вам также нужно будет "
"щелкнуть &RMB; и перезагрузить тему хотя-бы один раз после запуска, чтобы "
"настройки записались в ваш домашний каталог."

#. Tag: para
#: index.docbook:165
#, fuzzy, no-c-format
msgid ""
"Another solution is to create a desktop config file in the &kde; autostart "
"folder."
msgstr ""
"Другой способ - это создать файл настроек .desktop в каталоге автозапуска "
"&kde;."

#. Tag: para
#: index.docbook:167
#, no-c-format
msgid ""
"To create a desktop config file, open an editor and enter the following:"
msgstr ""
"Для создания такого файла, откройте текстовый редактор, и введите следующее:"

#. Tag: screen
#: index.docbook:169
#, no-c-format
msgid ""
"<userinput>[Desktop Entry]\n"
"Exec=superkaramba {location of theme file}.theme\n"
"Name={theme name}\n"
"Type=Application\n"
"X-KDE-StartupNotify=false</userinput>"
msgstr ""
"<userinput>[Desktop Entry]\n"
"Exec=superkaramba {путь к файлу темы}.theme\n"
"Name={название темы}\n"
"Type=Application\n"
"X-KDE-StartupNotify=false</userinput>"

#. Tag: para
#: index.docbook:170
#, fuzzy, no-c-format
msgid ""
"Then save it as <filename><replaceable>themename</replaceable>.desktop</"
"filename> into the autostart folder at <filename class=\"directory\">"
"$<envar>KDEHOME</envar>/Autostart/</filename>."
msgstr ""
"Потом сохраните его как <filename><replaceable>имятемы</replaceable>."
"desktop</filename> в каталоге автозапуска <filename class=\"directory\">"
"$<envar>KDEHOME</envar>/Autostart/</filename>."

#. Tag: para
#: index.docbook:177
#, no-c-format
msgid ""
"Why is the <guimenuitem>Edit Script</guimenuitem> option disabled when I "
"&RMB; click on a theme?"
msgstr ""
"Почему опция <guimenuitem>Изменить сценарий</guimenuitem> отключена, когда я "
"щелкаю &RMB; на теме?"

#. Tag: para
#: index.docbook:179
#, no-c-format
msgid ""
"Some &superkaramba; themes don't include a <application>Python</application> "
"script. This is normal and those themes will still work fine without one. "
"However, if the theme does use a <application>Python</application> script, "
"&superkaramba; may have not been able to load the script. This is usually "
"due to <application>Python</application> errors. Run the theme from the "
"command line to find out the specific error message."
msgstr ""
"Некоторые темы &superkaramba; не содержат сценария на <application>Python</"
"application>. Это нормально и эти темы работают и без него. Однако, если "
"тема использует сценарий на <application>Python</application> , то "
"&superkaramba; должно быть не может его загрузить. Это обычно случается, при "
"ошибках в <application>Python</application> сценарии. Запустите тему из "
"командной строки для поиска сообщений об ошибках."

#. Tag: para
#: index.docbook:190
#, no-c-format
msgid ""
"Why is the <guimenuitem>Configure Theme</guimenuitem> menu disabled when I "
"&RMB; click on a theme?"
msgstr ""
"Почему опция <guimenuitem>Настроить тему</guimenuitem> отключена, когда я "
"щелкаю &RMB; на теме?"

#. Tag: para
#: index.docbook:194
#, no-c-format
msgid ""
"The <guimenuitem>Configure Theme</guimenuitem> menu is only enabled if the "
"theme has added options to it. Some themes do not have any configuration "
"options or use a different configuration method."
msgstr ""
"Опция меню <guimenuitem>Настроить тему</guimenuitem> включена только, когда "
"тема добавляет опции в неё. Некоторые темы не имеют никаких настраиваемых "
"параметров или используют другой способ настройки."

#. Tag: title
#: index.docbook:202
#, no-c-format
msgid "Troubleshooting"
msgstr "Диагностика"

#. Tag: para
#: index.docbook:205
#, no-c-format
msgid "My themes start up in different locations each time I start &kde;."
msgstr "Мои темы запускаются в разных местах, при каждом старте &kde;."

#. Tag: para
#: index.docbook:209
#, no-c-format
msgid ""
"Close all &superkaramba; themes and then delete any session files in "
"<filename class=\"directory\">$<envar>KDEHOME</envar>/share/config/session/</"
"filename> that begin with &superkaramba;. Restart &kde; and start your "
"themes again. Now they will stay in the right place."
msgstr ""
"Закройте все темы &superkaramba; и удалите все файлы сессий в <filename "
"class=\"directory\">$<envar>KDEHOME</envar>/share/config/session/</"
"filename>, которые начинаются с &superkaramba;. Перезапустите &kde; и "
"запустите свою тему снова. Теперь они будут располагаться на своих местах."

#. Tag: para
#: index.docbook:218
#, no-c-format
msgid "The backgrounds of my themes are black instead of transparent."
msgstr "Фон моих тем чёрный вместо прозрачного."

#. Tag: para
#: index.docbook:220
#, no-c-format
msgid ""
"In &kde;, transparency only works when the desktop background is a wallpaper "
"image that is centered or scaled. If the background is a plain color or a "
"tiled image then &superkaramba; themes will not be transparent. In Gnome, "
"there is a known issue that themes will have a black background even when a "
"wallpaper is set. Unfortunately there is no current solution. Your theme "
"should work fine otherwise."
msgstr ""
"В &kde;, прозрачность работает только, если фон рабочего стола - картинка, "
"которая расположена по центру или растянута. Если фон - простой цвет или "
"расположенная черепицей картинка, тогда темы &superkaramba; не работают с "
"прозрачностью. В Гноме, известна ошибка, когда темы имеют чёрный фон, даже "
"если картинка на рабочем столе установлена. К сожалению пока нет способа "
"исправить это. Ваши темы всё же должны работать нормально."

#. Tag: title
#: index.docbook:234
#, no-c-format
msgid "Credits and Licenses"
msgstr "Авторы и лицензия"

#. Tag: para
#: index.docbook:236
#, no-c-format
msgid ""
"&superkaramba; is written by <personname><firstname>Adam</"
"firstname><surname>Geitgey</surname></personname> <email>adam@rootnode.org</"
"email> and <personname><firstname>Hans</firstname><surname>Karlsson</"
"surname></personname> <email>karlsson.h@home.se</email>"
msgstr ""
"Разработка: Адам Гайтги (<personname><firstname>Adam</"
"firstname><surname>Geitgey</surname></personname>) <email>adam@rootnode.org</"
"email> и Хэнс Крлссон (<personname><firstname>Hans</"
"firstname><surname>Karlsson</surname></personname>) <email>karlsson.h@home."
"se</email>"

#. Tag: trans_comment
#: index.docbook:244
#, no-c-format
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Превод на русский язык: Дмитрий Рязанцев <email>djm00n@rambler.ru</"
"email></para>"

#. Tag: chapter
#: index.docbook:244
#, no-c-format
msgid "&underFDL; &underGPL;"
msgstr "&underFDL; &underGPL;"
