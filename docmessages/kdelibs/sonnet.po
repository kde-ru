# translation of kspell.po to Russian
# Copyright (C) 2004 Free Software Foundation, Inc.
# Oleg Batalov <olegbatalov@mail.ru>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kspell\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:22+0000\n"
"PO-Revision-Date: 2004-04-13 11:50+0300\n"
"Last-Translator: Andrey Cherepanov <sibskull@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: index.docbook:11
#, no-c-format
msgid "The &sonnet; Handbook"
msgstr "Руководство &sonnet;"

#. Tag: author
#: index.docbook:13
#, no-c-format
msgid "&David.Sweet; &David.Sweet.mail;"
msgstr "&David.Sweet; &David.Sweet.mail; "

#. Tag: trans_comment
#: index.docbook:15
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Олег</"
"firstname><surname>Баталов</"
"surname><affiliation><address><email>olegbatalov@mail.ru</email></address></"
"affiliation><contrib>Перевод на русский язык</contrib></othercredit>"

#. Tag: para
#: index.docbook:22
#, fuzzy, no-c-format
msgid ""
"&sonnet; is the spelling checker used by &kde; applications such as &kate;, "
"&kmail;, and &kword;. It is a &GUI; frontend to <application>International "
"ISpell</application> and <application>ASpell</application>."
msgstr ""
"&sonnet; -- это программа проверки правописания, используемая приложениями "
"&kde; типа &kate;, &kmail; и &kword;. Она представляет собой графический "
"интерфейс к <application>International ISpell</application> и "
"<application>ASpell</application>."

#. Tag: keyword
#: index.docbook:29
#, no-c-format
msgid "spell"
msgstr "правописание"

#. Tag: keyword
#: index.docbook:30
#, no-c-format
msgid "spelling"
msgstr "проверка правописания"

#. Tag: keyword
#: index.docbook:31
#, no-c-format
msgid "sonnet"
msgstr ""

#. Tag: keyword
#: index.docbook:32
#, no-c-format
msgid "ispell"
msgstr "ispell "

#. Tag: keyword
#: index.docbook:33
#, no-c-format
msgid "aspell"
msgstr "aspell "

#. Tag: keyword
#: index.docbook:34
#, no-c-format
msgid "check"
msgstr "проверка"

#. Tag: keyword
#: index.docbook:35
#, no-c-format
msgid "checker"
msgstr "программа проверки"

#. Tag: keyword
#: index.docbook:36
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: title
#: index.docbook:41
#, no-c-format
msgid "Misspelled Word Dialog"
msgstr "Диалог ошибки правописания в слове"

#. Tag: para
#: index.docbook:44
#, no-c-format
msgid ""
"(If you do not have <application>Ispell</application> installed you can "
"obtain it from the <ulink url=\"http://fmg-www.cs.ucla.edu/geoff/ispell.html"
"\">International ISpell home page</ulink>. ASpell is available from the "
"<ulink url=\"http://aspell.sourceforge.net/\">ASpell home page</ulink>.)"
msgstr ""
"(Если у вас не установлен <application>Ispell</application>, вы можете "
"получить его с <ulink url=\"http://fmg-www.cs.ucla.edu/geoff/ispell.html"
"\">домашней страницы ISpell</ulink>. ASpell доступен на <ulink url=\"http://"
"aspell.sourceforge.net/\">домашней странице ASpell</ulink>.)"

#. Tag: title
#: index.docbook:52
#, no-c-format
msgid "General Use"
msgstr "Общие правила использования"

#. Tag: para
#: index.docbook:54
#, no-c-format
msgid ""
"The top line in the dialog displays a possibly misspelled word which was "
"found in your document. &sonnet; attempts to find an appropriate replacement "
"word. One or several may be found. The best guess is shown to the right of "
"<guilabel>Replacement:</guilabel>. To accept this replacement, click on "
"<guibutton>Replace</guibutton>. You may also select a word from the list of "
"<guilabel>Suggestions</guilabel> and then click <guibutton>Replace</"
"guibutton> to replace the misspelled word with the selected word."
msgstr ""
"Верхняя строка диалога отображает слово с возможной ошибкой, которое было "
"найдено в вашем документе. &sonnet; попытается найти соответствующие слова "
"для замены. Обычно бывает найдено одно или несколько слов. Наиболее "
"подходящее показано в поле <guilabel>Заменить на:</guilabel>. Чтобы принять "
"эту замену, нажмите <guibutton>Заменить</guibutton>. Также вы можете выбрать "
"слово из списка <guilabel>Предлагаемые слова</guilabel> и затем нажать "
"<guibutton>Заменить</guibutton>, чтобы заменить ошибочное слово на выбранное."

#. Tag: para
#: index.docbook:62
#, no-c-format
msgid "To keep your original spelling, click on <guilabel>Ignore</guilabel>."
msgstr ""
"Чтобы оставить оригинальное слово, нажмите <guilabel>Игнорировать</guilabel>."

#. Tag: para
#: index.docbook:64
#, fuzzy, no-c-format
msgid ""
"To stop the spellchecking -- keeping the changes you've already made -- "
"click on <guibutton>Stop</guibutton>."
msgstr ""
"Чтобы остановить проверку правописания, сохранив сделанные исправления, "
"нажмите кнопку <guibutton>Готово</guibutton>."

#. Tag: para
#: index.docbook:67
#, fuzzy, no-c-format
msgid ""
"To stop the spellchecking and cancel the changes you've already made, click "
"on <guibutton>Cancel</guibutton>."
msgstr ""
"Чтобы остановить проверку правописания, отменив сделанные исправления, "
"нажмите кнопку <guibutton>Отменить</guibutton>."

#. Tag: para
#: index.docbook:70
#, no-c-format
msgid ""
"Clicking on <guibutton>Replace All</guibutton> will initially perform the "
"same function as clicking on <guibutton>Replace</guibutton>, but will "
"automatically replace the misspelled word with the chosen replacement word, "
"if it appears again (at a later point) in your document."
msgstr ""
"Нажатие <guibutton>Заменить всё</guibutton> обычно выполняет ту же функцию, "
"что и нажатие <guibutton>Заменить</guibutton>, но автоматически заменит "
"ошибочное слово выбранным для замены, если оно встретится далее в документе."

#. Tag: para
#: index.docbook:75
#, no-c-format
msgid ""
"The <guibutton>Ignore All</guibutton> button ignores this and all future "
"occurrences of the misspelled word."
msgstr ""
"Кнопка <guibutton>Игнорировать все</guibutton> игнорирует это и все "
"последующие появления ошибочного слова."

#. Tag: para
#: index.docbook:78
#, no-c-format
msgid ""
"Clicking on <guibutton>Add</guibutton> will add the misspelled word to your "
"personal dictionary (this is distinct from the original system dictionary, "
"so the additions you make will not be seen by other users)."
msgstr ""
"Нажатие <guibutton>Добавить в словарь</guibutton> добавит слово с ошибкой в "
"ваш персональный словарь (он отличается от оригинального системного словаря, "
"так сделанные вами добавления не будут видны другим пользователям)."

#. Tag: title
#: index.docbook:85
#, no-c-format
msgid "Configuration Dialog"
msgstr "Диалог настройки"

#. Tag: title
#: index.docbook:89
#, no-c-format
msgid "Dictionaries"
msgstr "Словарь"

#. Tag: para
#: index.docbook:91
#, fuzzy, no-c-format
msgid ""
"You can choose the dictionary to use for spellchecking from the list of "
"installed dictionaries."
msgstr ""
"Вы можете выбрать из списка установленных словарь, который будет "
"использоваться при проверки правописания."

#. Tag: title
#: index.docbook:96
#, no-c-format
msgid "Encodings"
msgstr "Кодировка"

#. Tag: para
#: index.docbook:98
#, no-c-format
msgid "The most commonly used character encodings are:"
msgstr "Наиболее часто используемые кодировки:"

#. Tag: term
#: index.docbook:101
#, no-c-format
msgid "US-ASCII"
msgstr "US-ASCII"

#. Tag: para
#: index.docbook:103
#, no-c-format
msgid "This is the character set used for English text."
msgstr "Эта кодировка используется в английских текстах."

#. Tag: term
#: index.docbook:107
#, no-c-format
msgid "ISO-8859-1"
msgstr "ISO-8859-1"

#. Tag: para
#: index.docbook:109
#, no-c-format
msgid "This is used for Western European languages."
msgstr "Используется западно-европейскими языками."

#. Tag: term
#: index.docbook:113
#, no-c-format
msgid "UTF-8"
msgstr "UTF-8"

#. Tag: para
#: index.docbook:115
#, no-c-format
msgid ""
"This is a Unicode encoding that can be used for almost any language, if your "
"system has the necessary fonts."
msgstr ""
"Эта кодировка Unicode может использоваться практически всеми языками, если в "
"вашей системе установлены соответствующие шрифты."

#. Tag: para
#: index.docbook:122
#, no-c-format
msgid ""
"You should select the one that matches the character set you are using. In "
"some cases, dictionaries will support more than one encoding. A dictionary "
"might, for example, accept accented characters when <guilabel>ISO-8859-1</"
"guilabel> is selected, but accept email-style character combinations (like "
"<literal>'a</literal> for an accented <literal>a</literal>) when "
"<guilabel>US-ASCII</guilabel> is selected. Please see your dictionary's "
"distribution for more information."
msgstr ""
"Вы должны выбрать кодировку, соответствующую используемой кодировке в "
"системе. В некоторых случаях словари поддерживают несколько кодировок. "
"Словарь, например, может поддерживать диакритические символы, когда выбрана "
"кодировка <guilabel>ISO-8859-1</guilabel>, однако представлять их как "
"комбинацию символов почтового стиля (типа <literal>'a</literal> для "
"акцентированного <literal>a</literal>), когда выбран <guilabel>US-ASCII</"
"guilabel>. Подробности смотрите в поставке словаря."

#. Tag: title
#: index.docbook:131
#, no-c-format
msgid "Spell-checking client"
msgstr "Клиент"

#. Tag: para
#: index.docbook:133
#, no-c-format
msgid ""
"You may choose to use <application>Ispell</application> or "
"<application>Aspell</application> as the spell-checking backend for "
"&sonnet;. <application>Ispell</application> is more widely available and may "
"have better international support, but <application>Aspell</application> is "
"gaining popularity as it claims to give better suggestions for word "
"replacements."
msgstr ""
"Вы можете использовать <application>Ispell</application> или "
"<application>Aspell</application> для проверки правильности написания в "
"&sonnet;. <application>Ispell</application> наиболее широко распространён и "
"можете иметь лучшую международную поддержку, но <application>Aspell</"
"application> набирает популярность, поскольку считается, что она предлагает "
"лучшие варианты замены."

#. Tag: title
#: index.docbook:140
#, no-c-format
msgid "Other"
msgstr "Разное"

#. Tag: para
#: index.docbook:142
#, no-c-format
msgid ""
"It is recommend that you do not change the first two options unless you have "
"read the International <application>ISpell</application> man page."
msgstr ""
"Рекомендуется не изменять первые два параметра, пока вы не прочитали "
"страницу руководства международного <application>ISpell</application>."

#. Tag: title
#: index.docbook:148
#, no-c-format
msgid "Contact Information"
msgstr "Информация для контактов"

#. Tag: para
#: index.docbook:150
#, fuzzy, no-c-format
#| msgid ""
#| "For more information about &sonnet;, visit the <ulink url=\"http://www."
#| "chaos.umd.edu/~dsweet/KDE/KSpell\">&sonnet; Home Page</ulink>. In "
#| "particular, you will find information about programming the &sonnet; C++ "
#| "class."
msgid ""
"For more information about &sonnet;, visit the <ulink url=\"http://www.chaos."
"umd.edu/~dsweet/KDE/Sonnet\">&sonnet; Home Page</ulink>. In particular, you "
"will find information about programming the &sonnet; C++ class."
msgstr ""
"Чтобы получить больше информации о &sonnet;, посетите <ulink url=\"http://"
"www.chaos.umd.edu/~dsweet/KDE/KSpell\">домашнюю страницу &sonnet;</ulink>. В "
"частности, там можно найти сведения о работы с &sonnet; через классы C++."

#. Tag: para
#: index.docbook:153
#, no-c-format
msgid ""
"You may email the author/maintainer with questions and/or comments at &David."
"Sweet.mail;."
msgstr ""
"Вы можете написать письмо автору/координатору с вопросами и/или "
"комментариями по адресу &David.Sweet.mail;."

#. Tag: trans_comment
#: index.docbook:156
#, no-c-format
msgid "CREDIT_FOR_TRANSLATORS"
msgstr "<para>Олег Баталов<email>olegbatalov@mail.ru</email></para>"

#. Tag: chapter
#: index.docbook:156
#, no-c-format
msgid "&underFDL;"
msgstr ""

#~ msgid "kspell"
#~ msgstr "kspell "
