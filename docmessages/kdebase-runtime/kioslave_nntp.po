# KDE3 - kioslave_nntp.pot Russian translation
# translation of kioslave_nntp.po to Russian
# Copyright (C) 2004 Free Software Foundation, Inc.
# Valia V. Vaneeva <fattie@altlinux.ru>, 2004.
# Nickolai Shaforostoff <shafff@ukr.net>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kioslave_nntp\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:21+0000\n"
"PO-Revision-Date: 2004-08-10 14:17+0300\n"
"Last-Translator: Nickolai Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: nntp.docbook:2
#, no-c-format
msgid "nntp"
msgstr "nntp"

#. Tag: author
#: nntp.docbook:5
#, no-c-format
msgid "&Lauri.Watts; &Lauri.Watts.mail;"
msgstr "&Lauri.Watts; &Lauri.Watts.mail;"

#. Tag: trans_comment
#: nntp.docbook:6
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname></firstname><surname></"
"surname><affiliation><address><email></email></address></"
"affiliation><contrib></contrib></othercredit>"

#. Tag: para
#: nntp.docbook:9
#, no-c-format
msgid "The nntp kioslave accesses <acronym>NNTP</acronym> servers directly."
msgstr "Протокол для обращения к серверам <acronym>NNTP</acronym>."

#. Tag: para
#: nntp.docbook:12
#, no-c-format
msgid ""
"This kioslave can not be used with servers that do not implement the "
"<command>GROUP</command> command, including some versions of the popular "
"<application>INN</application> news server which is often used by "
"<acronym>ISP</acronym>s. It does work with <application>leafnode</"
"application>, which many people use to keep an offline cache of news "
"articles on their own hard drive or within their <acronym>LAN</acronym>."
msgstr ""
"Этот протокол не может использоваться в работе с серверами, не "
"поддерживающими команду <command>GROUP</command>, в том числе с некоторыми "
"версиями сервера <application>INN</application>, популярными у поставщиком "
"интернет-услуг. Этот протокол хорошо работает с сервером "
"<application>leafnode</application>, который многие используют дляхранения "
"статей групп новостей на жестком диске или в локальной сети."

#. Tag: para
#: nntp.docbook:20
#, no-c-format
msgid ""
"You can use the nntp kioslave by typing <userinput>nntp://yourserver/"
"groupname</userinput> into the &konqueror; <acronym>URL</acronym> bar."
msgstr ""
"Вы можете пользоваться этим протоколом так: ведите в строку адреса "
"&konqueror; <userinput>nntp://yourserver/groupname</userinput>."

#. Tag: para
#: nntp.docbook:24
#, no-c-format
msgid ""
"If you enter a group name, as above, and the group is available, you will "
"see the messages stored for that group as icons in &konqueror;."
msgstr ""
"Если вы введете имя группы, как это указано выше, и она будет доступна, "
"сообщения в ней будут представлены в виде пиктограмм."

#. Tag: para
#: nntp.docbook:28
#, no-c-format
msgid ""
"Clicking on a message will display it as plain text, including all headers. "
"This could be useful for debugging a news client to news server connection, "
"for example, to ensure that your new <application>leafnode</application> "
"server is working correctly."
msgstr ""
"Щелкнув по сообщению, вы откроете его как обычный текстовый файл, при этом "
"вам будут видны все заголовки. Возможно, удобнее всего этот протокол "
"использовать для настройки совместной работы клиента чтения групп новостей и "
"сервера <application>leafnode</application>."

#. Tag: para
#: nntp.docbook:33
#, no-c-format
msgid ""
"If you don't enter a group name, and only the server name, you will see a "
"list of available groups."
msgstr ""
"Если вы не введете имя группы, а введете только имя сервера, вы увидите "
"список всех доступных групп."

#. Tag: para
#: nntp.docbook:36
#, no-c-format
msgid ""
"Please be aware that this could take an enormous amount of time, and will "
"cause a lot of network traffic. Some commercial usenet servers have 60,000 "
"or more groups available, and doing such a thing may cause your desktop to "
"freeze."
msgstr ""
"Пожалуйста, имейте в виду, что загрузка всего списка может занять очень "
"много времени, так как при этом будет передано много данных. На некоторых "
"Usenet-серверах более 60 000 групп, так что во время загрузки их списка ваша "
"система может некоторое время не отвечать."
