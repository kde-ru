# KDE3 - kioslave_ldap.pot Russian translation
# translation of kioslave_ldap.po to Russian
# Copyright (C) 2004 Free Software Foundation, Inc.
# Valia V. Vaneeva <fattie@altlinux.ru>, 2004.
# Nickolai Shaforostoff <shafff@ukr.net>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kioslave_ldap\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:21+0000\n"
"PO-Revision-Date: 2004-08-10 14:16+0300\n"
"Last-Translator: Nickolai Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: ldap.docbook:2
#, no-c-format
msgid "ldap"
msgstr "ldap"

#. Tag: author
#: ldap.docbook:5
#, no-c-format
msgid "&Lauri.Watts; &Lauri.Watts.mail;"
msgstr "&Lauri.Watts; &Lauri.Watts.mail;"

#. Tag: trans_comment
#: ldap.docbook:6
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname></firstname><surname></"
"surname><affiliation><address><email></email></address></"
"affiliation><contrib></contrib></othercredit>"

#. Tag: para
#: ldap.docbook:10
#, no-c-format
msgid ""
"<acronym>ldap</acronym> is the lightweight directory access protocol. It "
"provides access to an X.500 directory, or to a stand-alone <acronym>LDAP</"
"acronym> server."
msgstr ""
"<acronym>ldap</acronym> &mdash; это простой протокол доступа к каталогам "
"(lightweight directory access protocol). Он предоставляет доступ к каталогу "
"X.500 или серверу <acronym>LDAP</acronym>."

#. Tag: para
#: ldap.docbook:14
#, no-c-format
msgid "You can use the ldap kioslave as follows:"
msgstr "Вы можете использовать этот протокол так:"

#. Tag: para
#: ldap.docbook:16
#, no-c-format
msgid ""
"<userinput>ldap://host:port/ou=People,o=where,c=de??sub</userinput> for a "
"subtree-query"
msgstr ""
"<userinput>ldap://host:port/ou=People,o=where,c=de??sub</userinput> (запрос "
"поддерева)"

#. Tag: para
#: ldap.docbook:19
#, no-c-format
msgid ""
"or <userinput>ldap://host:port/cn=MM,ou=People,o=where,c=de??base</"
"userinput> for a complete branch."
msgstr ""
"или так: <userinput>ldap://host:port/cn=MM,ou=People,o=where,c=de??base</"
"userinput> (полная ветвь)."
