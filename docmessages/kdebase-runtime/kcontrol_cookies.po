# KDE3 - kcontrol_cookies.pot Russian translation
# Copyright (C) 2004 Free Software Foundation, Inc.
# Vera Ponomareva <verun@VP10249>, 2004.
# Valia V. Vaneeva <fattie@altlinux.ru>, 2004.
# Nickolai Shaforostoff <shafff@ukr.net>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kcontrol_cookies\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:21+0000\n"
"PO-Revision-Date: 2004-08-10 17:12+0300\n"
"Last-Translator: Nickolai Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: author
#: index.docbook:12
#, no-c-format
msgid "&Krishna.Tateneni; &Krishna.Tateneni.mail;"
msgstr "&Krishna.Tateneni; &Krishna.Tateneni.mail;"

#. Tag: author
#: index.docbook:13
#, no-c-format
msgid "&Jost.Schenck; &Jost.Schenck.mail;"
msgstr "&Jost.Schenck; &Jost.Schenck.mail;"

#. Tag: trans_comment
#: index.docbook:14
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Вера</"
"firstname><surname>Пономарёва </"
"surname><affiliation><address><email>verun@vp10249.spb.edu </email></"
"address></affiliation><contrib> Перевод на русский</contrib></othercredit>"

#. Tag: keyword
#: index.docbook:21
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#: index.docbook:22
#, no-c-format
msgid "KControl"
msgstr "Центр настройки"

#. Tag: keyword
#: index.docbook:23
#, no-c-format
msgid "cookie"
msgstr "cookie"

#. Tag: title
#: index.docbook:29
#, no-c-format
msgid "Cookies"
msgstr "Cookies-данные"

#. Tag: para
#: index.docbook:31
#, no-c-format
msgid ""
"Cookies are a mechanism used by web sites to store and retrieve information "
"using your browser. For example, a web site may allow you to customize the "
"content and layout of the pages you see, so that your choices are persistent "
"across different visits to that web site."
msgstr ""
"Веб-сайты используют cookies-данные для хранения и извлечения информации с "
"помощью вашего обозревателя. Например, некоторые сайтыпозволяют настроить "
"содержание и формат страниц, которые вы просматриваете, так что выбранные "
"вами параметры сохраняются на протяжении всех посещений этого сайта."

#. Tag: para
#: index.docbook:36
#, no-c-format
msgid ""
"The web site is able to remember your preferences by storing a cookie on "
"your computer. Then, on future visits, the web site retrieves the "
"information stored in the cookie to format the content of the site according "
"to your previously specified preferences."
msgstr ""
"Сайт может запоминать ваши предпочтения, сохранив cookies-данные в вашем "
"компъютере. При последующих посещенияхсайт извлекает информацию из "
"<quote>cookie</quote> и форматирует содержимое сайта в соответствии с вашими "
"предпочтениями."

#. Tag: para
#: index.docbook:41
#, no-c-format
msgid ""
"Thus, cookies play a very useful role in web browsing. Unfortunately, web "
"sites often store and retrieve information in cookies without your explicit "
"knowledge or consent. Some of this information may be quite useful to the "
"web site owners, for example, by allowing them to collect summary statistics "
"on the number of visits different areas of the web sites get, or to "
"customize banner advertising."
msgstr ""
"Таким образом данные cookies играют очень важную роль в навигации по "
"Интернету. К сожалению, часто сайты хранят и извлекают информацию из файлов "
"<quote>cookie</quote> без вашего ведома и согласия. Иногда эта информация "
"очень полезна для владельцев сайта, например, они могут узнать статистику "
"посещений разных частей сайта или настроить рекламные баннеры."

#. Tag: para
#: index.docbook:48
#, no-c-format
msgid ""
"The cookies module of the &kcontrol; allows you to set policies for the use "
"of cookies when you are browsing the web with the &konqueror; web browser."
msgstr ""
"С помощью модуля <quote>Cookies-данные</quote> в &kcontrol; вы можете "
"установить правила обработки данных <quote>cookie</quote> во время работы в "
"&konqueror;."

#. Tag: para
#: index.docbook:52
#, no-c-format
msgid ""
"Note that the policies that you set using this control module will "
"<emphasis>not</emphasis> apply to other web browsers such as &Netscape;."
msgstr ""
"Обратите внимание, что правила, установленные в модуле настройки, "
"<emphasis>не</emphasis> применяются в других обозревателях, например в "
"&Netscape;."

#. Tag: title
#: index.docbook:58
#, no-c-format
msgid "Policy"
msgstr "Правило обработки"

#. Tag: para
#: index.docbook:60
#, no-c-format
msgid ""
"Using the <guilabel>Policy</guilabel> tab, you can configure the &kde; "
"applications that will handle cookies. You can do this by specifying a "
"general cookie policy as well as special cookie policies for certain domains "
"or hosts."
msgstr ""
"На вкладке <guilabel>Правило обработки</guilabel> вы можете сделать "
"настройки для <quote>cookies</quote> более удобными. Это можно сделать, "
"установив общие правила обработки файлов <quote>cookie</quote> или задав "
"отдельные правила для разных доменов и узлов."

#. Tag: para
#: index.docbook:65
#, no-c-format
msgid ""
"The top of the policy tab has a checkbox labeled <guilabel>Enable cookies</"
"guilabel>. If you leave this unchecked, cookies will be completely disabled. "
"However, this may make browsing rather inconvenient, especially as some web "
"sites require the use of browsers with cookies enabled."
msgstr ""
"В верхней части вкладки находится флажок <guilabel>Включить поддержку файлов "
"cookie</guilabel>. Если он не установлен, то поддержка файлов <quote>cookie</"
"quote> полностью отключена. Заметим, что это может затруднить просмотр, "
"особенно потому, что некоторые сайты требуют использование обозревателя с "
"поддержкой <quote>cookie</quote>."

#. Tag: para
#: index.docbook:71
#, no-c-format
msgid ""
"You will probably want to enable cookies and then set specific policies on "
"how you want them to be handled."
msgstr ""
"Возможно, вам понадобится включить поддержку <quote>cookie</quote>, а потом "
"установить определённые правила обращения с ними."

#. Tag: para
#: index.docbook:74
#, no-c-format
msgid "The first group of options create settings that apply to all cookies."
msgstr ""
"Первая группа параметров содержит установки, которые применяются ко всем "
"файлам <quote>cookie</quote>."

#. Tag: guilabel
#: index.docbook:78
#, no-c-format
msgid "Only accept cookies from originating server"
msgstr "Принимать файлы cookie только с оригинального сервера"

#. Tag: para
#: index.docbook:80
#, no-c-format
msgid ""
"Some pages try to set cookies from servers other than the one you are seeing "
"the <acronym>HTML</acronym> page from. For example, they show you "
"advertisements, and the advertisements are from another computer, often one "
"that belongs to a large advertising group. These advertisements may try to "
"set a cookie which would allow them to track the pages you view across "
"multiple web sites."
msgstr ""
"Некоторые страницы пытаются создать файлы <quote>cookie</quote> с серверов, "
"к которым не относится просматриваемая страница <acronym>HTML</acronym>. "
"Например, вы видите на странице рекламные объявления, причём объявления с "
"другого сервера, часто входящие в более крупную рекламную группу. Эти "
"объявления могут попытаться создать файл <quote>cookie</quote>, который "
"позволит им отслеживать страницы, просматриваемые вами на многих сайтах."

#. Tag: para
#: index.docbook:86
#, fuzzy, no-c-format
msgid ""
"Enabling this option will mean only cookies that come from the same web "
"server as you are explicitly connecting to will be accepted."
msgstr ""
"Отключение этого параметра значит, что будут приниматься только файлы "
"<quote>cookie</quote>, принадлежащие веб-серверу, с которым вы хотели "
"соединиться."

#. Tag: guilabel
#: index.docbook:92
#, no-c-format
msgid "Automatically accept session cookies"
msgstr "Автоматически принимать сеансные cookie"

#. Tag: para
#: index.docbook:95
#, no-c-format
msgid ""
"An increasingly common use for cookies is not to track your movements across "
"many visits to a web site, but to just follow what you do during one single "
"visit. Session cookies are saved as long as you are looking at the site, and "
"deleted when you leave it."
msgstr ""
"<quote>Cookie</quote> всё больше и больше используются не для того, чтобы "
"каждый раз следить за вашим перемещением по сайту, а для того чтобы "
"отследить ваши действия в течение одного визита. Сеансные <quote>cookie</"
"quote> сохраняются только на время просмотра сайта и удаляются, когда вы его "
"покидаете."

#. Tag: para
#: index.docbook:100
#, no-c-format
msgid ""
"Web sites can use this information for various things, most commonly it is a "
"convenience so that you do not have to keep logging in to view pages. For "
"example, on a webmail site, without some kind of session <acronym>ID</"
"acronym>, you would have to give your password again for each email you want "
"to read. There are other ways to achieve this, but cookies are simple and "
"very common."
msgstr ""
"Сайты могут использовать эту информацию в разных целях, чаще всего для "
"удобства, чтобы вам не приходилось заново регистрироваться для просмотра "
"страниц. Например, на сайте электронной почты, без идентификатора сеанса, "
"вам пришлось бы снова вводить пароль для каждого письма, которое вы хотите "
"прочитать. Есть и другие способы этого добиться, но данные <quote>cookie</"
"quote> &mdash; один из самых простых и распространённых."

#. Tag: para
#: index.docbook:107
#, no-c-format
msgid ""
"Enabling this option means that session cookies are always accepted, even if "
"you don't accept any other kind, and even if you choose to reject cookies "
"from a particular site, session cookies from that site will be accepted."
msgstr ""
"При включении этого параметра сеансные <quote>cookie</quote> принимаются "
"всегда, даже если вы не принимаете никакие другие. Если вы выберите не "
"принимать файлы <quote>cookie</quote> с определенного сайта, сеансные "
"<quote>cookie</quote> с этого сайта будут приниматься."

#. Tag: guilabel
#: index.docbook:115
#, no-c-format
msgid "Treat all cookies as session cookies"
msgstr "Воспринимать все файлы cookie как сеансные"

#. Tag: para
#: index.docbook:117
#, no-c-format
msgid ""
"If this option is enabled, all cookies are treated as session cookies. That "
"is, they are not kept when you leave the web site."
msgstr ""
"Если этот параметр включен, все файлы <quote>cookie</quote> воспринимаются "
"как сеансные.Это значит, что они не сохраняются, когда вы покидаете веб-сайт."

#. Tag: para
#: index.docbook:121
#, no-c-format
msgid ""
"The definition of <quote>leave the web site</quote> is vague. Some cookies "
"may hang around for a little while after you are no longer viewing any pages "
"on a particular web site. This is normal."
msgstr ""
"Понятие <quote>покинуть веб-сайт</quote> несколько туманно. Некоторые "
"<quote>cookie</quote> сохраняются ещё какое-то время, когда вы уже не "
"просматриваете страницы данного сайта. Это нормально."

#. Tag: para
#: index.docbook:130
#, no-c-format
msgid ""
"The section for <guilabel>Default Policy</guilabel> sets some further "
"options that are mutually exclusive &mdash; you can choose only one of these "
"options as the default, but you are free to set a different option for any "
"specific web server."
msgstr ""
"Раздел <guilabel>Правила по умолчанию</guilabel> предназначендля настройки "
"некоторых дополнителных параметров, которые являются взаимоисключающими "
"&mdash; вы можете выбрать только одно значение по умолчанию, но также "
"возможен выбор другого значения для любого отдельного сервера."

#. Tag: guilabel
#: index.docbook:137
#, no-c-format
msgid "Ask for confirmation"
msgstr "Подтверждать приём файла cookie"

#. Tag: para
#: index.docbook:139
#, no-c-format
msgid ""
"If this option is selected, you will be asked for confirmation every time a "
"cookie is stored or retrieved. You can selectively accept or reject each "
"cookie. The confirmation dialog will also allow you to set a domain specific "
"policy, if you do not want to confirm each cookie for that domain."
msgstr ""
"Если выбрано это значение, у вас будут спрашивать подтверждение каждый раз "
"при сохранении файла <quote>cookie</quote>. Вы можете выборочно принимать "
"или отвергать <quote>cookie</quote>. Окно подтверждения позволяет также "
"установить правила для целого домена, если вы не хотите подтверждать приём "
"каждого <quote>cookie</quote> для этого домена."

#. Tag: guilabel
#: index.docbook:147
#, no-c-format
msgid "Accept all cookies"
msgstr "Принимать все файлы cookie"

#. Tag: para
#: index.docbook:149
#, no-c-format
msgid ""
"If this option is selected, all cookies will be accepted without asking for "
"confirmation."
msgstr ""
"Если выбрано это значение, будут приниматься все файлы <quote>cookie</quote> "
"без запроса о подтверждении."

#. Tag: guilabel
#: index.docbook:154
#, no-c-format
msgid "Reject all cookies"
msgstr "Отвергать все файлы cookie"

#. Tag: para
#: index.docbook:156
#, no-c-format
msgid ""
"If this option is selected, all cookies will be rejected without asking for "
"confirmation."
msgstr ""
"Если выбрано это значение, все файлы <quote>cookie</quote> будут отвергаться "
"без запроса о подтверждении."

#. Tag: para
#: index.docbook:162
#, no-c-format
msgid ""
"In addition to the default policy for handling of cookies, which you can set "
"by selecting one of the three options described above, you can also set "
"policies for specific host domains using the controls in the "
"<guilabel>Domain Specific</guilabel> group."
msgstr ""
"Кроме правил обращения с файлами <quote>cookie</quote> по умолчанию, которые "
"устанавливаются выбором одного из трёх описанных выше значений, вы можете "
"устанавливать правила для отдельных доменов, используя настройки в группе "
"<quote>Правила для отдельных доменов</quote>."

#. Tag: para
#: index.docbook:167
#, no-c-format
msgid ""
"The Ask, Accept, or Reject policy can be applied to a specific domain by "
"clicking on the <guibutton>New...</guibutton> button, which brings up a "
"dialog. In this dialog, you can type the name of the domain (with a leading "
"dot), then select the policy you want to apply to this domain. Note that "
"entries may also get added while you are browsing, if the default policy is "
"to ask for confirmation, and you choose a general policy for a specific host "
"(for example, by selecting <guilabel>Reject all cookies from this domain</"
"guilabel> when asked to confirm a cookie)."
msgstr ""
"Правила <quote>Запретить</quote>, <quote>Разрешить</quote>, "
"<quote>Спрашивать</quote> могут быть применены к отдельному домену. Для "
"этого нужно вызвать окно, нажав на кнопку <quote>Добавить...</quote>. "
"Введите в окне название домена (предваренное точкой, например, <quote>."
"domain.com</quote>) и выберите для него правила обработки <quote>cookie</"
"quote>. Обратите внимание, этот список может пополняться сам во время "
"просмотра, если по умолчанию выбран параметр <quote>Спрашивать "
"подтверждение</quote>, так что при запросе вы определяете правила для "
"отдельного узла, наример, выбрав <guibutton>Отвергать все файлы cookie с "
"этого домена</guibutton>."

#. Tag: para
#: index.docbook:177
#, no-c-format
msgid ""
"You can also select a specific host domain from the list and click the "
"<guibutton>Change</guibutton> button to choose a different policy for that "
"domain than the one shown in the list."
msgstr ""
"Можно изменить правила для конкретного домена, указанные в списке, выбрав "
"этот домен и нажав на кнопку <guibutton>Изменить...</guibutton>."

#. Tag: para
#: index.docbook:181
#, no-c-format
msgid ""
"To delete a domain specific policy, choose a domain from the list, and then "
"click the <guibutton>Delete</guibutton> button. The default policy will "
"apply to domains which have been deleted from the list."
msgstr ""
"Чтобы удалить правила для отдельного домена, выберите этот домен из списка и "
"нажмите на кнопку <guibutton>Удалить</guibutton>. К доменам, удаленным из "
"списка, будут применены правила по умолчанию."

#. Tag: title
#: index.docbook:188
#, no-c-format
msgid "Management"
msgstr "Принятые файлы cookie"

#. Tag: para
#: index.docbook:190
#, no-c-format
msgid ""
"In the <guilabel>Management</guilabel> tab you can browse and selectively "
"delete cookies that have been set in the past."
msgstr ""
"На вкладке <guilabel>Принятые файлы cookie</guilabel> можно просмотреть и "
"выборочно удалить принятые ранее файлы <quote>cookie</quote>."

#. Tag: para
#: index.docbook:193
#, no-c-format
msgid ""
"In the upper part of this dialog, you can see a list of domains displayed as "
"a tree. Click on the little <guiicon>+</guiicon> next to a domain to see all "
"cookies that have been set for this particular target domain. If you select "
"one of these cookies, you will notice that its contents will show up in the "
"frame <guilabel>Cookie Details</guilabel> below."
msgstr ""
"В верхней части окна вы найдёте список доменов, показанный в виде дерева. "
"Щёлкните по пиктограмме <guiicon>+</guiicon> напротив домена и вы увидите "
"все файлы <quote>cookie</quote>, принятые с этого конкретного домена. Если "
"выбрать один из этих <quote>cookie</quote>, его содержимое появится внизу, в "
"рамке <guilabel>Подробные сведения о закладке</guilabel>."

#. Tag: para
#: index.docbook:199
#, no-c-format
msgid ""
"By clicking the <guibutton>Delete</guibutton> button you can now delete the "
"selected cookie. Click <guibutton>Delete All</guibutton> to delete all "
"cookies stored."
msgstr ""
"Теперь, нажав на кнопку <guibutton>Удалить</guibutton>, вы можете удалить "
"выбранный <quote>cookie</quote>. Чтобы удалить все сохранённые "
"<quote>cookie</quote>, нажмите на <guibutton>Удалить все</guibutton>."

#. Tag: para
#: index.docbook:202
#, no-c-format
msgid ""
"Choose <guibutton>Reload List</guibutton> to reload the list from your hard "
"disk. You might want to do this if you have had the module open and are "
"testing web sites, or have made many changes in the module itself."
msgstr ""
"Нажмите <guibutton>Обновить список</guibutton>, чтобы обновить список с "
"вашего жёсткого диска. Это может понадобиться, когда модуль открыт во время "
"тестирования сайтов, или когда вы сделали много изменений в самом модуле."
