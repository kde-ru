# Translation of kdebugdialog.po into Russian
#
# translation of kdebugdialog.po to Russian
# KDE3 - kdebugdialog.pot doc Russian translation.
# Copyright (C) 2002, KDE Team.
#
# Denis Peplin <info@volginfo.ru>, 2002.
# Gregory Mokhin <mok@kde.ru>, 2002,2003.
# Nick Shaforostoff <shafff@ukr.net>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kdebugdialog\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-07-24 03:21+0000\n"
"PO-Revision-Date: 2006-07-16 19:22+0300\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: index.docbook:14
#, no-c-format
msgid "The &kdebugdialog; Handbook"
msgstr "Руководство &kdebugdialog;"

#. Tag: author
#: index.docbook:17
#, no-c-format
msgid "&David.Faure; &David.Faure.mail;"
msgstr "&David.Faure; &David.Faure.mail;"

#. Tag: trans_comment
#: index.docbook:18
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Денис</"
"firstname><surname>Пеплин</surname><affiliation><address><email>den@volginfo."
"ru</email></address></affiliation><contrib>Перевод на русский</contrib></"
"othercredit>"

#. Tag: para
#: index.docbook:25
#, no-c-format
msgid "This document describes &kdebugdialog;."
msgstr "Данный документ описывает &kdebugdialog;"

#. Tag: keyword
#: index.docbook:29
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#: index.docbook:30
#, no-c-format
msgid "KDebugdialog"
msgstr "KDebugdialog"

#. Tag: title
#: index.docbook:35
#, no-c-format
msgid "What is &kdebugdialog;?"
msgstr "Что такое &kdebugdialog;?"

#. Tag: para
#: index.docbook:37
#, no-c-format
msgid "It is a dialog box for managing diagnostic messages at runtime."
msgstr ""
"Это диалог для управления диагностическими сообщениями во время работы "
"программы."

#. Tag: para
#: index.docbook:40
#, no-c-format
msgid ""
"If you simply start <command>kdebugdialog</command>, you will see a list of "
"<quote>areas</quote>, that can be disabled or enabled. A <function>kDebug"
"(area)</function> call in the code will show something in the debug output "
"only if the area is enabled."
msgstr ""
"Если вы просто запустите <command>kdebugdialog</command>, то увидите список "
"<quote>областей</quote>, которые могут быть включены или выключены. Вызов "
"kDebug(область) в программе будет выводить отладочную информацию, только "
"если область включена."

#. Tag: para
#: index.docbook:45
#, no-c-format
msgid ""
"Note that kWarning, kError and kFatal always appear, they are NOT controlled "
"by this setting."
msgstr ""
"Обратите внимание, что kWarning, kError и kFatal выводятся всегда, они не "
"зависят от этих установок."

#. Tag: guilabel
#: index.docbook:50
#, no-c-format
msgid "Area"
msgstr "Область"

#. Tag: para
#: index.docbook:52
#, no-c-format
msgid ""
"The areas which should only be output. Every message that is not mentioned "
"here will simply not be output (unless this field remains empty, which is "
"the default, and means that all messages should be output). You can enter "
"several areas separated by commas here, and you can also use area ranges "
"with the syntax start-end. Thus a valid entry could be: 117,214-289,356-"
"359,221. Do not use whitespaces."
msgstr ""
"Только те области, которые должны быть выведены. Каждое сообщение, не "
"указанное здесь, просто не будет выведено (по умолчанию, пока это поле "
"остается пустым, выводятся все сообщения). Вы можете ввести несколько "
"областей, разделенных запятыми, также вы можете использовать диапазоны "
"областей в синтаксисе начало-конец. Таким образом, следующая запись "
"корректна: 117,214-289,356-359,221. Не используйте пробелы!"

#. Tag: para
#: index.docbook:63
#, no-c-format
msgid ""
"If you start <command>kdebugdialog</command> <option>--fullmode</option>, "
"then for every severity level you can define separately what should be done "
"with the diagnostic messages of that level, and the same for each debug area."
msgstr ""
"Если вы запустите <command>kdebugdialog</command> <option>--fullmode</"
"option>, то сможете задавать независимо для каждого уровня ошибок, что "
"должно быть сделано с диагностическими сообщениями на этом уровне, и так для "
"каждой отладочной области."

#. Tag: para
#: index.docbook:68
#, no-c-format
msgid ""
"In full mode, first you should select the debug area you are interested in "
"from the drop down box at the top."
msgstr ""
"В режиме полной отладки вы должны выбрать область из выпадающего списка "
"вверху окна."

#. Tag: para
#: index.docbook:71
#, no-c-format
msgid "You may independently set the output for various types of messages:"
msgstr "Вы можете независимо задавать вывод для различных типов сообщений:"

#. Tag: guilabel
#: index.docbook:75
#, no-c-format
msgid "Information"
msgstr "Информационные"

#. Tag: guilabel
#: index.docbook:76
#, no-c-format
msgid "Warning"
msgstr "Предупреждения"

#. Tag: guilabel
#: index.docbook:77
#, no-c-format
msgid "Error"
msgstr "Ошибки"

#. Tag: guilabel
#: index.docbook:78
#, no-c-format
msgid "Fatal Error"
msgstr "Грубые ошибки"

#. Tag: para
#: index.docbook:81
#, no-c-format
msgid "For each of these types, you can set the following:"
msgstr "Для каждого из типов можно указать:"

#. Tag: guilabel
#: index.docbook:85
#, no-c-format
msgid "Output to:"
msgstr "Вывод в:"

#. Tag: para
#: index.docbook:87
#, no-c-format
msgid ""
"In this Combobox, you can choose where the messages should be output. The "
"choices are: <quote>File</quote>, <quote>Message Box</quote>, <quote>Shell</"
"quote> (meaning stderr) and <quote>Syslog</quote>. Please do not direct "
"fatal messages to syslog unless you are the system administrator yourself. "
"The default is <quote>Message Box</quote>."
msgstr ""
"Вывод: в этом меню вы можете выбрать, куда должны быть выведены сообщения. "
"Варианты: <quote>Файл</quote>, <quote>Окно сообщения</quote>, "
"<quote>Оболочка</quote> (т.е. stderr) и <quote>Журнал системных сообщений</"
"quote> (syslog). Не направляйте сообщения о грубых ошибках в журнал "
"системных сообщений, если вы не системный администратор. Вывод по умолчанию: "
"<quote>Окно сообщения</quote>."

#. Tag: guilabel
#: index.docbook:97
#, no-c-format
msgid "Filename:"
msgstr "Имя файла:"

#. Tag: para
#: index.docbook:98
#, no-c-format
msgid ""
"This is only enabled when you have chosen <quote>File</quote> as the output "
"and provides the name of that file (which is interpreted as relative to the "
"current folder). The default is <filename>kdebug.dbg</filename>."
msgstr ""
"Файл: это работает только если вы выбрали <quote>Файл</quote> для вывода и "
"задали имя этого файла (которое определяется от текущего каталога). По "
"умолчанию это kdebug.dbg."

#. Tag: para
#: index.docbook:106
#, no-c-format
msgid ""
"Apart from this, you can also tick the check box <guilabel>Abort on fatal "
"errors</guilabel>. In this case, if a diagnostic message with the severity "
"level <computeroutput>KDEBUG_FATAL</computeroutput> is output, the "
"application aborts with a SIGABRT after outputting the message."
msgstr ""
"Помимо этого, вы можете также включить опцию <guilabel>Отмена при фатальных "
"ошибках</guilabel>. В этом случае, если диагностическое сообщение будет "
"уровня <computeroutput>KDEBUG_FATAL</computeroutput>, приложение завершится "
"по сигналу SIGABRT после вывода сообщения."

#. Tag: para
#: index.docbook:112
#, no-c-format
msgid ""
"When you close the dialog by pressing <guibutton>OK</guibutton>, your "
"entries apply immediately and are saved in <filename>kdebugrc</filename>. "
"When you press <guibutton>Cancel</guibutton>, your entries are discarded and "
"the old ones are restored."
msgstr ""
"Нажав <guibutton>ОК</guibutton>, вы закроете диалог, при этом ваши установки "
"будут сохранены в <filename>kdebugrc</filename> и сразу же войдут в силу. "
"Если вы нажмёте <guibutton>Отмена</guibutton>, ваши установки будут отменены "
"и восстановлены старые."

#. Tag: para
#: index.docbook:118
#, no-c-format
msgid "Credits to Kalle Dalheimer for the original version of &kdebugdialog;"
msgstr "Разработчик исходной версии &kdebugdialog; - Kalle Dalheimer."

#. Tag: trans_comment
#: index.docbook:121
#, fuzzy, no-c-format
#| msgid "ROLES_OF_TRANSLATORS"
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Денис</"
"firstname><surname>Пеплин</surname><affiliation><address><email>den@volginfo."
"ru</email></address></affiliation><contrib>Перевод на русский</contrib></"
"othercredit>"

#. Tag: chapter
#: index.docbook:121
#, no-c-format
msgid "&underFDL;"
msgstr ""
