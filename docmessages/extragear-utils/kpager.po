# Translation of kpager.po into Russian
# Copyright (C) 2001, KDE Team.
#
# Olga Karpov <karpovolga@hotmail.com>, 2001.
# Nick Shaforostoff <shafff@ukr.net>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kpager\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-02-11 07:19+0000\n"
"PO-Revision-Date: 2006-07-16 19:32+0300\n"
"Last-Translator: Nick Shaforostoff <shafff@ukr.net>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Tag: title
#: index.docbook:13
#, no-c-format
msgid "The &kpager; Handbook"
msgstr "Руководство &kpager;"

#. Tag: author
#: index.docbook:16
#, no-c-format
msgid "&Dirk.Doerflinger; &Dirk.Doerflinger.mail;"
msgstr "&Dirk.Doerflinger; &Dirk.Doerflinger.mail;"

#. Tag: othercredit
#: index.docbook:18
#, no-c-format
msgid "&Antonio.Larrosa.Jimenez; &Antonio.Larrosa.Jimenez.mail;"
msgstr "&Antonio.Larrosa.Jimenez; &Antonio.Larrosa.Jimenez.mail;"

#. Tag: othercredit
#: index.docbook:23
#, no-c-format
msgid "&Matthias.Elter; &Matthias.Elter.mail;"
msgstr "&Matthias.Elter; &Matthias.Elter.mail;"

#. Tag: othercredit
#: index.docbook:29
#, no-c-format
msgid "&Matthias.Ettrich; &Matthias.Ettrich.mail;"
msgstr "&Matthias.Ettrich; &Matthias.Ettrich.mail;"

#. Tag: trans_comment
#: index.docbook:34
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Ольга</"
"firstname><surname>Карпова</"
"surname><affiliation><address><email>karpovolga@hotmail.com</email></"
"address></affiliation><contrib>Перевод на русский</contrib></othercredit>"

#. Tag: holder
#: index.docbook:40
#, no-c-format
msgid "&Dirk.Doerflinger;"
msgstr "&Dirk.Doerflinger;"

#. Tag: para
#: index.docbook:49
#, no-c-format
msgid "&kpager; gives you a thumbnail view of all virtual desktops."
msgstr "&kpager; cхематично отображает все виртуальные рабочие столы."

#. Tag: keyword
#: index.docbook:55
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#: index.docbook:56
#, no-c-format
msgid "pager"
msgstr "pager"

#. Tag: keyword
#: index.docbook:57
#, no-c-format
msgid "kpager"
msgstr "kpager"

#. Tag: keyword
#: index.docbook:58
#, no-c-format
msgid "desktop"
msgstr "рабочий стол"

#. Tag: keyword
#: index.docbook:59
#, no-c-format
msgid "overview"
msgstr "общие сведения"

#. Tag: title
#: index.docbook:65
#, no-c-format
msgid "Introduction"
msgstr "Введение"

#. Tag: para
#: index.docbook:67
#, no-c-format
msgid ""
"&kpager; gives you a thumbnail sketch of all your desktops. It is a handy "
"tool to let you see, resize or close windows on any desktop and move windows "
"around within or between desktops."
msgstr ""
"&kpager; cхематично отображает все ваши виртуальные рабочие столы. Это "
"полезный инструмент, позволяющий просматривать, изменять размер и закрывать "
"окна на любом рабочем столе, а также перемещать окна в пределах одного или "
"между разными рабочими столами."

#. Tag: title
#: index.docbook:75
#, no-c-format
msgid "Using &kpager;"
msgstr "Использование &kpager;"

#. Tag: screeninfo
#: index.docbook:78
#, no-c-format
msgid "Here's a screenshot of &kpager;"
msgstr "Это снимок &kpager;"

#. Tag: phrase
#: index.docbook:84 index.docbook:200
#, no-c-format
msgid "Screenshot"
msgstr "Снимок экрана"

#. Tag: title
#: index.docbook:90
#, no-c-format
msgid "More &kpager; features"
msgstr "Дополнительные возможности &kpager;"

#. Tag: para
#: index.docbook:92
#, no-c-format
msgid ""
"&kpager; can show all virtual desktops and the applications within them. It "
"can be used to chose an application or even to move applications within "
"virtual desktops or to others."
msgstr ""
"&kpager; показывает все виртуальные рабочие столы и запущенные на них "
"приложения. Его можно использовать для выбора приложения, а также для "
"перемещения приложений в пределах одного или между разными виртуальными "
"рабочими столами."

#. Tag: title
#: index.docbook:101
#, no-c-format
msgid "Command Reference"
msgstr "Справочник по командам"

#. Tag: title
#: index.docbook:104
#, no-c-format
msgid "The main &kpager; window"
msgstr "Главное окно &kpager;"

#. Tag: title
#: index.docbook:107
#, no-c-format
msgid "Using the mouse"
msgstr "Использование мыши "

#. Tag: para
#: index.docbook:109
#, no-c-format
msgid ""
"In &kpager; you can activate applications by clicking them with the "
"<mousebutton>left</mousebutton> button."
msgstr ""
"В &kpager; вы можете активизировать приложения щелчком <mousebutton>левой</"
"mousebutton> кнопки мыши."

#. Tag: para
#: index.docbook:112
#, no-c-format
msgid ""
"The <mousebutton>middle</mousebutton> button of the mouse can be used for "
"dragging applications within &kpager;. Applications can either be moved "
"within a virtual desktop or to another one."
msgstr ""
"<mousebutton>Средняя</mousebutton> кнопка мыши может использоваться для "
"перемещения приложений в &kpager;. Можно передвигать приложения в пределах "
"одного виртуального рабочего стола или переносить их на другой."

#. Tag: para
#: index.docbook:116
#, no-c-format
msgid ""
"Clicking the <mousebutton>right</mousebutton> mouse button anywhere in "
"&kpager; will open a context menu."
msgstr ""
"Щелчок <mousebutton>правой</mousebutton> кнопки мыши в &kpager; вызывает "
"контекстное меню."

#. Tag: title
#: index.docbook:122
#, no-c-format
msgid "The Context Menu"
msgstr "Контекстное меню"

#. Tag: para
#: index.docbook:124
#, no-c-format
msgid ""
"The context menu depends on where the <mousebutton>right</mousebutton> mouse "
"button is clicked: If it is clicked on the empty background of &kpager;, it "
"only has two items: <guimenuitem>Configure KPager</guimenuitem> and "
"<guimenuitem>Quit</guimenuitem>. Otherwise, if clicked on a window, there "
"are also the name and the icon of the application, and "
"<guimenuitem>Minimize</guimenuitem>, <guimenuitem>Maximize</guimenuitem>, "
"<guimenuitem>To Desktop</guimenuitem> and <guimenuitem>Close</guimenuitem> "
"are displayed. See below for a detailed description of the menu items."
msgstr ""
"Содержимое контекстного меню зависит от того, где была нажата "
"<mousebutton>правая</mousebutton> кнопка мыши: если щелчок был сделан на "
"свободном фоне &kpager;, в меню будут лишь два пункта: "
"<guimenuitem>Настройка</guimenuitem> и <guimenuitem>Выход</guimenuitem>. В "
"случае, если щелчок был сделан в окне, в меню будут указаны имя и "
"пиктограмма приложения, а также пункты <guimenuitem>Свернуть</guimenuitem>, "
"<guimenuitem>Распахнуть</guimenuitem>, <guimenuitem>На рабочий стол</"
"guimenuitem> и <guimenuitem>Закрыть</guimenuitem>. Детальное описание "
"пунктов меню представлено ниже."

#. Tag: guimenuitem
#: index.docbook:137
#, no-c-format
msgid "Maximize"
msgstr "Распахнуть"

#. Tag: action
#: index.docbook:139
#, no-c-format
msgid ""
"Maximizes the application window to the whole desktop. This item only "
"appears if right clicked on an application window."
msgstr ""
"Увеличивает размер окна приложения до размеров рабочего стола. Этот пункт "
"появляется только при щелчке правой кнопки мыши в окне приложения."

#. Tag: guimenuitem
#: index.docbook:146
#, no-c-format
msgid "Minimize"
msgstr "Свернуть"

#. Tag: action
#: index.docbook:148
#, no-c-format
msgid ""
"Iconifies the application. This item only appears if right clicked on an "
"application window."
msgstr ""
"Сворачивает приложение в пиктограмму панели. Этот пункт появляется только "
"при щелчке правой кнопки мыши в окне приложения."

#. Tag: guimenuitem
#: index.docbook:154
#, no-c-format
msgid "To Desktop"
msgstr "На рабочий стол"

#. Tag: action
#: index.docbook:156
#, no-c-format
msgid ""
"Sends the application window to the chosen virtual desktop. This item only "
"appears if right clicked on an application window."
msgstr ""
"Отправляет окно приложения на выбранный виртуальный рабочий стол. Этот пункт "
"появляется только при щелчке правой кнопки мыши в окне приложения."

#. Tag: guimenuitem
#: index.docbook:163
#, no-c-format
msgid "Close"
msgstr "Закрыть"

#. Tag: action
#: index.docbook:165
#, no-c-format
msgid ""
"Closes the clicked application. This item only appears if right clicked on "
"an application window."
msgstr ""
"Закрывает приложение. Этот пункт появляется только при щелчке правой кнопки "
"мыши в окне приложения."

#. Tag: guimenuitem
#: index.docbook:171
#, no-c-format
msgid "Configure Pager"
msgstr "Настройки"

#. Tag: para
#: index.docbook:173
#, no-c-format
msgid ""
"<action>Opens</action> the <link linkend=\"kpager-settings\">settings "
"dialog</link>."
msgstr ""
"<action>Открывает</action> диалог <link linkend=\"kpager-settings"
"\">настроек</link>."

#. Tag: guimenuitem
#: index.docbook:180
#, no-c-format
msgid "Quit"
msgstr "Выход"

#. Tag: para
#: index.docbook:182
#, no-c-format
msgid "<action>Quits</action> &kpager;."
msgstr "<action>Завершает работу</action> &kpager;."

#. Tag: title
#: index.docbook:191
#, no-c-format
msgid "The Settings Dialog"
msgstr "Настройка"

#. Tag: screeninfo
#: index.docbook:194
#, no-c-format
msgid "Here's a screenshot of the settings dialog"
msgstr "Это снимок диалога настроек"

#. Tag: para
#: index.docbook:205
#, no-c-format
msgid ""
"In the settings dialog you will find five check boxes and two groups of "
"radio buttons."
msgstr "В диалоге настроек есть пять флажков и две группы переключателей."

#. Tag: guilabel
#: index.docbook:210
#, no-c-format
msgid "Enable Window Dragging"
msgstr "Разрешить перетаскивание окна"

#. Tag: para
#: index.docbook:211
#, no-c-format
msgid ""
"If this box is checked, you can drag windows inside of &kpager; with the "
"&MMB;. Windows can be dragged over the desktop or even to another desktop."
msgstr ""
"Если отмечен этот флажок, вы можете перемещать окна приложений в &kpager; "
"средней кнопкой мыши. Можно перемещать окна приложений в пределах одного "
"рабочего стола или переносить их на другой."

#. Tag: guilabel
#: index.docbook:218
#, no-c-format
msgid "Show Name"
msgstr "Показать название"

#. Tag: para
#: index.docbook:219
#, no-c-format
msgid ""
"If this box is checked, the names of the desktops are shown in the main view "
"of &kpager;."
msgstr ""
"Если отмечен этот флажок, в главном окне &kpager; указываются названия "
"рабочих столов."

#. Tag: guilabel
#: index.docbook:224
#, no-c-format
msgid "Show Number"
msgstr "Показать номер"

#. Tag: para
#: index.docbook:225
#, no-c-format
msgid ""
"If this box is checked, the numbers of the desktops are shown in the main "
"view of &kpager;. If it is checked together with the <guilabel>Show Name</"
"guilabel> box, the name is displayed with ordinals, <abbrev>e.g.</abbrev> "
"<guilabel>1. Desktop</guilabel>."
msgstr ""
"Если отмечен этот флажок, в главном окне &kpager; показываются номера "
"рабочих столов. Если одновременно с этим отмечен флажок <guilabel>Показать "
"название</guilabel>, показывается как номер, так и название рабочего стола, "
"<abbrev>напр.</abbrev> <guilabel>Рабочий стол 1</guilabel>."

#. Tag: guilabel
#: index.docbook:232
#, no-c-format
msgid "Show Background"
msgstr "Показать фон"

#. Tag: para
#: index.docbook:233
#, no-c-format
msgid ""
"If this box is checked, the wallpaper of each desktop - if set - will be "
"shown as backgrounds in &kpager;, too."
msgstr ""
"Если отмечен этот флажок, в &kpager; также будут видны (если они настроены) "
"в виде фона обои каждого рабочего стола."

#. Tag: guilabel
#: index.docbook:238
#, no-c-format
msgid "Show Windows"
msgstr "Показать окна"

#. Tag: para
#: index.docbook:239
#, no-c-format
msgid ""
"If this box is checked, the applications on the desktops are shown in "
"&kpager; as small views. Otherwise, &kpager; will remain empty, just for "
"choosing the virtual desktops."
msgstr ""
"Если отмечен этот флажок, в &kpager; будут показаны приложения на рабочих "
"столах. В противном случае, изображения виртуальных рабочих столов в "
"&kpager; пусты и могут использоваться лишь для переключения между рабочими "
"столами."

#. Tag: guilabel
#: index.docbook:246
#, no-c-format
msgid "Type of Window"
msgstr "Отображение окон"

#. Tag: para
#: index.docbook:247
#, no-c-format
msgid ""
"This group of radio buttons sets the kind of view for the application window "
"views. <guilabel>Plain</guilabel> will show just empty rectangles with the "
"proportions of the application window, <guilabel>Icon</guilabel> will show "
"them with their standard icon and <guilabel>Pixmap</guilabel> with a small "
"view of the contents of the application window. Note that using the pixmap "
"mode is only recommended for very fast machines."
msgstr ""
"Эта группа переключателей устанавливает, каким образом изображать окна "
"приложений. <guilabel>Контуры</guilabel> покажет пустые прямоугольники с "
"пропорциями окна приложения. <guilabel>Иконка</guilabel> покажет также их "
"стандартную иконку, а <guilabel>Снимок</guilabel> - окна приложений с "
"уменьшенным изображением их содержимого. Учтите, режим <quote>снимка</quote> "
"рекомендуется использовать лишь на очень быстрых машинах."

#. Tag: guilabel
#: index.docbook:257
#, no-c-format
msgid "Layout"
msgstr "Вид главного окна"

#. Tag: para
#: index.docbook:258
#, no-c-format
msgid ""
"In this group the layout of the &kpager; main view can be set. "
"<guilabel>Classical</guilabel> will show &kpager; in a 2xn grid like pager "
"applications in some other window managers, <guilabel>Horizontal</guilabel> "
"will show the virtual desktops in a horizontal view and <guilabel>Vertical</"
"guilabel> in a vertical row, which may perfectly fit on the side of the "
"desktop."
msgstr ""
"В этой группе можно настроить вид главного окна &kpager;. "
"<guilabel>Классический</guilabel> приложения в окне &kpager; будут "
"расположены в два ряда, подобно переключателям окон в других оконных "
"менеджерах. <guilabel>Горизонтальный</guilabel> расположит изображения "
"виртуальных рабочих столов горизонтально, а <guilabel>Вертикальный</"
"guilabel> — вертикально, например, вдоль боковой границы рабочего стола."

#. Tag: title
#: index.docbook:272
#, no-c-format
msgid "Questions and Answers"
msgstr "Вопросы и ответы"

#. Tag: para
#: index.docbook:277
#, no-c-format
msgid "Why could I need &kpager;?"
msgstr "Для чего может понадобиться &kpager;?"

#. Tag: para
#: index.docbook:279
#, no-c-format
msgid ""
"&kpager; can be used as an alternative to the pager applet in the panel. It "
"has the advantage of being resizeable and within this able to show icon or "
"pixmap views of the running applications, move the windows across desktops "
"and run outside of the panel."
msgstr ""
"&kpager; может использоваться как альтернатива аплету переключателя окон на "
"панели. Его преимущество заключается в том, что он может показывать "
"изображения иконки и снимка запущенного приложения, передвигать окна между "
"рабочими столами и размещаться за пределами панели."

#. Tag: para
#: index.docbook:286
#, no-c-format
msgid "How can I change the behavior of &kpager;?"
msgstr "Как изменить режимы работы &kpager;?"

#. Tag: para
#: index.docbook:288
#, no-c-format
msgid ""
"Clicking the <mousebutton>right</mousebutton> mouse button anywhere within "
"&kpager; lets you chose <guilabel>Configure</guilabel> from the context menu "
"for displaying the <link linkend=\"kpager-settings\">settings dialog</link>"
msgstr ""
"Щелчок <mousebutton>правой</mousebutton> кнопки мыши в &kpager; позволяет "
"выбрать контекстное меню с пунктом <guilabel>Настройки</guilabel>, "
"вызывающим <link linkend=\"kpager-settings\">диалог настроек</link>."

#. Tag: para
#: index.docbook:296
#, no-c-format
msgid "Windows are transparent by default, how do I turn this off?"
msgstr "По умолчанию окна прозрачные. Как это отключить?"

#. Tag: para
#: index.docbook:299
#, no-c-format
msgid ""
"Currently, you cannot turn that option off within the config dialog, but you "
"can do it manually like this:"
msgstr ""
"На данный момент вы не можете отключить этот параметр в диалоге настроек, но "
"можете это сделать вручную следующим образом:"

#. Tag: para
#: index.docbook:302
#, no-c-format
msgid ""
"Open the file <filename>$KDEHOME/share/config/kpagerrc</filename> with any "
"text editor like &kedit; or <application>vi</application>. If you have no "
"rights to write that file, you may need to do it as root or contact your "
"system administrator. In this file you will have to add a new key with the "
"name <userinput>windowTransparentMode</userinput> with a number as value. "
"Values are:"
msgstr ""
"Откройте файл <filename>$KDEHOME/share/config/kpagerrc</filename> любым "
"текстовым редактором наподобие &kedit; или <application>vi</application>. "
"Если у вас нет прав для записи в этот файл, вероятно, вам придется сделать "
"это как root или связаться с вашим системным администратором. В этом файле "
"нужно добавить новый ключ с именем <userinput>windowTransparentMode</"
"userinput> и числовое значение:"

#. Tag: member
#: index.docbook:310
#, no-c-format
msgid "0 - No transparent windows at all."
msgstr "0 — отсутствие прозрачных окон."

#. Tag: member
#: index.docbook:311
#, no-c-format
msgid "1 - Only maximized windows are transparent."
msgstr "1 — только распахнутые окна прозрачные."

#. Tag: member
#: index.docbook:312
#, no-c-format
msgid "2 - all windows are transparent (default)."
msgstr "2 — все окна прозрачные (по умолчанию)."

#. Tag: title
#: index.docbook:323
#, no-c-format
msgid "Credits and License"
msgstr "Авторские права и лицензирование"

#. Tag: para
#: index.docbook:325
#, no-c-format
msgid "&kpager;"
msgstr "&kpager;"

#. Tag: para
#: index.docbook:329
#, no-c-format
msgid "Program copyright 2000 Antonio Larrosa <email>larrosa@kde.org</email>"
msgstr ""
"Программа: copyright 2000 Antonio Larrosa <email>larrosa@kde.org</email>"

#. Tag: para
#: index.docbook:333
#, no-c-format
msgid ""
"Documentation copyright 2000 by &Dirk.Doerflinger; &Dirk.Doerflinger.mail;"
msgstr ""
"Документация: &copy; 2000 Дирк Доэрфлингер (&Dirk.Doerflinger;) &Dirk."
"Doerflinger.mail;"

#. Tag: trans_comment
#: index.docbook:338
#, no-c-format
msgid "CREDITS_FOR_TRANSLATORS"
msgstr ""
"<para>Перевод на русский — Ольга Карпова <email>karpovolga@hotmail.com</"
"email></para>"

#. Tag: chapter
#: index.docbook:338
#, no-c-format
msgid "&underFDL; &underGPL;"
msgstr "&underFDL; &underGPL;"

#. Tag: title
#: index.docbook:346
#, no-c-format
msgid "Installation"
msgstr "Установка"

#. Tag: title
#: index.docbook:349
#, no-c-format
msgid "How to obtain &kpager;"
msgstr "Где взять &kpager;"

#. Tag: sect1
#: index.docbook:349
#, no-c-format
msgid "&install.intro.documentation;"
msgstr "&install.intro.documentation;"

#. Tag: title
#: index.docbook:356
#, no-c-format
msgid "Requirements"
msgstr "Требования"

#. Tag: para
#: index.docbook:358
#, no-c-format
msgid ""
"As &kpager; is part of the &package; package, you will just need an "
"installation of the main &kde; packages."
msgstr ""
"Поскольку &kpager; входит в пакет &package;, необходимо лишь установить "
"основные пакеты &kde;."

#. Tag: title
#: index.docbook:364
#, no-c-format
msgid "Compilation and Installation"
msgstr "Сборка и установка"

#. Tag: sect1
#: index.docbook:364
#, no-c-format
msgid "&install.compile.documentation;"
msgstr "&install.compile.documentation;"
