<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" 
"dtd/kdex.dtd" [
  <!ENTITY kappname "&kmenuedit;">
  <!ENTITY package "kdebase">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->
]>

<book lang="&language;">

<bookinfo>

<title
>Справочное руководство по &kmenuedit;</title>

<authorgroup>
<author
>&Milos.Prudek; &Milos.Prudek.mail;</author>

<othercredit role="reviewer"
>&Lauri.Watts; &Lauri.Watts.mail; </othercredit>
<othercredit role="translator"
><firstname
>Олег</firstname
><surname
>Баталов</surname
><affiliation
><address
><email
>olegbatalov@mail.ru</email
></address
></affiliation
><contrib
>Перевод на русский язык</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2000</year>
<holder
>&Milos.Prudek;</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2000-12-14</date>
<releaseinfo
>0.00.01</releaseinfo>

<abstract
><para
>&kmenuedit; позволяет редактировать главное меню &kde;. </para
></abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Редактор меню KDE</keyword>
<keyword
>kmenuedit</keyword>
<keyword
>приложение</keyword>
<keyword
>программа</keyword>
<keyword
>меню</keyword>
<keyword
>kicker</keyword>

</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
>&kmenuedit; позволяет редактировать главное меню &kde;.</para>

<para
>&kmenuedit; можно запустить щелчком правой кнопкой мыши по <inlinemediaobject
><imageobject
><imagedata fileref="i_k_button.png" format="PNG"/></imageobject
></inlinemediaobject
> кнопке <guiicon
>K</guiicon
> или выбрав пункт <guimenuitem
>Редактор меню</guimenuitem
> из подменю <guisubmenu
>Настройка &mdash; Другие</guisubmenu
> <guimenu
>Главного</guimenu
> меню.</para>

<para
>&kmenuedit; позволяет:</para>

<itemizedlist>
<listitem
><para
>Просматривать и редактировать <guimenu
>Главное</guimenu
> меню</para
></listitem>
<listitem
><para
><guimenuitem
>Вырезать</guimenuitem
>, <guimenuitem
>Копировать</guimenuitem
> и <guimenuitem
>Вставлять</guimenuitem
> пункты меню.</para
></listitem>
<listitem
><para
>Создавать и удалять подменю</para
></listitem>
</itemizedlist>

</chapter>

<chapter id="using-kmenuedit">
<title
>Использование &kmenuedit;</title>

<para
>На левой панели приложения изображена структура главного меню. При просмотре элементов левой панели, на правой отображается подробная информация по выбранному пункту меню.</para>

<sect1 id="details-general">
<title
>Общая информация о программе</title>

<variablelist>
<varlistentry>
<term
><guilabel
>Заголовок</guilabel
></term>
<listitem
><para
>Это название программы, которое будет показано в <guimenu
>Главном</guimenu
> меню. Оно может отличаться от реального имени исполняемого файла. Например, для <command
>mc</command
> заголовок выглядит как "<application
>Midnight Commander</application
>".</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Длинный заголовок</guilabel
></term>
<listitem
><para
>В этом поле программа описывается более подробно (поле необязательно для заполнения).</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Команда</guilabel
></term>
<listitem
><para
>Имя исполняемого файла программы. Убедитесь, что у вас есть права на запуск программы.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Тип</guilabel
></term>
<listitem
><para
>Это поле схоже с полем комментариев. Здесь можно описать тип программы.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Рабочий каталог</guilabel
></term>
<listitem
><para
>Укажите путь к рабочему каталогу программы. Этот каталог станет текущим при запуске программы, причем это не обязательно должен быть каталог, в котором находится исполняемый файл.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guiicon
>Список пиктограмм</guiicon
></term>
<listitem
><para
><inlinemediaobject
><imageobject
><imagedata 
fileref="icon_sets.png" format="PNG"/></imageobject
></inlinemediaobject
> Нажмите на эту кнопку, чтобы выбрать пиктограмму для программы.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Запускать в терминале</guilabel
></term>
<listitem
><para
>Установите этот флажок, если программа требует для работы эмулятор терминала. В основном это относится к <link linkend="gloss-console-application"
>консольным приложениям</link
>.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Параметры терминала</guilabel
></term>
<listitem
><para
>В этом поле можно указать все параметры терминала.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Запускать от имени другого пользователя</guilabel
></term>
<listitem
><para
>Если хотите запустить программу от имени другого пользователя, установите этот флажок и укажите его имя в поле <guilabel
>Имя пользователя</guilabel
>.</para
></listitem>
</varlistentry>
</variablelist>

<para
>Также можно для запуска программы назначить свою комбинацию клавиш.</para>

<para
>Для этого нажмите на кнопку справа от флажка <guilabel
>Текущая клавиша</guilabel
>, и нажмите нужную комбинацию клавиш.</para>

<para
>Появится окно, в котором можно назначить еще одну комбинацию, выбрав вариант <guilabel
>Альтернативно</guilabel
>. Это бывает полезно, например, если вы часто меняете раскладки клавиатуры и неудобно использовать одну комбинацию.</para>

<para
>Если бы ошиблись, нажмите кнопку рядом с переключателем, чтобы удалить комбинацию. Чтобы использовать более одной клавиши, установите флажок <guilabel
>Многоклавишный</guilabel
>.</para>

<para
>По умолчанию установлен флажок <guilabel
>Автозакрытие</guilabel
>, так что после выбора комбинации окно закроется. Если вы этого не хотите, снимите флажок.</para>

</sect1
> 

</chapter>

<chapter id="menu-reference">
<title
>Описание меню</title>

<variablelist>
<varlistentry id="file-new-item">
<term
><menuchoice
><guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_new_item.png" format="PNG"/></imageobject
></inlinemediaobject
>Файл</guimenu
><guimenuitem
>Новый элемент</guimenuitem
> </menuchoice
></term
> <listitem
><para
><action
>Добавить новый элемент меню.</action
></para
></listitem>
</varlistentry>

<varlistentry id="file-new-submenu">
<term
><menuchoice
><guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_new_submenu.png" format="PNG"/></imageobject
></inlinemediaobject
>Файл</guimenu
><guimenuitem
>Новое подменю</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Добавить новое подменю.</action
></para
></listitem>
</varlistentry>

<varlistentry id="file-quit">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
></shortcut
> <guimenu
>Файл</guimenu
><guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Завершить работу</action
> с &kmenuedit;.</para
></listitem>
</varlistentry>

<varlistentry id="edit-cut">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>X</keycap
></keycombo
></shortcut
> <guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_cut.png" format="PNG"/></imageobject
></inlinemediaobject
>Правка</guimenu
><guimenuitem
>Вырезать</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вырезать текущий элемент меню и поместить его в буфер обмена</action
>. Если вы хотите переместить элемент меню, сперва нужно вырезать его, перейти, используя левую панель, в место назначения и использовать функцию <guimenuitem
>Вставить</guimenuitem
>.</para
></listitem
> 
</varlistentry>

<varlistentry id="edit-copy">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
></shortcut
> <guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_copy.png" format="PNG"/></imageobject
></inlinemediaobject
>Правка</guimenu
><guimenuitem
>Копировать</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Скопировать текущий элемент меню в буфер обмена</action
>. После этого можно использовать функцию <guimenuitem
>Вставить</guimenuitem
>, чтобы поместить скопированный пункт в нужное место. Один и то же элемент можно вставлять несколько раз.</para
></listitem>
</varlistentry>

<varlistentry id="edit-paste">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>V</keycap
></keycombo
></shortcut
> <guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_paste.png" format="PNG"/></imageobject
></inlinemediaobject
> Правка</guimenu
><guimenuitem
>Вставить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вставить элемент меню из буфера обмена</action
> в выбранную часть <guimenu
>Главного</guimenu
> меню. Чтобы <guimenuitem
>Вставить</guimenuitem
> элемент меню, сперва нужно его <guimenuitem
>Вырезать</guimenuitem
> или <guimenuitem
>Копировать</guimenuitem
>.</para
></listitem>
</varlistentry>

<varlistentry id="edit-delete">
<term
><menuchoice
><guimenu
><inlinemediaobject
><imageobject
><imagedata fileref="i_delete.png" format="PNG"/></imageobject
></inlinemediaobject
>Правка</guimenu
> <guimenuitem
>Удалить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Удалить выбранный элемент меню.</action
></para
></listitem>
</varlistentry>
</variablelist>
&help.menu.documentation; <glossary id="glossary">
<title
>Глоссарий</title
> 

<glossentry id="gloss-terminal-emulator">
<glossterm
>Эмулятор терминала</glossterm>
<glossdef>
<para
>Эмулятор терминала &mdash; это просто оболочка (shell) в окне; в других системах это обычно называется <quote
>командной строкой</quote
>. Чтобы использовать оболочку, вам нужно знать хотя бы несколько команд уровня системы.</para>
</glossdef>
</glossentry>

<glossentry id="gloss-applet">
<glossterm
>Аплет</glossterm>
<glossdef
><para
>Небольшое приложение, требующее совсем немного памяти и места на экране, но дающее некоторую полезную информацию или предоставляющее комбинацию клавиш. Например, аплет <application
>Часы</application
> показывает текущие время и дату (и даже календарь на месяц при щелчке), аплет <application
>Системный монитор</application
> дает представление (в текущем времени) о том, насколько занята в данный момент ваша машина.</para
> </glossdef
> 
</glossentry>

<glossentry id="gloss-legacy-application">
<glossterm
><quote
>Чужое</quote
> приложение</glossterm>
<glossdef>

<para
>Приложение X-window, которое было написано без расчета на &kde;. Такие приложения прекрасно работают в &kde;, однако не получают автоматически сигнал при закрытии сеанса &kde;. Так что не забывайте сохранять открытые в таких приложениях документы перед выходом из &kde;. </para
> <para
>Также, многие из этих приложений не поддерживают копирование и вставку из приложений &kde;. Обозреватель &Netscape; 4.x яркий тому пример. Некоторые приложения <footnote
><para
><ulink url="http://www.gnome.org"
>GNOME</ulink
> имеют ограниченную поддержку взаимодействия с &kde;.</para
></footnote
>.</para>
</glossdef>
</glossentry>

<glossentry id="gloss-console-application">
<glossterm
>Консольные приложения</glossterm>
<glossdef>

<para
>Приложение, первоначально написанное в неграфической, ориентированной на текст среде. Такие приложения прекрасно работают в &kde;. Для этого им необходим эмулятор терминала, такой как &konsole;. Такие приложения не получают автоматически сигнал при закрытии сеанса &kde;. Так что не забывайте сохранять открытые в таких приложениях документы перед выходом из &kde;.</para>

<para
>Консольные приложения поддерживают копирование и вставку из собственных приложений &kde; или в них. Выделите текст в консольном приложении мышкой, переключитесь в приложение &kde; и нажмите <keycombo action="simul"
>&Ctrl; <keycap
>V</keycap
></keycombo
> для вставки. Если вы хотите копировать из приложения &kde; в консольное приложение, сначала выделите текст, нажмите <keycombo action="simul"
>&Ctrl; <keycap
>C</keycap
></keycombo
>, перейдите в консольное приложение и нажмите среднюю кнопку мыши.<footnote
><para
>Если у вашей мыши нет средней кнопки, можно нажать <mousebutton
>левую</mousebutton
> и <mousebutton
>правую</mousebutton
> кнопки одновременно. Это называется <quote
>эмуляцией средней кнопки</quote
> и должно поддерживаться вашей операционной системой.</para
></footnote
>.</para>

</glossdef>
</glossentry>

</glossary>
</chapter>

<chapter id="credits">

<title
>Авторские права и лицензирование</title>

<para
>&kmenuedit; </para>
<para
>Программа &copy; 2002, &Raffaele.Sandrini;</para>

<para
>Участники проекта:</para>
<itemizedlist>
<listitem
><para
>&Matthias.Elter; &Matthias.Elter.mail; - Оригинальный автор</para
> 
</listitem>
<listitem
><para
>&Matthias.Ettrich; &Matthias.Ettrich.mail;</para>
</listitem>
<listitem
><para
>&Daniel.M.Duley; &Daniel.M.Duley.mail;</para>
</listitem>
<listitem
><para
>&Preston.Brown; &Preston.Brown.mail;</para>
</listitem>
</itemizedlist>

<para
>Авторское право на документацию &copy; 2000 &Milos.Prudek;</para>

<para
>Обновлено для &kde; 3.0 &Lauri.Watts; &Lauri.Watts.mail; 2002</para>

<para
>Перевод на русский - Екатерина С. Пыжова <email
>haleth@yandex.ru</email
></para
> <para
>Обновление перевода - Олег Баталов<email
>olegbatalov@mail.ru</email
></para
>  
&underFDL; &underGPL; </chapter>



&documentation.index; 
</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes: nil
sgml-general-insert-case: lower
End:
-->

