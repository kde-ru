<chapter id="control-center">
<!-- Uncomment the <*info
> below and add your name to be -->
<!-- credited for writing this section. -->

<!--
<chapterinfo>
<authorgroup
>Here you can set up different profiles for your Wireless card,
to be able to quickly switch settings if you connect to multiple
networks. You can select a profile to be loaded on KDE startup.
<author>
<firstname
>Your First Name here</firstname>
<surname
>Your Surname here </surname>
</author>
</authorgroup>
</chapterinfo>
-->

<title
>Центр управления KDE</title>

<indexterm
><primary
>&kcontrolcenter;</primary
></indexterm>
<indexterm
><primary
>&kcontrol;</primary
></indexterm>
<indexterm
><primary
>Настройка</primary
></indexterm>
<indexterm
><primary
>Адаптация</primary
></indexterm>

<mediaobject>
<imageobject>
<imagedata fileref="kcontrol.png" format="PNG"/>
</imageobject>
<textobject
><phrase
>Центр управления</phrase
></textobject>
<caption
><para
>Центр управления</para
></caption>
</mediaobject>

<para
>Центр управления KDE - это место, где можно изменить настройки, влияющие на всю среду &kde;. Вы можете открыть его, используя пункт меню <guimenuitem
>Центр управления </guimenuitem
> в &kmenu; или набрав в командной строке <filename
>kcontrol</filename
>.</para>

<para
>Параметры поделены по нескольким основным категориям, каждая из которых содержит несколько вкладок с параметрами. Для отображения страницы установок раскройте основную категорию, щёлкнув на <guilabel
>+</guilabel
> кнопке следующей за ней и затем щёлкните на имени нужной страницы. Страница с параметрами появится при этом справа, и вы сможете менять желаемые параметры. Никакие изменения не вступят в силу, пока вы не нажмёте кнопку <guibutton
>Применить</guibutton
>. Если вы передумаете после того как произвели некоторые изменения и хотите восстановить параметры такими, какими они были до этого, просто нажмите кнопку <guibutton
>Сбросить</guibutton
>.</para>

<para
>Если вам необходима дополнительная помощь по странице, посетите страницу, нажав на вкладке  <guilabel
>Справка</guilabel
>. Возможно вы захотите заглянуть в руководство по &kcontrolcenter; ,которое вы сможете открыть через меню <menuchoice
><guimenu
>Справка</guimenu
><guimenuitem
>&kcontrolcenter; Руководство</guimenuitem
></menuchoice
>.</para>


<sect1 id="kcontrol-appearance-and-themes">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>

<title
>Внешний вид &amp; Темы</title
> 

<para
>Здесь вы найдёте установки, которые изменят настройки &kde; рабочий стол и внешний вид приложений.</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Фон</guilabel
></term>
<listitem>
<para
>Этот раздел управляет цветом или изображением, которые установлены как фон рабочего стола. Эти параметры могут быть применены ко всем виртуальным рабочим областям или только к какой-то одной конкретной. Множество фоновых обоев поставляется вместе с &kde; или вы можете подобрать свои собственные.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Цвета</guilabel
></term>
<listitem>
<para
>Здесь можно изменить цвета ваших приложений kde. Множество цветовых схем поставляется вместе с &kde; по умолчанию, другие вы можете найти на kde-look.org. Также вы можете создавать свои собственные. Также вы можете изменять контраст и выбирать где бы вам хотелось применить цвета  &kde; к не приложениям не из kde, для целостности общего внешнего вида.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Шрифты</guilabel
></term>
<listitem>
<para
>Здесь вы можете настраивать различные параметры шрифтов в &kde; приложениях. Также вы сможете редактировать установки сглаживания, включая ряд шрифтов для исключения их из списка автоматического сглаживания.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Пиктограмы</guilabel
></term>
<listitem>
<para
>Здесь можно управлять темами значков и прочими параметрами, относящимися к значкам. Новые темы значков можно скачать с kde-look.org, и установить здесь. Вы можете также и удалить тему значков, выбрав её в списке и нажав "Удалить". Также вы можете установить размеры значков для разных нужд в &kde; и применить к значкам дополнительные эффекты.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Запустить обратную связь (реакцию)</guilabel
></term>
<listitem>
<para
>Здесь вы  можете изменять какой вид курсора и/или реакция на панели задач, которая вам нравится при загрузке приложений. Вы также можете установить длительность отображения процесса загрузки. Например, по умолчанию установлено мигание курсора длительностью до 30 секунд, или до загрузки приложения.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Хранитель экрана</guilabel
></term>
<listitem>
<para
>Здесь вы можете задать установки для хранителя экрана. Вы можете установить время бездействия до старта хранителя, а также необходимость проверки пароля для разблокирования экрана. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Заставка</guilabel
></term>
<listitem>
<para
>Здесь можно установить, удалить и протестировать экранную заставку, появляющуюся при запуске  &kde;. Дополнительные заставки можно скачать с <ulink url="http://www.kde-look.org"
> http://www.kde-look.org</ulink
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Стиль</guilabel
></term>
<listitem>
<para
>В этом разделе можно менять стиль графических элементов. Множество стилей поставляется с  &kde;, многие могут быть загружены с <ulink url="http://www.kde-look.org"
>http://www.kde-look.org</ulink
>. Также здесь вы можете включить или выключить такие опции интерфейса, как прозрачные меню, показ значков на кнопках и всплывающих подсказках. Одни стили имеют больше параметров, другие - меньше.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Темы</guilabel
></term>
<listitem>
<para
>Здесь можно создавать и управлять пользовательскими темами. Темы являются комбинацией фона, хранителя экрана, декорации окна, набора значков, шрифтов и стиля графических элементов. Это позволяет сохранить предпочтительное оформление и применить его одним нажатием кнопки мыши.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Декорации окна</guilabel
></term>
<listitem>
<para
>Здесь можно настраивать оформление окна. Вы можете изменять как стиль, так и расположение кнопок. Одни окна имеют больше опций декорирования, другие меньше.</para>
</listitem>
</varlistentry>
</variablelist>

<!-- Add links to "further reading" here -->
<itemizedlist>
<title
>Дополнительная информация</title
> <listitem
><para
>Если &kcontrolcenter; не содержит необходимых настроек, вероятно придётся вручную редактировать конфигурационный файл. Смотрите <xref linkend="hand-editing-config-files"/> для дополнительной информации о том, как это сделать.</para>
</listitem>

<listitem
><para
>Если смена внешнего вида вашего рабочего стола в &kde; доставляет вам радость, вы сможете найти множество тем и стилей по адресу <ulink url="http://kde-look.org"
>kde-look.org</ulink
>.</para>
</listitem>

</itemizedlist>


</sect1>

<sect1 id="kcontrol-desktop">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>
<title
>Рабочий стол</title>

<para
>Здесь содержатся опции для настройки внешнего вида и поведения рабочего стола в  &kde;.</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Поведение</guilabel
></term>
<listitem>
<para
>Здесь вы сможете настроить поведение рабочего стола. Это то место куда вы можете пойти для установки таких настроек как скрытие или отображение значков рабочего стола, всплывающих подсказок и раскладки значков. Также вы сможете настроить предпросмотр основных видов файлов и какие из устройств вы хотите чтобы отображались в виде значков.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Несколько рабочих столов</guilabel
></term>
<listitem>
<para
>Здесь можно настроить количество виртуальных рабочих столов или рабочих мест и то, как бы вы хотели их вызывать. По умолчанию &kde; имеет 4 виртуальных рабочих стола, и вы можете увеличить их число до 20. Также вы можете настроить переключение между виртуальными рабочими столами, используя колесо мыши.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Панели</guilabel
></term>
<listitem>
<para
>Здесь можно изменить параметры для &kicker; и других панелей &kde;. Среди параметров размер, положение, длина и скрытие. Вы также можете изменять внешний вид панели, её прозрачность, фоновый рисунок и масштабирование значков. Также здесь вы можете настроить различные опции меню, включая то, какие из приложений поместить в &kmenu;.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Панель задач</guilabel
></term>
<listitem>
<para
>Модуль панели задач позволяет настраивать параметры, относящиеся к вашей панели задач. Вы можете настраивать одновременно для отображения окон со всех рабочих мест, группировать одинаковые задачи и то, какие действия требуется назначить для кнопок мыши.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Поведение окна</guilabel
></term>
<listitem>
<para
>Здесь можно настроить параметры, относящиеся к поведению оконного менеджера &kwin;. &kwin; чрезвычайно настраиваем и имеет продвинутые возможности, такие как предотвращение перехвата фокуса и различные политики фокуса, как следование фокуса за курсором мыши. Вы также можете настроить, какие действия требуется назначить определённым клавишам и событиям мыши.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Параметры отдельных окон</guilabel
></term>
<listitem>
<para
>Это продвинутое  диалоговое окно настройки, где вы можете установить свойства поведения конкретных окон. Существует множество параметров для тонкой настройки  раскладки окна, включая то, какие позиции на экране должны занимать определённые окна при открытии, и  как оно должно отображаться на панели задач или на переключателе рабочих столов. Вы можете указать окна по приложению, или даже по специфичной роли внутри приложения.</para>
</listitem>
</varlistentry>
</variablelist>

<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
> -->


</sect1>

<sect1 id="kcontrol-internet-and-network">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>
<title
>Сеть и Интернет</title
> 

<para
>Здесь можно настроить работу с Интернет и локальной сетью в &kde;.</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Общий рабочий стол</guilabel
></term>
<listitem>
<para
>Общий рабочий стол позволяет совместно работать в текущем сеансе, или делает возможным вход на вашу машину из другого места. Затем вы сможете использовать  VNC-подобную программу клиента  из &kde; - Приложение удалённого доступа (Remote Desktop Connection) для управления рабочим столом по сети. Приложение очень полезно, в случае, когда вам понадобится чья-то помощь.  </para>

<para
>Здесь можно создать и управлять приглашениями, а также правилами защиты для соединений без приглашения. Также вы можете настроить, будет ли показано фоновое изображение и на каком порту будет ожидать вызов служба. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Общий доступ к файлам</guilabel
></term>
<listitem>
<para
>Служба Samba (&Microsoft; &Windows;) и <acronym
>NFS</acronym
> (&UNIX;) служит для общего доступа к файлам. Для того чтобы изменять настройки данного модуля, необходим пароль пользователя root или пароль администратора. Здесь вы сможете указать каким пользователям позволен доступ к файлам без знания пароля root, и каким пользователям позволено это назначать. Вы также можете настроить, какие папки следует сделать общедоступными, используя определённый уровень доступа, и кому позволено просматривать эти ресурсы.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Просмотр локальной сети</guilabel
></term>
<listitem>
<para
>Здесь можно настроить параметры, относящиеся к просмотру сетевых ресурсов в &konqueror;. &konqueror; способен просматривать различные сетевые ресурсы и работать с удалёнными файлами так, как будто они на вашей машине. Вы можете указать ему запомнить ваше предпочтительное имя регистрации и пароль для соединения с &Windows; ресурсами (Samba). Вы можете также указать какой тип сетевых ресурсов вы бы хотели просматривать, включая &FTP;, <acronym
>NFS</acronym
> и <acronym
>SMB</acronym
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Чат в локальной сети</guilabel
></term>
<listitem>
<para
>Данный модуль позволяет настраивать параметры, относящиеся к &UNIX; <command
>talk</command
> демону. Это очень простая сетевая программа для общения, работающая в терминале, созданная для общения по локальной сети. Некоторые её свойства можно задать, такие как автоответчик, который будет отправлять вам по email входящие сообщения и будет пересылать сообщения в другое место.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Свойства</guilabel
></term>
<listitem>
<para
>Здесь можно установить дополнительные сетевые параметры, такие как значение времени ожидания сетевого соединения. Рекомендуем оставить эти значения параметров без изменений, если вы не знаете точно их назначения.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Прокси</guilabel
></term>
<listitem>
<para
>Здесь можно настроить &kde; для соединения с прокси сервером, а не напрямую с интернетом. Рекомендуется не изменять эти параметры, если вы не знаете, зачем вам это нужно. Если используется прокси сервер, то системный администратор сможет дать вам детальную информацию о настройках, которые сюда нужно внести.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Параметры Samba</guilabel
></term>
<listitem>
<para
>Модуль настройки службы Samba требует прав <systemitem class="username"
>root</systemitem
> или администратора. Это продвинутый инструмент для настройки, который позволяет вам контролировать безопасность сервера Samba, общие ресурсы, пользователей и принтеры в интуитивно понятном графическом интерфейсе. Это очень мощный инструмент с поддержкой настройки всего - от простого файла и принтера до использования вашего Samba сервера в качестве контроллера домена &Windows; NT.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Web-браузер</guilabel
></term>
<listitem>
<para
>Данный модуль предназначен для настройки параметров относящихся к программе &konqueror; в качестве web-браузера. Обычные настройки вы можете найти в самом браузере, такие как настройки cookie, кэша и журнала, здесь задаются параметры комбинаций клавиш, модулей и шрифтов.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Беспроводная сеть</guilabel
></term>
<listitem>
<para
>Здесь можно задать различные профили для беспроводной сетевой карты, для возможности быстрого переключения настроек в случае, если вы соединяетесь с несколькими сетями. Вы можете указать, какой профиль будет активизирован при запуске  &kde;.</para>
</listitem>
</varlistentry>
</variablelist>


<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
>-->


</sect1>

<sect1 id="kcontrol-kde-components">


<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>

<title
>Компоненты &kde;</title>

<para
>Здесь можно изменять дополнительные настройки &kde; , такие как связи файлов и приложения по умолчанию.</para>

<variablelist>

<varlistentry
><term
><guilabel
>Выбор компонентов</guilabel
></term>

<listitem
><para
>Программа выбора компонентов позволяет выбрать предпочтительные приложения для работы с различными службами. Здесь вы можете задать программу клиента Email Client, встроенный текстовый редактор, средство быстрого обмена сообщениями, программу эмуляции терминала и Web-браузер. Если вам нравится <application
>Xterm</application
>, <application
>Vim</application
> или  <application
>Mozilla</application
>, это как раз то место, где можно установить эти предпочтения.</para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Связи файлов</guilabel
></term>

<listitem
><para
>Здесь можно настроить все связи файлов. Вы можете указать тип файла, и выбрать с каким приложением он будет открываться. Также вы можете указать каким значком будет представлен каждый тип файла, и выбрать способ открытия - встроенной или отдельной программой. </para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Файловый менеджер</guilabel
></term>

<listitem
><para
>Здесь можно настроить поведение программы &konqueror; в режиме файлового менеджера. Доступны такие параметры, как шрифт и размер шрифта, перечень различных сетевых протоколов и контекстных меню. &konqueror; - это очень мощный и настраиваемый инструмент управления файлами, изобилующий настройками. За дополнительной информацией обратитесь к руководству по &konqueror;.</para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Быстродействие KDE</guilabel
></term>

<listitem
><para
>Здесь располагаются настройки, относящиеся к использованию памяти программой &konqueror;. <guilabel
>Минимизировать использование памяти</guilabel
> позволяет указать, будет ли вновь открываемый экземпляр &konqueror; открываться как новый экземпляр программы или вновь открываемые окна &konqueror; будут обращаться к единому экземпляру программы в памяти. Это позволяет снизить затраты памяти. Также вы можете установить предварительный запуск &konqueror; сразу после старта &kde; , что позволит сократить время его запуска.</para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Настройка ресурсов KDE</guilabel
></term>

<listitem
><para
><remark
>Не готово</remark
></para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Управление службами</guilabel
></term>

<listitem
><para
>Модуль управления службами показывает статический список служб, которые запускаются по запросу, и второй список  служб, которыми управляет пользователь. Службы из первого списка нельзя изменить. Службы из второго списка вы можете активизировать или убрать их загрузку при запуске и запускать их вручную.</para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Менеджер сеансов</guilabel
></term>

<listitem
><para
>Здесь можно настроить управление сеансами &kde;. Можно задать, чтобы &kde; запоминала сеанс и восстанавливала приложения при следующем входе в систему. Можно также исключить определённые приложения из управления сеансами или вообще его выключить.</para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Проверка орфографии</guilabel
></term>

<listitem
><para
>Здесь можно настроить проверку орфографии в &kde;. Модуль позволяет задать, какую из программ проверки орфографии следует использовать, какие типы ошибок проверять и какой словарь использовать по умолчанию. Поддерживаются обе программы, <application
>ASpell</application
> и <application
>ISpell.</application
></para>
</listitem>
</varlistentry>


<varlistentry
><term
><guilabel
>Конфигурация компонента Vim</guilabel
></term>

<listitem
><para
>Данный модуль позволяет настроить использование программы <application
>Vim</application
> как встроенного компонента. Для этого необходимо, чтобы на компьютере были установлены свежие версии программы <application
>Gvim</application
> или  <application
>Kvim</application
>. Вы можете настроить внешний вид редактора и задать путь к программе <command
>vim</command
>.</para>
</listitem>
</varlistentry>

</variablelist>

<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
> -->


</sect1>

<sect1 id="kcontrol-peripherals">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>
<title
>Периферия</title>

<para
>Здесь можно изменить параметры периферийных устройств, таких как клавиатура и джойстик.</para>

<variablelist>
<varlistentry
><term
><guilabel
>Дисплей</guilabel
></term>

<listitem
><para
>Здесь можно изменить параметры дисплея, такие как размер, ориентацию и частоту обновления и указать, применять ли эти установки при загрузке &kde;. На вкладке <guilabel
>Управление питанием</guilabel
> вы можете настроить установки энергосбережения, например, гашение экрана. </para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Джойстик</guilabel
></term>

<listitem
><para
>Здесь можно настроить джойстик и протестировать правильность его работы. Также здесь вы можете откалибровать джойстик и вручную установить устройство, если джойстик не был корректно распознан автоматически. </para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Клавиатура</guilabel
></term>

<listitem
><para
>Этот модуль позволяет настроить основные параметры клавиатуры, такие как время задержки  и частота повтора, а также предпочтительный режим клавиши NumLock при загрузке KDE.</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Мышь</guilabel
></term>

<listitem
><para
>Здесь можно настроить параметры мыши. Вы можете переключить последовательность кнопок, установить обратное направление прокрутки или изменить поведение нажимаемых значков. Вы также можете просмотреть, установить и выбрать темы для курсора. Вкладка <guilabel
>Дополнительно</guilabel
> позволит провести тонкую настройку параметров устройства.</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Принтеры</guilabel
></term>

<listitem
><para
>Здесь можно настроить печать, используя различные системы печати. Вы можете добавить локальные или удалённые принтеры, просмотреть текущие задания печати и параметры принтеров.</para>
</listitem>
</varlistentry>

</variablelist>

<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
> -->


</sect1>

<sect1 id="kcontrol-power-control">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>

<title
>Управление питанием</title>

<para
>Данный раздел имеет единственный модуль, <guilabel
>Аккумулятор ноутбука</guilabel
>. Здесь можно настроить внешний вид и поведение монитора батарей <application
>Klaptopdaemon</application
>. Вы можете выбрать значки для отображения различных уровней зарядки и установить предупреждения для связанных событий. Если уровень аккумулятор разрядится до критического уровня, вы можете настроить демон для корректного завершения работы вашего переносного компьютера, чтобы защититься от потери данных. </para>

<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
>-->

</sect1>

<sect1 id="kcontrol-regional-and-accessibility">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>

<title
>Региональные и специальные возможности</title>
<para
>Здесь можно настроить параметры для региона и локали, а также специальные возможности для пользователей с ограниченными физическими возможностями.</para>

<variablelist>

<varlistentry
><term
><guilabel
>Специальные возможности</guilabel
></term>

<listitem
><para
>Здесь можно настроить параметры для пользователей, имеющих проблемы со слухом или использованием клавиатуры. Вы можете настроить сопровождение системных звуков визуальными сигналами, такими как мигание экрана или инверсия цветов. Также вы можете настроить такие параметры как "липкие клавиши" и "медленные клавиши".</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Страна/область и язык</guilabel
></term>

<listitem
><para
>Этот модуль позволяет настраивать параметры, специфичные для вашей страны , такие как язык, валюта и формат даты. Для доступа к другим языкам установите соответствующие пакеты kde-i18n.</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Действия по вводу</guilabel
></term>

<listitem
><para
>Здесь можно настроить действия, такие как действия мыши и комбинации клавиш клавиатуры для запуска приложений и выполнения команд.</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Раскладка клавиатуры</guilabel
></term>

<listitem
><para
>Данный модуль позволяет настроить приложение <application
>Kxkb</application
>, переключатель раскладок клавиатуры, используемых в модуле xkb системы &X-Window;. Программа позволяет переключаться между различными раскладками, используя индикатор в системной панели или комбинацию клавиш. Вы можете включить или выключить раскладку клавиатуры непосредственно в этом диалоговом окне или добавить другие раскладки. Дополнительные параметры позволяют переключать раскладку глобально для всех приложений или для каждого окна в отдельности.</para
></listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Комбинации клавиш</guilabel
></term>

<listitem
><para
>Здесь можно настроить глобальные комбинации клавиш для &kde;. Существует несколько готовых схем комбинаций клавиш, которые вы можете использовать, если привыкли к ним в &Windows; или &MacOS;. Также вы можете создать свою собственную схему и настроить свои комбинации клавиш. </para
></listitem>
</varlistentry>
</variablelist>

<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
> -->


</sect1>

<!--<sect1 id="kcontrol-security-and-privacy">

<title
>Security &amp; Privacy</title>
<para
>to be written</para>

<itemizedlist>
<title
>Related Information</title>
<listitem
><para
></para>
</listitem>
</itemizedlist>

</sect1
>-->

<!-- <sect1 id="kcontrol-sound-and-multimedia">
<title
>Sound &amp; Multimedia</title>
<para
>to be written</para>

<itemizedlist>
<title
>Related Information</title>
<listitem
><para
></para>
</listitem>
</itemizedlist>

</sect1
> -->

<sect1 id="kcontrol-system-administration">
<sect1info>
<authorgroup>
<author
>&J.Hall; &J.Hall.mail; </author>
</authorgroup>
</sect1info>

<title
>Системное администрирование</title>
<para
>Данный модуль позволяет настроить такие аспекты системы, как начальный загрузчик и ядро и поможет выполнить необходимые системные задачи. Большинство параметров в данном разделе требуют знания пароля root или администратора для того, чтобы вносить изменения.</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Менеджер загрузки (LILO)</guilabel
></term>
<listitem
><para
>Если вы используете популярный системный загрузчик <acronym
>LILO</acronym
>, то здесь можно его настроить. Вы можете настроить расположение модуля, задать интервал времени для экрана загрузки <acronym
>LILO</acronym
>, а также добавить или изменить загружаемые модули и операционные системы в список начальной загрузки.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Дата и время</guilabel
></term>
<listitem
><para
>Данный модуль позволяем настроить системную дату и время. Вы можете установить дату, время и также временную зону. Данные параметры применимы для всей системы в целом.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Установка шрифтов</guilabel
></term>
<listitem
><para
>Здесь вы сможете настроить как персональные, так и системные шрифты. Это диалоговое окно позволяет установить новый шрифт, удалить старый и просмотреть установленные шрифты. По умолчанию отображаются персональные шрифты. Для изменения системных шрифтов нажмите кнопку <guibutton
>Режим администратора</guibutton
>.</para
> </listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Ноутбук IBM Thinkpad</guilabel
></term>
<listitem
><para
>Этот модуль позволяет настроить специальные клавиши на ноутбуке IBM Thinkpad. Для использования этой возможности необходим установленный модуль <quote
>nvram</quote
>.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Ядро Linux</guilabel
></term>
<listitem
><para
>Если вы используете &kde; в &Linux;, то этот модуль &kcontrol; позволяет создавать или изменять конфигурационные файлы для ядра &Linux;. Этот конфигуратор совместим с ядром версии не выше 2.5.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Менеджер входа в систему</guilabel
></term>
<listitem
><para
>Данный модуль позволяет настроить менеджер входа в систему &kde; - &kdm;. &kdm; - это мощный менеджер входа с широким спектром параметров. Он поддерживает переключение пользователей, удалённое подключение в графическом режиме и имеет полностью настраиваемый внешний вид. Дополнительная информация приведена в руководстве &kdm;. </para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Пути</guilabel
></term>
<listitem
><para
>В этом окне можно настроить основные каталоги для расположения важных файлов. Каталог <guilabel
>Рабочий стол</guilabel
> содержит все файлы с вашего рабочего стола. Каталог <guilabel
>Autostart</guilabel
> содержит файлы или ссылки на файлы, которые требуется запустить во время запуска &kde; , и папка <guilabel
>Документы</guilabel
> является каталогом по умолчанию, откуда приложения из &kde; будут открывать и куда будут сохранять документы..</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Ноутбук Sony Vaio</guilabel
></term>
<listitem
><para
>Этот модуль позволяет настроить дополнительные функции для ноутбука Sony Vaio. Если у вас есть Sony Vaio, то для использования этого раздела необходимо установить драйвер <quote
>sonypi</quote
>.</para
></listitem>
</varlistentry>
</variablelist>
<!-- Add links to "further reading" here -->
<!-- <itemizedlist>
<title
>Related Information</title>
<listitem
><para
>to be written</para>
</listitem>
</itemizedlist
>-->
</sect1>
</chapter>

<!-- Keep this comment at the end of the file
Local variables:
mode: xml
sgml-omittag:nil
sgml-shorttag:nil
sgml-namecase-general:nil
sgml-general-insert-case:lower
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:0
sgml-indent-data:true
sgml-parent-document:("index.docbook" "book" "chapter")
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->

