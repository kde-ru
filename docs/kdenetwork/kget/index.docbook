<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!-- Define an entity for your application if it is not part of KDE
       CVS -->
  <!ENTITY kget "<application
>KGet</application
>">
  <!ENTITY kappname "&kget;"
><!-- replace kget here
                                            do *not* replace kappname-->
  <!ENTITY package "kdenetwork">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Russian "INCLUDE"
> 
]>

<book lang="&language;">

<bookinfo>
<title
>Руководство &kget;</title>

<authorgroup>
<author
><personname
> <firstname
>Jonathan</firstname
> <othername
>E.</othername
> <surname
>Drews</surname
> </personname
> <email
>j.e.drews@att.net</email
> </author>

<othercredit role="translator"
><firstname
>Олег</firstname
> <surname
>Баталов</surname
> <affiliation
><address
> <email
>batalov@twiga.kz</email
> </address
></affiliation
> <contrib
>Перевод на русский</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2003</year>
<holder
>Jonathan E. Drews</holder>
</copyright>
<legalnotice
>&FDLNotice;</legalnotice>

<date
>2003-11-11</date>
<releaseinfo
>0.01.00</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kget; позволяет вам группировать загрузки. В большинстве случаев &kget; может продолжить, если компьютер был выключен, пока загрузка не завершена. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>kget</keyword>
<keyword
>kppp</keyword>
<keyword
>nothing else</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
>Для запуска загрузки документа или пакета, перетащите его &URL; на &kget;. </para>
</chapter>

<chapter id="using-kget">
<title
>Использование &kget;</title>

<sect1 id="kget-features">
<title
>Руководство &kget;</title>

<para
>Это краткая обучающая программа по некоторым особенностям &kget;. Загрузим три файла. Предположим, что вы хотите загрузить сначала средний и затем первый и последний. </para>


<orderedlist>
<listitem>
<para
>Переведите в автономный режим. Щёлкните на <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_on.png" format="PNG"/> </imageobject
></inlinemediaobject
>чтобы изменить его на <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_off.png" format="PNG"/> </imageobject
></inlinemediaobject
>. </para>
</listitem>

<listitem>
<para
>Щёлкните на первой записи с помощью &LMB;. Удерживая нажатой клавишу &Ctrl; щёлкните на нижней записи. &kget; должен выглядеть подобно: <screenshot>
<screeninfo
>Снимок экрана &kget;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="kget1.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Элементы, которые будут загружены в Konqueror</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>


</listitem>

<listitem
><para
>Щёлкните на кнопке задержки <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_delay.png" format="PNG"/> </imageobject
></inlinemediaobject
>, чтобы препятствовать загрузки этих элементов. </para
></listitem>

<listitem
><para
>Теперь щёлкните на среднем элементе, чтобы выделить его. Верхний и нижний элементы больше не будут выделены. <screenshot>
<screeninfo
>Снимок экрана &kget;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="kget2.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Элементы, которые будут загружены в Konqueror</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para
></listitem>

<listitem
><para
>Щёлкните на <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_off.png" format="PNG"/> </imageobject
></inlinemediaobject
> чтобы изменить его на режим в сети <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_on.png" format="PNG"/> </imageobject
></inlinemediaobject
> и &kget; начнёт загрузку среднего элемента. </para
></listitem>

<listitem
><para
>Щёлкните на первой записи, и удерживая клавишу &Shift; на последней. &kget; должен выглядеть подобно: <screenshot>
<screeninfo
>Снимок экрана &kget;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="kget3.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Элементы, которые будут загружены в Konqueror</phrase>
	  </textobject>
	</mediaobject>
</screenshot>


<note
><para
>Удержание клавиши &Ctrl; позволяет вам щелчками &LMB; выбирать индивидуальные элементы, удержание &Shift; позволяет последовательные элементы. </para
></note
></para>

</listitem>

<listitem
><para
>Теперь щёлкните на <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_queue.png" format="PNG"/> </imageobject
></inlinemediaobject
> для загрузки выделенных элементов. </para
></listitem>

</orderedlist>
</sect1>
</chapter>

<chapter id="commands">
<title
>Описание команд</title>

<sect1 id="kapp-mainwindow">
<title
>Главное окно &kget;</title>

<sect2>
<title
>Меню <guimenu
>Файл</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>O</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Открыть</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Открывает окно закачки, куда вы можете вставить &URL;. Короткая кнопка <inlinemediaobject
><imageobject>
	    <imagedata fileref="fileopen.png" format="PNG"/>
	  </imageobject
></inlinemediaobject
> </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>V</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Вставить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вставляет содержимое буфера обмена в окно закачки. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_paste.png" format="PNG"/>
	  </imageobject
></inlinemediaobject
></action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Экспорт списка закачек...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Открывает диалог Сохранить как</action
>, который позволяет сохранить выделенный &URL; как файл <filename
>.kget</filename
>. Использование этой особенности: </para>
<itemizedlist>
<listitem
><para
>Переведите в автономный режим, нажав <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_on.png" format="PNG"/> </imageobject
></inlinemediaobject
>чтобы сделать это <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_off.png" format="PNG"/> </imageobject
></inlinemediaobject
>. </para
></listitem>
<listitem
><para
>Перетащите на  &kget; &URL; которые вы хотите загрузить </para
></listitem>

<listitem
><para
>Затем щёлкните на первом элементе чтобы он стал выделенным. </para
></listitem>

<listitem
><para
>Удерживая клавишу <userinput
><keycombo
> <keycap
>Shift</keycap
></keycombo
></userinput
>щёлкните на последнем &URL;, чтобы выделить элементы подобно: <screenshot>
<screeninfo
>Изображение &kget; сохраняющего экспортируемый файл</screeninfo>
	<mediaobject>
               <imageobject>
	    <imagedata fileref="kget4.png" format="PNG"/>
	       </imageobject>
        </mediaobject>
</screenshot>
</para
></listitem>

<listitem
><para
>Теперь выберите в меню <menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Экспорт списка закачек...</guimenuitem
></menuchoice
> и введите имя файла <filename
>.kget</filename
> для ваших закачек. </para
></listitem>
</itemizedlist>


<para
>Это особенность используется для сохранения элементов, которые загружаются периодически, типа снапшотов репозитория &kde;. </para>
</listitem>

</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>&URL;</guimenu
> <guimenuitem
>Импорт списка закачек...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Загрузка файла <filename
>.kget</filename
> который был создан командой<menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Экспорт списка закачек ...</guimenuitem
></menuchoice
> </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Импорт текстового файла...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Импорт &URL;, которые находятся в текстовых файлах.</action
> Это мощная возможность позволяющая импортировать &URL; из текстовых файлов и полученных вами писем. Это может различать обычный текст и &URL;, предполагая что &URL; начинается в левой части документа. &kget; находит &URL; и помещает в главное окно. <note
><para
>Эта особенность игнорирует обычный текст только если вы нажмёте пиктограмму режима эксперта <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_expert.png" format="PNG"/> </imageobject
></inlinemediaobject
>. Если режим эксперта не используется тогда текстовый файл должен содержать только  &URL;. </para
></note
> 
</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Выйти</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Выход из &kget;</action
></para
></listitem>
</varlistentry>
</variablelist>


</sect2>

<sect2>
<title
>Меню <guimenu
>Вид</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Окно журнала</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Открывает окно журнала, отображающее произошедшие события. Это полезно, для просмотра событий в процессе долговременной загрузки. Здесь вы можете увидеть, какие пакеты были пропущены либо произошло превышение времени операции. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_logwindow.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Цель для перетаскивания</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Цель для перетаскивания это пиктограмма на экране позволяющая скрывать и восстанавливать окно &kget;. Она используется на приведённом в беспорядок рабочем столе, когда вы ходите периодически проверять статус загрузки. Короткая кнопка  <inlinemediaobject
><imageobject>
    <imagedata fileref="cr22-action-tool_drop_target.png" format="PNG"/>
	  </imageobject
></inlinemediaobject
> </action
></para
></listitem>
</varlistentry>
</variablelist>

</sect2>

<sect2>
<title
>Меню Передачи</title>
<note
><para
>Чтобы сделать активной (не недоступной) загрузку для пунктов этого меню, вы должны выделить её щёлкнув на ней &LMB;. </para
></note>
<para>
<variablelist>
<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Копировать &URL; в буфер обмена</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Помещает выделенную запись в буфер обмена &kde; (Klipper). </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Открыть отдельное окно</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отобразить текущую закачку в отдельном окне. </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Переместить в начало</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Перемещает выделенную запись в начало списка закачек.</action>
<note
><para
>&URL; из начала списка &kget; загружает в первую очередь.</para>
</note>
</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Переместить конец</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Перемещает выделенную запись в конец списка закачек. </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Продолжить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Возобновляет загрузку, которая была приостановлена. Короткая клавиша  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_resume.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Пауза</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Приостанавливает загрузку если она запущена. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_pause.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>


<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Удалить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Удаляет выделенную запись из главного окна &kget;. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_delete.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Перезапустить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Комбинирует функции Паузы и Восстановления в одной кнопке. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_restart.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Очередь</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Начинает загружать выделенные в &kget; элементы. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_queue.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Таймер</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Задерживает загрузку выделенных элементов на одну минуту. Это бывает полезно если вам необходимо приостановить загрузку и проверить почту либо зайти на сайт. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_timer.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Передачи</guimenu
> <guimenuitem
>Отложить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отложить загрузку выделенных записей на неопределённое время.</action
> Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_delay.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
<note
><para>
<inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_queue.png" format="PNG"/>
	  </imageobject
></inlinemediaobject
>, <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_timer.png" format="PNG"/> </imageobject
></inlinemediaobject
> and <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_delay.png" format="PNG"/> </imageobject
></inlinemediaobject
> это переключающие кнопки. Только одна из них может быть выбрана.</para>
</note>
</para
></listitem>
</varlistentry>

</variablelist>
</para>
</sect2>

<sect2>
<title
>Меню Параметры</title>

<variablelist>

<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Режим эксперт</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отключает запросы подтверждения. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_expert.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Использовать последний каталог</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>&kget; будет игнорировать текущие настройки каталогов и будет помещать новые загружаемые файлы в каталог, в который был загружен последний файл. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_uselastdir.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>


<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Автономный режим</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Переключает &kget; из сетевого (готовность загружать), <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_on.png" format="PNG"/> </imageobject
></inlinemediaobject
>, режима в автономный <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_offline_mode_off.png" format="PNG"/> </imageobject
></inlinemediaobject
>. Автономный режим используется когда вы хотите добавить &URL; в, &kget; без начала немедленной загрузки. </action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Режим автоотключения</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Указание &kget; отключить модем</action
>. Обычно используется &kppp;. Короткая кнопка <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_disconnect.png" format="PNG"/> </imageobject
></inlinemediaobject
>. При этом должен использоваться режим эксперта <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_expert.png" format="PNG"/> </imageobject
></inlinemediaobject
>. <note
><para
>Для пользователей SuSE в меню <menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Настроить KGet...</guimenuitem
></menuchoice
> <guilabel
> Автоматизация</guilabel
> необходимо заменить команду  <userinput
><command
>cinternet <option
>-i</option
> <parameter
>ppp0</parameter
> <option
>-0</option
> </command
></userinput
> на <userinput
><command
>kppp <option
>-k </option
></command
></userinput
>. Для пользователей Fedora Core команда должна быть <userinput
><command
>/usr/sbin/usernetctl <parameter
>ppp0</parameter
> <option
>down</option
></command
></userinput
>. </para>
</note>
</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Режим автовыхода</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Закрывает после того как все загрузки завершены. При этом должен использоваться режим эксперта. Короткая кнопка  <inlinemediaobject
><imageobject>
	    <imagedata fileref="cr22-action-tool_shutdown.png" format="PNG"/>
	  </imageobject
></inlinemediaobject>
</action
></para
></listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2>
<title
>Меню <guimenu
>Помощь</guimenu
></title>
&help.menu.documentation; </sect2>

</sect1>
</chapter>

<chapter id="credits">

<title
>Благодарности и лицензия</title>

<para
>&kget; </para>
<para
>Авторское право на программу 1998 Matej Moss </para>
<para
>Благодарности: <itemizedlist>
<listitem
><para
>Patrick Charbonnier<email
>pch@freeshell.org</email
></para>
</listitem>
<listitem
><para
>Carsten Pfeiffer<email
>pfeiffer@kde.org</email
></para>
</listitem>
</itemizedlist>
</para>

<para
>Авторское право на документацию 2003 Jonathan Drews <email
>j.e.drews@att.net</email
> </para>

<para
>Перевод на русский: Олег Баталов, <email
>olegbatalov@mail.ru</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

<sect1 id="getting-kapp">
<title
>Как получить &kget;</title>
&install.intro.documentation; </sect1>



<sect1 id="compilation">
<title
>Компиляция и установка</title>
&install.compile.documentation; </sect1>

<sect1 id="configuration">
<title
>Специальный настройки</title>
<sect2>
<title
>Настройка автоотключения</title>

<para
>Это особенность используется для автоматического отключения вашего модема, когда все загрузки завершены. Для настройки автоотключения в &kget;: </para>

<itemizedlist>
<listitem
><para
>Выберите в меню <menuchoice
> <guimenu
>Параметры</guimenu
> <guimenuitem
>Режим автоотключения </guimenuitem
></menuchoice
> для <action
> отключения модема.</action
> Обычно используется &kppp;. Короткая кнопка <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_disconnect.png" format="PNG"/> </imageobject
></inlinemediaobject
>. При этом должен использоваться режим эксперта <inlinemediaobject
><imageobject
> <imagedata fileref="cr22-action-tool_expert.png" format="PNG"/> </imageobject
></inlinemediaobject
></para>
</listitem>

<listitem
><para
>Для пользователей SuSE в меню <menuchoice
> <guimenu
>Настройка</guimenu
> <guimenuitem
>Настроить &kget;...</guimenuitem
> </menuchoice
> <guilabel
>Автоматизация</guilabel
> необходимо заменить команду <userinput
><command
>cinternet <option
>-i</option
> <parameter
>ppp0</parameter
> <option
>-0</option
></command
></userinput
> на <userinput
><command
>kppp <option
>-k </option
></command
></userinput
>.</para>
</listitem>

<listitem
><para
>Для пользователей Fedora Core команда должна быть <userinput
><command
>/usr/sbin/usernetctl <parameter
>ppp0</parameter
> <option
>down</option
></command
></userinput
></para>
</listitem>
</itemizedlist>

</sect2>

<sect2>
<title
>Загрузка в определённые каталоги</title>

<para
>Для загрузки файлов  jpeg в определённый каталог: </para>
<itemizedlist>
<listitem
><para
>Перейдите в меню <menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Настроить &kget;...</guimenuitem
></menuchoice
> <guilabel
> Каталоги</guilabel
> </para>
</listitem>

<listitem
><para
>Как показано ниже, используя расширения <filename
>.jpg</filename
> и <filename
>.jpeg</filename
>, введите имена файлов  которые вы хотите загрузить. </para>
<screenshot>
<screeninfo
>Снимок экрана &kget;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="kget5.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Загрузка в определяемые файлы</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</listitem>

<listitem>
<para
>Нажмите кнопки <interface
>Применить</interface
> и <interface
>OK</interface
>. Теперь, когда вы будете загружать файлы <filename
>/home/kdecvs/pics/JPEG</filename
>. они будут сохраняться в каталоге <filename
>/home/kdecvs/pics/JPEG</filename
>. </para>
</listitem>
</itemizedlist>

</sect2>
</sect1>

</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab 
-->
