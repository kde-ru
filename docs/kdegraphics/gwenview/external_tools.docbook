<chapter id="external_tools">
<title
>Внешние инструменты</title>

<para
>Вы можете легко расширить возможности &kappname; подключая внешние программы. Эти программы (инструменты) будут доступны в контекстном меню эскиза и изображения. Посмотрите в меню <menuchoice
><guimenu
>Внешние программы</guimenu
></menuchoice
>.</para>

<para
>&kappname; поставляется с несколькими готовыми внешними программами, но вы легко можете добавить требуемые в окне <guilabel
>Настроить внешние программы</guilabel
>. Вы можете открыть его из меню <menuchoice
><guimenu
>Настройка</guimenu
><guimenuitem
>Настроить внешние инструменты...</guimenuitem
></menuchoice
>. </para>
  
<para
>Вот так оно выглядит: <screenshot>
  <screeninfo
>Окно Внешние инструменты</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="external_tools_dialog.png" format="PNG"/>
    </imageobject>
  </mediaobject>
</screenshot>
</para>

<para
>В этом примере я подключил внешнюю программу, которая называется Exiftran и служит для регенерации EXIF тегов в изображениях JPEG. Вот более детальное описание его определения:</para>

<itemizedlist>
  <listitem>
    <para
>Поле <guilabel
>Имя:</guilabel
> содержит описание вашей программы, которое будет отображено в меню. </para>
  </listitem>
  <listitem>
    <para
>Поле <guilabel
>Команда:</guilabel
> содержит команду командной строки, которая будет запущена при вызове вашей программы. Нажмите на маленький знак вопроса справа от поля, чтобы узнать какие ключевые слова можно использовать в этом поле. Эти ключевые слова будут заменены на соответствующие загруженному при вызове программы изображению. </para>
  </listitem>
  <listitem>
    <para
>Кнопка справа позволяет назначить значок вашей программе. </para>
  </listitem>
  <listitem>
    <para
>Группа <guilabel
>Связи файлов</guilabel
> показывает, с какими типами файлов может работать ваша программа. Ваша программа будет отображена в меню <menuchoice
><guimenu
>Внешние инструменты</guimenu
></menuchoice
>, если выбранный файл корректного типа. В моём примере эта программа открывает только JPEG изображения, поэтому я выбрал <guilabel
>Другой:</guilabel
>, потом <guilabel
>image/jpeg</guilabel
> и <guilabel
>image/pjpeg</guilabel
>. </para>
  </listitem>
</itemizedlist>

<tip>
  <para
>Найти другие примеры подключения внешних программ можно на <ulink url="http://gwenview.sourceforge.net/tools"
>сайте &kappname;</ulink
>. </para>
</tip>

</chapter>
<!-- vim: set ft=xml: -->
