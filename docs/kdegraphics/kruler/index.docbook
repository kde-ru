<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kappname "&kruler;">
  <!ENTITY package "kdegraphics">
  <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->
  <!ENTITY % addindex "IGNORE">
]>

<book lang="&language;">

<bookinfo>
<title
>Руководство &kruler;</title>

<authorgroup>
<author
><firstname
>Lauri</firstname
> <surname
>Watts</surname
> <affiliation
> <address
><email
>lauri@kde.org</email
></address>
</affiliation>
</author>

<othercredit role="translator"
><firstname
>Иван </firstname
> <surname
>Кашуков </surname
><affiliation
><address
><email
>dolphin210@yandex.ru </email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 
</authorgroup>

<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2001</year>
<holder
>Lauri Watts</holder>
</copyright>

<date
>2003-09-27</date>
<releaseinfo
>3.2</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kruler; &mdash; приложение для измерения объектов на экране. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>KRuler</keyword>
<keyword
>kdegraphics</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
>&kruler; &mdash; очень простое приложение, созданное лишь для того, чтобы измерять объекты на экране.</para>

<para
>Чтобы запустить &kruler;, выберите <menuchoice
><guimenu
>Графика</guimenu
> <guimenuitem
>Экранная линейка KDE</guimenuitem
></menuchoice
> в <guimenu
>K</guimenu
>-меню.</para>

<para
>Чтобы перемещать &kruler; по экрану, нажмите и удерживайте левую кнопку мыши. Курсор при этом примет форму руки.</para>

<para
>Когда мышь находится над линейкой, курсор принимает форму удлинённой стрелки, с кружком в начале. При перемещении курсора &kruler; будет отображать расстояние между нулевой отметкой и кружком, а также <acronym
>HTML</acronym
>-код цвета пикселя, находящегося в кружке. Это помогает получить код цвета из какого-либо изображения. Если курсор не касается линейки, он принимает обычную форму. При этом вы можете работать с другими приложениями.</para>

<para
>Вы можете повернуть линейку, используя команды из контекстного меню. Оно описано в следующей главе.</para>

</chapter>

<chapter id="menu-reference">
<title
>Справочник команд</title>

<para
>При щелчке правой кнопкой мыши по линейке появится контекстное меню, включающее следующие пункты:</para>

<variablelist>
<varlistentry>
<term>
<menuchoice>
<guisubmenu
>Ориентация</guisubmenu>
</menuchoice>
</term>
<listitem>
<para
>Это подменю включает команды, с помощью которых вы можете повернуть линейку &kruler;.</para>

<variablelist>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>N</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Север</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку шкалой вверх</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>E</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Восток</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку шкалой вправо</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>S</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Юг</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку шкалой вниз</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>W</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Запад</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку шкалой влево</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>R</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Повернуть вправо</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку по часовой стрелке.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>L</keycap
> </shortcut
> <guisubmenu
>Ориентация</guisubmenu
> <guimenuitem
>Повернуть влево</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Повернуть линейку против часовой стрелки.</para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice>
<guisubmenu
>Длина</guisubmenu>
</menuchoice>
</term>
<listitem>
<para
>С помощью команд этого подменю вы можете изменить длину линейки &kruler;.</para>
<variablelist>
<varlistentry>
<term>
<menuchoice
><shortcut
> &Ctrl;<keycap
>S</keycap
> </shortcut
> <guisubmenu
>Длина</guisubmenu
> <guimenuitem
>Короткая</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>Линейка &kruler; станет короткой &mdash; около 385 пикселей.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> &Ctrl;<keycap
>M</keycap
> </shortcut
> <guisubmenu
>Длина</guisubmenu
> <guimenuitem
>Средняя</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>&kruler; примет среднюю длину &mdash; около 640 пикселей.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> &Ctrl;<keycap
>T</keycap
> </shortcut
> <guisubmenu
>Длина</guisubmenu
> <guimenuitem
>Длинная</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>&kruler; примет длину около 960 пикселей.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><shortcut
> &Ctrl;<keycap
>F</keycap
> </shortcut
> <guisubmenu
>Длина</guisubmenu
> <guimenuitem
>По ширине экрана</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
>&kruler; примет длину экрана.</para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice>
<guimenuitem
>Цвет...</guimenuitem>
</menuchoice>
</term>
<listitem>
<para
>Открыть окно выбора цвета &kde;. В нём вы сможете выбрать фоновый цвет линейки.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice>
<guisubmenu
>Справка</guisubmenu>
</menuchoice>
</term>
<listitem>
<variablelist>
<varlistentry>
<term>
<menuchoice
><guisubmenu
>Справка</guisubmenu
> <guimenuitem
>Экранная линейка KDE</guimenuitem
> </menuchoice>
</term>
<listitem
><para
>Открыть руководство &kruler; (этот документ) в справочной системе &kde;.</para
> </listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><guisubmenu
>Справка</guisubmenu
> <guimenuitem
>Что это?</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Над курсором появится вопросительный знак.</action
> Щёлкните по любой части окна &kruler;, чтобы получить сведения о его функции (если такие данные существуют).</para
></listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><guisubmenu
>Справка</guisubmenu
> <guimenuitem
>О программе Экранная линейка KDE</guimenuitem
> </menuchoice>
</term
> 
<listitem>
<para
><action
>Вывести версию программы и информацию об авторе.</action
></para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice
><guimenu
>Справка</guimenu
> <guimenuitem
>О KDE</guimenuitem
> </menuchoice>
</term>
<listitem
><para
><action
>Вывести версию KDE и некоторую другую информацию, связанную с KDE.</action
></para
></listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
<varlistentry>
<term>
<menuchoice>
<guimenuitem
>Выход</guimenuitem>
</menuchoice>
</term>
<listitem>
<para
><action
>Выйти</action
> из &kruler;</para>
</listitem>
</varlistentry>
</variablelist>

</chapter>

<chapter id="credits-and-license">
<title
>Благодарности и лицензирование</title>

<para
>&kruler;</para>

<para
>Авторские права на программу принадлежат 2000, 2001 Till Krech <email
>till@snafu.de</email
></para>

<para
>Благодарю Gunnstein Lye <email
>gl@ez.no</email
> за начальный перенос на &kde; 2</para>

<para
>Авторские права на документацию принадлежат Lauri Watts <email
>lauri@kde.org</email
></para>

<para
>Перевод на русский &mdash; Иван Кашуков <email
>dolphin210@yandex.ru</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

&install.intro.documentation;

&install.compile.documentation;

</appendix>

&documentation.index; 
</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes: nil
sgml-general-insert-case: lower
End:
-->

