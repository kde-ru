<!--Dear translator: please NEVER translate the id or anything inside the tags as they are needed in english by the application
     Thanks a lot in advance.-->
<chapter id="using-kturtle">
<title
>Использование &kturtle;</title>

<screenshot>
  <screeninfo
>Снимок экрана &kturtle;</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="mainwindow_flower_nrs.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Главное окно &kturtle;</phrase>
    </textobject>
  </mediaobject>
</screenshot>

<para
>Главное окно KTurtle имеет две главных области: <link linkend="the-code-editor"
>редактор кода</link
> (3), расположенный слева, который предназначен для ввода команд &logo;, и <link linkend="the-canvas"
>холста</link
> (4) , расположенного справа, на котором изображаются результаты выполнения команд. Холст – место для игр Черепашки,  где она перемещается и рисует. Также в главном окне <link linkend="the-menubar"
>есть меню</link
> (1), из которого производится управление программной оболочкой, <link linkend="the-toolbar"
>панель инструментов</link
> (4), которая предоставляет быстрый доступ к часто используемым командам, и <link linkend="the-statusbar"
>панель состояния</link
> (5), на которой отображается различная информация KTurtle.</para>

<sect1 id="the-code-editor">
<title
>Редактор кода.</title>
<para
>Редактор кода предназначен для ввода команд &logo;. Он обладает всеми свойствами, присущими современному редактору. Большинство команд вызывается из меню <link linkend="the-edit-menu"
>Правка</link
> и <link linkend="the-tools-menu"
>Сервис</link
>. Редактор кода может быть “прилеплен” к любой границе главного окна или же, будучи выведен как отдельное окно, располагаться в любом месте рабочего стола.</para>
<para
>Существуют разные пути получения кода в редакторе. Простейший – использовать готовый пример. Для этого необходимо вызвать пункт Открыть примеры в меню <link linkend="the-file-menu"
>Файл</link
> и выбрать необходимый пример в появившемся диалоге. Имя файла говорит о том, что данный пример демонстрирует (например квадрат.logo предназначен для построения квадрата). Выбранный файл будет открыт в <link linkend="the-code-editor"
>редакторе кода</link
> и его можно будет запустить на выполнение, вызвав пункт Выполнить команды из меню Файл.</para>
<para
>Вы можете открывать &logo; файлы вызывая пункт <menuchoice
><guimenu
>Файл</guimenu
><guimenuitem
>Открыть</guimenuitem
></menuchoice
>.</para>
<para
>Третий путь – ввод кода в редакторе вручную или методом <quote
>Copy/paste</quote
> (через буфер обмена) из какого-либо источника.  </para>
<para
>Позиция курсора отображается справа в <link linkend="the-statusbar"
>панели состояния</link
> номерами строки и столбца. </para>
</sect1>

<sect1 id="the-canvas">
<title
>Холст</title>
<para
>Холст – это область, предназначенная для отображения результатов выполнения команд. Другими словами – это место для игр Черепашки. После ввода каких-либо команд в <link linkend="the-code-editor"
>редакторе кода</link
> и вызове пункта Выполнить команды из меню Файл могут произойти две вещи: или программа выполнится успешно и вы увидите некоторые изменения на холсте, или, если были допущены ошибки, вы получите сообщение, которое уведомит вас о характере допущенной ошибки.</para>
<para
>Это сообщение поможет устранить ошибки.</para>
<para
>Построенное в ходе выполнения команд изображение может быть сохранено в файл  (<menuchoice
><guimenu
>Файл</guimenu
><guimenuitem
>Сохранить холст</guimenuitem
></menuchoice
>) или напечатано (<menuchoice
><guimenu
>Файл</guimenu
><guimenuitem
>Печать</guimenuitem
></menuchoice
>)</para>
</sect1>

<sect1 id="the-menubar">
<title
>Меню</title>
<para
>В главном меню вы найдете все команды программной оболочки &kturtle;. В н`м присутствуют следующие группы: <guimenu
>Файл</guimenu
>, <guimenu
>Правка</guimenu
>, <guimenu
>Вид</guimenu
>, <guimenu
>Сервис</guimenu
>, <guimenu
>Настройка</guimenu
> и <guimenu
>Справка</guimenu
>. В данном разделе все они описаны подробно.</para>

<sect2 id="the-file-menu">
<title
>Меню <guimenu
>Файл</guimenu
></title>

<sect3 id="file-new">
<title
>Создать</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
><keycombo
>&Ctrl;<keycap
>N</keycap
></keycombo
></shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Создать</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Создаёт новый, пустой файл &logo;.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-open">
<title
>Открыть</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
><keycombo
>&Ctrl;<keycap
>O</keycap
></keycombo
></shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Открыть...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Открывает &logo; файл.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-open-recent">
<title
>Открыть недавние</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Недавно открытые</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Открывает файлы &logo;, которые недавно открывались.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-open-examples">
<title
>Открыть примеры</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>E</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Открыть примеры</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Показывает папку, в которой расположены файлы примеров &logo;. Для того, чтобы эти файлы открылись на вашем языке необходимо предварительно выбрать его из пункта Настройка KTurtle меню Настройка.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-execute">
<title
>Выполнить команды</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Alt;<keysym
>Return</keysym
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Выполнить команды</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Запускает на  выполнение команды &logo;, введенные в редакторе кода.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-save">
<title
>Сохранить</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>S</keycap
></keycombo
> </shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Сохранить</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Сохраняет текущий открытый файл &logo;.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-save-as">
<title
>Сохранить как</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Сохранить как...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Сохраняет текущий открытый файл &logo; в выбранном месте.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-save-canvas">
<title
>Сохранить холст</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Сохранить холст</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Сохраняет текущие рисунки на холсте в файл изображения.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-pause">
<title
>Пауза</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
><keysym
>Pause</keysym
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Пауза</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Приостанавливает выполнение</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-stop">
<title
>Остановить выполнение</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
><keysym
>Escape</keysym
></keycombo
> </shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Остановить выполнение</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Прекращает выполнение команд. Этот пункт доступен только когда команды выполняются.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-speed">
<title
>Скорость выполнения</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Скорость выполнения</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Если скорость 'Обычная' (по умолчанию), трудно уследить за тем, что присходит на холсте. С помощью этого пункта можно замедлить скорость выполнения, чтобы проследить за каждым его шагом.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-print">
<title
>Печать</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>P</keycap
></keycombo
> </shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Печать...         </guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Печатает либо код, находящийся в редакторе, либо рисунки на холсте.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
<sect3 id="file-quit">
<title
>Выход</title>
  <variablelist>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
><guimenu
>Файл</guimenu
> <guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Выход из KTurtle.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect3>
</sect2>

<sect2 id="the-edit-menu">
  <title
>Меню <guimenu
>Правка</guimenu
></title>
  <variablelist>
    <anchor id="edit-undo"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>Z</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Отменить</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Отменяет последние изменения в коде. KTurtle имеет неограниченное число отмен!</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-redo"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;&Shift;<keycap
>Z</keycap
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Повторить</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Возвращает отмененные изменения в коде.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-cut"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>X</keycap
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Вырезать</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Вырезает выделенный текст из редактора в буфер обмена.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-copy"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>C</keycap
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Копировать</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Копирует выделенный текст из редактора в буфер обмена.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-paste"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>V</keycap
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Вставить</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Вставляет текст из буфера обмена в редактор.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-find"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>F</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Найти...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Поиск выражений в тексте.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-find-next"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
><keysym
>F3</keysym
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Найти далее</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Поиск следующего вхождения фразы в тексте.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="edit-replace"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>R</keycap
></keycombo
> </shortcut
><guimenu
>Правка</guimenu
> <guimenuitem
>Заменить...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Позволяет произвести замену текста в редакторе.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect2>

<sect2 id="the-view-menu">
  <title
>Меню <guimenu
>Вид</guimenu
></title>
  <variablelist>
    <anchor id="view-fullscreen"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;&Shift;<keycap
>F</keycap
></keycombo
> </shortcut
> <guimenu
>Вид</guimenu
><guimenuitem
>Полноэкранный режим</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Позволяет переключаться в полноэкранный режим.</para>
                <para
>Примечание: В полноэкранном режиме, во время выполнения команд, все, кроме холста, скрыто. Это позволяет создавать “полноэкранные” программы в KTurtle.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="view-linenumbers"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
><keysym
>F11</keysym
></keycombo
> </shortcut
><guimenu
>Вид</guimenu
><guimenuitem
>Показывать нумерацию строк</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Позволяет включать/выключать показ нумерации строк в редакторе. Может быть полезно при поиске ошибок.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect2>

<sect2 id="the-tools-menu">
  <title
>Меню <guimenu
>Сервис</guimenu
></title>
  <variablelist>
    <anchor id="tools-color-picker"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Alt;<keycap
>C</keycap
></keycombo
> </shortcut
> <guimenu
>Сервис</guimenu
> <guimenuitem
>Выбор цвета</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Вызывает инструмент выбора цвета. Используя color picker можно легко подобрать нужный цвет и вставить его цифровое представление в код программы.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="tools-indent"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>I</keycap
></keycombo
> </shortcut
><guimenu
>Сервис</guimenu
><guimenuitem
>Выравнивание</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Этот пункт “выравнивает” (добавляет пустое пространство в)  выделенную строку. Когда “выравнивание” используется грамотно, оно облегчает чтение текста. Все примеры используют “выравнивание”</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="tools-unindent"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;&Shift;<keycap
>I</keycap
></keycombo
> </shortcut
><guimenu
>Сервис</guimenu
><guimenuitem
>Убрать выравнивание</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Эта опция отменяет “выравнивание” (убирает пустое пространство в) выбранной строке.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="tools-clean-indent"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Сервис</guimenu
> <guimenuitem
>Снять отступ</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Снимает все отступы на выделенных строках.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="tools-comment"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;<keycap
>D</keycap
></keycombo
> </shortcut
> <guimenu
>Сервис</guimenu
><guimenuitem
>Комментировать</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Данная команда добавляет символы комментария (#) в выделенную строку. Все, следующее за символом комментария до конца строки, игнорируется при выполнении кода. Комментарии позволяют программисту пояснить назначение того или иного куска кода или скрыть от интерпретатора часть кода, чтобы он не выполнялся.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="tools-uncomment"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
>&Ctrl;&Shift;<keycap
>D</keycap
></keycombo
> </shortcut
><guimenu
>Сервис</guimenu
><guimenuitem
>Убрать комментарий</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Удаляет символы комментария с выбранной строки.</para
></listitem>
    </varlistentry>
  </variablelist>
</sect2>

<sect2 id="the-setting-menu">
  <title
>Меню <guimenu
>Настройка</guimenu
></title>
    <variablelist>
    <anchor id="toggle-toolbar"/>
    <varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Показать/Скрыть панель инструментов</guimenuitem
> </menuchoice
></term>
<listitem
><para
>Управляет отображением  панели инструментов</para
></listitem>
</varlistentry>
  </variablelist
>  
  <variablelist>
      <anchor id="toggle-statusbar"/>
   <varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Показать/Скрыть статусную строку</guimenuitem
> </menuchoice
></term>
<listitem
><para
>Управляет отображением  статусной строки</para
></listitem>
</varlistentry>
  </variablelist>
    <variablelist>
    <anchor id="tools-advanced"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Настройка</guimenu
> <guisubmenu
>Расширенные настройки</guisubmenu
> </menuchoice
></term>
      <listitem
><para
>Здесь можно настроить опции, которые обычно в настройке не нуждаются. Подменю <guisubmenu
>Расширенные настройки</guisubmenu
> имеет три пункта: <guimenuitem
>Настроить редактор</guimenuitem
> (стандартный диалог настроек редактора &kate;), <guimenuitem
>Комбинации клавиш</guimenuitem
> (стандартный диалог настроек &kde;) и <guimenuitem
>Настройка панели инструментов</guimenuitem
> (стандартный диалог настройки панели инструментов &kde;).</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="settings-configure"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Настройка &kturtle;...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Применяется для конфигурирования KTurtle. Здесь можно поменять язык команд &logo; или установить новый исходный размер холста.</para
></listitem>
    </varlistentry>
  </variablelist>

</sect2>

<sect2 id="the-help-menu">
  <title
>Меню <guimenu
>Справка</guimenu
></title>
  <variablelist>
    <anchor id="help-handbook"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Справка</guimenu
><guimenuitem
>Руководство KTurtle</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Вызывает руководство по KTurtle, которое вы в данный момент читаете.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="help-whats-this"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
><keycombo
>&Shift;<keysym
>F1</keysym
></keycombo
> </shortcut
><guimenu
>Справка</guimenu
><guimenuitem
>Что это?</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>После активации данного пункта меню курсор приобретет вид стрелки с вопросительным знаком. После этого, когда вы кликнете на каком-либо элементе в окне KTurtle, появится подсказка об этом элементе и его предназначении.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="help-context-help"/>
    <varlistentry>
      <term
><menuchoice
><shortcut
> <keycombo
><keysym
>F2</keysym
></keycombo
> </shortcut
><guimenu
>Справка</guimenu
> <guimenuitem
>Помощь по ...</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Очень полезная функция -  вызывает помощь по тому элементу кода, рядом с которым находится курсор в редакторе. Например, вы используете команду <userinput
>напиши</userinput
> в своей программе и хотите узнать побольше о ней из руководства. Вам достаточно переместить курсор на неё и нажать F1. Руководство по KTurtle откроется на странице с описанием команды.</para>
      <para
>Контекстная помощь очень важна при обучении программированию.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="help-report-bug"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Справка</guimenu
><guimenuitem
>Отчёт об ошибках</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Используйте данный пункт для уведомления разработчиков об ошибках в KTurtle. Это поможет делать программу лучше от версии к версии.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="help-about-kturtle"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Справка</guimenu
> <guimenuitem
>О &kturtle;</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Здесь вы найдёте информации об авторах &kturtle; и лицензиях, с которыми она распространяется.</para
></listitem>
    </varlistentry>
  </variablelist>
  <variablelist>
    <anchor id="help-about-kde"/>
    <varlistentry>
      <term
><menuchoice
><guimenu
>Справка</guimenu
> <guimenuitem
>О &kde;</guimenuitem
> </menuchoice
></term>
      <listitem
><para
>Здесь вы найдёте информацию о KDE. Если вы до сих пор не знаете что это такое – не проходите мимо!</para
></listitem>
    </varlistentry>
  </variablelist>
</sect2>

</sect1>

<sect1 id="the-toolbar">
<title
>Панель инструментов</title>
<para
>Здесь вы можете быстро добраться до наиболее часто используемых функций. По умолчанию, вы найдете здесь большинство используемых команд, включая <guiicon
>Выполнить команды</guiicon
> и <guiicon
>Прекратить выполнение</guiicon
>.</para>
<para
>Вы можете настроить панель инструментов под себя используя <menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Расширенные настройки</guimenuitem
><guimenuitem
>Настройка панели инструментов</guimenuitem
></menuchoice
></para>
</sect1>

<sect1 id="the-statusbar">
<title
>Статусная строка.</title>
<para
>В статусной строке выводится информации от &kturtle;. Слева показывается последнее действие. Справа показывается текущая позиция курсора (номера строк и столбцов). Посередине показывается текущий язык, используемый для ввода команд.</para>
</sect1>

</chapter>
