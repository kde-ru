<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
 <!ENTITY kappname "&kalzium;"
><!-- replace kapp here -->
 <!ENTITY package "kdeedu">
 <!ENTITY % addindex "IGNORE">
 <!ENTITY % Russian "INCLUDE"
><!-- change language only here -->
]>

<book lang="&language;">

<bookinfo>
<title
>Руководство &kalzium;</title>

<authorgroup>
<author
><firstname
>Carsten</firstname
> <surname
>Niehaus</surname
> <affiliation
> <address
><email
>cniehaus@kde.org</email
></address>
</affiliation>
</author>

<othercredit role="developer"
><firstname
>Robert</firstname
> <surname
>Gogolok</surname
> <affiliation
> <address
><email
>mail@robert-gogolok.de</email
></address>
</affiliation>
<contrib
>Разработчик</contrib>
</othercredit>

<othercredit role="developer"
><firstname
>Anne-Marie</firstname
> <surname
>Mahfouf</surname
> <affiliation
> <address
><email
>annma@kde.org</email
></address>
</affiliation>
<contrib
>Дополнение документации</contrib>
</othercredit>

<othercredit role="translator"
><firstname
>Николай</firstname
><surname
>Шафоростов</surname
><affiliation
><address
><email
>shafff@ukr.net</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 

</authorgroup>

<copyright>
<year
>2001</year>
<year
>2002</year>
<year
>2004</year>
<holder
>Carsten Niehaus</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2004-06-24</date>
<releaseinfo
>1.1.91</releaseinfo>

<abstract>
<para
>&kalzium; &mdash; это программа для работы с  периодической системой химических элементов. Вы можете использовать её для поиска информации об элементах.</para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>обучение</keyword>
<keyword
>элементы</keyword>
<keyword
>химия</keyword>
<keyword
>PSE</keyword>
<keyword
>обучение</keyword>
<keyword
>kalzium</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
>&kalzium; предоставляет разнообразную информацию о Периодической системе элементов (Periodic System of Elements, <acronym
>PSE</acronym
>). В ней вы можете найти много полезной информации о конкретном элементе. Если же её вам будет недостаточно, вы можете воспользоваться поиском в Интернете (доступным из программы). Всё доступно бесплатно по лицензии &GNU; Public License. </para>

<para
>Вы можете визуализировать периодическую таблицу элементов по блокам, группам, кислотности или состояниям вещества. Вы также можете создать построение данных диапазона элементов (по атомном и удельном весах, плотности, IE1, IE2, электроотрицательности), или вернуться во времени и посмотреть какие элементы были известны в разные времена. По этой теме доступен небольшой тест. </para>

</chapter>

<chapter id="using-kalzium">
<title
>Использование &kalzium;</title>

<para
>Снимки экрана с &kalzium; в действии:</para>

<screenshot>
<screeninfo
>Главное окно &kalzium; сразу после первого запуска</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot1.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Главное окно &kalzium;</phrase>
</textobject>
</mediaobject>
</screenshot>

<para
>&kalzium; очень проста в использовании. Она сделана для всех, кто хочет чему-то научиться, а студенты могут воспользоваться ей как небольшой базой данных.</para>

<sect1 id="info-dlg">
<title
>Информация об элементе</title>

<screenshot>
<screeninfo
>Упрощённое окно информации</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot3.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Упрощённое окно информации</phrase>
</textobject>
<caption
><para
>Снимок окна упрощённой информации об элементе</para
> 
</caption>
</mediaobject>
</screenshot>

<para
>В этом окне вы можете получить информацию, известную &kalzium; о данном элементе. Оно разделено на три части - <guilabel
>Общее</guilabel
>, <guilabel
>Состояния</guilabel
> и <guilabel
>Энергия</guilabel
>.</para>

<screenshot>
<screeninfo
>Окно расширенной информации</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot4.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Окно расширенной информации</phrase>
</textobject>
<caption
><para
>Снимок окна расширенной информации об элементе</para>
</caption>
</mediaobject>
</screenshot>

<para
>В окне сведений, представляется обзор элемента, его символ и атомный вес. Вкладка Прочее содержит информацию о дате его открытия, удельный вес. На вкладке Фотография помещена фотография элемента если таковая доступна. Вкладка Энергии предоставляет сведения об энергии элемента. Химические данные содержит некоторые атомарные даные и Модель атома - структуру атома элемента.</para>

</sect1>

<!-- Don't have empty chapters if possible - comment them out until -->
<!-- they get contents
<sect1 id="quiz">
	<title
>The Quiz</title>
	 
</sect1
> -->


<sect1 id="colors">
<title
>Расцветка</title>

<para
>&kalzium; может расцвечивать элементы <quote
>блокам</quote
> и <quote
>группам</quote
>, их взаимодействию с кислотами и их агрегатное состояние (твёрдое / жидкое / газообразное) при указанной температуре.</para>

<para
>Цветовые схемы можно изменить в меню <guimenu
>Look</guimenu
>. </para>
<itemizedlist>
<listitem
><para
><guimenuitem
>Отключить цветовую схему</guimenuitem
>: все элементы будут иметь одинаковый цвет. Его можно изменить в  <guimenu
>Настройка</guimenu
> -> <guimenuitem
>Настроить Kalzium...</guimenuitem
> -> Цвета. </para
></listitem>
<listitem
><para
><guimenuitem
>Показать группы</guimenuitem
>: разные цвета для разный групп. Группа - это вертикальный столбик в периобической таблице. В стандартной таблице 18 групп. Элементы одной группы имеют схожик конфигурации по электронам валентной оболочки, что делает их свойства также близкими. </para
></listitem>
<listitem
><para
><guimenuitem
>Показать блоки</guimenuitem
>: отдельный цвет для каждого блока. </para
></listitem>
<listitem
><para
><guimenuitem
>Показать кислотность</guimenuitem
>: кислотность представляется разными цветами. </para
></listitem>
</itemizedlist>

</sect1>


<sect1 id="numeration">
<title
>Классификация</title>

<para
>Вы можете изменить классификацию элементов в соответствии с системой <guilabel
>IUPAC</guilabel
> или <guilabel
>CAS</guilabel
>, или же <guilabel
>выключить</guilabel
> её.</para>

<itemizedlist>
<listitem
><para
>Не нумеровать периоды.</para
></listitem>
<listitem
><para
><acronym
>IUPAC</acronym
> &mdash; <firstterm
>International Union of Pure and Applied Chemistry</firstterm
> (международный союз теоретической и прикладной химии). Это организация, которая определяет большинство стандартов в химии. </para
></listitem>
<listitem
><para
><acronym
>CAS</acronym
> &mdash; <firstterm
>Chemical Abstracts Service</firstterm
> (служба химических определений). По их нумерации 18 групп имеют номера от 1 до 18. Нумерация <acronym
>IUPAC</acronym
> &mdash; официальная, а нумерация <acronym
>CAS</acronym
> &mdash; наиболее часто используемая.</para
></listitem>
<listitem
><para
><guimenuitem
>Показать старый IUPAC</guimenuitem
>: Старая система IUPAC со столбцами с римскими цифрами и буквами 'A' и 'B'. Так что столбцы с первого по седьмой назывались  'IA'..'VIIA', с 8 по 10 - 'VIIIA', с 11 по 17 - 'IB'..'VIIB' и столбец 18 имел имя 'VIII'. </para
></listitem>
</itemizedlist>


</sect1>

<sect1 id="miscellaneous">
<title
>Прочее</title>



<sect2 id="timeline">
<title
>Шкала времени</title>

<para
>Вы можете включить эту возможность, нажав на крайнюю правую кнопку на панели инструментов или через <menuchoice
><guimenu
>Прочее</guimenu
><guimenuitem
>Показывать шкалу времени</guimenuitem
></menuchoice
>. При этом вы увидите ползунок под периодической системой элементов, показывающей элементы, открытые до 2004 года. Если вы переместите ползунок влево, то можете заметить, что некоторые элементы исчезли.</para>

<para
>Ползунок устанавливает год, на который были известны те или иные элементы.Например, переместив ползунок на 1856 год, вы будете видеть только элементы, которые были известны в 1856 году. </para>

<para
>Вы также можете заметить, что некоторые элементы не видны и в 2004 году. Они не открыты до сих пор и их существование только предсказано. </para>

<para
>После скрытия шкалы времени, таблица продолжит содержать элементы выбранного года, который также сохранится и будет восстановлен при следующем запуске Kalzium . </para>
<screenshot>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot6.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
><quote
>Шкала времени</quote
></phrase>
</textobject>
<caption
><para
>Элементы, известные в 1856</para>
</caption>
</mediaobject>
</screenshot>
</sect2>

<sect2 id="plot_data">
<title
>Визуализация данных</title>
<para
>Диалог <guimenuitem
>Визуализация данных</guimenuitem
> позволяет наглядно отобразить некоторые данные элементов. </para>
<screenshot>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot5.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Диалог <quote
>Построение данных</quote
></phrase>
</textobject>
<caption
><para
>&kalzium; может визуализировать некоторые данные диапазона элементов.</para>
</caption>
</mediaobject>
</screenshot>
</sect2>

<sect2 id="state_of_matter">
<title
>Состояние вещества</title>
<para
>С помощью <guimenuitem
>Показать/скрыть состояние вещества</guimenuitem
>: вы можете выбрать какое состояние вам нужно в зависимости от температуры. Вы также можете легко визуализировать количество элементов, какие из них, например, трёрдые при данной температуре. </para>
<screenshot>
<mediaobject>
<imageobject>
<imagedata fileref="screenshot2.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Диалог <quote
>Состояние вещества</quote
></phrase>
</textobject>
<caption
><para
>&kalzium; может сообщать, в каком агрегатном состоянии находятся элементы при данной температуре.</para>
</caption>
</mediaobject>
</screenshot>
</sect2>
<sect2 id="show_hide_legend">
<title
>Показать/скрыть легенду</title>
<para
>Показывать ли легенду для текущей схемы (группы, блоки, кислотность). Состояние её сохраняется в конфигурационном файле и будет восстановлено при следующем запуске. </para>
</sect2>

</sect1>
</chapter>

<chapter id="config">
<title
>Настройка &kalzium;</title>

<para
>&kalzium; содержит много настроек, к которым вы можете добраться через пункт <guimenuitem
>Настроить &kalzium;...</guimenuitem
> меню <guimenu
>Настройка</guimenu
>. Появится вот такое окно: </para>

<screenshot>
<screeninfo
>Настройки</screeninfo>
<mediaobject>
  <imageobject>
		<imagedata fileref="settings.png" format="PNG"/>
  </imageobject>
  <textobject>
    <phrase
>Настройки</phrase>
  </textobject>
</mediaobject>
</screenshot>


<itemizedlist>
 
 
<listitem
><para
>Во вкладке Тест вы можете выбрать уровень его сложности. </para
></listitem>
<listitem
><para
>На вкладке "Прочее", вы можете настроить метод вызова дополнительной информации, а также единицы: энергия по умолчанию в кДж/моль, но вы можете выбрать. Температуры по умолчанию отображаются в Кельвинах, но их можно сменить на градусы Цельсия или Фаренгейта </para
></listitem>
</itemizedlist>

</chapter>

<chapter id="faq">
<title
>Вопросы и ответы</title>

<qandaset id="faqlist">
<qandaentry>
<question>
<para
>Станет ли &kalzium; платным?</para>
</question>
<answer>
<para
>Нет. Никогда. Но автору всегда будет приятно получить е-mail или DVD в качестве <quote
>благодарности</quote
>. &kalzium; распространяется по лицензии <ulink url="http://www.gnu.org/licenses/licenses.html#GPL"
>GPL</ulink
>, что говорит о том, что вам никогда не придётся платить за неё.</para>
</answer>
</qandaentry>
<qandaentry>

<question>
<para
>Будет ли версия для &Windows;?</para>
</question>
<answer>
<para
>Нет, но вы можете загрузить исходный код и осуществить перенос самостоятельно (библиотека Trolltech &Qt; доступна и для Windows). Но зачем вам нужна &Windows;? Возьмите последний дистрибутив Linux и спокойно живите в мире легального и бесплатного программного обеспечения.</para>

</answer>
</qandaentry>

</qandaset>
</chapter>

<chapter id="contribute">
<title
>Чем я могу помочь?</title>

<qandaset id="tasks">
<qandaentry>

<question>
<para
>Поддержать автора данными.</para>
</question>

<answer>
<para
>В нашем мире прогресс идёт очень быстро. Если вы найдёте неправильно указанное или пропущенное значение, сообщите об этом автору.</para>
</answer>
</qandaentry>

<qandaentry>
<question>
<para
>Сообщить об ошибках или высказать свои предложения</para>
</question>
<answer>
<para
>Автор будет благодарен за содействие. Пишите по адресу <email
>cniehaus@kde.org</email
> (на английском). </para>
</answer>

</qandaentry>
</qandaset>
</chapter>

<chapter id="credits">

<title
>Благодарности и лицензия</title>
<para
>&kalzium;</para>
<para
>Авторские права на программу (C) 2001-2004 Carsten Niehaus <email
>cniehaus@kde.org</email
> </para>

<para
>Благодарности:</para>

<itemizedlist>
<listitem
><para
>Robert Gogolok, помощь в разработке и кодирование </para
></listitem>
</itemizedlist>

<para
>Н. Шафоростов <email
>admin@program.net.ua</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

<sect1 id="getting-kalzium">
<title
>Как получить &kalzium;</title>
&install.intro.documentation; </sect1>

<sect1 id="requirements">
<title
>Требования</title>

<para
>&kalzium; можно найти на его <ulink url="http://edu.kde.org/kalzium"
>домашней странице</ulink
>, а также в пакете KDE-Edu.</para>

<para
>Вы можете найти список изменений в файле ChangeLog в дистрибутиве &kalzium;.</para>
</sect1>

<sect1 id="compilation">
<title
>Сборка и установка</title>
&install.compile.documentation; </sect1>

</appendix>

&documentation.index;
</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

// vim:ts=2:sw=2:tw=78:noet:noai
-->
