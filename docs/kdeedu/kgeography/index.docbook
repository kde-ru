<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kgeography "<application
>KGeography</application
>">
  <!ENTITY kappname "&kgeography;">
  <!ENTITY package "playground/edu">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Russian "INCLUDE"
> 
]>

<book lang="&language;">

<bookinfo>
<title
>Руководство &kgeography;</title>

<authorgroup>
<author
><personname
> <firstname
>Anne-Marie</firstname
> <surname
>Mahfouf</surname
> </personname
> <email
>annma@kde.org</email
> </author>
</authorgroup>

<othercredit role="translator"
><firstname
>Владимир</firstname
><surname
>Давыдов</surname
><affiliation
><address
><email
>vdavydov@solvo.ru</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 

<copyright>
<year
>2005</year>
<holder
>Anne-Marie Mahfouf</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2005-08-02</date>
<releaseinfo
>0.4</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kgeography; - это инструмент для изучения географии для KDE. На данный момент существуют шесть режимов работы с ним: <itemizedlist>
<listitem>
<para
>Просмотр карт с выдачей названий стран и регионов по щелчку на них</para>
</listitem>
<listitem>
<para
>Игра, в которой необходимо щёлкнуть на стране или регионе, имя которого показано на экране</para>
</listitem>
<listitem>
<para
>Игра, в которой вам называется столица, а вы должны выбрать страну или регион, к которым она принадлежит</para>
</listitem
><listitem>
<para
>Игра, в которой вам даётся страна или регион на карте, а вы должны назвать её столицу</para>
</listitem
><listitem>
<para
>Игра, в которой нужно угадать название страны или региона по его флагу</para>
</listitem
><listitem>
<para
>Игра, в которой нужно угадать флаг страны или региона, показанного на карте</para>
</listitem>
</itemizedlist>
</para>

</abstract>


<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeedu</keyword>
<keyword
>KGeography</keyword>
<keyword
>география</keyword>
<keyword
>карты</keyword>
<keyword
>страны</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<!-- The introduction chapter contains a brief introduction for the
application that explains what it does and where to report
problems. Basically a long version of the abstract.  Don't include a
revision history. (see installation appendix comment) -->

<para
>&kgeography; - это инструмент KDE для изучения географии. Он позволяет вам пополнять знания о политических границах некоторых стран (границы, столицы, флаги, при их наличии). </para>
<para
>Карты, доступные в текущем выпуске: Австрия, Азия, Африка, Бразилия, Германия, Европа, Испания, Италия, Италия с провинциями, Канада, Китай, Норвегия, Польша, Северная и Центральная Америка, США, Франция, Южная Америка и карта мира. </para>
</chapter>

<chapter id="quick-start">
<title
>Начало работы</title>
<para
>При первом запуске &kgeography; вас спросят с какой карты начать <screenshot>
<screeninfo
>Это снимок экрана &kgeography; при первомзапуске</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start1.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Это снимок экрана &kgeography; при первом запуске</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Вот мы выбираем Канаду <screenshot>
<screeninfo
>Выбрана Канада</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start2.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Выбрана Канада</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Теперь главное окно &kgeography; откроется с картой Канады <screenshot>
<screeninfo
>Главное окно &kgeography;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start3.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Главное окно &kgeography;</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>В меню слева вы можете выбрать, каким видом тестирования или обучения хотите заняться <screenshot>
<screeninfo
>Меню &kgeography;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start4.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Меню &kgeography;</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Карта: при щелчке на какой-либо части карты показываются сведения о ней. <screenshot>
<screeninfo
>Карта</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start5.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Карта</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Найти на карте: введите количество вопросов, на которые вы готовы ответить <screenshot>
<screeninfo
>Сколько вопросов</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start6.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Сколько вопросов</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Затем вам потребуется щёлкнуть на определённой области <screenshot>
<screeninfo
>Покажите область на карте</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start7.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Покажите область на карте</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>После того, как вы закончите со всеми вопросами, будут выведены результаты, в которых будут сообщено о правильных и неправильных ответах: <screenshot>
<screeninfo
>Ваши результаты</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start8.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Ваши результаты</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Угадайте страну или регион по столице. Будет предлагаться 4 варианта ответа: <screenshot>
<screeninfo
>Угадайте страну или регион по столице</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start9.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Угадайте страну или регион по столице</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Результаты показываются после окончания теста <screenshot>
<screeninfo
>Ваши результаты</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start10.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Ваши результаты</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Столицы: Угадайте угадать столицу страны или региона. Даётся четыре варианта ответа и вы должны выбрать правильный. Как и в предыдущем случае, в конце будут сообщены результаты </para>
<para
>Флаг страны: вам показывается флаг, а вы должны найти на карте соответствующую ему страну или регион. <screenshot>
<screeninfo
>Флаг страны</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start11.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Флаг страны</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Флаги: вам предлагают название страны или региона, а вы должны выбрать правильный флаг из четырёх <screenshot>
<screeninfo
>Сопоставьте флаг и область</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="first-start12.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Сопоставьте флаг и область</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
</chapter>

<chapter id="using-kapp">
<title
>Работа с &kgeography;</title>

<para
>&kgeography; отображает информацию о некоторых странах и позволяет проверить, как вы владеете этой информацией <screenshot>
<screeninfo
>Снимок экрана &kgeography;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="kgeography.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Снимок экрана</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>


<sect1 id="kapp-features">
<title
>Дополнительные возможности &kgeography;</title>

<para
>Ещё одна возможность &kgeography; - доступность масштабирования карты <screenshot
> <screeninfo
>Масштаб</screeninfo
> <mediaobject
> <imageobject
> <imagedata fileref="zoom.png" format="PNG"/> </imageobject
> <textobject
> <phrase
>Масштаб</phrase
> </textobject
> </mediaobject
> </screenshot
> В режиме масштабирования вы можете нажать &RMB; для возврата к оригинальному размеру карты (Команда <guimenuitem
>Обычный размер</guimenuitem
> делает то же самое). Используйте полосы прокрутки для перемещения карты. Команда <guimenuitem
>Перетащить</guimenuitem
> позволяет сдвигать карту левой кнопкой мыши. </para>

</sect1>
</chapter>

<!--<chapter id="teachers-parents">
<title
>Teachers/Parents guide to &kgeography; </title>
 This chapter should emphasize the educational aims of your app and
guide the parent/teacher on how to help the child using it.
</chapter
>-->

<chapter id="how-to-maps">
<title
>Создавание карт</title>
<para
>Карта в &kgeography; состоит из двух файлов: один - изображение с картой и второй с её описанием </para>
<sect1 id="helper-tool">
<title
>Инструмент помощник</title>
<para
><ulink url="http://kgeography.berlios.de/gen_map.pl"
>Здесь</ulink
> вы можетенайти инструмент (созданный Yann Verley), который помог ему при созданием карты Франции. Чтобы понять, как его использовать просто запустите прграмму и прочтите справку. Основное, что он делает - создает файл <literal role="extension"
>.kgm</literal
> из обычного текстового фала и записывает файл с цветами, ассоциированными для каждой из стран, таким образом, сверясь с ним вы можете заполнить карту.</para>
</sect1>
<sect1 id="description-file">
<title
>Файл с описанием</title>
<para
>Файл с описанием является обязательным, он имеет расширение <literal role="extension"
>.kgm</literal
>. Файл должен начинаться с <sgmltag class="starttag"
>map</sgmltag
> и заканчиваться на <sgmltag class="endtag"
>map</sgmltag
>. </para>
<para
>Между этими тегами должны быть: <itemizedlist>
<listitem>
  <para
><sgmltag class="starttag"
>mapFile</sgmltag
> и <sgmltag class="endtag"
>mapFile</sgmltag
> - имя файла (без пути к нему), содержащему изображение карты, &eg; <quote
>europe.png</quote
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>name</sgmltag
> и <sgmltag class="endtag"
>name</sgmltag
> - название карты, &eg; <quote
>Европа</quote
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>division</sgmltag
> и <sgmltag class="endtag"
>division</sgmltag
> для всех стран и регионов на карте.</para>
</listitem>
</itemizedlist>
 </para>
<para
>Для каждой страны или региона олжны быть указаны следующие теги: <itemizedlist>
<listitem>
  <para
><sgmltag class="starttag"
>name</sgmltag
> и <sgmltag class="endtag"
>name</sgmltag
> - название страны или региона, например <quote
>Албания</quote
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>capital</sgmltag
> и <sgmltag class="endtag"
>capital</sgmltag
> - столица, например <quote
>Тирана</quote
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>ignore</sgmltag
> and <sgmltag class="endtag"
>ignore</sgmltag
>: могут быть выставлены в <userinput
>yes</userinput
>, <userinput
>allowClickMode</userinput
> и <userinput
>no</userinput
>Если тег имеет значение <userinput
>yes</userinput
>, то такая область будет проигнорирована при запросе областей на карте. Так, будто данной области не существует в режиме теста &kgeography;. Если данный тег выставлен в <userinput
>allowClickMode</userinput
>, то &kgeography; будет запрашивать область в режиме <guibutton
>Нажми на область на карте...</guibutton
>, но не в других режимах тестирования. Выставление данного тега в <userinput
>no</userinput
> означает, что область будет представлена во всех режимах (просмотр и тестирование). Тег является дополнительным. Когда тега <sgmltag class="starttag"
>ignore</sgmltag
> нет, подразумевается, что он установлен в <userinput
>no</userinput
>. Пример: <quote
>Алжирия</quote
> имеет тег, установленный в <userinput
>yes</userinput
> для карты <quote
>Европа</quote
>, что означает, что <quote
>Алжир</quote
> не будет частью любого теста, вразделе <quote
>Европа</quote
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>flag</sgmltag
> и <sgmltag class="endtag"
>flag</sgmltag
> - файл (без указания пути, содержащий флаг страны или региона, &eg; <quote
>albania.png</quote
>. Этот тег не нужен для областей, у которых тег <sgmltag class="starttag"
>ignore</sgmltag
> установлен в <userinput
>yes</userinput
>.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>color</sgmltag
> и <sgmltag class="endtag"
>color</sgmltag
> - цвет, которым будет закрашена страна или регион на карте .</para>
</listitem>
</itemizedlist>
 </para>
<para
>Цвет задаётся с помощью трёх тегов: <itemizedlist>
<listitem>
  <para
><sgmltag class="starttag"
>red</sgmltag
> и <sgmltag class="endtag"
>red</sgmltag
> - красный компонент цвета. Допустимы значения между 0 и 255.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>green</sgmltag
> и <sgmltag class="endtag"
>green</sgmltag
> - зелёный компонент цвета. Допустимы значения между 0 и 255.</para>
</listitem>
<listitem>
  <para
><sgmltag class="starttag"
>blue</sgmltag
> и <sgmltag class="endtag"
>blue</sgmltag
> - синий компонент цвета. Допустимы значения между 0 и 255.255.</para>
</listitem>
</itemizedlist>
</para>
<tip>
 <para
>Часто бывает удобно создать 2 или 3 ложных страны или регион, таких как <quote
>Вода</quote
>, <quote
>Граница</quote
> и <quote
>Побережье</quote
> и указать для них тег<sgmltag class="starttag"
>ignore</sgmltag
>, со значением <userinput
>yes</userinput
>. </para>
</tip>
 <important>
   <para
>Все названия (столицы, страны и регионы) должны быть на английском.</para>
</important>
</sect1>
<sect1 id="map-file">
<title
>Файл карты</title>
<para
>Файл карты очень прост, но трудоёмок в изготовлении. Он должен быть в формате <acronym
>PNG</acronym
>. Несколько карт для преобразования можно найти на сайте <ulink url="https://www.cia.gov/cia/publications/factbook/docs/refmaps.html"
>Cia Reference Maps</ulink
>. Если ни одна из карт не подойдёт, то вы можете загрузить <ulink url="https://www.cia.gov/cia/publications/factbook/reference_maps/pdf/political_world.pdf"
>world pdf</ulink
>, сделать снимок экрана с этого и дальше работать со снимком. Каждая область на карте должна иметь один и только один цвет. Для достижения этого вы можете использовать программы обработки сообщений, такие как <application
>The Gimp</application
> и <application
>Kolourpaint</application
>. </para>
</sect1>
<sect1 id="flags">
<title
>Флаги</title>
<para
>Если вы использовали тег <sgmltag class="starttag"
>flag</sgmltag
>,  то вы должны предоставить файл с флагом. Он должны быть в формате <acronym
>PNG</acronym
> и лучше, если в разрешении300x200 pixels и вы предоставите для него <acronym
>SVG</acronym
> файл. Раздобыть флаги в <acronym
>SVG</acronym
> для почти всех стран и некоторых областей вы можете на <ulink url="http://www.sodipodi.com/index.php3?section=clipart/flags"
>,в коллекции флагов Sodipodi</ulink
>. </para>
</sect1>
<sect1 id="how-to-test">
<title
>Как тестировать</title>
<para
>Перед отсылкой карты Альберту на <email
>tsdgeos@terra.es</email
>, вы должны проверить её на отсутствие ошибок. Для этого сделайте следующее: <itemizedlist
> <listitem
> <para
>Поместите описание карты и файлы изображений с картой в <filename class="directory"
>$<envar
>KDEDIR</envar
>/share/apps/kgeography/</filename
></para
> </listitem
> <listitem
> <para
>Поместите файлы флагов в формате <acronym
>PNG</acronym
> (если они есть) в <filename class="directory"
>$<envar
>KDEDIR</envar
>/share/apps/kgeography/flags/</filename
></para
> </listitem
> </itemizedlist
> После этого вы должны получить возможность открывать вашу карту в &kgeography;. </para>
 <para
>Если вам неизвестно, где находится <filename class="directory"
>$<envar
>KDEDIR</envar
></filename
>,введите <userinput
><command
>kde-config</command
> <option
>--prefix</option
></userinput
> в командной строке. </para>
</sect1>
<sect1 id="non-political-maps">
<title
>Неполитические карты</title>
<para
>Возможно ли создавать неполитические карты? Да! <screenshot>
<screeninfo
>Пример создания неполитической карты</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="river.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>Пример создания неполитической карты</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>
<para
>Разумеется, концепция стран/регионов может быть расширена и на реки и горы. При создании карты, имейте в виду, что в большинстве случаев река или гора будет меньше, чем область для нажатия, которую вы сможете задать. В данном примере река будет иметь такую область и цвет&lt;20,76,34&gt; в качестве идентификатора. </para>
</sect1>
</chapter>

<chapter id="commands">
<title
>Справочник команд</title>

<!-- (OPTIONAL, BUT RECOMMENDED) This chapter should list all of the
application windows and their menubar and toolbar commands for easy reference.
Also include any keys that have a special function but have no equivalent in the
menus or toolbars. This may not be necessary for small apps or apps with no tool
or menu bars. -->

<sect1 id="kapp-mainwindow">
<title
>Главное окно &kgeography;</title>

<sect2>
<title
>Меню <guimenu
>Файл</guimenu
></title>
<para>
<variablelist>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>O</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Открыть карту...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вызывает диалог выбора карты</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Ы</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Quits</action
> &kgeography;</para
></listitem>
</varlistentry>
</variablelist>
</para>

</sect2>

<sect2>
<title
>Меню <guimenu
>Вид</guimenu
></title>
<para>
<variablelist>
<varlistentry>
<term
><menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Масштаб</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Переключение в режим масштабирования</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Обычный размер</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Возврат</action
> карты к её оригинальному размеру</para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Перетащить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Перетащить</action
> текущую карту</para
></listitem>
</varlistentry>
</variablelist>
</para>

</sect2>

<sect2>
<title
>Меню <guimenu
>Настройка</guimenu
></title>
<para>
<variablelist>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Показать/спрятать панель инструментов</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Показать/спрятать панель инструментов &kgeography;</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Комбинации клавиш...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Настройка клавиш быстрого доступа &kgeography;.</action
></para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Панели инструментов...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Настройка панели инструментов &kgeography;</action
></para
></listitem>
</varlistentry>

</variablelist>
</para>

</sect2>
<sect2>
<title
>Меню <guimenu
>Справка</guimenu
></title>
&help.menu.documentation; </sect2>

</sect1>
</chapter>

<!--<chapter id="faq">
<title
>Questions and Answers</title>


&reporting.bugs;
&updating.documentation;

<qandaset id="faqlist">
<qandaentry>
<question>
<para
>My Mouse doesn't work. How do I quit &kgeography;?</para>
</question>
<answer>
<para
>You silly goose! Check out the <link linkend="commands"
>Commands
Section</link
> for the answer.</para>
</answer>
</qandaentry>
<qandaentry>
<question>
<para
>Why can't I twiddle my documents?</para>
</question>
<answer>
<para
>You can only twiddle your documents if you have the foobar.lib
installed.</para>
</answer>
</qandaentry>
</qandaset>
</chapter
>-->

<chapter id="credits">

<!-- Include credits for the programmers, documentation writers, and
contributors here. The license for your software should then be included below
the credits with a reference to the appropriate license file included in the KDE
distribution. -->

<title
>Разработчики и лицензирование</title>

<para
>&kgeography; </para>
<para
>Права на программу 2004-2005 Albert Astals Cid <email
>tsdgeos@terra.es</email
> </para>
<!--<para>
Contributors:
<itemizedlist>
<listitem
><para
>Konqui the KDE Dragon <email
>konqui@kde.org</email
></para>
</listitem>
<listitem
><para
>Tux the Linux Penguin <email
>tux@linux.org</email
></para>
</listitem>
</itemizedlist>
</para
>-->

<para
>Права на документацию &copy; 2005 Anne-Marie Mahfouf <email
>annma@kde.org</email
> </para>

<para
>Владимир Давыдов<email
>trotski@inbox.ru</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

<sect1 id="getting-kapp">
<title
>Где взять &kgeography;</title>
&install.intro.documentation; </sect1>


<sect1 id="compilation">
<title
>Сборка и установка</title>
&install.compile.documentation; </sect1>


</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab 
-->
