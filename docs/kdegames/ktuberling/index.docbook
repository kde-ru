<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kappname "&ktuberling;">
  <!ENTITY package "kdegames">
  <!ENTITY technical.reference SYSTEM "technical-reference.docbook">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->
]>

<book lang="&language;">
<bookinfo>
<title
>Руководство &ktuberling;</title>
<authorgroup>

<author
><firstname
>Eric</firstname
> <surname
>Bischoff</surname
> <affiliation
> <address
><email
>e.bischoff@noos.fr</email
></address>
</affiliation
></author>

<author
><firstname
>Paul</firstname
> <othername
>E.</othername
> <surname
>Ahlquist</surname
> <lineage
>Jr.</lineage
> <affiliation
> <address
><email
>pea@ahlquist.org</email
></address>
</affiliation>
</author>

<othercredit role="reviewer"
><firstname
>Lauri</firstname
> <surname
>Watts</surname
> <contrib
>Редактор</contrib>
</othercredit>

<othercredit role="translator"
><firstname
>Кирилл</firstname
><surname
>Бирюков</surname
><affiliation
><address
><email
>birk@rdi-kvant.ru</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 

</authorgroup>

<date
>2002-04-15</date>
<releaseinfo
>0.03.02</releaseinfo>

<copyright>
<year
>1999</year
><year
>2000</year
><year
>2002</year>
<holder
>Eric Bischoff</holder>
</copyright>

<copyright>
<year
>2001</year>
<holder
>Paul E. Ahlquist Jr.</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<abstract>
<para
>Клубень - игра, предназначенная для малышей. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Клубень</keyword>
<keyword
>игра</keyword>
<keyword
>дети</keyword>
<keyword
>tuberling</keyword>
<keyword
>картофелина</keyword>
<keyword
>пингвин</keyword>
<keyword
>аквариум</keyword>
</keywordset>
</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para>
<inlinemediaobject
><imageobject>
<imagedata fileref="ktuberling.png" format="PNG"/></imageobject>
</inlinemediaobject
> Клубень - игра, предназначенная для малышей, хотя она может понравиться и взрослым, сохранившим детскую непосредственность. </para>

<para
>По своей сути, эта игра - нечто вроде <quote
>редактора картофелины</quote
>. Вы можете приклеивать глаза, рты, усы, другие части лица и всякие прибамбасы на простой картофельный клубень. Кроме того, в игре ещё есть пингвин и аквариум, на которые можно приклеивать и другие вещи. </para>

<para
>В этой игре нет ни победителей ни проигравших. Единственное её предназначение - создавать забавные и весёлые лица. </para>

<para
>В игре есть музей или галерея изображений (как в музее восковых фигур у <quote
>мадам Тюссо</quote
>), где вы можете найти много забавных примеров декорированных картофелин, аквариумов и пингвинов. Вы можете послать свои произведения автору программы, <ulink url="mailto:e.bischoff@noos.fr"
>Эрику Бишофу</ulink
>, и он включит их в галерею, если у него найдётся свободная минутка. </para>

<para
>&ktuberling; также может <quote
>говорить</quote
>. Он произносит имена объектов, которые вы перетаскиваете. Он будет <quote
>говорить</quote
> на вашем родном языке или на английском, если звуков для вашего языка не существует. На данный момент Клубень может говорить на датском, немецком, английском, испанском, французском и других языках. </para>
</chapter>

<chapter id="onscreen-fundamentals">
<title
>Основные действия</title>

<sect1 id="mouse-operation">
<title
>Действия мышью</title>

<para
>В главном окне расположены две основные области: </para>

<itemizedlist>
<listitem
><para
>Слева - <quote
>игровое поле</quote
>: область, куда вы будете перетаскивать объекты. </para
></listitem>
<listitem
><para
>Справа - область с объектами, которые можно использовать для перетаскивания. </para
></listitem>
</itemizedlist>

<screenshot>
<screeninfo
>Основное окно &ktuberling;</screeninfo>
<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="gameboard.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="gameboard.eps"/>
</imageobject>
<textobject
><phrase
>Основное окно</phrase
></textobject>
</mediaobject>
</screenshot>

<sect2 id="place-object">
<title
>Размещение объектов</title>

<para
>Чтобы разместить объект, подведите курсор мыши к области <quote
>объектов</quote
> в правой части окна. Щёлкните кнопкой мыши на том объекте, который вы хотите переместить и, не отпуская кнопку, перетащите объект в левую часть окна - на <quote
>поле игры</quote
> туда, куда вы хотите его поместить. Отпустите кнопку мыши. </para>
</sect2>

<sect2 id="move-object">
<title
>Перемещение объектов</title>
<para
>Объект, помещённый в левую область окна игры (<quote
>игровое поле</quote
>) может быть перемещён на другое место. Для этого просто щёлкните по объекту и перетащите его мышкой на новое место. При щелчке мышью на объекте, он помещается поверх всех тех объектов, которые он полностью или частично закрывает. Это свойство можно использовать чтобы правильно разместить глаза и очки. </para>
</sect2>

<sect2 id="remove-object">
<title
>Удаление объекта</title>
<para
>Чтобы удалить объект помещённый в игровое поле, перетащите его с <quote
>игрового поля</quote
> обратно в область объектов. </para>
</sect2>
</sect1>

<sect1 id="the-tool-bar">
<title
>Панель инструментов</title>

<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="toolbar.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="toolbar.eps"/>
</imageobject>
<textobject
><phrase
>Панель инструментов</phrase
></textobject>
</mediaobject>

<para
>В панели инструментов расположены кнопки для наиболее часто используемых функций </para>

<table>
<title
>Кнопки панели инструментов</title>
<tgroup cols="4">

<thead>
<row>
<entry
>Кнопка</entry>
<entry
>Имя</entry>
<entry
>Эквивалент меню</entry>
<entry
>Действие</entry>
</row>
</thead>
<tbody>

<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-new.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.new.eps"/>
  </imageobject>
	<textobject
><phrase
>Кнопка новой игры</phrase
></textobject>
  </inlinemediaobject>
  </entry>
<entry>
  <guiicon
>Новая игра</guiicon
></entry>
<entry>
  <link linkend="game-new"
><menuchoice
><guimenu
>Игра</guimenu
> <guimenuitem
>Новая</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Очищает игровое поле, удаляя с него все объекты. </entry>
</row>


<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-open.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.open.eps"/>
  </imageobject>
	<textobject
><phrase
>Кнопка загрузки</phrase
></textobject>
  </inlinemediaobject>
  </entry>
<entry
><guiicon
>Загрузить</guiicon
></entry>
<entry>
  <link linkend="game-load"
><menuchoice
><guimenu
>Игра</guimenu
> <guimenuitem
>Загрузить...</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Открывает существующий файл <quote
>Клубня</quote
> из галереи или из других каталогов. </entry>
</row>

<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-save.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.save.eps"/>
  </imageobject>
  <textobject
><phrase
>Кнопка сохранения</phrase
></textobject>
  </inlinemediaobject>
</entry>
<entry
><guiicon
>Сохранить</guiicon
></entry>
<entry>
  <link linkend="game-save"
><menuchoice
><guimenu
>Игра</guimenu
> <guimenuitem
>Сохранить</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Сохраняет ваше произведение в ваш домашний или любой другой каталог. Созданное вами существо сохраняется в небольшом файле, куда записываются только координаты составляющих его объектов. </entry>
</row>


<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-print.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.print.eps"/>
  </imageobject>
  <textobject
><phrase
>Кнопка печати</phrase
></textobject>
</inlinemediaobject>
</entry>
<entry>
<guiicon
>Печать</guiicon>
</entry>
<entry>
  <link linkend="game-print"
><menuchoice
><guimenu
>Игра</guimenu
> <guimenuitem
>Печать</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Печатает вашу картинку в формате &PostScript;. </entry>
</row>

<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-undo.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.undo.eps"/>
  </imageobject>
  <textobject
><phrase
>Кнопка отмены</phrase
></textobject>
  </inlinemediaobject>
  </entry>
<entry>
  <guiicon
>Отменить действие</guiicon>
  </entry>
<entry>
  <link linkend="edit-undo"
><menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Отменить</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Отменяет последнее действие.</entry>
</row>

<row>
<entry>
  <inlinemediaobject>
  <imageobject>
  <imagedata format="PNG" fileref="button-redo.png"/>
  </imageobject>
  <imageobject>
  <imagedata format="EPS" fileref="button.redo.eps"/>
  </imageobject>
  <textobject
><phrase
>Кнопка повтора</phrase
></textobject>
  </inlinemediaobject>
  </entry>
<entry>
<guiicon
>Повторить отменённое действие</guiicon>
</entry>
<entry>
  <link linkend="edit-redo"
><menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Повторить</guimenuitem
></menuchoice>
  </link>
  </entry>
<entry
>Повторяет последнее отменённое действие.</entry>
</row>

<!--  Currently there is no HELP button on the toolbar
<row>
<entry>
<inlinemediaobject>
<imageobject>
<imagedata format="PNG" fileref="button-help.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="button.help.eps"/>
</imageobject>
<textobject
><phrase
>Help Button</phrase
></textobject>
</inlinemediaobject>
</entry>
<entry>
Help
</entry>
<entry
>Displays this handbook.</entry>
</row>
-->

</tbody>
</tgroup>
</table>
</sect1>

<sect1 id="the-menu-items">
<title
>Пункты меню</title>

<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-raw.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.raw.eps"/>
</imageobject>
<textobject
><phrase
>Панель меню</phrase
></textobject>
</mediaobject>

<sect2>
<title
>Меню <guimenu
>Игра</guimenu
></title>
<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-game.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.game.eps"/>
</imageobject>
<textobject
><phrase
>Меню <guimenu
>Игра</guimenu
></phrase
></textobject>
</mediaobject>

<variablelist>

<varlistentry id="game-new">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>N</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Новая</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Очистить</action
> игровую область </para
></listitem>
</varlistentry>

<varlistentry id="game-load">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>O</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Загрузить...</guimenuitem
> </menuchoice
></term>
 <listitem
><para
><action
>Отрыть существующий файл</action
> из галереи или из любого другого места.</para
></listitem>
</varlistentry>

<varlistentry id="game-save">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>S</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Сохранить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Сохранить</action
> картинку. Созданное вами существо сохраняется  в небольшом файле, куда записываются только координаты составляющих его объектов. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Игра</guimenu
> <guimenuitem
>Сохранить как изображение...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Создать графический файл</action
>, содержащий изображение созданного существа. Доступные форматы: XPM, JPEG, PNG и BMP. </para
></listitem>
</varlistentry>

<varlistentry id="game-print">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>P</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Печать...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Печать</action
> изображения в формате &PostScript;. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Выход</action
> из игры. </para
></listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2>
<title
>Меню <guimenu
>Правка</guimenu
></title>
<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-edit.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.edit.eps"/>
</imageobject>
<textobject
><phrase
>Меню Правка</phrase
></textobject>
</mediaobject>

<variablelist>

<varlistentry id="edit-undo">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Z</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Отменить действие</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отменить</action
> размещение последнего объекта. </para
></listitem>
</varlistentry>

<varlistentry id="edit-redo">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Shift</keycap
> <keycap
>Z</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Повторить отменённое действие</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Повторить</action
> размещение последнего объекта.Эта опция меню будет активна только в том случае, если вы предварительно отменяли свои действия с помощью пункта меню <quote
>Отменить действие</quote
>. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Копировать</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Копирование</action
> игрового поля в буфер обмена.</para
></listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2>
<title
>Меню <guimenu
>Игровое поле</guimenu
></title>

<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-playground.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.playground.eps"/>
</imageobject>
<textobject
><phrase
>Меню <guimenu
>Игровое поле</guimenu
></phrase
></textobject>
</mediaobject>

<variablelist>

<varlistentry>
<term
><menuchoice
><guimenu
>Игровое поле</guimenu
> <guimenuitem
>Клубень</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить игровое поле с картофельным клубнем</action
>. &ktuberling; запомнит тип последнего выбранного игрового поля и включит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Игровое поле</guimenu
> <guimenuitem
>Пингвин</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить игровое поле с пингвином</action
>. &ktuberling; запомнит тип последнего выбранного игрового поля и включит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Игровое поле</guimenu
> <guimenuitem
>Аквариум</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить игровое поле с аквариумом</action
>. &ktuberling; запомнит тип последнего выбранного игрового поля и включит его при следующем запуске игры.</para
></listitem>
</varlistentry>
</variablelist>
</sect2>

<sect2>
<title
>Меню <guimenu
>Звуки</guimenu
></title>

<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-speech.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.speech.eps"/>
</imageobject>
<textobject
><phrase
>Меню <guimenu
>Звуки</guimenu
></phrase
></textobject>
</mediaobject>

<para
>Чтобы слышать звуки, вам нужно установить пакет kdemultimedia и запустить <command
>artsd</command
>. </para>

<variablelist>

<varlistentry>
<term
><menuchoice
><guimenu
>Звуки</guimenu
> <guimenuitem
>Выключить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить или выключить звук</action
>. &ktuberling; запомнит этот параметр и восстановит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Звуки</guimenu
> <guimenuitem
>По-датски</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить или выключить звуки по-датски</action
>. Этот пункт меню будет недоступен, если не установлены датские звуковые файлы. &ktuberling; запомнит этот параметр и восстановит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Звуки</guimenu
> <guimenuitem
>По-немецки</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить или выключить звуки по-немецки</action
>. Этот пункт меню будет недоступен, если не установлены немецкие звуковые файлы. &ktuberling; запомнит этот параметр и восстановит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Звуки</guimenu
> <guimenuitem
>По-английски</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Включить или выключить звуки по-английски</action
>. Этот пункт меню будет недоступен, если не установлены английские звуковые файлы. &ktuberling; запомнит этот параметр и восстановит его при следующем запуске игры.</para
></listitem>
</varlistentry>

<varlistentry>
<term
>и т. д...</term>
<listitem
><para
>То же и для других языков.</para
></listitem>
</varlistentry>
</variablelist>
</sect2>

<sect2>
<title
>Меню <guimenu
>Справка</guimenu
></title>
<mediaobject>
<imageobject>
<imagedata format="PNG" fileref="menu-help.png"/>
</imageobject>
<imageobject>
<imagedata format="EPS" fileref="menu.help.eps"/>
</imageobject>
<textobject
><phrase
>Меню Справка</phrase
></textobject>
</mediaobject>
&help.menu.documentation; </sect2>
</sect1>
</chapter>

&technical.reference;

<chapter id="credits_license">
<title
>Авторские права и лицензирование</title>

<para
>&ktuberling;</para>

<itemizedlist>
<listitem>
<para
>Джон Кэлхаун - Оригинальная идея, рисунки и звуки на английском языке</para>
</listitem>

<listitem>
<para
>Эрик Бишоф <email
>ebisch@cybercable.tm.fr</email
> - программирование под &kde;</para>
</listitem>

<listitem>
<para
>Fran&ccedil;ois-Xavier Duranceau <email
>duranceau@free.fr</email
> - Тесты, советы и Помощь</para>
</listitem>

<listitem>
<para
>Agnieszka Czajkowska <email
>agnieszka@imagegalaxy.de</email
> - Изображение пингвина</para>
</listitem>

<listitem>
<para
>Бас Виллемс <email
>next@euronet.nl</email
> - Переработка графики и тема аквариума</para>
</listitem>

<listitem>
<para
>Роджер Ларссон <email
>roger.larsson@norran.net</email
> - Настройка звука</para>
</listitem>

<listitem>
<para
>Петер Сильва <email
>peter.silva@videotron.ca</email
> - Чтение и редактирование документации</para>
</listitem>

<listitem>
<para
>Пол Олквист <email
>pea@ahlquist.org</email
> - Исправления документации</para>
</listitem>
</itemizedlist
> 

<para
>Эта игра посвящена моей маленькой дочери Сунниве Бишоф</para>

<para
>Благодарности: Apple Computer и проекту &LinuxPPC; за портирование на &Mac; и на &Linux;. &ktuberling; не мог бы быть создан без вас!</para>

<para
>Перевод на русский - Кирилл Бирюков <email
>birk@rdi-kvant.ru</email
></para
> 
&underFDL; &underGPL; </chapter>


<appendix id="installation">
<title
>Установка</title>

<sect1 id="how-to-obtain-ktuberling">
<title
>Где взять <application
>Клубень</application
></title>
&install.intro.documentation; </sect1>

<sect1 id="requirements">
<title
>Требования</title>

<para
>Для успешной сборки &ktuberling;, требуется &kde; не ниже 3.0. Все необходимые библиотеки и сам &ktuberling; можно взять на сайте &kde-ftp;.</para>

<para
>Для поддержки звука вам понадобится пакет kdemultimedia. </para>
</sect1>

<sect1 id="compilation-and-installation">
<title
>Сборка и установка</title>
&install.compile.documentation; </sect1>
</appendix>

</book>
