<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kappname "&kbounce;">
  <!ENTITY package "kdegames">
  <!ENTITY % Russian "INCLUDE"
> 
  <!ENTITY % addindex "IGNORE">
]>

<book lang="&language;">

<bookinfo>
<title
>Справочное руководство по &kbounce;</title>

<authorgroup>
<author
>&Aaron.J.Seigo; &Aaron.J.Seigo.mail; </author>

<othercredit role="reviewer"
>&Lauri.Watts; &Lauri.Watts.mail; </othercredit>

<othercredit role="translator"
><firstname
>Валя</firstname
><surname
>Ванеева</surname
><affiliation
><address
><email
>fattie@altlinux.ru</email
></address
></affiliation
><contrib
>Перевод на русский язык</contrib
></othercredit
> 

</authorgroup>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2003-09-19</date>
<releaseinfo
>0.5</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kbounce; &mdash; игра с шарами для &kde;. </para>
</abstract>


<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdegames</keyword>
<keyword
>jezzball</keyword>
</keywordset>

</bookinfo>

<chapter id="how-to-play">
<title
>Как играть</title>

<para
>В &kbounce; играют на поле, окруженном стеной, с двумя или более мячами, которые отскакивают от стен.</para>

<para
>Стены отмечены более темным цветом, чем свободное пространство.</para>

<para
>Размер поля уменьшается, если вы создаете стену и при этом никакой шар не оказывается попавшим в ловушку. Чтобы пройти уровень, игроку за данное время нужно уменьшить размер поля по крайней мере на 75&percnt;.</para>

<para
>На каждом следующем уровне к игре добавляется по одному шару, а игроку дается больше жизней и времени.</para>

<para
>Подсчет очков зависит от того, насколько вы уменьшили площадь.</para>

<para
>Новые стену строятся по щелчку <mousebutton
>левой</mousebutton
> кнопкой мыши на свободном пространстве поля. После щелчка, начиная от клетки, где он был сделан,  в противоположных направлениях начинают строиться две части стены. За один промежуток времени может строиться только одна стена.</para>

<screenshot>
<screeninfo
>Постройка стены</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="jezball_newWall.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Постройка стены</phrase>
</textobject>
</mediaobject>
</screenshot>

<para
>На то время, когда курсор находится на свободном поле, он принимает форму пары стрелок, указывающих в противоположых направлениях, горизонтально или вертикально. Стрелки указывают направления, в которых будет строиться стена при щелчке <mousebutton
>левой</mousebutton
> кнопкой мыши. Сменить направление можно щелчком <mousebutton
>правой</mousebutton
> кнопки мыши.</para>

<para
>У новой стены есть <quote
>голова</quote
>, которая движется от клетк, где вы щелкнули мышью. Стена может быть разрушена, если <quote
>голова</quote
> еще не дошла до другой стены. Если шар попадет в строящуюся стену, она полностью исчезнет, а вы потеряете одну жизнь. Если шар попадет точно в <quote
>голову</quote
> по направлению роста стены, стена перестанет строиться в эту сторону, но станет крепкой, жизнь вы не потеряете. Если шар попадет в <quote
>голову</quote
> с любой другой стороны, он просто отскочит, а стена будет продолжать строиться.</para>

</chapter>

<chapter id="strategy">
<title
>Стратегия</title>

<para
>Для многих игроков сложности начинаются уже на третьем или четвёртом уровне из-за количества прыгающих шаров.</para>

<para
>Чтобы играть в &kbounce; хорошо, нужно строить коридоры. Для этого начните постройку одной стены рядом с какой-нибудь другой, сделайте так, чтобы если одна стена и могла быть разрушена попаданием шара, другая не смогла бы.</para>

<screenshot>
<screeninfo
>Постройка коридора</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="jezball_corridor1.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Постройка коридора</phrase>
</textobject>
</mediaobject>
</screenshot>

<para
>Вы получите коридор, занимающий совсем немного места, но огрниченный с трех сторон стенами. Подождите, пока шар попадет в коридор, и закройте его новой стеной. Хотя при постройке коридоров вы можете потерять жизни, в одном коридоре можно закрыть несколько шаров.</para>

<screenshot>
<screeninfo
>Ловушка для шара</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="jezball_corridor2.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Ловушка для шара</phrase>
</textobject>
</mediaobject>
</screenshot>

<para
>Последний совет &mdash; не торопитесь. Вам дается довольно много времени (поле <guilabel
>Время</guilabel
> справа внизу). Если вы будете слишком спешить, может ничего не выйти.</para>

</chapter>

<chapter id="menu-reference">
<title
>Меню</title>

<sect1 id="game-menu">
<title
>Меню <guimenu
>Игра</guimenu
></title>

<variablelist>
<varlistentry>
<term>
<menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>N</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Новая игра</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Начать новую игру.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term>
<menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>End</keycap
> </keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Закончить игру</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Закончить текущую игру.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term>
<menuchoice
><shortcut
> <keycap
>P</keycap
></shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Пауза</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Приостановить игру.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term>
<menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>H</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Показать рекорды</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Показать таблицу луших результатов.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
> <guimenu
>Игра</guimenu
> <guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Закрыть</action
> &kbounce;.</para>
</listitem>
</varlistentry>
</variablelist>
</sect1>

<sect1 id="background-menu">
<title
>Меню <guimenu
>Фон</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><guimenu
>Фон</guimenu
> <guimenuitem
>Выбрать каталог с рисунками...</guimenuitem
> </menuchoice>
</term>
<listitem>
<para
><action
>Выбрать изображения для фона.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Фон</guimenu
> <guimenuitem
>Показать рисунки</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Показать фоновое изображение.</action
></para>
</listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="settings-menu">
<title
>Меню <guimenu
>Настройка</guimenu
></title>
<variablelist>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Показать панель инструментов</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Отобразить/скрыть панель инструментов &kbounce;.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Показать панель состояния</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Отобразить/скрыть панель состояния &kbounce;.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Настроить комбинациии клавиш...</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Настроить комбинации клавиш для пунктов меню &kbounce;.</action
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><guimenu
>Настройка</guimenu
> <guimenuitem
>Включить звуки</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Включить звуки игры.</action
></para>
</listitem>
</varlistentry>

</variablelist>
</sect1>


<sect1 id="help-menu">
<title
>Меню <guimenu
>Помощь</guimenu
></title>
&help.menu.documentation; </sect1>

</chapter>

<chapter id="credits-and-licenses">
<title
>Авторские права и лицензирование</title>


<para
>&kbounce;</para>

<para
>Авторские права принадлежат Штефану Шимански (Stefan Schimanski), 2000.</para>

<itemizedlist>
<title
>Разработчики</title>
<listitem>
<para
>Штефан Шимански <email
>schimmi@kde.org</email
></para>
</listitem>
<listitem>
<para
>Сандро Сигала (Sandro Sigala) <email
>ssigala@globalnet.it</email
>, лучшие результаты</para>
</listitem>
</itemizedlist>

<para
>Авторские права на документацию принадлежат Аарону Дж. Сейго (Aaron J. Seigo) <email
>aseigo@olympusproject.org</email
>, 2002.</para>

<para
>Это руководство посвящено Дэннису Пауэллу (Dennis E. Powell).</para>

<para
>Валя Ванеева <email
>fattie@altlinux.ru</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

&install.intro.documentation;

&install.compile.documentation;

</appendix>

&documentation.index; 
</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes: nil
sgml-general-insert-case: lower
End:
-->

