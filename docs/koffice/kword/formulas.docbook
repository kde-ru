<sect1 id="formulas">
<sect1info>
<authorgroup>
<author
><firstname
>Mike</firstname
> <surname
>McBride</surname
> </author>
<othercredit role="translator"
><firstname
>Андрей</firstname
><surname
>Черепанов</surname
><affiliation
><address
><email
>sibskull@mail.ru</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 
</authorgroup>
</sect1info>
<title
>Формулы</title>
<indexterm
><primary
>формулы</primary
></indexterm>

<para
>&kword; может включать в текст формулы, созданные в общем для всех приложений &koffice; редакторе формул.</para>
<note
><para
>Это визуальный редактор формул. В настоящее время &kword; не имеет возможности решать математические уравнение</para
></note>

<sect2 id="formulas-add"
><title
>Добавление формулы</title>
<para
>Существует три способа добавить формулу в документ:</para>
<itemizedlist>
<listitem>
<para
>Через пункт меню <menuchoice
> <guimenu
>Вставить</guimenu
><guimenuitem
>Формулу</guimenuitem
></menuchoice
></para>
</listitem>

<listitem>
<para
>Используя клавишу <keycap
>F4</keycap
></para>
</listitem>

<listitem>
<para
>или нажатием кнопки <inlinemediaobject
><imageobject
><imagedata fileref="part-kformula.png" format="PNG"/></imageobject
></inlinemediaobject
> на панели инструментов.</para>
</listitem>
</itemizedlist>
<para
>&kword; создаст врезку с формулой в текущей позиции курсора.</para>
<note
><para
>Работа с врезкой с формулой отличается от работы с другими врезками &kword;: </para>
<itemizedlist>
<listitem
><para
><emphasis
>Все</emphasis
> врезки с формулами вставляются <glossterm linkend="definlineframe"
>в текст</glossterm
> по умолчанию. Эта врезка впоследствии может быть преобразована в <emphasis
>плавающую</emphasis
>.</para
></listitem>
<listitem
><para
>Размер врезки зависит только от полученного размера формулы.</para
></listitem>
<listitem
><para
>Врезки с формулами автоматически определяют позицию в строке текста для правильного выравнивания.</para
></listitem>
</itemizedlist>
</note>

</sect2>

<sect2 id="formula-move"
><title
>Перемещение формулы</title>
<para
>По умолчанию, врезки с формулами создаются <glossterm linkend="definlineframe"
>в тексте</glossterm
>. Как и другие врезки этого типа, врезки с формулами перемещаются вместе с текстом.</para>
<para
>Если вы хотите переместить формулу на любое другое место страницы, вы должны преобразовать её в плавающую врезку. Вы можете сделать это, нажав правую кнопку мыши и выбрав в появившемся контекстном меню пункт <guilabel
>Врезка в тексте</guilabel
> и врезка станет свободно перемещаемой. </para>
<para
>Преобразованную врезку <link linkend="move-frame"
>переместить</link
> как и любую другую плавающую врезку в &kword;.</para>
</sect2>

<sect2 id="formula-delete"
><title
>Удаление формулы</title>
<para
>Для удаления формулы из документа, <link linkend="delete-frame"
>удалите врезку</link
>, в которой она находится.</para>
</sect2>

<sect2 id="formula-edit"
><title
>Редактирование формулы</title>
<para
>Редактирование формул выходит за рамки данного руководства. Обратитесь к руководству по редактору формул &kformula;.</para>
</sect2>
</sect1>

