<sect1 id="links">
<sect1info>
<authorgroup>
<author
><firstname
>Mike</firstname
> <surname
>McBride</surname
> </author>
  
</authorgroup>
</sect1info>
<title
>Ссылки в документе </title>
<indexterm
><primary
>ссылки в документе</primary
></indexterm>

<para
>&kword; позволяет добавлять в документ веб-адреса, почтовые адреса и ссылки на файлы. Чаще всего эта возможность используется при создании веб-страниц.</para>

<para
>Вставив в документ адрес, вы сможете увидеть его описание, однако если вы сохраните документ как веб-страницу, адрес можно будет использовать как гиперссылку.</para>

<sect2 id="links-new"
><title
>Вставка ссылки</title>
<para
>Чтобы вставить в документ ссылку, выберите пункт меню <menuchoice
><guimenu
>Вставка</guimenu
> <guimenuitem
>Ссылка</guimenuitem
> </menuchoice
>. Появится диалоговое окно.</para>

<screenshot>
<mediaobject>
<imageobject>
<imagedata fileref="linkdlg.png" format="PNG"/></imageobject>
</mediaobject>
</screenshot>

<para
>С помощью левой панели выберите, какого типа будет ссылка.</para>
<para
>В поле для комментария введите название ссылки (например Сайт KOffice).</para>
<para
>В поле для адреса введите адрес Интернет, почтовый адрес или ссылку на файл (например http://www.koffice.org). </para>
<para
>Нажмите кнопку <guibutton
>OK</guibutton
>.</para>
<note
><para
>По умолчанию &kword; отображает ссылки подчёркнутыми (как в большинстве браузеров). Вы можете включить или отключить подчёркивание ссылок, для этого вызовите <menuchoice
><guimenu
>Настройка</guimenu
><guimenuitem
>Настроить &kword;...</guimenuitem
></menuchoice
> и далее выберите раздел <guilabel
>Дополнительно</guilabel
>. Более подробное описание можно найти в разделе <link linkend="opt-misc"
>Дополнительные параметры настройки</link
>.</para
></note>
</sect2>
<sect2 id="links-open"
><title
>Переход по ссылке</title>
<para
>Вы можете использовать ссылки внутри &kword;.</para>
<para
>Щёлкните по ссылке правой кнопкой мыши и в появившемся подменю выберите <guilabel
>Открыть ссылку</guilabel
>.</para>
<para
>Ссылка на веб-сайт будет открыта в новом окне браузера; ссылка на почтовый адрес позволяет вам создать новое сообщение в почтовой программе; ссылка на файл позволяет открыть его соответствующей программой.</para>
</sect2>

<sect2 id="links-copy"
><title
>Копировать ссылку на документ</title>
<para
>Вы можете использовать ссылку на документ в другом приложении, скопировав ей из &kword;.</para>
<para
>Просто щёлкните по ссылке правой кнопкой мыши и в появившемся подменю выберите <guilabel
>Открыть ссылку</guilabel
>.</para>
<para
>Адрес теперь скопирован в буфер обмена. Вы можете вставить его в другом приложении.</para>
</sect2>
<sect2 id="links-edit"
><title
>Изменение ссылки</title>
<para
>Чтобы изменить параметры ссылки, щёлкните по ней правой кнопкой мыши и в появившемся меню выберите <guilabel
>Изменить ссылку...</guilabel
>.</para>
<para
>Появится окно, в котором будут показаны текущие параметры выбранной ссылки.</para>
<screenshot>
<mediaobject>
<imageobject>
<imagedata fileref="linkdlg.png" format="PNG"/></imageobject>
</mediaobject>
</screenshot>

<para
>Внесите необходимые изменения и нажмите кнопку <guibutton
>OK</guibutton
>.</para>

</sect2
><sect2 id="links-delete"
><title
>Удаление ссылки</title>
<para
>Вы можете удалить ссылку, как любой другой текст.</para>
<para
>Переместите курсор к концу ссылки и нажмите <keycap
>Backspace</keycap
>, либо переместите его в начало и нажмите <keycap
>Delete</keycap
>. Вся ссылка будет удалена.</para>
</sect2>
<sect2 id="links-remove"
><title
>Преобразование ссылки в обычный текст</title>
<para
>Если вы хотите удалить ссылку, но оставить представляющий её текст, в её контекстном меню выберите <guilabel
>Удалить ссылку...</guilabel
>.</para>

</sect2>



</sect1>
