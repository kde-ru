<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
 <!ENTITY kappname "&koffice;">
 <!ENTITY package "koffice">
 <!ENTITY % addindex "IGNORE">
 <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->
]>

<!-- maintained by raphael.langerhorst@kdemail.net -->

<book lang="&language;">
<bookinfo>
<title
>&koffice;</title>
<subtitle
>Общее введение</subtitle>
<authorgroup>
<author
><firstname
>Raphael</firstname
> <surname
>Langerhorst</surname
> <affiliation
><address
><email
>raphael.langerhorst@kdemail.net</email
></address
></affiliation
> 
</author>
<author
><firstname
>Jost</firstname
> <surname
>Schenck</surname
> <affiliation
><address
><email
>jost@schenck.de</email
></address
></affiliation
> 
</author>
<othercredit role="translator"
><firstname
>Ivan</firstname
><surname
>Petrov</surname
><affiliation
><address
><email
>pis00593@udmglzs.udmene.ru</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 
</authorgroup>

<date
>2005-10-27</date>
<releaseinfo
>1.5.0</releaseinfo>

<abstract>
<para
>&koffice; - это интегрированный офисный пакет для K Desktop Environment (&kde;). </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>KOffice</keyword>
</keywordset>

</bookinfo>


<chapter id="introduction">
<title
>Введение</title>
<sect1>
<title
>Компоненты &koffice;</title>

<para
>&koffice; - это интегрированный офисный пакет для K Desktop Environment (&kde;). &koffice; в настоящее время состоит из следующих компонентов: </para>

<itemizedlist>
<listitem
><para
>&kword; (текстовый процессор, основанный на врезках)</para
></listitem>
<listitem
><para
>&kspread; (программа для работы с электронными таблицами)</para
></listitem
> 
<listitem
><para
>&kpresenter; (программа для подготовки презентаций)</para
></listitem>
<listitem
><para
>&kivio; (редактор схем и организационных диаграмм)</para
></listitem>
<listitem
><para
>Karbon14 (редактор векторной графики)</para
></listitem>
<listitem
><para
>&krita; (редактор растровой графики)</para
></listitem>
<listitem
><para
>&kugar; (генератор отчётов)</para
></listitem>
<listitem
><para
>Kexi (программа для работы с базами данных)</para
></listitem>
<listitem
><para
>&kchart; (программа для создания диаграмм и графиков)</para
></listitem>
<listitem
><para
>&kformula; (редактор математических формул)</para
></listitem>
</itemizedlist>

<para
>Так как все компоненты построены на основе модели KParts, программы &koffice; хорошо взаимодействуют друг с другом. Документ любого компонента &koffice; можно встроить в документ другого компонента. Например, таблицу, созданную в &kspread;, можно вставить прямо в документ &kword;. Таким образом, используя программы &koffice; можно создавать сложные составные документы. </para>

<para
>Механизм модулей позволяет расширять возможности &koffice;. В этом руководстве также описывается создания нового модуля. </para>

<para
>Это руководство охватывает только общие возможности &koffice;, одинаковые для всех программ пакета. Подробно о каждом компоненте можно прочитать в его руководстве. </para>

</sect1>

<sect1 id="features">
<title
>Обзор возможностей &koffice;</title>
<sect2 id="featureintegration">
<title
>Интеграция</title>
<para
>Компоненты &koffice; построены на основе общей инфраструктуры, что позволяет достичь тесной интеграции между ними. Вы также можете создать новый компонент. Для этого ознакомьтесь с главой о <link linkend="kparts"
>KParts</link
>. </para>
</sect2>
<sect2 id="featureleightweight">
<title
>Легковесность</title>
<para
>Не смотря на то, что в &koffice; так много компонентов, он остаётся "легковесным" - быстрым и нетребовательным к памяти. Этого удалось отчасти достичь благодаря использованию технологии &kde;. </para>
<para
>Разработчики &koffice; стараются по минимуму загружать интерфейс редко используемыми функциями. </para>
<para
>Многие недоступные функции можно реализовать самостоятельно написав или подключив дополнительный модуль или сценарий. Таким образом сам &koffice; остаётся легковесным. </para>
</sect2>
<sect2 id="featurecompleteness">
<title
>Завершённость</title>
<para
>В &koffice; доступно много компонентов, соответвтующих большинству ежедневных офисных и домашних задач. Расширить возможности &koffice; всегда можно посредством написания иди подключения дополнительных модулей, сценарией или целых компонентов. </para>
</sect2>
<sect2 id="featurefileformat">
<title
>Формат OASIS OpenDocument</title>
<para
>Одной из главнейших целей для разработчиков является поддержка всех арспространённых стандартнов, особенно на уровне форматов файлов. </para>
<para
>Форматы OASIS OpenDocument являются основными для компонентов &koffice;. </para>
</sect2>
<sect2 id="featurekde">
<title
>Возможности, предоставляемые KDE</title>
<para
>Поскольку &koffice; использует &kde;, все возможности, доступные в приложении &kde;, также доступны в &koffice;, в т.ч. DCOP, KParts, интеграция в рабочее окружение, настройка интерфейса. </para>
</sect2>

</sect1>

</chapter>
<chapter>
<title
>Технология &koffice;</title>

<sect1 id="kparts">
<title
>Kparts - строительные блоки &koffice;</title>

<!-- <sect2 id="kpartsintro"
> -->
<!-- <title
>KParts Introduction</title
> -->

<para
>Каждое приложение &koffice; спроектировано для выполнения очень специфических задач. Например, &kspread; - это программа для работы с электронными таблицами, &kword; - программа для обработки текстов. Сфокусированные на специфических задачах они прекрасно работают в конкретных областях. Тем не менее, в зависимости от того, какие задачи вы решаете с помощью &koffice;, вам может потребоваться функциональность, предоставляемая другими приложениями, но в <emphasis
>том же</emphasis
> документе. </para>

<para
>Предположим, что вы готовите документ в &kword; и хотите проиллюстрировать какую-то часть с помощью таблицы. И, хотя &kword; обладает собственными возможностями по вставке таблиц, этого может оказаться недостаточно. Например, вам нужно использовать особый формат валюты, или произвести вычисления используя формулы электронной таблицы. В настоящее время этого <emphasis
>можно</emphasis
> попытаться добиться используя программирование в &kword;. Тем не менее, в этой области, &kword; никогда не сможет заменить &kspread;, и если попытаться добиться такой же функциональности, это будет неприемлемо сложно, а исходные тексты будет невозможно сопровождать. </para>

<para
>Альтернативой этому является KParts. Его философия проста: пусть каждое приложение выполняет то, что умеет лучше всего. </para>

<!-- </sect2
> -->

<sect2 id="compound-docs">
<title
>Составные документы с использованием KParts</title>

<para
>С KParts вы можете использовать <emphasis
>всю</emphasis
> функциональность предлагаемую <emphasis
>всеми</emphasis
> приложениями &koffice;. Воспользоваться этим можно вставляя так называемые <quote
>части</quote
> в свой документ. Каждая из этих частей на самом деле - другой документ, то есть <quote
>документ в документе</quote
>. </para>

<para
>В приведенном выше примере документ &kspread; был встроен в документ &kword;. Во время правки таблицы, &kspread; работает в фоне. По окончании редактирования таблицы управление возвращается &kword;. Единственное видимое отличие заключается в том, что при редактировании таблицы меню и панели инструментов отображают инструменты &kspread;, а не &kword;. Возможность включения функциональности одного приложения в другое называется <quote
>встраиванием</quote
> (<quote
>embedding</quote
>). </para>

</sect2>


<sect2 id="kparts-try-it">
<title
>Использование KParts в документах</title>

<para
>Если вы никогда не работали с составными документами, то сперва это может сбивать с толку. Следование пошаговым инструкциям покажет, что использование KParts также просто, как и работа с одним приложением. </para>

<procedure>
<title
>Вставка компонента &kspread; в приложение &kword;</title>
<step>
<para
>Запустите &kword;. Это можно сделать с помощью панели или набрав <userinput
><command
>kword</command
></userinput
> в командной строке. </para>
</step>
<step>
<para
>Создайте новый пустой документ. Если хотите, то можете набрать какой-нибудь текст. </para>
</step>
<step>
<para
>Выберите <menuchoice
><guimenu
>Вставить</guimenu
><guisubmenu
>Врезку с объектом</guisubmenu
><guimenuitem
>Электронные таблицы</guimenuitem
></menuchoice
>. Указатель мыши изменит вид и станет крестиком. Это говорит о том, что необходимо выделить область. </para>
</step
> 
<step>
<para
>Нажмите левую кнопку мыши в том месте, где хотите расположить верхний правый угол вашей таблицы, и удерживая её, укажите курсором нижний левый угол. Отпустите кнопку. Появится диалог, с помощью которого можно вставить уже существующий документ или создать новый. Создайте новую электронную таблицу. Вот и всё -- вы сделали это. </para>
</step>
</procedure>

<para
>Просто, не правда ли? Теперь вы видите врезку с таблицей в документе &kword;. Дважды щёлкните на таблице и посмотрите, что произойдёт. Вы заметите что: </para>

<itemizedlist>
<listitem>
<para
>Строка меню и панели инструментов изменились. Теперь они отображают инструменты &kspread;. </para>
</listitem
> 
<listitem>
<para
>Врезка с таблицей содержит элементы интерфейса &kspread;, с полосами прокрутки, строкой вкладок для выбора таблиц и .т.д. </para>
</listitem>
</itemizedlist>

<para
>Попробуйте изменить таблицу. Вы увидите, что это не отличается от работы с &kspread;. На самом деле вы <emphasis
>работаете</emphasis
> с &kspread;. </para>

<para
>Теперь щёлкните по документу &kword; вне области таблицы. Вернутся строка меню и панели инструментов &kword;, а элементы &kspread; исчезнут. Таблица осталась и содержит все произведённые вами изменения. </para>

<para
>KParts очень просто использовать. Попробуйте вставить другие компоненты или даже компоненты в компоненты. </para>

</sect2>

</sect1>

</chapter>

<chapter id="configuration">
<title
>Настройка &koffice; и вашей системы</title>

<para
>Хотя &koffice; работает достаточно неплохо и сразу после установки, для получения наибольшей отдачи от &koffice; можно оптимизировать некоторые вещи. Эта часть рассказывает, что можно сделать чтобы добиться наилучших результатов от новой офисной среды. </para>

<sect1 id="the-font-issue">
<title
>Оптимизация вывода шрифтов</title>
<para
>Шрифты - это больная тема для X Windows. В этой секции мы опишем некоторые проблемы, которые часто встречаются пользователям &koffice;. Некоторые проблемы связаны на с &koffice;, а с настройкой системы, поэтому чтобы решить их вам может потребоваться изменить файлы настройки. Если у вас нет доступа к учетной записи root на вашем компьютере, то попросите системного оператора об этом и укажите ему или ей на это руководство. Так как тема про шрифты слишком сложна чтобы полностью описать всё тут, возможно вам придётся прочитать <ulink url="http://www.pegasus.rutgers.edu/~elflord/font_howto/Font-HOWTO/"
>Font HOWTO</ulink
>, из которого и взята следующая информация. Там можно найти некоторые дополнительные детали. </para>

<sect2>
<title
>Как получить красивые масштабируемые шрифты на экране</title>

<para
>STUFF </para>

</sect2>

<sect2
><title
>Как получить корректную распечатку</title>

<para
>Хотя &koffice; может автоматически оперировать шрифтами X11 <emphasis
>на экране</emphasis
>, распечатка может вызвать проблемы: в большинстве систем печать происходит через <application
>ghostscript</application
>. И, в то время как &koffice; знает имена шрифтов используемых X Windows, он обычно <emphasis
>не</emphasis
> знает имена шрифтов используемых <application
>ghostscript</application
>. &koffice; старается угадать эти имена, что, к несчастью, не всегда заканчивается успешно. </para>

<para
>Эту проблему можно решить, хотя это и не просто. На самом деле, возможно вы используете дистрибутив в котором большая часть работы уже сделана (так что если у вас нет проблем с печатью, вы можете пропустить эту секцию). Вам необходимо сделать чтобы <application
>ghostscript</application
> знал как преобразовать предполагаемые имена шрифтов, используемые в &koffice;, в его собственные имена шрифтов. Это можно сделать добавив строки в файл <filename
>Fontmap</filename
>. Строки с псевдонимами в <filename
>Fontmap</filename
> выглядят как следующий пример:</para
> 

<example>
<title
>Псевдоним в <application
>ghostscript</application
> Fontmap</title>
<screen
>/Algerian-Roman /Algerian       ;
</screen>
</example>

<para
>Заметьте, что в этом примере пробел перед ';' обязателен. &koffice; использует имя Algerian-Roman для шрифта Algerian. Вам надо будет добавить подобные строки для всех шрифтов которые &koffice; не может нормально отобразить. Чтобы упростить эту задачу Donovan Rebbechi написал на perl скрипт, который можно найти на <ulink url="http://pegasus.rutgers.edu/~elflord/font_howto/kwdfont"
>http://pegasus.rutgers.edu/~elflord/font_howto/kwdfont</ulink
>. Если предположить что у вас есть файл со шрифтом <filename
>/usr/share/ghostscript/fonts/fontfile.ttf</filename
>, то для того чтобы создать необходимые псевдонимы вам надо ввести <userinput
><command
>kwdfont</command
> <parameter
>/usr/share/ghostscript/fonts/fontfile.ttf</parameter
></userinput
>. Этот скрипт должен работать в большинстве случаев. Как было сказано выше, вам необходимо ознакомиться с <ulink url="http://pegasus.rutgers.edu/~elflord/font_howto/html/"
>Font HOWTO</ulink
>. </para>

</sect2>
</sect1>

<sect1 id="custom-gui">
<title
>Настройка &GUI; &koffice;</title>

<para
>Хотя &koffice; поставляется с &GUI; (графическим интерфейсом пользователя), который удовлетворяет потребности большинства пользователей, могут быть хорошие причины чтобы захотеть изменить вид программ. </para>

<para
>Моя мама, например, немного опасается кнопок и пунктов меню которых она не понимает. Чтобы приспособить &koffice; к её потребностям, я уменьшил &GUI; до необходимого минимума функций. Так как ей нужно только писать письма и использовать несколько шаблонов, нет необходимости для большей функциональности чем сохранение, загрузка, печать и т.п. </para>

<para
>Благодаря концепции <quote
>действий</quote
> &Qt; и &kde; вы можете свободно настраивать строки меню и панели инструментов &koffice;. К сожалению в настоящее время ещё не создано простых диалогов для этого. &koffice; хранит параметры &GUI; в &XML; файлах и вам придётся редактировать их. К счастью эта ситуацию в будущем изменится; а в настоящее время вам понадобятся базовые знания по тому, как работают &XML; документы (или <acronym
>HTML</acronym
>, который является подмножеством &XML;). [Концепцию <quote
>действий</quote
> необходимо объяснить более подробно -- kt.] </para>

<para
>Обычно, каждое приложение &koffice; содержит по крайней мере два &XML; файла: один описывает &GUI; оболочки (по большей части это то, что вы видите когда не открыт ни один документ), а второй - &GUI; соответствующей части то что вы обычно и видите). Например, для &kword; эти два файла называются <filename
>kword_shell.rc</filename
> и <filename
>kword.rc</filename
>. </para>

<para
>Далее приведен простой пример такого rc файла. </para>

<example>
<title
>Пример простого rc файла</title>
<screen
>&lt;!DOCTYPE QConfig &gt;&lt;qconfig&gt;
&lt;menubar&gt;
&lt;menu name="Edit"&gt;&lt;text&gt;Edit&lt;/text&gt;
&lt;action name="edit_cut"/&gt;
&lt;action name="edit_copy"/&gt;
&lt;action name="edit_paste"/&gt;
&lt;separator/&gt;
&lt;action name="edit_find"/&gt;
&lt;/menu&gt;
&lt;menu name="Insert"&gt;&lt;text&gt;Insert&lt;/text&gt;
&lt;action name="insert_picture"/&gt;
&lt;action name="insert_clipart"/&gt;
&lt;menu name="Variable"&gt;&lt;text&gt;Variable&lt;/text&gt;
&lt;action name="insert_var_datefix"/&gt;
&lt;action name="insert_var_datevar"/&gt;
&lt;/menu&gt;
&lt;/menu&gt;
&lt;/menubar&gt;
&lt;toolbars&gt;
&lt;toolbar name="edit_toolbar" position="right"&gt;
&lt;action name="edit_cut"/&gt;
&lt;action name="edit_copy"/&gt;
&lt;action name="edit_paste"/&gt;
&lt;/toolbar&gt;
&lt;/toolbars&gt;
&lt;/qconfig&gt;
</screen>
</example>

</sect1>

</chapter>

<chapter id="more-info">
<title
>Как получить дополнительную информацию</title>

<sect1 id="other-manuals">
<title
>Другие руководства &koffice;</title>

<para
>Для получения детальной информации по разным приложениям &koffice;, пожалуйста обращайтесь к соответствующим руководствам. </para>

</sect1>

<sect1 id="links">
<title
>Ссылки</title>

<para
>Если вы ищете информацию о &kde; или &koffice;, вам могут быть полезны следующие ссылки. </para>

<itemizedlist>
<listitem>
<para
><ulink url="http://koffice.kde.org"
>Домашняя страница &koffice;</ulink
>. Здесь вы можете найти информацию о том как получить и установить &koffice;, новости о разработке &koffice;, снимки экрана &etc; </para
></listitem>
<listitem>
<para
><ulink url="http://www.kde.org"
>Домашняя страница KDE</ulink
>. KDE это наиболее продвинутая и абсолютно свободная интерактивная среда для unix-подобных операционных систем. &koffice; использует библиотеки &kde;. </para
></listitem>
<listitem>
<para
><ulink url="http://www.trolltech.com"
>Trolltech</ulink
>. Создатели инструментария &Qt;. &kde; и &koffice; используют &Qt;. </para
></listitem>
</itemizedlist>

</sect1>

</chapter>

<chapter id="programmersinfo">
<title
>Программирование &koffice;</title>

<sect1 id="programmingintro">
<title
>Введение</title>
<para
>Если вы хотите участвовать в разработке &koffice;, вам могут быть интересны следующие ресурсы: </para>

<itemizedlist>
<listitem
><para
>На <ulink url="http://developer.kde.org"
>http://developer.kde.org</ulink
> вы найдёте много документации о программировании с &Qt; и &kde;. Здесь находится полная интерактивная версия документации библиотек &kde;. </para
></listitem
> 
<listitem
><para
>В исходных кодах &koffice; в каталоге <filename class="directory"
>example</filename
> находится пример приложения. </para
></listitem>
</itemizedlist>
</sect1>

<!--
<sect1 id="programmingplugin">
<title
>Developing &koffice; Plugins</title>
<para>
TO BE WRITTEN.
</para>
</sect1>

<sect1 id="scripting">
<title
>Scripting &koffice;</title>
<para>
This section gives you information on how you can automate and
script &koffice;.
</para>
<sect2 id="dcop">
<title
>DCOP</title>
<para>
TO BE WRITTEN.
</para>
</sect2>
</sect1>

<sect1 id="programmingreport">
<title
>Creating Reports with Kugar</title>
<para>
TO BE WRITTEN.

Maybe this should go completely into the
Kugar manual? So we just keep a reference to the kugar manual
here.

</para>
</sect1>

<sect1 id="programmingdatabase">
<title
>Integrating a Database with Kexi</title>
<para>
TO BE WRITTEN.

Again, should this just be a reference to kexi documentation?

</para>
</sect1>

<sect1 id="programmingcomponents">
<title
>Developing &koffice; Components</title>
<para>
TO BE WRITTEN.
</para>
</sect1>

-->

</chapter>

<chapter id="legal">
<title
>Авторские права и лицензия</title>

<para
>&koffice; - это результат объединённых усилий множества разработчиков. Авторские права на каждый файл с исходными текстами &koffice; принадлежат тем людям которые этот файл написали, а их имена, вместе с лицензией которая применяется к каждому конкретному файлу, находится в начале каждого файла с исходными текстами. Имена основных разработчиков можно найти на <ulink url="http://www.koffice.org/people.php"
>http://www.koffice.org/people.php</ulink
>. </para>

<para
>Авторские права на это руководство принадлежат Джосту Скэнку (Jost Schenck). Его можно распространять свободно при условии неизменения информации об авторских правах. Его можно изменять при условии ,что вы отправите изменения автору, или разместите их на <acronym
>CVS</acronym
> &kde;. Автор не несёт ответственности за всё, что происходит в результате использования этого руководства. </para>

<para
>Авторские права на другие руководства &koffice; принадлежат из авторам. </para>
&underFDL; &underGPL; </chapter>
  
<appendix id="installation">
<title
>Установка</title>

<sect1 id="requirements">
<title
>Системные требования</title>

<para
>Чтобы установить и использовать &koffice; вам необходимы: </para>

<itemizedlist>
<listitem
><para
>Работающая система &UNIX; (например, &Linux; или BSD) с настроенной системой &X-Window; (например XFree86 или X.org). </para
></listitem>
<listitem
><para
>Библиотеки &Qt; 3.3 от Trolltech. Для получения большей информации смотри <ulink url="http://www.trolltech.com/"
>www.trolltech.com</ulink
>. </para
></listitem>
<listitem
><para
>K Desktop Environment 3.3 (&kde; 3.3) или новее. &koffice; невозможно откомпилировать с более ранними версиями &kde;. Информацию по получению и установке &kde; можно найти на &kde-http; </para
></listitem>
<listitem
><para
>Права на установку программного обеспечения на ваш компьютер. Если это ваш собственный компьютер, то это не проблема. Если вы используете компьютер, находящийся в сети, проконсультируйтесь с вашим администратором. </para
></listitem>
</itemizedlist>

<para
>Заметьте, что несмотря на то, что вам нужны библиотеки &kde; 3.3 (или более новые), вы можете использовать &koffice; в других интерактивных средах (например, XFCE или Gnome). </para>

<para
>Если вы планируете <link linkend="from-source"
>компилировать &koffice; из исходных текстов</link
>, вам также потребуется: </para>

<itemizedlist>
<listitem
><para
><command
>automake</command
> 1.6.1 или выше. Загрузить её можно с <ulink url="ftp://ftp.gnu.org/pub/gnu/automake/"
>ftp://ftp.gnu.org/pub/gnu/automake/</ulink
> </para
></listitem>
<listitem
><para
><command
>autoconf</command
> 2.53 или выше. Загрузить её можно с<ulink url="ftp://ftp.gnu.org/pub/gnu/autoconf/"
>ftp://ftp.gnu.org/pub/gnu/autoconf/</ulink
> </para
></listitem>
<listitem
><para
>Компилятор C++ который поддерживает исключения, лучше если это будет самая новая версия GCC. (Информация о его получении и установке находится на <ulink url="http://gcc.gnu.org"
>http://gcc.gnu.org</ulink
>.) </para
></listitem>
<listitem
><para
>Пакеты разработки Troll Tech &Qt; 3.3. Если эти пакеты не установлены на вашем компьютере и они не включены в дистрибутив вашей операционной системы, то возможно вам придётся скомпилировать &Qt; из исходных текстов. Эти исходные коды можно загрузить с <ulink url="http://www.trolltech.com/download/index.html"
>http://www.trolltech.com/download/index.html</ulink
>. </para
></listitem>
</itemizedlist>

<para
>После того как вы определили что система удовлетворяет этим требованиям вы должны решить: компилировать исходный код или установить предварительно откомпилированные бинарные пакеты. </para>

<para
>Если вы хотите найти самую свежую бинарную вервию &koffice;, то вы можете найти больше информации если пройдёте по этой ссылке: <link linkend="getting-binaries"
>Getting Binary Packages</link
>. </para>

<para
>Если для вашей системы нет предварительно откомпилированных бинарных пакетов, то вы можете загрузить исходные тексты и откомпилировать их самостоятельно. Инструкции по получению текущих исходных текстов (и что делать с этими исходными текстами после того как вы получили их) можно найти по этой ссылке: <link linkend="getting-source"
>Getting the source code</link
>. </para
> 

<note
><para
>Для получения большей информации посетите <ulink url="http://koffice.kde.org"
>домашнюю страницу &koffice;</ulink
>. Здесь вы всегда сможете найти самую свежую информацию о бинарных дистрибутивах и дистрибутивах с исходными текстами! </para
></note>

</sect1>


<sect1 id="getting-binaries">
<title
>Получение бинарных пакетов</title>

<para
>Самые свежие версии бинарных пакетов можно загрузить с: </para>

<para>
<ulink url="http://koffice.kde.org/releases/1.4.0-release.php"
>http://koffice.kde.org/releases/1.4.0-release.php</ulink>
</para>

<para
>или с одного из множества зеркал. Текущий список сайтов с зеркалами можно найти на: </para>

<para>
<ulink url="http://www.kde.org/mirrors.html"
>http://www.kde.org/mirrors.html</ulink>
</para>

<note
><para
>Необходимо использовать бинарные пакеты совместимые с вашей системой. Если вы используете операционную систему &Linux; и не можете найти бинарный пакет на веб сайте &koffice;, или одном из зеркал, возможно вы сможете найти его на сайте дистрибутива. </para
></note>

</sect1>


<sect1 id="from-binaries">
<title
>Из бинарных пакетов</title>

<para
>Чтобы установить &koffice; из предварительно откомпилированных бинарных пакетов вам надо: </para>

<procedure>
<step>
<para
>Убедиться в том что установлено и работает всё необходимое программное обеспечение (за исключением, &koffice;, конечно). Системные требования можно узнать перейдя по этой ссылке: <link linkend="requirements"
>Системные требования</link
>.</para
></step>
<step
><para
>Загрузите бинарный пакет (или пакеты) &koffice; во временный каталог.</para
></step>
<step
><para
>Если имя файла заканчивается на <literal role="extension"
>.rpm</literal
> (файл менеджера пакетов &RedHat;), то &koffice; можно установить следующей командой: <screen
><prompt
>$</prompt
><userinput
><command
>rpm</command
><option
>-U </option
><replaceable
>имя_файла</replaceable
></userinput
></screen
> 
</para>
<para
>Если имя файла заканчивается на <literal role="extension"
>.deb</literal
> (файл пакета Debian), то &koffice; можно установить следующей командой: <screen
><prompt
>$</prompt
><userinput
><command
>dpkg</command
><option
>-i </option
><replaceable
>имя_файла</replaceable
></userinput
></screen>
</para>
<para
>Если имя файла заканчивается на <literal role="extension"
>.tar.gz</literal
> или <literal role="extension"
>.tgz</literal
> (файл с тарболом), то &koffice; можно установить следующими командами:</para>
<screen
><prompt
>$</prompt
> <userinput
><command
>cd</command
> /</userinput>
<prompt
>$</prompt
> <userinput
><command
>tar</command
><option
>-xzvf</option
><replaceable
>имя_файла</replaceable
></userinput
>
</screen>
<para
>В этих примерах <emphasis
>имя_файла</emphasis
> необходимо заменить на полное имя файла с пакетом <emphasis
>включая полный путь</emphasis
>, если вы находитесь не в том каталоге куда сохранили файл.</para>
</step>
<step
><para
>И это всё. Теперь &koffice; установлен на вашем компьютере. </para
></step>
</procedure>

<note
><para
>Если на вашем компьютере установлена графическая оболочка для управления пакетами, такая как &kpackage; или <application
>GnoRPM</application
>, то может оказаться более удобным использовать её, а не командную строку. Проконсультируйтесь в документации по этой программе как устанавливать с её помощью. </para
></note>

</sect1>


<sect1 id="getting-source">
<title
>Получение исходного кода</title>

<para
>Текущую версию исходных кодов можно загрузить с <ulink url="http://koffice.kde.org/"
>http://koffice.kde.org/</ulink
> или с одного из множества сайтов зеркал. Текущий список сайтов зеркал можно найти на: </para>

<para>
<ulink url="http://www.kde.org/mirrors.html"
>http://www.kde.org/mirrors.html</ulink>
</para>

</sect1>


<sect1 id="from-source">
<title
>Из исходных кодов</title>

<para
>Если вы хотите собрать &koffice; из исходных кодов, вам необходимо: </para>

<procedure>
<step
><para
>Убедиться в том что установлено и работает всё необходимое программное обеспечение (за исключением, &koffice;, конечно). Системные требования можно узнать перейдя по этой ссылке: <link linkend="requirements"
>Системные требования</link
>. </para
></step>
<step
><para
>Загрузить исходные коды &koffice; во временный каталог. </para
></step>
<step
><para
>Если имя файла заканчивается на <literal role="extension"
>.src.rpm</literal
> (файл менеджера пакетов &RedHat;), то исходные тексты &koffice; можно установить командой: <screen
><prompt
>$</prompt
><userinput
><command
>rpm</command
><option
>-U</option
> <replaceable
>имя_файла</replaceable
></userinput
></screen
></para>

<para
>Если имя файла заканчивается на <literal role="extension"
>.src.tar.gz</literal
> или <literal role="extension"
>.src.tgz</literal
> (файл с тарболом), то исходные тексты &koffice; можно установить командой: <screen
><prompt
>$</prompt
> <userinput
><command
>tar</command
> <option
>-xzvf</option
> <replaceable
>имя_файла</replaceable
></userinput
></screen>
</para
></step>
<step
><para
>Теперь исходные коды &koffice; установлены на вашем компьютере.</para
></step>
</procedure>

</sect1>

</appendix>

&documentation.index; 
</book>

