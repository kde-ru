<!-- If you want to edit or validate this document separately, uncomment
this prolog
<?xml version="1.0" ?>
<!DOCTYPE chapter PUBLIC "-//KDE//DTD DocBook XML V4.1-Based Variant V1.0//EN" "dtd/kdex.dtd">
-->
<sect1 id="kugartemplate">
<sect1info>
<authorgroup>
<author
><firstname
>Alexander</firstname
> <surname
>Dymo</surname
>  <affiliation
> <address
><email
>cloudtemple@mksat.net </email
></address>
</affiliation>
</author>
<author
><firstname
>Phil</firstname
> <surname
>Thompson</surname
>  <affiliation
> <address
><email
>phil@river-bank.demon.co.uk </email
></address>
</affiliation>
</author>
<othercredit role="translator"
><firstname
>Олег</firstname
><surname
>Баталов</surname
><affiliation
><address
><email
>olegbatalov@mail.ru</email
></address
></affiliation
><contrib
>Перевод на русский язык</contrib
></othercredit
> 
</authorgroup>
</sect1info>
<title
>Элемент <sgmltag class="element"
>KugarTemplate</sgmltag
></title>

<para
>Элемент  <sgmltag class="element"
>KugarTemplate</sgmltag
> определяет атрибуты отчёта, в частности размер страницы, её ориентацию и выравнивание. </para>

<synopsis
>&lt;!ELEMENT KugarTemplate (<link linkend="report-header-and-footer"
>ReportHeader</link
>, <link linkend="page-header-and-footer"
>PageHeader</link
>, <link linkend="detail-header-and-footer"
>DetailHeader</link
>*, <link linkend="detail"
>Detail</link
>*, <link linkend="detail-header-and-footer"
>DetailFooter</link
>*, <link linkend="page-header-and-footer"
>PageFooter</link
>, <link linkend="report-header-and-footer"
>ReportFooter</link
>)&gt;
&lt;!ATTLIST KugarTemplate
PageSize        CDATA #REQUIRED
PageOrientation CDATA #REQUIRED
TopMargin       CDATA #REQUIRED
BottomMargin    CDATA #REQUIRED
LeftMargin      CDATA #REQUIRED
RightMargin     CDATA #REQUIRED&gt;
</synopsis>

<variablelist>

<varlistentry>
<term
>Элементы</term>

<listitem>

<para
><sgmltag class="element"
>KugarTemplate</sgmltag
>содержит следующие элементы: </para>

<variablelist>
<varlistentry>
<term
><link linkend="report-header-and-footer"
><sgmltag class="element"
>ReportHeader</sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>ReportHeader</sgmltag
> определяет секцию отчёта, которая обычно печатается в его начале. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="page-header-and-footer"
><sgmltag class="element"
>PageHeader </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>PageHeader</sgmltag
> определяет секцию отчёта, которая обычно печатается в верхней части каждой страницы отчёта. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="detail-header-and-footer"
><sgmltag class="element"
>DetailHeader </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>DetailHeader</sgmltag
> определяет секцию отчёта, которая обычно печатается перед секцией указанного уровня. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="detail"
><sgmltag class="element"
>Detail </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>DetailHeader</sgmltag
> определяет секцию отчёта, которая содержит его данные. Отчёт может содержать неограниченное количество секций. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="detail-header-and-footer"
><sgmltag class="element"
>DetailFooter </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>DetailFooter</sgmltag
> определяет секцию отчёта, которая печатается после секции данных указанного уровня. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="page-header-and-footer"
><sgmltag class="element"
>PageFooter </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>PageFooter</sgmltag
> определяет секцию отчёта, которая печатается внизу каждой страницы отчёта. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><link linkend="report-header-and-footer"
><sgmltag class="element"
>ReportFooter </sgmltag
></link
></term>
<listitem>
<para
>Элемент <sgmltag class="element"
>PageFooter</sgmltag
> определяет секцию отчёта, которая обычно печатается в конце отчёта. </para>
</listitem>
</varlistentry>

</variablelist>

</listitem>

</varlistentry>

<varlistentry>
<term
>Attributes</term>
<listitem>

<variablelist>
<varlistentry>
<term
><anchor id="kut-pagesize"/><sgmltag class="attribute"
>PageSize </sgmltag
></term>
<listitem>
<para
>Установить размер страницы отчёта. Доступны следующие значения: </para>

<informaltable>
<tgroup cols="2">
<thead>
<row>
<entry
>Значение</entry>
<entry
>Размер страницы</entry>
</row>
</thead>

<tbody>
<row>
<entry
>0</entry>
<entry
>A4</entry>
</row>
<row>
<entry
>1</entry>
<entry
>B5</entry>
</row>
<row>
<entry
>2</entry>
<entry
>Letter</entry>
</row>
<row>
<entry
>3</entry>
<entry
>Legal</entry>
</row>
<row>
<entry
>4</entry>
<entry
>Executive</entry>
</row>
<row>
<entry
>5</entry>
<entry
>A0</entry>
</row>
<row>
<entry
>6</entry>
<entry
>A1</entry>
</row>
<row>
<entry
>7</entry>
<entry
>A2</entry>
</row>
<row>
<entry
>8</entry>
<entry
>A3</entry>
</row>
<row>
<entry
>9</entry>
<entry
>A5</entry>
</row>
<row>
<entry
>10</entry>
<entry
>A6</entry>
</row>
<row>
<entry
>11</entry>
<entry
>A7</entry>
</row>
<row>
<entry
>12</entry>
<entry
>A8</entry>
</row>
<row>
<entry
>13</entry>
<entry
>A9</entry>
</row>
<row>
<entry
>14</entry>
<entry
>B0</entry>
</row>
<row>
<entry
>15</entry>
<entry
>B1</entry>
</row>
<row>
<entry
>16</entry>
<entry
>B10</entry>
</row>
<row>
<entry
>17</entry>
<entry
>B2</entry>
</row>
<row>
<entry
>18</entry>
<entry
>B3</entry>
</row>
<row>
<entry
>19</entry>
<entry
>B4</entry>
</row>
<row>
<entry
>20</entry>
<entry
>B6</entry>
</row>
<row>
<entry
>21</entry>
<entry
>B7</entry>
</row>
<row>
<entry
>22</entry>
<entry
>B8</entry>
</row>
<row>
<entry
>23</entry>
<entry
>B9</entry>
</row>
<row>
<entry
>24</entry>
<entry
>C5E</entry>
</row>
<row>
<entry
>25</entry>
<entry
>Comm10E</entry>
</row>
<row>
<entry
>26</entry>
<entry
>DLE</entry>
</row>
<row>
<entry
>27</entry>
<entry
>Folio</entry>
</row>
<row>
<entry
>28</entry>
<entry
>Ledger</entry>
</row>
<row>
<entry
>29</entry>
<entry
>Tabloid</entry>
</row>
<row>
<entry
>30</entry>
<entry
>NPageSize</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</listitem>
</varlistentry>

<varlistentry>
<term
><anchor id="kut-pageorient"/><sgmltag class="attribute"
>PageOrientation</sgmltag
></term>
<listitem>
<para
>Установить ориентацию страницы отчёта. </para>

<informaltable>
<tgroup cols="2">
<thead>
<row>
<entry
>Значение</entry>
<entry
>Ориентация</entry>
</row>
</thead>
<tbody>
<row>
<entry
>0</entry>
<entry
>Портретная</entry>
</row>
<row>
<entry
>1</entry>
<entry
>Альбомная</entry>
</row>
</tbody>
</tgroup>
</informaltable>
</listitem>
</varlistentry>

<varlistentry>
<term
><anchor id="kut-topmargin"/><sgmltag class="attribute"
>TopMargin</sgmltag
></term>
<listitem>
<para
>Устанавливает верхнее поле страницы отчёта </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><anchor id="kut-botmargin"/><sgmltag class="attribute"
>BottomMargin</sgmltag
></term>
<listitem>
<para
>Устанавливает нижнее поле страницы отчёта </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><anchor id="kut-leftmargin"/><sgmltag class="attribute"
>LeftMargin</sgmltag
></term>
<listitem>
<para
>Устанавливает левое поле страницы отчёта </para>
</listitem>
</varlistentry
><varlistentry>
<term
><anchor id="kut-rightmargin"/><sgmltag class="attribute"
>RightMargin</sgmltag
></term>
<listitem>
<para
>Устанавливает правое поле страницы отчёта </para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
</variablelist>
</sect1>
