<chapter id="usage">
<chapterinfo>
<authorgroup>
<author
><firstname
>Ben</firstname
> <surname
>Lamb</surname
>  <affiliation
> <address
> <email
>kde@zurgy.org </email>
</address>
</affiliation>
</author>
<othercredit role="translator"
><firstname
>Олег</firstname
><surname
>Баталов</surname
><affiliation
><address
><email
>olegbatalov@mail.ru</email
></address
></affiliation
><contrib
>Перевод на русский язык</contrib
></othercredit
> 
</authorgroup>
</chapterinfo>

<title
>Основы использования</title>

<sect1 id="starting">
<title
>Запуск &kivio;</title>
<para
>Подобно остальным приложениям &koffice;, при старте &kivio; открывает диалог создания документа. Три вкладки предлагают создание нового документа, открытие сохранённого или выбор документа из списка недавно сохранённых.</para>
<para
>Если вы не знакомы с &kivio; начните с создания пустого документа, дважды щёлкнув на шаблоне <guilabel
>Пустой документ</guilabel
> </para>
<para
>Главное окно &kivio; содержит вид текущего документа. отображающий, список страниц и края листа, а также сетку. Окружают этот вид горизонтальные и вертикальные линейки. По умолчанию панель инструментов помещена с левой стороны, а ниже вида документа вкладки страниц. Ниже всего находится строка состояния, в которой показывается текущая позиция курсора как положение на странице.</para>
<para
>В верхней части окна расположены две основные панели инструментов. Они содержат кнопки для наиболее часто используемых возможностей&kivio;.</para>
</sect1>

<sect1 id="stencils">
<title
>Использование объектов</title>
<para
>Diagrams are created by placing different pre-drawn shapes and stencils on the page. Stencils can represent many different things and &kivio; comes with a wide selection. For ease of reference they are grouped into collections known as stencil sets. To use a stencil set select <menuchoice
><guimenu
>Tools</guimenu
><guimenuitem
>Add Stencil Set</guimenuitem
></menuchoice
>. The stencil sets have been sub-divided into several groups; a sub-menu lists these. The shapes in the stencil set are shown on the left-hand side of the document view.</para>
<para
>Объекты сгруппированы в наборы только для организационной цели; вполне возможно добавлять несколько наборов объектов в документ и &kivio; позволяет вам свободно смешивать и совмещать объекты из различных наборов.</para>
<para
>Чтобы поместить объект в документ, просто перетащите его из области объектов на страницу.</para>

<sect2>
<title
>Выделение объектов</title>
<para
>&kivio; identifies the stencil currently being edited by displaying eight green squares on the corners and edges of a square around the stencil. Stencils shown in this manner are described as being selected. The green rectangles are known as handles. To change the currently selected stencil move the mouse pointer over the stencil you wish to select and press the &LMB;. To select all the stencils on the current page select <menuchoice
><guimenu
>Edit</guimenu
><guimenuitem
>Select All</guimenuitem
></menuchoice
>. To deselect all stencils select <menuchoice
><guimenu
>Edit</guimenu
><guimenuitem
>Select None</guimenuitem
></menuchoice
>. </para>
</sect2>

<sect2>
<title
>Перемещение объектов</title>
<para
>To move a stencil to another position on the page, select the stencil. Note the pointer will change to a cross with four arrows. Hold down the &LMB; and move the mouse to the new position. Release the mouse button, and the stencil will move to the new position.</para>
</sect2>

<sect2>
<title
>Изменение размеров объектов</title>
<para
>Select the stencil, and then move the mouse pointer over one of the handles. Note the mouse pointer change. Hold down the &LMB; and move the mouse. The coordinates of the stencil will be adjusted continuously as the mouse is moved. When you are happy with the new size release the mouse button and the stencil will be resized.</para>
</sect2>

<sect2>
<title
>Отмена ошибочных действий</title>
<para
>Если в любой момент вы захотите отменить последние изменения, которые вы сделали в документе, можете выбрать <menuchoice
> <guimenu
>Правка</guimenu
><guimenuitem
>Отменить действие</guimenuitem
></menuchoice
> для возврата изменения. Это действие  также присутствует и на панели инструментов.</para>
<para
>Если вы отменили действие и решили, что это было ошибкой, выберите пункт меню <menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Повторить отменённое действие</guimenuitem
> </menuchoice
> чтобы восстановить изменения. &kivio; хранит несколько действий для отмены/восстановления нескольких изменений.</para>
</sect2>

<sect2>
<title
>Ограничения объектов</title>
<para
>Обратите внимание, что объекты в настоящее время не могут вращаться.</para>
</sect2>

<sect2>
<title
>Надписи</title>
<para
>Объекты могут содержать текст, описывающий их функции. Чтобы добавить текст к объекту, поместите курсор мыши на объект и дважды щёлкните на нём. Появится диалог в котором вы можете указать текст надписи. Введите текст и нажмите кнопку <guibutton
>OK </guibutton
>. Текст должен появиться в объекте.</para>
<para
>Размер шрифта, выравнивание и стиль текста могут быть настроены при выборе пункт меню <menuchoice
><guimenu
>Формат</guimenu
> <guimenuitem
>Текст</guimenuitem
></menuchoice
>. Альтернативно вы можете использовать панель форматирования текста. Настройки текста применяются только к выделенным объектам.</para>
</sect2>

<sect2>
<title
>Выделение нескольких объектов</title>
<para
>В &kivio; существует два способа выделить несколько объектов.</para>
<orderedlist>
<listitem>
<para
>Щёлкнуть внутри первого объекта, который вы хотите выделить. Удерживайте нажатой клавишу Shift, пока вы щёлкаете на последующих объектах. Все выделенные объекты будут показываться с маркерами вокруг них.</para>
</listitem>
<listitem>
<para
>Move the mouse pointer to a blank area of the document near the stencils you want to select.</para>
<para
>Hold down the &LMB; and move the mouse to the other side of the stencils you want to select, drawing a rectangle around them.</para>
<para
>The rectangle must completely encompass the stencils for them to be selected. </para>
</listitem>
</orderedlist>
</sect2>

<sect2>
<title
>Изменение цвета заливки и линий</title>
<para
>Объекты могут быть заполнены однородным цветом и цвет линии также может быть изменён. Выделите объект, затем выберите пункт меню <menuchoice
><guimenu
>Формат</guimenu
><guimenuitem
>Объекты и соединения</guimenuitem
></menuchoice
>.</para>
<para
>На панели формат также присутствует кнопка <guibutton
>Цвет фона</guibutton
>. При нажатии на стрелке с правой стороны этой кнопки появится выпадающая палитра для выбора цвета. Щелчок непосредственно на кнопке откроет диалог выбора цвета, позволяющий вам выбрать любой цвет. Аналогичная кнопка существует и для смены цвета линий.</para>
</sect2>

<sect2>
<title
>Изменение толщины линий</title>
<para
>Толщина линий может быть скорректирована в пункте меню <menuchoice
><guimenu
>Формат</guimenu
><guimenuitem
>Объекты и соединения</guimenuitem
></menuchoice
>, либо с помощью выпадающего списка <guibutton
>Толщина линии</guibutton
> в панели формат. Толщина линии может быть указана явно в миллиметрах или уменьшена/увеличена с помощью стрелок справа от текстового поля.</para>
</sect2>

<sect2>
<title
>Соединение объектов</title>
<para
>Инструмент соединений &kivio; позволяет рисовать линии между объектами. Линии остаются связанными с объектами когда объекты перемещаются на новое место в странице.</para>
<para
>Инструмент соединения расположен на панели инструментов (по умолчанию она находится у левого края окна). Выберите этот инструмент и указатель мыши должен смениться. </para>
<para
>Обратите внимание, что у объектов на гранях появились небольшие синие крестики. Они обозначают точки, от которых могут идти соединительные линии.</para>
<para
>Для соединения двух объектов поместите курсор на одну из точек соединения на первом объекте, нажмите &LMB; и, удерживая её, переместите курсор мыши к точке соединения на другом объекте. Зелёный квадрат на конце линии изменится на красный, когда он находится на точке соединения, сообщая таким образом, что объекты могут быть соединены линией. </para>
<para
>Когда вы закончили использование инструмента соединения, щёлкните на инструменте стрелки, чтобы вернуть обычные функции выбора.</para>
<note
><para
>Объект может иметь несколько соединений к одной точке соединения.</para
></note>
<para
>Некоторые свойства линии соединения могут быть настроены, включая её толщину и цвет. Это делается также, как и для объектов.</para>
<para
>Соединения также могут иметь надписи. Чтобы добавить текст надписи к  соединительной линии, просто дважды щёлкните на ней и введите текст в появившемся диалоге. Надпись будет отображаться с зелёным маркером. Используйте его для изменения положения надписи.</para>
<para
>Стрелки можно добавить на начало или конец соединительной линии. Выделите соединительную линию и выберите <menuchoice
><guimenu
>Формат</guimenu
><guimenuitem
>Стрелки</guimenuitem
></menuchoice
>.</para>
<para
>Ограничения: соединения в настоящее время не выравниваются по объектам, вам необходимо настраивать это вручную. Текст всегда отображается горизонтально, поворот текста в настоящее время невозможен. </para>
</sect2>

<sect2>
<title
>Группировка объектов</title>
<para
>Несколько объектов могут быть сгруппированы. Когда это сделано, любые изменения сделанные с одним объектом, затрагивают и остальные в группе.</para>
<para
>To make a group select the stencils and select <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Group Selected Stencils</guimenuitem
></menuchoice
>.</para>
<para
>The procedure can be reversed by selecting a group of stencils and select <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Ungroup Selected Stencils</guimenuitem
></menuchoice
>.</para>
</sect2>

<sect2>
<title
>Наложение объектов</title>
<para
>Stencils can be positioned on top of one another. A stencil drawn on top of another stencil will obscure the stencil underneath it. To adjust the drawing order select one of the stencils and select either <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Bring to Front</guimenuitem
></menuchoice
> or <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Send to Back</guimenuitem
></menuchoice
>.</para>
<para
><guimenuitem
>Переместить вниз</guimenuitem
> заставит объект быть перекрытым любым другим.<guimenuitem
>Переместить вверх</guimenuitem
> заставит объект перекрывать любой другой. </para>
</sect2>

<sect2>
<title
>Блокировка позиции объектов</title>
<para
>Свойства объектов могут быть заблокированы для предотвращения их случайного изменения. Для установки защиты объекта используется плавающая панель Защита. Она может быть вызвана выбором <menuchoice
><guimenu
>Вид</guimenu
><guisubmenu
>Выплывающие панели</guisubmenu
><guimenuitem
>Защита</guimenuitem
></menuchoice
>. Выберите объект или объекты, которые вы хотите заблокировать, затем пометьте в панели защиты свойства, которые хотите защитить.</para>
<para
>Защита доступна для высоты, ширины, пропорций, позиции X и Y и удаления. Защита удаления препятствует удалению объекта из документа.</para>
</sect2>

<sect2>
<title
>Размеры и положение объектов</title>
<para
>Для того, чтобы точно указать размеры объекта, используется плавающая панель Геометрия. Для отображения этой панели выберите <menuchoice
><guimenu
>Вид</guimenu
><guisubmenu
>Выплывающие панели</guisubmenu
><guimenuitem
> Положение и размеры</guimenuitem
></menuchoice
>. Панель содержит текстовые поля для указания вертикальной и горизонтальной позиции объекта и его высоту и ширину.</para>

<tip
><para
>Хотя единицами измерения являются пункты, вы можете вводить в поле и другие единицы измерения. Например, введя 2.5cm, вы укажите размер в сантиметрах. Можно указать также миллиметры (mm) и дюймы (in).</para
></tip>

</sect2>

</sect1>

<sect1 id="viewing">
<title
>Просмотр документа</title>

<sect2>
<title
>Приближение и удаление</title>
<para
>To adjust the zoom level select <menuchoice
><guimenu
>View</guimenu
><guimenuitem
>Zoom Level</guimenuitem
></menuchoice
>. This sub-menu contains a list of percentages from 33% to 500%; selecting one of these magnify the document by that percentage.</para>
<para
>Также список масштабов присутствует и на панели Правка, которая по умолчанию находится в верхней части экрана.</para>
<para
>Второй метод управления масштабом состоит в использовании инструмента Увеличитель. Он доступен в меню инструментов в левой части окна. Щелчок на документе инструментом увеличитель увеличивает масштаб.  Удержание нажатой клавиши &Shift; при щелчке уменьшает масштаб.</para>
<para
>Как альтернативный вариант вы можете удерживая нажатой &LMB; нарисовать прямоугольник, и, когда отпустите клавишу мыши, будет установлен такой масштаб, чтобы эта часть документа заняла весь вид.</para>
</sect2>

<sect2>
<title
>Перемещение документа в виде</title>
<para
>Инструмент Передвигать позволяет вам перемещать документ, регулируя его видимую область. Это очень полезно при работе с большим значением масштаба, когда документ не виден полностью. Инструмент Передвигать находится под кнопкой Масштаб. Когда он активирован, указатель мыши похож на руку. Для сдвига документа, поместите указатель мыши на текущую видимую часть документа, и, удерживая нажатой&LMB;, двигайте документ.</para>
</sect2>

<sect2>
<title
>Просмотр</title>
<para
>Выплывающая панель Просмотр предоставляет быстрый способ скорректировать масштаб и перемещаться по документу. Чтобы включить её, выберите пункт меню <menuchoice
><guimenu
>Вид</guimenu
><guisubmenu
>Выплывающие панели</guisubmenu
><guimenuitem
>Просмотр</guimenuitem
></menuchoice
>.</para>
<para
>Панель содержит в верхней части пять кнопок: <guibutton
>Увеличить </guibutton
>, <guibutton
>Уменьшить</guibutton
>, <guibutton
>Показать границы страницы</guibutton
> и две кнопки <guibutton
>Автомасштаба</guibutton
>. Одна для увеличения, другая - для уменьшения.</para>
<para
>Миниатюрный вид документа показан внизу панели. Красный прямоугольник указывает часть документа, которая видна в текущий момент. Щелчок на этой панели перемещает вид. Прямоугольник также может перетаскиваться.</para>
<para
>В нижней части панели присутствует ползунок для регулирования масштаба и поле, содержащее текущее значение масштаба.</para>
</sect2>

<sect2>
<title
>Использование выплавающих панелей</title>
<para
>All dockers have a handle on one edge which enables them to be moved around and positioned on any edge of the main view. They can also float anywhere on the screen. If they are not floating they shrink when not in use to remain unobtrusive, however, this behavior can be toggled by clicking on the icon that looks like a small pin. Next to this is a button marked with a cross that removes the docker.</para>
</sect2>

<sect2>
<title
>Несколько видов</title>
<para
>Вы можете иметь несколько видов одного и того-же документа, отображаемых одновременно.</para>
<para
>Выберите <guimenuitem
>Новый вид</guimenuitem
> в меню <guimenu
>Вид</guimenu
> и будет открыто новое окно &kivio;, отображающее тот-же документ.</para>
<para
>Дополнительные окна могут быть закрыты и &kivio; запросит сохранения документа только при закрытии последнего окна. Все окна, относящиеся к одному документа, могут быть закрыты одновременно при выборе <menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Закрыть все виды</guimenuitem
></menuchoice
>.</para>
</sect2>

<sect2>
<title
>Разделение видов</title>
<para
>Также возможно разделить окно на две или более части, которые могут использоваться для просмотра различных частей одного документа. Чтобы разделить окно, выберите <menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Разделить вид</guimenuitem
></menuchoice
>. Ориентация разбиения может быть выбрана как вертикальная или горизонтальная в пункте меню <menuchoice
><guimenu
>Вид</guimenu
><guimenuitem
>Расположение разделителя</guimenuitem
></menuchoice
>. Чтобы восстановить окно в первоначальное состояние, выберите <menuchoice
><guimenu
>Вид</guimenu
><guimenuitem
>Удалить вид</guimenuitem
></menuchoice
>. </para>
</sect2>

<sect2>
<title
>Управление видами</title>
<para
>The View Manager allows you to store the current display settings, including zoom level and position. You can then adjust the view and still be able to revert back to the previous settings. A number of different settings can be stored and given names for ease of reference.</para>
<para
>Чтобы отобразить менеджера видов, выберите <menuchoice
><guimenu
>Вид</guimenu
> <guisubmenu
>Выплывающие панели</guisubmenu
><guimenuitem
>Виды</guimenuitem
></menuchoice
>. Панель содержит пять кнопок и список сохранённых видов.</para>
<para
>Первая кнопка - <guibutton
>Добавить текущий вид</guibutton
>, сохраняет текущие параметры вида. Как только параметры сохранены, вы можете изменять их. Для возвращение к любым сохранённым настройкам, щёлкните на их имени в списке.</para>
<para
>По умолчанию &kivio; именует настройки видов номером страницы и масштабом. Чтобы ввести другое имя, нажмите среднюю кнопку, <guibutton
>Переименовать</guibutton
>, что позволит указать другое название.</para>
<para
>Вторая кнопка, <guibutton
>Удалить</guibutton
>, удаляет текущий выделенный набор из сохранённых настроек.</para>
<para
>Заключительные две кнопки <guibutton
>Переместить выше</guibutton
> и<guibutton
>Переместить ниже</guibutton
>, позволяют изменять порядок настроек в списке.</para>
</sect2>

<sect2>
<title
>Настройка экрана</title>
<para
>Меню <guimenu
>Вид</guimenu
> содержит параметры для отображения  границ и полей страницы, линеек, направляющих и сетки.</para>
</sect2>

</sect1>

</chapter>
