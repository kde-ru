<chapter id="working">
<chapterinfo>
<authorgroup>
<author
><firstname
>Ben</firstname
> <surname
>Lamb</surname
>  <affiliation
> <address
> <email
>kde@zurgy.org </email>
</address>
</affiliation>
</author>
<othercredit role="translator"
><firstname
>Олег</firstname
><surname
>Баталов</surname
><affiliation
><address
><email
>olegbatalov@mail.ru</email
></address
></affiliation
><contrib
>Перевод на русский язык</contrib
></othercredit
> 
</authorgroup>
</chapterinfo>

<title
>Работа с &kivio;</title>

<sect1 id="opening">
<title
>Открытие документа</title>
<para
>To open a previously saved document select <menuchoice
><guimenu
>File</guimenu
><guimenuitem
>Open...</guimenuitem
></menuchoice
>, a standard &kde; file dialog box will appear.</para>
<para
>Недавно сохранённые документы перечислены в подменю <menuchoice
><guimenu
>Файл</guimenu
> <guimenuitem
>Открыть последние</guimenuitem
></menuchoice
>.</para>
</sect1>

<sect1 id="saving">
<title
>Сохранение документа</title>
<para
>To save the current document select <menuchoice
><guimenu
>File</guimenu
><guimenuitem
>Save</guimenuitem
></menuchoice
> or <menuchoice
><guimenu
>File</guimenu
><guimenuitem
>Save As...</guimenuitem
></menuchoice
> to give an existing file a different name.</para>
<note
><para
>Можно сохранить документ в формате ранней версии &kivio;. Выберите версию, в которой необходимо сохранить, в выпадающем списке <guilabel
>Фильтр</guilabel
> диалога сохранения.</para
></note>
</sect1>

<sect1 id="printing">
<title
>Печать</title>
<para
>The document can be printed by selecting <menuchoice
><guimenu
>File</guimenu
><guimenuitem
>Print...</guimenuitem
></menuchoice
>. The standard &kde; printing dialog appears. A preview of the printed output can be obtained by selecting <menuchoice
><guimenu
>File</guimenu
><guimenuitem
>Print Preview...</guimenuitem
></menuchoice
>.</para>
</sect1>

<sect1 id="import-export">
<title
>Импорт/экспорт документов</title>
<para
>Пункты <guimenuitem
>Импорт</guimenuitem
> и <guimenuitem
>Экспорт</guimenuitem
> меню Файл выполняют в настоящее время те же функции, что и <guimenuitem
>Открыть</guimenuitem
> и <guimenuitem
>Сохранить</guimenuitem
>.</para>
</sect1>

<sect1 id="multiple-pages">
<title
>Документы с несколькими страницами</title>
<para
>Документ &kivio; может содержать несколько страниц диаграмм.</para>

<sect2>
<title
>Добавление страниц</title>
<para
>Чтобы <action
>добавить дополнительные страницы</action
> в документ выберите <menuchoice
><guimenu
>Страница</guimenu
><guimenuitem
>Вставить страницу</guimenuitem
></menuchoice
>. Вкладки в нижней части вида отображают все страницы, которые не были скрыты.</para>
</sect2>

<sect2>
<title
>Удаление страниц</title>
<para
>Ненужные страницы могут быть удалены через пункт меню <menuchoice
><guimenu
>Страница</guimenu
><guimenuitem
>Удалить страницу</guimenuitem
></menuchoice
>.</para>
</sect2>

<sect2>
<title
>Переименование страниц</title>
<para
>Название страницы может быть изменено двойным щелчком мыши на вкладке этой страницы. Появится диалог изменения названия страницы.</para>
</sect2>

<sect2>
<title
>Работа со страницами</title>
<para
>Порядок страниц может быть изменён перетаскиванием вкладок.</para>
<note
><para
>Кнопки со стрелками около вкладок страниц позволяют прокручивать вкладки страниц.</para
></note>
<para
>Если в документе много страниц и вы не хотите видеть все их вкладки, вы можете скрыть страницы, выбрав пункт меню  <menuchoice
><guimenu
>Страница</guimenu
><guimenuitem
>Скрыть страницу</guimenuitem
></menuchoice
>. Это удалит из вкладок выбранную в настоящий момент страницу.</para>
<para
>Чтобы показать скрытую страницу, выберите  <menuchoice
><guimenu
>Страница</guimenu
> <guimenuitem
>Показать страницу</guimenuitem
></menuchoice
> и укажите в списке страницу, которую вы хотите видеть.</para>
</sect2>

</sect1>

<sect1 id="page-export">
<title
>Экспорт страницы</title>
<para
>Отдельные страницы могут быть экспортированы в растровые графические файлы. Для экспорта текущей страницы выберите пункт меню <menuchoice
><guimenu
>Страница</guimenu
><guimenuitem
>Экспорт страницы</guimenuitem
></menuchoice
>. Появится стандартный диалог выбора файлов &kde;. Формат экспорта определяется именем файла и он должен быть одним из расширений, перечисленных ниже.</para>
<para
>Поддерживаются следующие форматы:</para>

<itemizedlist>
<listitem
><para
>Windows Bitmap (.bmp) </para
></listitem>
<listitem
><para
>Encapsulated Postscript (.eps) </para
></listitem>
<listitem
><para
>JPEG (.jpeg) </para
></listitem>
<listitem
><para
>.krl </para
></listitem>
<listitem
><para
>.pmb </para
></listitem>
<listitem
><para
>PCX (.pcx) </para
></listitem>
<listitem
><para
>Portable Grayscale Map .pgm </para
></listitem>
<listitem
><para
>Portable Pixmap File .ppm </para
></listitem>
<listitem
><para
>Targa (.tga) </para
></listitem>
<listitem
><para
>.xbm </para
></listitem>
<listitem
><para
>.xpm </para
></listitem>
<listitem
><para
>.xv</para
></listitem>
</itemizedlist>
</sect1>

<sect1 id="grid">
<title
>Сетка</title
>	
<para
>&kivio; имеет равномерно распределённую по документу сетку, чтобы облегчить позиционирование объектов. Видимость сетки может быть переключена выбором <menuchoice
><guimenu
>Вид</guimenu
> <guimenuitem
>Показать сетку</guimenuitem
></menuchoice
>.</para>
<para
>Границы объектов прилепляются по ближайшей точке на сетке, независимо от того видима ли она или нет. Чтобы отключить такое поведение, выберите пункт меню <menuchoice
><guimenu
>Вид</guimenu
><guimenuitem
>Привязать к сетке</guimenuitem
> </menuchoice
>.</para>
<para
>Для настройки сетки выберите <menuchoice
><guimenu
>Настройки</guimenu
> <guimenuitem
>Настроить Kivio...</guimenuitem
></menuchoice
> и щёлкните на пиктограмме <guiicon
>Сетка</guiicon
>.</para>
<para
>Эта вкладка позволяет настроить цвет сетки, горизонтальный и вертикальный интервал, а также расстояние привязки.</para>
<para
>Совет: вы можете иметь не квадратные ячейки сетки, указав различные вертикальные и горизонтальные интервалы.</para>
</sect1>

<sect1 id="text-tool">
<title
>Свободный текст</title>
<para
>Инструмент Текст может использоваться для добавления в документ текста, который не привязан ни к какому объекту. Это бывает полезно для включения информации о рисунке.</para>
<para
>Чтобы добавить свободный текст, отмените выбор объектов, выбрав пункт меню <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
>Отменить выбор</guimenuitem
></menuchoice
>, затем активируйте инструмент Текст, выбрав пункт меню <menuchoice
><guimenu
>Сервис</guimenu
><guimenuitem
>Текст</guimenuitem
></menuchoice
>. Курсор мыши изменится, сообщая что выбран инструмент Текст.</para>
<para
>Drag a box to contain the text. A dialog box will appear prompting for the text, enter some text and click <guibutton
>OK</guibutton
>. The formatting of the text can be adjusted by selecting <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Text</guimenuitem
></menuchoice
> or by using the toolbars.</para>
</sect1>

<sect1 id="page-layout">
<title
>Параметры страницы</title>
<para
>The page size, margins and orientation, portrait or landscape, can be adjusted using the <guilabel
>Page Size and Margins</guilabel
> dialog box. This is accessible by selecting <menuchoice
><guimenu
>Format</guimenu
><guimenuitem
>Page Layout...</guimenuitem
></menuchoice
>.</para>
</sect1>

<sect1 id="guides">
<title
>Использование направляющих</title>
<para
>Направляющие - это линии, используемые для позиционирования объектов. Они используются не столько как визуальные элементы, сколь для выравнивания объектов по определённой линии. &kivio; прицепляет объекты к направляющим. Как только граница объекта находится на некотором расстоянии до направляющей, она будет перемещена в позицию направляющей.</para>

<sect2>
<title
>Добавление направляющих</title>
<para
>Чтобы добавить направляющую, поместите указатель мыши на горизонтальную или вертикальную линейку, в зависимости от типа необходимой вами направляющей. Удерживая нажатой &LMB;, перетащите курсор на документ и отпустите её в позиции, в которую хотите установить направляющую. Пока вы перетаскиваете курсор мыши, направляющая соответственно будет перемещаться.</para>
</sect2>

<sect2>
<title
>Перемещение направляющих</title>
<para
>Как только направляющая помещена на место, вы можете изменить её позицию, перетаскивая её при нажатой &LMB;. Когда вы отпустите кнопку мыши, направляющая останется в новой позиции.</para>
</sect2>

<sect2>
<title
>Видимость и выравнивание</title>
<para
>Чтобы скрыть направляющие, выберите <menuchoice
><guimenu
>Вид</guimenu
><guimenuitem
>Показать направляющие</guimenuitem
></menuchoice
>, это переключает их видимость.</para>
<para
>По умолчанию края объектов прилепляются к ближайшей направляющей. Для отмены такого поведения уберите флажок <menuchoice
><guimenu
>Вид</guimenu
><guimenuitem
>Привязать к направляющим</guimenuitem
></menuchoice
>.</para>
</sect2>

</sect1>

<sect1 id="layers">
<title
>Слои</title>
<para
>&kivio; позволяет разбивать сложные схемы на несколько слоёв. Видимость слоёв может переключаться. Эта функциональность полезна, если вы имеете базовую схему, например, план этажа, и вам необходимо создать несколько дополнительных схем, базирующихся на ней, для различных целей. Информация для каждой цели может быть помещена на отдельный слой. Каждая страница документа содержит собственный набор слоёв.</para>

<sect2>
<title
>Плавающая панель слоёв</title>
<para
>Скорректировать текущие слои страницы вы можете, выбрав пункт меню <menuchoice
><guimenu
>Вид</guimenu
><guisubmenu
>Выплывающие панели</guisubmenu
><guimenuitem
>Слои</guimenuitem
></menuchoice
>. Плавающая панель менеджера слоёв позволяет создавать слои, удалять и переименовывать их. Она также используется для настройки видимости слоя, порядка наложения и выбора текущего слоя.</para>
<para
>&kivio; поддерживает концепцию текущего активного слоя, в котором доступны любые действия редактирования. По умолчанию страница содержит только один слой и он является активным.</para>
</sect2>

<sect2>
<title
>Добавление слоёв</title>
<para
>Используя плавающую панель слоёв, вы можете добавить новый слой нажатием первой слева кнопки <guibutton
>Новый слой</guibutton
>. Слои могут быть удалены нажатием второй кнопки, <guibutton
>Удалить слой</guibutton
>. &kivio; последовательно нумерует слои. Чтобы переименовать слой, выделите его в списке и нажмите кнопку <guibutton
>Переименовать слой</guibutton
>.</para>
</sect2>

<sect2>
<title
>Параметры слоя</title>
<para
>Слои накладываются друг на друга в порядке как они перечислены в списке. Наложение происходит сверху вниз так, что объекты на нижнем слое в списке будут прорисованы поверх всех объектов на других слоях. Порядок слоёв в списке может быть изменён выбором слоя и использованием кнопок <guibutton
>Сместить слой вверх</guibutton
> и <guibutton
>Сместить слой вниз</guibutton
> для изменения его позиции.</para>
<para
>Each layer is listed with four small icons next to its name. From left to right they represent:</para>
<variablelist>
<varlistentry>
<term
>Visibility (shown as a small eye)</term>
<listitem
><para
>Clicking on the icon will toggle the layer's visibility. </para
></listitem>
</varlistentry>
<varlistentry>
<term
>Printable (shown as a small printer)</term>
<listitem
><para
>Toggles if the layer should be printed with the document or not. You might use a non-printing layer to keep notes to yourself, or as a scrapbook to hold images you're not ready to use yet.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
>Editable (shown as a small pencil)</term>
<listitem
><para
>Toggles if you can edit a layer or not. You might want to mark some layers non-editable while you work on others, to prevent inadvertant changes.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
>Connectable (shown as a small connector)</term>
<listitem
><para
>You can permit connecting of items that lie on different layers with this icon.</para
></listitem>
</varlistentry>
</variablelist>

</sect2>
</sect1>

<sect1 id="arranging">
<title
>Выравнивание объектов</title>

<sect2>
<title
>Инструмент выравнивания</title>
<para
>Инструмент выравнивания перестраивает группу объектов так чтобы были выровнены по краю или невидимому центру. Чтобы использовать этот инструмент, выделите несколько объектов, затем выберите пункт меню <menuchoice
><guimenu
>Формат</guimenu
><guimenuitem
>Выровнять и распределить...</guimenuitem
></menuchoice
>. Если объекты должны быть выровнены по горизонтали, вы выбираете выравнивание по верхнему или нижнему краю либо по центру. Для объектов, которые необходимо выровнять по вертикали, вы можете выбрать выравнивание по левому или правому краю либо по центру.</para>
</sect2>

<sect2>
<title
>Инструмент распределения</title>
<para
>Инструмент распределения выравнивает группу объектов так чтобы между ними были одинаковые расстояния.</para>
<para
>Сначала выделите объекты, которые вы хотите распределить, затем выберите пункт меню <menuchoice
><guimenu
>Формат</guimenu
> <guimenuitem
>Выровнять и распределить...</guimenuitem
></menuchoice
> и в появившемся диалоге щёлкните на вкладке <guilabel
>Распределение</guilabel
>.</para>
<para
>Вы можете выбрать каким образом объекты должны быть распределены: равное расстояние было, например, между левыми краями всех объектов. Или промежуток между объектами. Выберите параметр <guilabel
>Прижать</guilabel
>, если хотите изменить порядок распределения.</para>
<para
>Вторая опция устанавливает, должно ли размещение использовать всё пространство страницы или только выделенных объектов. Если выбран второй вариант, объекты не будут занимать больше места чем занимали первоначально.</para>
<note
><para
>Параметр для использования всей области страницы учитывает размеры её краёв.</para
></note>
</sect2>
</sect1>

</chapter>
