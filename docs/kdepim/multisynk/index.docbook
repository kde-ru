<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" 
"dtd/kdex.dtd" [
 <!ENTITY multisynk "MultiSynk">
 <!ENTITY kappname "&multisynk;">
 <!ENTITY package "kdepim">
 <!ENTITY konnectors-chapter SYSTEM "konnectors-chapter.docbook">
 <!ENTITY % addindex "IGNORE">
 <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->  
]>

<book lang="&language;">
<bookinfo>
<title
>Руководство &multisynk;</title>
<authorgroup>
<author
><firstname
>Tobias</firstname
> <surname
>Koenig</surname
> <affiliation
> <address
><email
>tokoe@kde.org</email
></address>
</affiliation>
</author>

<othercredit role="translator"
><firstname
>Олег</firstname
><surname
>Финкельштейн</surname
><affiliation
><address
><email
>doublewolf@yandex.ru</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 
</authorgroup>

<date
>2004-11-02</date>
<releaseinfo
>1.0</releaseinfo>

<legalnotice>
&FDLNotice;
</legalnotice>

<abstract>
<para
>&multisynk; -- это приложение для синхронизации переносных устройств для &kde;.</para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>синхронизация</keyword>
<keyword
>наладонник</keyword>
<keyword
>OPIE</keyword>
<keyword
>Qtopia</keyword>

</keywordset>
</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
>&multisynk; это интерфейс к инфраструктуре синхронизации &kde; -- kitсhensynk. Он позволяет синхронизировать множество мобильных устройств -- таких, как наладонные компьютеры и мобильные телефоны, -- с вашим рабочим столом KDE, а также позволяет непосредственно синхронизировать несколько рабочих столов KDE. </para>

<para
>Графический интерфейс схож с multisync, так что всем работавшим с ней будет легко разобраться и с multisynk. </para>
</chapter>

<chapter id="using-multisynk">
<title
>Использование &multisynk;</title>

<sect1 id="general">
<title
>Основная информация</title>

<para
>&multisynk; основан на платформе kitсhensync. Назначение платформы -- синхронизация различных данных (события, задачи, контакты), причём программа синхронизации ничего не знает об обрабатываемых данных. Сама синхронизация осуществляется при помощи так называемой Коннекторной пары. Коннекторная пара состоит из двух провайдеров данных (коннекторов), которые загружают данные из источника (файла, мобильного устройства или сети), пропускают их через программу синхронизации и записывают обратно в источник. </para>

<para
>Также, коннекторная пара может помочь программе синхронизации решить, как разрешить конкретный конфликт. Это делается либо автоматически (например, когда контактная информация изменена и на настольном компьютере, и на наладоннике, можно всегда обновлять информацию на наладоннике), или вручную -- тогда каждый раз будет появляться диалог, запрашивающий, какую из версий надо сохранить. </para>

</sect1>

<sect1 id="getting-started">
<title
>Начало работы</title>

<para
>Как только вы запустили &multisynk; (используя меню или набрав <command
>multisynk</command
> в консоли), будет отображено главное окно программы: <screenshot>
<screeninfo
>Запуск &multisynk;</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="mainwin.png" format="PNG"/></imageobject>
<textobject
><phrase
>Главное окно &multisynk;.</phrase
></textobject>
<caption
><para
>Главное окно &multisynk;.</para
></caption>
</mediaobject>
</screenshot>
</para>

<para
>Из меню или с помощью панели инструментов вы можете выполнить следующие действия: <variablelist>
<varlistentry>
<term
><guilabel
>Создать</guilabel
></term>
<listitem
><para
>Создать новую Коннекторную пару.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Изменить</guilabel
></term>
<listitem
><para
>Изменить выбранную Коннекторную пару.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Удалить</guilabel
></term>
<listitem
><para
>Удалить выбранную коннекторную пару.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Журнал</guilabel
></term>
<listitem
><para
>Показать диалог, показывающий ход процесса синхронизации.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Синхронизировать</guilabel
></term>
<listitem
><para
>Синхронизировать выбранную Коннекторную пару.</para
></listitem>
</varlistentry>
</variablelist>
</para>

<sect2 id="add-konnector-pair">
<title
>Добавление Коннекторной пару</title>

<para
>Для добавления Коннекторной пары щёлкните на первой иконке панели инструментов или выберите пункт Создать из меню Файл. Появится такой диалог: <screenshot
><screeninfo
>Диалог создания Коннекторной пары</screeninfo
><mediaobject
><imageobject
><imagedata fileref="editdialog.png" format="PNG"/></imageobject
><textobject
><phrase
>Диалог создания Коннекторной пары.</phrase
></textobject
><caption
><para
>Диалог создания Коннекторной пары</para
></caption
></mediaobject
></screenshot
> В этом диалоге вы можете выбрать Коннекторы, которые должны будут стать частью Коннекторной пары, они могут быть сконфигурированы нажатием на <guibutton
>Настроить...</guibutton
> после выбора <link linkend="konnectors-chapter-available"
>типа</link
>. Дополнительно вы можете дать паре имя для более простой идентификации в обзоре Коннекторных пар. </para>

<para
>На второй закладке диалога настройки Коннекторной пары вы можете выбрать способ, которым будут решаться конфликты. <screenshot
><screeninfo
>Диалог создания Коннекторной пары</screeninfo
> <mediaobject
><imageobject
><imagedata fileref="editdialog-conflicts.png" format="PNG"/></imageobject
><textobject
><phrase
>Диалог создания Коннекторной пары.</phrase
></textobject
> <caption
><para
>Диалог создания Коннекторной пары.</para
></caption
> </mediaobject
> </screenshot
> Возможны следующие варианты: <variablelist>
<varlistentry>
<term
><guilabel
>Разрешать вручную</guilabel
></term>
<listitem
><para
>Будет показан диалог, в котором вы сможете выбрать, какую запись надо сохранить. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Использовать запись первичного модуля</guilabel
></term>
<listitem
><para
>Всегда используются данные из первого Коннектора.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Использовать запись вторичного модуля</guilabel
></term>
<listitem
><para
>Всегда используются данные из второго Коннектора.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Использовать записи первичного и вторичного модулей</guilabel
></term>
<listitem
><para
>Изменённые данные дублируются с обеих сторон.</para
></listitem>
</varlistentry>
</variablelist>

</para>
</sect2>

<sect2 id="edit-konnector-pair">
<title
>Изменить Коннекторную пару</title>
<para
>Для изменения настроек Коннекторной пары, выберите ее в списке пар и нажмите <guilabel
>Изменить...</guilabel
> в меню или на панели инструментов. Появляющийся диалог идентичен появляющемуся при добавлении Коннекторной пары. </para>
</sect2>

<sect2 id="delete-konnector-pair">
<title
>Удалить Коннекторную пару</title>
<para
>Для удаления Коннекторной пары, выберите её в списке и нажмите <guilabel
>Удалить</guilabel
> на панели инструментов или в меню. Появится запрос на подтверждение -- действительно ли вы удаляете то, что хотели. </para>
</sect2>

<sect2 id="start-sync-process">
<title
>Начало процесса синхронизации</title>
<para
>После того, как вы <link linkend="add-konnector-pair"
>добавили</link
> Коннекторную пару, вы можете выбрать её в списке и нажать <guilabel
>Синхронизировать</guilabel
> на панели инструментов, или выбрать это же действие из меню. Процесс синхронизации будет отображаться в статусной колонке в списке Коннекторных пар и в <link linkend="log-dialog"
>журнале</link
>. </para>
</sect2>

<sect2 id="log-dialog">
<title
>Журнальный диалог</title>
<para
>Журнал собирает всю информацию о процессе синхронизации отображает ее с указанием времени в отдельном окне. <screenshot>
<screeninfo
>Журнальный диалог</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="logdialog.png" format="PNG"/></imageobject>
<textobject
><phrase
>Журнальный диалог.</phrase
></textobject>
<caption
><para
>Журнальный диалог.</para
></caption>
</mediaobject>
</screenshot>
</para>
</sect2>

</sect1>
</chapter>

&konnectors-chapter;

<chapter id="command-line">
<title
>Ключи командной строки</title>
<para
>&multisynk; поддерживает все ключи, общие для программ &kde; и &Qt;, вы можете получить их список, набрав <userinput
><option
>--help</option
></userinput
>, <userinput
><option
>--help-kde</option
></userinput
>, и <userinput
><option
>--help-qt</option
></userinput
>.</para>
</chapter>

<chapter id="credits">
<title
>Авторы и лицензия</title>

<para
>&multisynk; - программа синхронизации для &kde;</para>

<para
>Copyright &copy; 2004, Tobias Koenig</para>
<para
>В данный момент программа поддерживается Тобиасом Коэнигом (Tobias Koenig) <email
>tokoe@kde.org</email
>.</para>
&underFDL; </chapter>
</book>

