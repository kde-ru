<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" 
"dtd/kdex.dtd" [
  <!ENTITY kappname "&kcron;">
  <!ENTITY package "kdeadmin">
  <!ENTITY % Russian "INCLUDE"
> <!-- change language only here -->
  <!ENTITY % addindex "IGNORE">
]>

<book lang="&language;">

<bookinfo>

<title
>Руководство &kcron;</title>

<authorgroup>

<author
><firstname
>Morgan</firstname
> <othername
>N.</othername
> <surname
>Sandquist</surname
> <affiliation
><address
><email
>morgan@pipeline.com</email
></address>
</affiliation>
</author>

<othercredit role="developer"
><firstname
>Gary</firstname
> <surname
>Meyer</surname
> <affiliation
><address
><email
>gary@meyer.net</email
></address>
</affiliation>
<contrib
>Разработчик</contrib>
</othercredit>

<othercredit role="reviewer"
><firstname
>Lauri</firstname
> <surname
>Watts</surname
> <affiliation
><address
><email
>lauri@kde.org</email
></address>
</affiliation>
<contrib
>Редактор</contrib>
</othercredit>

<othercredit role="translator"
><firstname
>Andrei</firstname
><surname
>Darashenka</surname
><affiliation
><address
><email
>adorosh+KDE.RU@smolevichi.org.by</email
></address
></affiliation
><contrib
>Перевод на русский</contrib
></othercredit
> 

</authorgroup>

<copyright>
<year
>2000</year>
<holder
>Morgan N. Sandquist</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2003-09-16</date>
<releaseinfo
>3.1.91</releaseinfo>

<abstract
><para
>&kcron; - это приложение, планирующее выполнение других программ.</para
></abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeadmin</keyword>
<keyword
>KCron</keyword>
<keyword
>cron</keyword>
<keyword
>crontab</keyword>
<keyword
>планировщик</keyword>
</keywordset>


</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<para
><command
>cron</command
>  - это приложение для планирования запуска программ в фоновом режиме. &kcron; - это графический интерфейс системного планировщика cron &UNIX;</para>

</chapter>

<chapter id="using-kcron">
<title
>Использование &kcron;</title>

<important
><para
>Не забудьте запустить <filename
>crond</filename
> - демон cron, иначе &kcron; не будет работать.</para
></important>

<sect1 id="kcron-start-up">
<title
>Запуск &kcron;</title>

<para
>После запуска &kcron; вы увидите сводку существующих запланированных заданий и соответствующих переменных окружения. Если программа исполняется от суперпользователя, вы увидите задания для всех пользователей системы. В этом случае каждый каталог может быть раскрыт или свёрнут.</para>

<screenshot>
<screeninfo
>окно &kcron;</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="kcronstart.png" format="PNG"/></imageobject>
<textobject
><phrase
>окно &kcron;</phrase
></textobject>
</mediaobject>
</screenshot>

<sect2>
<title
>Запланированные задания</title>

<para
>Запланированные задания показываются на вкладке <guilabel
>Задания</guilabel
>. Для каждого задания отображается следующее:</para>

<variablelist>

<varlistentry>
<term
><guilabel
>Имя</guilabel
></term>
<listitem
><para
>Имя для идентификации запланированной задачи</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Значение</guilabel
></term>
<listitem
><para
>Файл программы и параметры запуска.</para
></listitem>
</varlistentry>
<varlistentry
> 
<term
><guilabel
>Описание</guilabel
></term>
<listitem
><para
>Понятное описание запланированного задания</para
></listitem>
</varlistentry
> 
</variablelist>

<para
>Если задание заблокировано, имя файла и параметры запуска перестанут быть доступны, и описание тоже заблокируется.</para>

</sect2>

<sect2>
<title
>Переменные окружения</title>

<para
>Переменные окружения возникают под папкой <guilabel
>Переменные</guilabel
>. Для каждой переменной окружения отображается следующее: </para>

<variablelist>
<varlistentry>
<term
><guilabel
>Имя</guilabel
></term>
<listitem
><para
>Имя переменной.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Значение</guilabel
></term>
<listitem
><para
>Значение переменной.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Описание</guilabel
></term>
<listitem
><para
>Понятное описание переменной.</para
></listitem>
</varlistentry>
</variablelist>

<para
>Переменные окружения, показанные здесь, заменят любую другую переменную окружения для всех запланированных заданий. Если переменная окружения будет заблокирована, то заблокируются значение и описание переменной.</para>

<screenshot>
<screeninfo
>главное окно &kcron;</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="kcron.png" format="PNG"/></imageobject>
<textobject
><phrase
>главное окно &kcron;</phrase
></textobject>
</mediaobject>
</screenshot>

</sect2>
</sect1>

<sect1 id="new-task">
<title
>Добавление запланированных заданий</title>

<para
>Для добавления нового задания сначала выберите папку <guilabel
>Задания</guilabel
>. Затем выберите <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
>Новое...</guimenuitem
></menuchoice
>. Можно также использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Новое...</guimenuitem
></menuchoice
>, или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>N</keycap
></keycombo
>.</para>

<sect2>
<title
>Диалог <guilabel
>Правка задания</guilabel
></title>

<screenshot>
<screeninfo
>Диалог <guilabel
>Правка задания</guilabel
>.</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="newtask.png" format="PNG"/></imageobject>
<textobject
><phrase
>Диалог <guilabel
> Правка задания</guilabel
></phrase
></textobject>
</mediaobject>
</screenshot>

<variablelist>
<varlistentry>
<term
><guilabel
>Комментарий</guilabel
></term>
<listitem
><para
>Введите описание задания, которое нужно запланировать.</para>
</listitem
> 
</varlistentry>
<varlistentry>
<term
><guilabel
>Программа</guilabel
></term>
<listitem
><para
>Введите имя программы. Можно указывать как абсолютный так и относительный путь. Если хотите найти программу, нажмите <guibutton
>Выбрать...</guibutton
></para>
</listitem
> 
</varlistentry>
<varlistentry>
<term
><guibutton
>Включено</guibutton
></term>
<listitem
><para
>Для включения или выключения задания выберите или снимите отметку с флажка <guilabel
>Включено</guilabel
></para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Без извещений</guilabel
></term>
<listitem>
<para
>Выключает журналирование и вывод команды. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Месяцы</guilabel
></term>
<listitem
><para
>Выберите месяцы, по которым задание будет выполняться.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Дни месяца</guilabel
></term>
<listitem
><para
>Выберите дни месяца, по которым задание будет выполняться.</para>
</listitem
> 
</varlistentry>
<varlistentry>
<term
><guilabel
>Дни недели</guilabel
></term>
<listitem
><para
>Выберите дни недели, по которым задание будет выполняться.</para>
</listitem
> 
</varlistentry>
<varlistentry>
<term
><guilabel
>Ежедневно</guilabel
></term>
<listitem
><para
>Если хотите, чтобы задание выполнялось ежедневно, выберите <guibutton
>Выполнять каждый день</guibutton
></para>
</listitem
> 
</varlistentry>
<varlistentry>
<term
><guilabel
>Часы</guilabel
></term>
<listitem
><para
>Выберите часы, по которым задание будет запускаться на выполнение.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Минуты</guilabel
></term>
<listitem
><para
>Выберите минуты, по которым задание будет запускаться на выполнение. &kcron; не поддерживает планирование чаще, чем раз в минуту.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guibutton
>ОК</guibutton
></term>
<listitem
><para
>Завершить создание этого задания.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guibutton
>Отмена</guibutton
></term>
<listitem
><para
>Прервать процесс создания этого задания.</para>
</listitem>
</varlistentry>
</variablelist>

<para
>Если выбрать дни недели и дни месяца вместе, задание будет выполняться при достижении любого из условий. Например, если указать выполнение 1-го и 15-го числа любого месяца и указать  выполняться по воскресеньям, то задание запустится и 1-го, и 15-го числа вне зависимости от дня недели, и будет запускться по всем воскресеньям месяца.</para>

<para
>Запланированное задание не установится в системе и не начнёт выполняться, пока не будет сохранёт файл <filename
>crontab</filename
>.</para>

</sect2>
</sect1>

<sect1 id="manage-tasks">
<title
>Управление запланированными заданиями</title>

<para
>Также как и с созданием, изменение запланированного задания не будет введено в действие и не начнёт выполняться по новому расписанию, пока не будет сохранёт файл <filename
>crontab</filename
>.</para>

<sect2>
<title
>Вырезание запланированного задания</title>

<para
>Для вырезания запланированного задания, сначалы выберите это задание. Потом нажмите <menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Вырезать</guimenuitem
></menuchoice
>.</para
> <para
>Также можно нажать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Вырезать</guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>X</keycap
></keycombo
></para>

</sect2>

<sect2>
<title
>Копирование запланированных заданий</title>

<para
>Чтобы скопировать запланированное задание, выберите сначала задание для копирования, затем нажмите <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
> Копировать</guimenuitem
></menuchoice
>.</para>

<para
>Также можно нажать <mousebutton
> правую</mousebutton
> кнопку мыши и выбрать<menuchoice
><guimenuitem
> Копировать</guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
> Ctrl</keycap
><keycap
> C</keycap
></keycombo
>.</para>

</sect2>

<sect2>
<title
>Вставка запланированных заданий</title>

<para
>Чтобы вставить задания, они должны предварительно быть вырезаны или скопированы в буфер, после чего разрешится вставка заданий. Выберите папку <guilabel
> Задания</guilabel
> и после этого <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
>Вставить</guimenuitem
></menuchoice
>.</para>

<para
>Также можно нажать <mousebutton
> правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
> Вставить</guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>V</keycap
></keycombo
>.</para>

</sect2>

<sect2>
<title
>Изменение запланированных заданий</title>

<para
>Для изменения запланированного задания сначала выберите его, а потом нажмите <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
>Изменить</guimenuitem
></menuchoice
>.</para>

<para
>Также можно нажать <mousebutton
> правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Изменить... </guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>O</keycap
></keycombo
>. Вы увидите диалог <guilabel
>Правка задания</guilabel
>, в котором можно изменить всё также, как и в диалоге <link linkend="new-task"
>Новое задание</link
>.</para>

</sect2>

<sect2>
<title
>Удаление запланированных заданий</title>

<para
>Для удаления запланированного задания сначала выберите его, затем нажмите <menuchoice
><guimenu
> Правка</guimenu
><guimenuitem
> Удалить</guimenuitem
></menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <guimenuitem
>Удалить</guimenuitem
>.</para>

</sect2>

<sect2>
<title
>Включение/выключение запланированных заданий</title>

<para
>Для разрешения запрещёных запланированных заданий сначала выделите его. Запрещённые задания имеют пометку <guilabel
>Отключено</guilabel
> в описании. Затем выберите <menuchoice
> <guimenu
> Правка</guimenu
> <guimenuitem
>Включено</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Включено</guimenuitem
></menuchoice
>. Убедитесь в правильности отображаемого файла программы, параметров и описания.</para>

</sect2>

<sect2>
<title
>Выполнение запланированных заданий</title>

<para
>Можно сразу запустить задание на выполнение, выбрав <menuchoice
> <guimenu
> Правка</guimenu
> <guimenuitem
>Запустить сейчас</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Запустить сейчас</guimenuitem
></menuchoice
>. </para>

</sect2>
</sect1>

<sect1 id="new-variable">
<title
>Добавление переменных окружения</title>

<para
>Для создания новой переменной окружения сначала выберите папку <guilabel
>Переменные</guilabel
> и потом нажмите <menuchoice
> <guimenu
>Правка</guimenu
> <guimenuitem
> Новая...</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Новая...</guimenuitem
></menuchoice
>. или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>N</keycap
></keycombo
>.</para>

<sect2>
<title
>Диалог <guilabel
>Правка переменной</guilabel
></title>

<screenshot>
<screeninfo
>Диалог <guilabel
>Редактировать переменные</guilabel
></screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="newvariable.png" format="PNG"/></imageobject>
<textobject
><phrase
>Диалог<guilabel
> Редактировать переменные</guilabel
></phrase
></textobject>
</mediaobject>
</screenshot>

<variablelist>
<varlistentry>
<term
><guilabel
>Переменная</guilabel
></term
> 
<listitem
><para
>Введите имя переменной окружения. Можно использовать выпадающий список для ввода наиболее часто используемых имён переменных окружения, включая: </para>

<variablelist>

<varlistentry>
<term
><guimenuitem
>HOME</guimenuitem
></term>
<listitem
><para
>Используется для переназначения домашнего каталога пользователя.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guimenuitem
>MAILTO</guimenuitem
></term>
<listitem
><para
>Адресат для отправки писем вместо адреса пользователя.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guimenuitem
>PATH</guimenuitem
></term>
<listitem
><para
>Список каталогов для поиска выполняемых файлов.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guimenuitem
>SHELL</guimenuitem
></term>
<listitem
><para
>Будет запущена программа-оболочка вместо используемой по умолчанию.</para
></listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Значение</guilabel
></term>
<listitem
><para
>Введите значение переменной окружения.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Комментарий</guilabel
></term>
<listitem
><para
>Введите описание для этой переменной окружения, например цель её использования.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Включить</guilabel
></term>
<listitem>
<para
>Для включения/выключения использования переменной окружения, выберите кнопку <guibutton
>Включить</guibutton
>.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guibutton
>ОК</guibutton
></term>
<listitem>
<para
>Завершает установку переменной.</para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guibutton
>Отмена</guibutton
></term>
<listitem>
<para
>Отменяет установку переменной.</para>
</listitem>
</varlistentry>
</variablelist>

<para
>Переменная окружения не вступит в силу до сохранения файла <filename
>crontab</filename
>.</para>

</sect2>
</sect1>

<sect1 id="manage-variables">
<title
>Управление переменными окружения</title>

<note
><para
>Также как и при создании переменных, изменение их не вступит в силу до сохранения файла <filename
>crontab</filename
>.</para
></note>

<sect2>
<title
>Вырезание переменных окружения</title>

<para
>Для вырезания переменной окружения сначала выделите необходимую переменную. Затем выберите <menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Вырезать</guimenuitem
></menuchoice
>.</para>

<para
>Также можно нажать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Вырезать</guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>X</keycap
></keycombo
></para>

</sect2>

<sect2>
<title
>Копирование переменных окружения</title>

<para
>Для копирования переменной окружения сначала выделите необходимую переменную. Затем выберите <menuchoice
> <guimenu
>Правка</guimenu
> <guimenuitem
>Копировать</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <guimenuitem
>Копировать</guimenuitem
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>C</keycap
></keycombo
>.</para>

</sect2>

<sect2>
<title
>Вставка переменных окружения</title>

<para
>Для вставки переменной окружения сначала необходимо её скопировать или вырезать в буфер обмена. Только после копирования или вырезания появится возможность вставки. Выберите папку <guilabel
>Переменные</guilabel
>, затем выберите <menuchoice
><guimenu
>Правка</guimenu
><guimenuitem
>Вставить</guimenuitem
></menuchoice
>.</para>

<para
>Также можно нажать <mousebutton
> правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
> Вставить</guimenuitem
></menuchoice
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>V</keycap
></keycombo
>.</para>
</sect2>

<sect2>
<title
>Изменение переменных окружения</title>

<para
>Для изменения переменной окружения сначала выделите необходимую переменную. Затем выберите <menuchoice
> <guimenu
> Правка</guimenu
><guimenuitem
> Изменить...</guimenuitem
> </menuchoice
>. Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <guimenuitem
>Изменить...</guimenuitem
> или просто нажать <keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>O</keycap
></keycombo
>. Появится диалог <guilabel
>Правка переменной</guilabel
>, в котором можно исправлять переменную как и в <link linkend="new-variable"
>диалоге новой переменной</link
>.</para>

</sect2>

<sect2>
<title
>Удаление переменных окружения</title>

<para
>Для удаления переменной окружения сначала выделите необходимую переменную. Затем выберите <menuchoice
><guimenu
> Правка</guimenu
><guimenuitem
> Удалить</guimenuitem
></menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <guimenuitem
>Удалить</guimenuitem
>.</para>

</sect2>

<sect2>
<title
>Включение/Выключение переменных окружения</title>

<para
>Для включения или отключения переменной окружения сначала выделите необходимую переменную. Отключенные переменные будут отмечены как <guilabel
>Отключено</guilabel
>. Затем выберите <menuchoice
> <guimenu
>Правка</guimenu
> <guimenuitem
>Включить</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно использовать <mousebutton
>правую</mousebutton
> кнопку мыши и выбрать <menuchoice
><guimenuitem
>Включено</guimenuitem
></menuchoice
>. Убедитесь, что имя и значение переменной окружения верны.</para>

</sect2>
</sect1>

<sect1 id="saving-crontab">
<title
>Сохранение файла <filename
>crontab</filename
></title>

<para
>После установки всех необходимых заданий и переменных окружения, сохраните файл <filename
>crontab</filename
>, выбрав <menuchoice
> <guimenu
> Файл</guimenu
> <guimenuitem
>Сохранить</guimenuitem
> </menuchoice
>.</para>

<para
>Также можно просто нажать<keycombo action="simul"
><keycap
>Ctrl</keycap
><keycap
>S</keycap
></keycombo
>. Добавления или изменения не вступят в силу, пока вы не сделаете это.</para>

</sect1>

<sect1 id="printing-crontab">
<title
>Печать файла <filename
>crontab</filename
></title>

<para
>Для распечатывания файла <filename
>crontab</filename
> после сохранения, выберите <menuchoice
> <guimenu
>Файл</guimenu
><guimenuitem
> Печать</guimenuitem
> </menuchoice
>.</para>


<!-- FIXME: New screenshot of expanded print dialog -->

<screenshot>
<screeninfo
>Печать файла<filename
> crontab</filename
></screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="print.png" format="PNG"/></imageobject>
<textobject
><phrase
>Печать файла <filename
>crontab</filename
></phrase
></textobject>
</mediaobject>
</screenshot>

<para
>Отобразится стандартный диалог печати &kde;. При нажатии кнопки <guilabel
>Раскрыть</guilabel
> появятся две дополнительных настройки &kcron; в диалоге печати.</para>

<variablelist>
<varlistentry>
<term
><guilabel
>Печать Crontab</guilabel
></term>
<listitem>
<para
>Печатает файл <filename
>crontab</filename
> для текущего пользователя.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Печатать для всех пользователей</guilabel
></term>
<listitem>
<para
>Печатает файл <filename
>crontab</filename
> для всех пользователей. Эта настройка активна только для суперпользователей.</para>
</listitem>
</varlistentry>

</variablelist>

</sect1>
</chapter>

<chapter id="commands">
<title
>Справочник по командам</title>

<sect1 id="kcron-mainwindow">
<title
>Главное окно &kcron;</title>

<sect2>
<title
>Меню <guimenu
>Файл</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>S</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Сохранить</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Сохранить изменения</action
> в файле <filename
>crontab</filename
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>P</keycap
></keycombo
></shortcut
> <guimenu
> Файл</guimenu
><guimenuitem
>Печать...</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Печатать</action
> файл <filename
>crontab</filename
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
> <guimenu
>Файл</guimenu
> <guimenuitem
>Выход</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Выйти</action
> из &kcron;</para>
</listitem>
</varlistentry>
</variablelist>

</sect2>
<sect2>
<title
>Меню <guimenu
>Правка</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>X</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Вырезать</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вырезать выбранную задачу или переменную.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Копировать</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Копировать выбранную задачу или переменную.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>V</keycap
></keycombo
> </shortcut
> <guimenu
>Правка</guimenu
> <guimenuitem
>Вставка</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Вставить задачу или переменную, которая была ранее скопирована или вырезана.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>N</keycap
></keycombo
></shortcut
> <guimenu
>Правка</guimenu
><guimenuitem
> Новое...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Создать новое задание или переменную.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl;<keycap
>O</keycap
></keycombo
></shortcut
> <guimenu
>Правка</guimenu
><guimenuitem
> Изменить...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Изменить выбранные задания или переменные.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Удалить</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Удалить выбранное задание или переменную.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Включен</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Разрешить/запретить выбранное задание или переменную.</action
></para>
</listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Правка</guimenu
> <guimenuitem
>Выполнить сейчас</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Немедленно запустить выбранное задание.</action
></para
></listitem>
</varlistentry>
</variablelist>

</sect2>

<sect2>
<title
>Меню <guimenu
>Параметры</guimenu
></title>

<variablelist>
<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Показать панель инструментов</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отображать панель инструментов.</action
></para
></listitem>
</varlistentry>
<varlistentry>
<term
><menuchoice
><guimenu
>Параметры</guimenu
> <guimenuitem
>Показать строку состояния</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Отображать строку состояния.</action
></para
></listitem>
</varlistentry>
</variablelist>

</sect2>

<sect2>
<title
>Меню <guimenu
>Справка</guimenu
></title>
&help.menu.documentation; </sect2>
</sect1>
</chapter>

<chapter id="faq">
<title
>Вопросы и ответы</title>

<qandaset id="questions-and-answers">
<qandaentry>
<question>
<para
>Почему не работают изменения, которые я внёс в запланированные задачи?</para>
</question>
<answer
><para
>Эти изменения будут иметь силу только после <link linkend="saving-crontab"
>сохранения</link
> файла <filename
>crontab</filename
>.</para>
</answer>
</qandaentry>
</qandaset>
</chapter>

<chapter id="credits">

<title
>Авторские права и лицензирование</title>

<para
>&kcron;</para>

<para
>Авторское право принадлежит Gary Meyer  <email
>gary@meyer.net</email
>, 2002</para>

<para
>Авторское право на документацию принадлежит Morgan N. Sandquist <email
>morgan@pipeline.com</email
>, 2000</para>
<para
>Перевод на русский: Андрей Дорошенко <email
>adorosh+KDE.RU@smolevichi.org.by</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Установка</title>

<sect1 id="getting-kcron">
<title
>Как получить &kcron;</title>
&install.intro.documentation; </sect1>

<sect1 id="requirements">
<title
>Требования</title>

<para
>Для того, чтобы успешно собрать &kcron; нужны следующие библиотеки:</para>

<itemizedlist>
<listitem
><para
><command
>cron</command
>, например <command
>vixie cron</command
>. &kcron; использует команду <filename
>crontab</filename
> для изменения пользовательских запланированных заданий.</para
></listitem>
<listitem
><para
>&UNIX;, совместимый со стандартом POSIX, например использующий библиотеку glibc. &kcron; использует некоторые стандартные системные вызовы &UNIX; для локализации даты и времени.</para
></listitem
> 
</itemizedlist>

</sect1>

<sect1 id="compilation">
<title
>Сборка и установка</title>
&install.compile.documentation; </sect1>

</appendix>

&documentation.index; 
</book>

<!--
Local Variables:
mode: sgml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:
-->     
