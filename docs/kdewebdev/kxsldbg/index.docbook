<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kxsldbg "<application
>KXSLDbg</application
>">
  <!ENTITY appname "kxsldbg">
  <!ENTITY package "quanta">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Russian "INCLUDE">
  <!ENTITY configure-section SYSTEM "kxsldbg_configure.docbook">
  <!ENTITY mainwindow-section SYSTEM "kxsldbg_mainwindow.docbook">
  <!ENTITY inspector-section SYSTEM "kxsldbg_inspector.docbook">
  <!ENTITY tools-section SYSTEM "kxsldbg_tools.docbook">
  <!ENTITY credits-chapter SYSTEM "credits.docbook">
  <!ENTITY callstack SYSTEM "callstack.docbook">
  <!ENTITY entities SYSTEM "entities.docbook">
  <!ENTITY sources SYSTEM "sources.docbook">
  <!ENTITY templates SYSTEM "templates.docbook">
  <!ENTITY variables SYSTEM "variables.docbook">
  <!ENTITY xsldbg "<application
>xsldbg</application
>">
  <!ENTITY DTD "<acronym
>DTD</acronym
>">
  <!ENTITY XSD "<acronym
>XSD</acronym
>">
  <!ENTITY XSLT "<acronym
>XSLT</acronym
>">

]>

<book lang="&language;">

<bookinfo>
<title
>Руководство &kxsldbg;</title>

<authorgroup>
<author
><firstname
>Keith</firstname
> <surname
>Isdale</surname
> <affiliation
> <address
><email
>k_isdale@tpg.com.au</email
></address>
</affiliation>
</author>
  
</authorgroup>

<copyright>
<year
>2002</year>
<year
>2003</year>
<year
>2004</year>
<holder
>Keith Isdale</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>
<date
>2004-11-18</date>
<releaseinfo
>0.5</releaseinfo>

<abstract>
<para
>&kxsldbg; обеспечивает графический интерфейс для &xsldbg;, с поддержкой отладки скриптов &XSLT;. </para>
</abstract>

<keywordset>
<keyword
>kde</keyword>
<keyword
>xsldbg</keyword>
<keyword
>libxslt</keyword>
<keyword
>отладчик</keyword>
</keywordset>
</bookinfo>

<chapter id="introduction">
<title
>Введение</title>

<sect1 id="features">
<title
>Возможности</title>

<para
>&kxsldbg; обеспечивает доступ к большинству команд &xsldbg; для <itemizedlist>
<listitem>
<para
>Установки и изменения точек останова </para>
</listitem>
<listitem>
<para
>Отображения значения выражений XPaths </para>
</listitem>
<listitem>
<para
>Отображения информации о точках останова, шаблонах, переменных, стеке вызовов, таблицах стилей и доступных элементах. </para>
</listitem>
<listitem>
<para
>Установки и изменения точек останова и переменных </para>
</listitem>
<listitem>
<para
>Перемещения по &XSL;-исходникам и документам &XML; через XPath </para>
</listitem>
<listitem>
<para
>Поиска PUBLIC и SYSTEM ID в текущем каталоге &XML; </para>
</listitem>
</itemizedlist>
</para>
</sect1>

<sect1 id="new-features">
<title
>Новые возможности</title>
<para
>&kxsldbg; теперь может </para>
<itemizedlist>
<listitem>
<para
>Устанавливать и изменять значения переменных </para>
</listitem>
<listitem>
<para
>Выводить текст в главном окне с помощью библиотек &kate; </para>
</listitem>
</itemizedlist>
</sect1>


</chapter>

<chapter id="using-kxsldbg">
<title
>Использование &kxsldbg;</title>
&configure-section; &mainwindow-section; &inspector-section; &variables; &callstack; &templates; &sources; &entities; &tools-section; </chapter>

&credits-chapter;

</book>
